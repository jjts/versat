#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../ft4232h/ft_spi.h"

int main( int argc, char **argv )
{
	int i;
	int iport = 0; //Channel 0
	unsigned int addr_hex, data_hex;
	int addr_dec, data_dec;
	char addr_str[100], data_str[100];
	
	if(argc < 3){
		printf("error:too few arguments!\n");
		return -1;
	}else if(argc > 3){
		printf("error:too much arguments!\n");
		return -1;
	}
	
	i = opendev_int(&iport, BAUD);
	if(i!=0) return 0;
	
	/*Process command from user*/
	sscanf(argv[1],"%X",&addr_hex);
	sprintf(addr_str,"%d",addr_hex);
	sscanf(addr_str,"%d",&addr_dec);
	
	sscanf(argv[2],"%X",&data_hex);
	sprintf(data_str,"%d",data_hex);
	sscanf(data_str,"%d",&data_dec);
	
	if(addr_dec < 0 || addr_dec > 15){ //Test address
		printf("error: wrong command!\nwrite command needs two arguments:<address> <data>\naddress must be between 0x0 and 0xF and data has no more than 32 bits (ex:./write 0xB 0x08DD9C68)\n\n");
		return -1;
	}
	
	/*Give command to versat by SPI*/
	spi_write_int((unsigned char)addr_hex, data_hex, iport);
	
	/*Print wrote data*/
	printf("\tAddress\tData\nwrote\t%04X\t%08X\n", addr_hex, data_hex);
	
	
	closedev(iport);
	
	return 0;
}

