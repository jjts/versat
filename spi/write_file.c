#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../ft4232h/ft_spi.h"

#define BUFFER_SIZE 1024

int main( int argc, char **argv )
{
	int i;
	int iport = 0; //Channel 0
	char addr_str[100], data_str[100], buffer[BUFFER_SIZE+1];
	unsigned int addr_hex, data_hex, nop_3;
	int addr_dec, data_dec;
	FILE * fp = NULL;
	
	if(argc < 3){
		printf("error:too few arguments!\n");
		return -1;
	}else if(argc > 3){
		printf("error:too much arguments!\n");
		return -1;
	}
	
	/*Open .hex file*/
	fp = fopen(argv[2],"r");
	if(fp == NULL){
		printf("error:can't open \"%s\" file\n", argv[2]);
		return -1;
	}
	
	/*Process command from user*/
	sscanf(argv[1],"%X",&addr_hex);
	sprintf(addr_str,"%d",addr_hex);
	sscanf(addr_str,"%d",&addr_dec);
	
	if(addr_dec < 0 || addr_dec > 16383){ //Test address
		printf("error: wrong command!\nload_file command needs two arguments:<address> <source file path>\naddress must be beetwen 0x0 and 0x3FFF (ex:./read_file 0x900 ../opcode_src/lpf2.hex)\n\n");
		return -1;
	}
	
	i = opendev_int(&iport, BAUD);
	if(i!=0) return 0;
	
	printf("\tAddress\tData\n");
	
	/*load first word from file*/
	fgets(buffer,BUFFER_SIZE,fp);
	
	/*Process data from file*/	
	sscanf(buffer,"%X",&data_hex);
	sprintf(data_str,"%d",data_hex);
	sscanf(data_str,"%d",&data_dec);
	
	/*Send data to versat by SPI*/
	spi_write_int(0xF, data_hex, iport);
	
	/*Clear R14 by SPI*/
	spi_write_int(0xE, 0x0, iport);
	
	/*Send data to versat by SPI*/
	spi_write_int(0x0, addr_hex, iport);
	
	/*Print wrote data*/
	printf("wrote\t%04X\t%08X\n", addr_hex, data_hex);
	addr_hex++;
	
	/*Load the rest data from file*/
	while(fgets(buffer,BUFFER_SIZE,fp) != NULL){
		nop_3 = 0x0;
		
		//Wait for nop-3
		while(nop_3 != 0x3){
			spi_read_int(0xE, &nop_3, iport);
		}
		
		//Process data from file
		sscanf(buffer,"%X",&data_hex);
		sprintf(data_str,"%d",data_hex);
		sscanf(data_str,"%d",&data_dec);
		
		//Send data to versat by SPI
		spi_write_int(0xF, data_hex, iport);
		
		/*Clear R14 by SPI*/
		spi_write_int(0xE, 0x0, iport);
		
		//Print wrote data
		printf("wrote\t%04X\t%08X\n", addr_hex, data_hex);
		
		addr_hex++;
	}
	
	nop_3 = 0x0;
	
	//Wait for nop-3
	while(nop_3 != 0x3){
		spi_read_int(0xE, &nop_3, iport);
	}
	
	/*Send command to versat by SPI*/
	spi_write_int(0xE, 0x2, iport);
	
	closedev(iport);
	
	return 0;
}

