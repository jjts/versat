#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../ft4232h/ft_spi.h"

int main( int argc, char **argv )
{
	int i;
	int iport = 0; //Channel 0
	unsigned int addr_hex, data_hex;
	int addr_dec;
	char addr_str[100];
	
	if(argc < 2){
		printf("error:too few arguments!\n");
		return -1;
	}else if(argc > 2){
		printf("error:too much arguments!\n");
		return -1;
	}
	
	i = opendev_int(&iport, BAUD);
	if(i!=0) return 0;
	
	/*Process command from user*/	
	sscanf(argv[1],"%X",&addr_hex);
	sprintf(addr_str,"%d",addr_hex);
	sscanf(addr_str,"%d",&addr_dec);
	
	if(addr_dec < 0 || addr_dec > 15){ //Test address
		printf("error: wrong command!\nread command needs one argument:<address>\naddress must be between 0x0 and 0xF (ex:./read 0xF)\n\n");
		return -1;
	}
	
	/*Give command to versat by SPI*/
	spi_read_int((unsigned char)addr_hex, &data_hex, iport);
	
	/*Print read data*/
	printf("\tAddress\tData\nread\t%04X\t%08X\n", addr_hex, data_hex);
	
	closedev(iport);
	
	return 0;
}

