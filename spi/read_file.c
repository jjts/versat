#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../ft4232h/ft_spi.h"

int main( int argc, char **argv )
{
	int i;
	int iport = 0; //Channel 0
	char addr_str[100];
	unsigned int addr_hex, data_hex, nop_4;
	int addr_dec, size;
	
	if(argc < 3){
		printf("error:too few arguments!\n");
		return -1;
	}else if(argc > 3){
		printf("error:too much arguments!\n");
		return -1;
	}
	
	/*Process command from user*/
	sscanf(argv[1],"%X",&addr_hex);
	sprintf(addr_str,"%d",addr_hex);
	sscanf(addr_str,"%d",&addr_dec);
	
	sscanf(argv[2],"%d",&size);
	
	if(addr_dec < 0 || addr_dec >= 16384 || size <= 0){ //Test address and size
		printf("error: wrong command!\nread_file command needs two arguments:<address> <size>\naddress must be beetwen 0x0 and 0x3FFF and size must be a positive integer (ex:./read_file 0x2020 54)\n\n");
		return -1;
	}
	
	i = opendev_int(&iport, BAUD);
	if(i!=0) return 0;
	
	/*Send command to versat by SPI*/
	spi_write_int(0xE, 0x4, iport);
	
	/*Send data to versat by SPI*/
	spi_write_int(0x0, addr_hex, iport);
	
	printf("\tAddress\tData\n");
	
	/*Load data from versat*/
	while(size > 0){
		nop_4 = 0x4;
		
		/*Wait for nop-4*/
		while(nop_4 == 0x4){
			spi_read_int(0xE, &nop_4, iport);
		}
		
		/*Receive data to versat by SPI*/
		spi_read_int(0xF, &data_hex, iport);
		
		/*Print read data*/
		printf("read\t%04X\t%08X\n", addr_hex, data_hex);
		
		if(size > 1) /*Data already read*/
			spi_write_int(0xE, 0x4, iport);
		
		addr_hex++;
		size--;
	}
	
	/*Send command to versat by SPI*/
	spi_write_int(0xE, 0x2, iport);
	
	closedev(iport);
	
	return 0;
}

