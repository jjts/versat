/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

  Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Andre Lopes <andre.a.lopes@netcabo.pt>
             Guilherme Luz <gui_luz_93@hotmail.com>
             Joao Dias Lopes <joao.d.lopes91@gmail.com>
             Francisco Nunes <ftcnunes@gmail.com>
 
***************************************************************************** */

`timescale 1ns / 1ps

module altera_top (
		   input  clk50,
		   input  nrst,

		   // Control slave interface to master CPU 
		   input  sclk, 
		   input  ss, 
		   input  mosi, 
		   output miso,
			output locked
		   );
   wire 		  rst;
   wire 		  clk;
   
   assign rst = ~nrst | ~locked;

   //PLL
   alt_top pll0 (
	    .areset(~nrst),
	    .inclk0(clk50),
	    .c0(clk),
	    .locked(locked)
	    );

   //xtop
   xtop top0 (
   	      .clk(clk),
	      .rst(rst),

	      // Control slave interface to master CPU 
	      .sclk(sclk), 
	      .ss(ss), 
	      .mosi(mosi), 
	      .miso(miso)
	      );
endmodule // altera_top
