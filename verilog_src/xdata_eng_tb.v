/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias Lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps

`include "xdefs.v"
`include "xmem_map.v"
`include "./xaludefs.v"

module xdata_eng_tb;

	//parameters 
	parameter clk_period = 20; //ns
        
	// Inputs
	reg clk;
        reg rst;
	reg [`CONFIG_BITS-1:0] config_bus;
	reg rw_req;
	reg rw_rnw;
	reg [`INT_ADDR_W-1:0] rw_addr;
	reg [`DATA_W-1:0] rw_data_to_wr;
        reg dma_req;
	reg dma_rnw;
	reg [`DADDR_W+1:0] dma_addr;
	reg [`DATA_W-1:0] dma_data_in;

	// Outputs
	wire [`DATA_W-1:0] rw_data_to_rd;
	
	//configs 
	reg [`ALU_FNS_W-1:0] alu_fns;
	reg [`N_W-1:0] sela;
	reg [`N_W-1:0] selb;
	
	reg  [`DADDR_W-1:0] iterations;
	reg [`PERIOD_W-1:0] period;
	reg [`PERIOD_W-1:0] duty;

	reg [`DADDR_W-1:0] start;
        reg signed [`DADDR_W-1:0] shift;
	reg signed [`DADDR_W-1:0] incr;
	reg [`PERIOD_W-1:0] delay;
	reg reverse;
	reg ext;
        
        //auxiliary signals
        real x_r;
        real y_r;
        reg signed [`DATA_W-1:0] x;
        reg signed [`DATA_W-1:0] y;
        reg [`DATA_W-1:0] result_thread;
        reg [`DATA_W-1:0] count;
        reg done, done_1, d_thread;
        
        
        integer fp;
	
	// Instantiate the Unit Under Test (UUT)
	xdata_eng uut (
		.clk(clk), 
		.rst(rst),
		.config_bus(config_bus), 
		.rw_req(rw_req), 
		.rw_rnw(rw_rnw), 
		.rw_addr(rw_addr), 
		.rw_data_to_wr(rw_data_to_wr), 
		.rw_data_to_rd(rw_data_to_rd),
		.dma_req(dma_req),
		.dma_rnw(dma_rnw),
		.dma_addr(dma_addr),
		.dma_data_in(dma_data_in)
	);

	initial begin
	        
`ifdef DEBUG
		$dumpfile("xdata_eng.vcd");
      		$dumpvars();
`endif
	        
	        fp = $fopen("./xdata_eng.out","w");
	        
		// Initialize Inputs
		clk = 1;
		rw_req = 0;
		rw_rnw = 1;
		rw_addr = 0;
		rw_data_to_wr = 0;
	        dma_req = 0;
	        dma_rnw = 1;
	        rst = 0;
	        done = 0;

	        #(clk_period + 1)
		rst = 1;

	        #clk_period
		rst = 0;
	        
		// configure alulite3
		#clk_period      
		alu_fns = `ALU_ADD;
		sela = `salu0;
		selb = `salu1;
		
		config_bus[`ALU_LITE3_CONFIG_OFFSET -: `ALU_LITE_CONFIG_BITS] = 
					{sela, selb, alu_fns};

		config_bus[`MULT0_CONFIG_OFFSET-1 -: `MULT_CONFIG_BITS] = 
					{sela, selb, 2'b11};
		
	        //init engine
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
		rw_data_to_wr = `DATA_W'h001FFFFD; //init - new configuration of reset
	        rw_req = 1;
		
		#(3*clk_period) //because of the memory delay and config_reg_shadow load, the init_bit needs at least 3 clock ticks to reset functional units properly
		rw_data_to_wr = `DATA_W'h00000002; //run 		
	        
	        #clk_period
	        rw_req = 0;
	        
		// write 10 to output of alu0
		rw_addr = `ALU0;
		rw_req = 1;
		rw_rnw = 0;
		rw_data_to_wr = 10;
		
		// write 10 to output of alu1
		#clk_period
		rw_addr = `ALU1;
		
		//read alulite2 output
		#clk_period
		rw_rnw = 1;
		rw_addr = `ALULITE2;
		
		//read mult0 output 
		#clk_period		
		rw_addr = `MULT0;
	        
	        //multithreading running an add and product operations
		#clk_period
		//global configuration reset
		config_bus = 0;
	        
		//setup memory 0 for reading vector a and b
		sela = `sdisabled;
		selb = `sdisabled;
		start = 1;
	        shift = 0;
		incr = 1;
		delay = 0;
		reverse = 0;
		ext = 0;
	        iterations = 19;
		period = 0;
		duty = 0;
		//setup port A
		config_bus[`MEM0A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, shift, incr, delay, reverse, ext};
	
		start = 21;
		//setup port B
		config_bus[`MEM0B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, shift, incr, delay, reverse, ext};
    
	        //setup memory 1 for storing vector a + b
		sela = `salu1;
		start = 0;
		incr = 1;
		delay = 3;
		reverse = 0;
		ext = 0;
	        iterations = 19;
		period = 0;
		duty = 0;
		//setup port A
		config_bus[`MEM1A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, shift, incr, delay, reverse, ext};

	        //setup alu3 for adding a and b 
		alu_fns = `ALU_ADD;
		sela = `smem0A;
		selb = `smem0B;
		
		config_bus[`ALU1_CONFIG_OFFSET-1 -: `ALU_CONFIG_BITS] = 
					{sela, selb, alu_fns};

		//init engine
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
		rw_data_to_wr = `DATA_W'h001C0001; //init - new configuration of reset
	        rw_req = 1;
		
		#(3*clk_period) //because of the memory delay and config_reg_shadow load, the init_bit needs at least 3 clock ticks to reset functional units properly
		rw_data_to_wr = `DATA_W'h001C0802; //run 		
	        
	        #clk_period
	        rw_req = 0;
                
	        //product
	        #(2*clk_period)
		d_thread = 0;
	        
		//setup memory 2 for reading vector a and b
		sela = `sdisabled;
		selb = `sdisabled;
		start = 1;
	        shift = 0;
		incr = 2;
		delay = 0;
		reverse = 0;
		ext = 0;
		iterations = 19;
		period = 0;
		duty = 0;
		//setup port A
		config_bus[`MEM2A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, shift, incr, delay, reverse, ext};
	
		start = 2;
		//setup port B
		config_bus[`MEM2B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, shift, incr, delay, reverse, ext};
    
	        //setup memory 3 for storing vector a * b
		sela = `smul1;
		start = 0;
	        shift = 0;
		incr = 1;
		delay = 4;
		reverse = 0;
		ext = 0;
		iterations = 19;
		period = 0;
		duty = 0;
		//setup port A
		config_bus[`MEM3A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, shift, incr, delay, reverse, ext};

	        //setup mul1 for multiplying a and b 
		sela = `smem2A; //inc of 2 because there are 2 ports per memory
		selb = `smem2B;
		
		config_bus[`MULT1_CONFIG_OFFSET-1 -: `MULT_CONFIG_BITS] = 
					{sela, selb, 2'b11};

		//init engine
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
		rw_data_to_wr = `DATA_W'h0001C001; //init - new configuration of reset
	        rw_req = 1;
		
		#(3*clk_period) //because of the memory delay and config_reg_shadow load, the init_bit needs at least 3 clock ticks to reset functional units properly
		rw_data_to_wr = `DATA_W'h0001C022; //run		
	        
	        #clk_period
	        rw_req = 0;
	        
	        //dot product
	        #(25*clk_period) //wait for eng_done, so there are no operations ongoing
	        //print the result of test (passed if it passes the test, failed otherwise)
	        if(count == `DATA_W'h0000001C && result_thread == `DATA_W'h00000016)
	          $fwrite(fp,"Multithreading test -> passed\n");
	        else
		  $fwrite(fp,"Multithreading test -> failed\n");
	        
	        //global configuration reset
                config_bus = 0;
		
	        //setup memory 0 for reading vector a and b
		sela = `sdisabled;
		selb = `sdisabled;
		start = 1;
	        shift = 0;
		incr = 2;
		delay = 0;
		reverse = 0;
		ext = 0;
	        iterations = 19;
		period = 0;
		duty = 0;
		//setup port A
		config_bus[`MEM0A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, shift, incr, delay, reverse, ext};
	
		start = 2;
		//setup port B
		config_bus[`MEM0B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, shift, incr, delay, reverse, ext};
    
	        //setup memory 2 for storing vector a . b
		sela = `salulite1;
		start = 0;
	        shift = 0;
		incr = 0;
		delay = 6;
		reverse = 0;
		ext = 0;
	        iterations = 19;
		period = 0;
		duty = 0;
		//setup port A
		config_bus[`MEM2A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, shift, incr, delay, reverse, ext};

	        //setup mul0 for multiplying a and b 
		sela = `smem0A;
		selb = `smem0B;
		
		config_bus[`MULT0_CONFIG_OFFSET-1 -: `MULT_CONFIG_BITS] = 
					{sela, selb, 2'b11};

	        //setup alulite3 for adding mult0 and reg
		alu_fns = `ALU_ADD + 4'h8;
		sela = `s1;
		selb = `smul0;
		
		config_bus[`ALU_LITE1_CONFIG_OFFSET-1 -: `ALU_LITE_CONFIG_BITS] = 
					{sela, selb, alu_fns};
	        
					
		//init engine
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
		rw_data_to_wr = `DATA_W'h00190001; //init - new configuration of reset
	        rw_req = 1;
		
		#(3*clk_period) //because of the memory delay and config_reg_shadow load, the init_bit needs at least 3 clock ticks to reset functional units properly
		rw_data_to_wr = `DATA_W'h00190242; //run 		
	        
	        #clk_period
	        rw_req = 0;
	        
	        //filter LPB implementation first order
	        #(56*clk_period) //wait for eng_done
	        
	        //read result
		rw_addr = `ENG_MEM2;
		rw_rnw = 1;
	        rw_req = 1;
	        
	        #(2*clk_period)
	        rw_req = 0;
	        
	        //print the result of test (passed if it passes the test, failed otherwise)
	        if(rw_data_to_rd == `DATA_W'h00002814)
	          $fwrite(fp,"Dot Product test -> passed\n");
	        else
		  $fwrite(fp,"Dot Product test -> failed\n");
	        
	        //global configuration reset
	        config_bus = 0;
		
	        //init engine
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
		rw_data_to_wr = 1; //init
	        rw_req = 1;
	        
	        #clk_period
		rw_req = 0;
	        
	        // write 0.9 to output of alu1
		rw_addr = `ALU1;
		rw_req = 1;
		rw_rnw = 0;
		rw_data_to_wr = `DATA_W'h73333333; //0.9 in q1.31 format	        
	        
	        #(clk_period)
	        // write 0.1 to output of alulite2
		rw_addr = `ALULITE2;
		rw_data_to_wr = `DATA_W'h0CCCCCCD; //0.1 in q1.31 format
	        
	        #clk_period
		rw_req = 0;
	        
	        //setup memory 0 for reading signals x1 and x2
		sela = `sdisabled;
		selb = `sdisabled;
		start = 43;
	        shift = 0;
		incr = 1;
		delay = 0;
		reverse = 0;
		ext = 0;
	        iterations = 51;
		period = 7;
		duty = 0;
		//setup port A
		config_bus[`MEM0A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, shift, incr, delay, reverse, ext};
	
		//setup port B
		start = 95;
	        config_bus[`MEM0B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, shift, incr, delay, reverse, ext};
	        
	        //setup alulite3 for adding x1 and x2 
		alu_fns = `ALU_ADD;
		sela = `smem0A;
		selb = `smem0B;
		
		config_bus[`ALU_LITE3_CONFIG_OFFSET-1 -: `ALU_LITE_CONFIG_BITS] = 
					{sela, selb, alu_fns};
	       
	        //setup memory 1 for storing result of filter
		sela = `salu0;
	        selb = `sdisabled;
		start = 44;
	        shift = 0;
		incr = 1;
		delay = 8;
		reverse = 0;
		ext = 0;
	        iterations = 51;
		period = 7;
		duty = 0;
		//setup port A
		config_bus[`MEM1A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, shift, incr, delay, reverse, ext};
	        
	        //setup port B
	        delay = 2;
	        start = 43;
	        config_bus[`MEM1B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, shift, incr, delay, reverse, ext};
	        
	        //setup mul0 for multiplying alu1 and y(n-1)
		sela = `salu1;
		selb = `smem1B;
		
		config_bus[`MULT0_CONFIG_OFFSET-1 -: `MULT_CONFIG_BITS] = 
					{sela, selb, 2'b00};
                
	        //setup mul1 for multiplying alulite2 and alulite3 
		sela = `salulite2;
		selb = `salulite3;
		
		config_bus[`MULT1_CONFIG_OFFSET-1 -: `MULT_CONFIG_BITS] = 
					{sela, selb, 2'b00};
	        
	        //setup alu0 for adding mult0 and mult1
	        sela = `smul0;
	        selb = `smul1;
	        alu_fns = `ALU_ADD;
	        
	        config_bus[`ALU0_CONFIG_OFFSET-1 -: `ALU_CONFIG_BITS] =
       				        {sela, selb, alu_fns};
	        			
		//init engine
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
		rw_data_to_wr = `DATA_W'h001E0001; //init
	        rw_req = 1;
		
		#(3*clk_period) //because of the memory delay and config_reg_shadow load, the init_bit needs at least 3 clock ticks to reset functional units properly
		rw_data_to_wr = `DATA_W'h001E16FE; //run 		
	        
	        #clk_period
	        rw_req = 0;
	        
	        #(450*clk_period) //wait for results to display results
	        //global configuration reset
                config_bus = 0;
		
	        //setup memory 1 for reading signal y
		sela = `sdisabled;
		selb = `sdisabled;
		start = 44;
	        shift = 0;
		incr = 1;
		delay = 0;
		reverse = 0;
		ext = 0;
	        iterations = 51;
		period = 0;
		duty = 0;
		//setup port A
		config_bus[`MEM1A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, shift, incr, delay, reverse, ext};
	        
		//setup memory 0 for reading signals x1 and x2
		sela = `sdisabled;
		selb = `sdisabled;
		start = 43;
	        shift = 0;
		incr = 1;
		delay = 0;
		reverse = 0;
		ext = 0;
	        iterations = 51;
		period = 0;
		duty = 0;
		//setup port A
		config_bus[`MEM0A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, shift, incr, delay, reverse, ext};
	        
		start = 95;
		//setup port B
		config_bus[`MEM0B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, shift, incr, delay, reverse, ext};
	        
	        //setup alulite3 for adding x1 and x2 
		alu_fns = `ALU_ADD;
		sela = `smem0A;
		selb = `smem0B;
		
		config_bus[`ALU_LITE3_CONFIG_OFFSET-1 -: `ALU_LITE_CONFIG_BITS] = 
					{sela, selb, alu_fns};
	        
	        //setup alu0 for read signal y 
		alu_fns = `ALU_ADD;
		sela = `smem1A;
		selb = `salulite2;
		
		config_bus[`ALU0_CONFIG_OFFSET-1 -: `ALU_CONFIG_BITS] = 
					{sela, selb, alu_fns};

	        //setup alulite2 for reg constant 0
	        sela = `salulite2;
	        selb = `salulite2;
	        alu_fns = `ALU_LOGIC_AND;
	        
	        config_bus[`ALU_LITE2_CONFIG_OFFSET-1 -: `ALU_LITE_CONFIG_BITS] =
       				        {sela, selb, alu_fns};
	        
	        //init engine
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
		rw_data_to_wr = `DATA_W'h001C0001; //init
	        rw_req = 1;
		
		#(3*clk_period) //because of the memory delay and config_reg_shadow load, the init_bit needs at least 3 clock ticks to reset functional units properly
		rw_data_to_wr = `DATA_W'h001C1182; //run 		
	        
	        #clk_period
	        rw_req = 0;
	        
	        //filter LPB implementation second order
	        #(75*clk_period) //wait for mem_done
	        
	        //read result
		rw_addr = `ENG_MEM1 + `DATA_W'h5F;
		rw_rnw = 1;
	        rw_req = 1;
	        
	        #(2*clk_period)
	        rw_req = 0;
	        
	        //print the result of test (passed if it passes the test, failed otherwise)
	        if(rw_data_to_rd == `DATA_W'h08DD9C72)
	          $fwrite(fp,"Low Pass Filter first order test -> passed\n");
	        else
		  $fwrite(fp,"Low Pass Filter first order test -> failed\n");
	        
	        //global configuration reset
	        config_bus = 0;
		
	        //init engine
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
		rw_data_to_wr = 1; //init
	        rw_req = 1;
	        
	        #clk_period
		rw_req = 0;
	        
	        //write 0.0099 to output of alu1
		rw_addr = `ALU1;
		rw_req = 1;
		rw_rnw = 0;
		rw_data_to_wr = `DATA_W'h00a2339c; //0.0099 in q2.30 format
	        
	        #clk_period
	        //write 1 to output of mul3
		rw_addr = `MULT3;
		rw_data_to_wr = `DATA_W'h00000001; //1
	        
	        #clk_period
		rw_req = 0;
	        
	        //setup memory 0 for reading signals x1 and x2
		sela = `sdisabled;
		selb = `sdisabled;
		start = 43;
	        shift = 0;
		incr = 1;
		delay = 0;
		reverse = 0;
		ext = 0;
	        iterations = 51;
		period = 17;
		duty = 0;
		//setup port A
		config_bus[`MEM0A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, shift, incr, delay, reverse, ext};
	
		//setup port B
		start = 95;
	        config_bus[`MEM0B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, shift, incr, delay, reverse, ext};
	        
	        //setup alulite3 for adding x1 and x2 
		alu_fns = `ALU_ADD;
		sela = `smem0A;
		selb = `smem0B;
		
		config_bus[`ALU_LITE3_CONFIG_OFFSET-1 -: `ALU_LITE_CONFIG_BITS] = 
					{sela, selb, alu_fns};
	       
	        //setup memory 1 for storing result of filter
		sela = `sbs0;
	        selb = `sdisabled;
		start = 43;
	        shift = 0;
		incr = 1;
		delay = 17;
		reverse = 0;
		ext = 0;
	        iterations = 51;
		period = 17;
		duty = 0;
		//setup port A
		config_bus[`MEM1A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, shift, incr, delay, reverse, ext};
	        
	        //setup port B
	        delay = 0;
	        duty = 1;
	        shift = -1;
	        start = 41;
	        config_bus[`MEM1B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, shift, incr, delay, reverse, ext};
	        
	        //setup memory 2 for loading constants
		sela = `sdisabled;
	        selb = `sdisabled;
		start = 147;
	        shift = -2;
		incr = 1;
		delay = 0;
		reverse = 0;
		ext = 0;
	        iterations = 51;
		period = 17;
		duty = 1;
		//setup port A
		config_bus[`MEM2A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, shift, incr, delay, reverse, ext};
	        
	        //setup port B
	        delay = 4;
	        duty = 7;
	        start = 149;
	        shift = -8;
		config_bus[`MEM2B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, shift, incr, delay, reverse, ext};
	        
	        //setup mul2 for multiplying mem1B and mem2A
		sela = `smem2A;
		selb = `smem1B;
		
		config_bus[`MULT2_CONFIG_OFFSET-1 -: `MULT_CONFIG_BITS] = 
					{sela, selb, 2'b00};
                
	        //setup mul1 for multiplying alu1 and alu3 
		sela = `salu1;
		selb = `salulite3;
		
		config_bus[`MULT1_CONFIG_OFFSET-1 -: `MULT_CONFIG_BITS] = 
					{sela, selb, 2'b00};
	        
	        //setup alu0 for adding mult0 and mult1
	        sela = `smul0;
	        selb = `smul1;
	        alu_fns = `ALU_ADD;
	        
	        config_bus[`ALU0_CONFIG_OFFSET-1 -: `ALU_CONFIG_BITS] =
       				        {sela, selb, alu_fns};
	        
	        //setup mul0 for multiplying alulite2 and mem2B 
		sela = `salulite2;
		selb = `smem2B;
		
		config_bus[`MULT0_CONFIG_OFFSET-1 -: `MULT_CONFIG_BITS] = 
					{sela, selb, 2'b11};
	        
	        //setup alulite2 for adding mult0 and mult2
	        sela = `smul0;
	        selb = `smul2;
	        alu_fns = `ALU_ADD;
	        
	        config_bus[`ALU_LITE2_CONFIG_OFFSET-1 -: `ALU_LITE_CONFIG_BITS] =
       				        {sela, selb, alu_fns};
	        
	        //setup bs0 for shifting alu0 by smul3
		sela = `salu0;
		selb = `smul3;
		
		config_bus[`BS0_CONFIG_OFFSET-1 -: `BS_CONFIG_BITS] = 
					{sela, selb, 2'b01};
	        
		//init engine
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
		rw_data_to_wr = `DATA_W'h001F8001; //init
	        rw_req = 1;
		
		#(3*clk_period) //because of the memory delay and config_reg_shadow load, the init_bit needs at least 3 clock ticks to reset functional units properly
		rw_data_to_wr = `DATA_W'h001F9756; //run 		
	        
	        #clk_period
	        rw_req = 0;
	        
	        #(980*clk_period) //wait for results to display results
	        //global configuration reset
                config_bus = 0;
		
	        //setup memory 1 for reading signal y
		sela = `sdisabled;
		selb = `sdisabled;
		start = 43;
	        shift = 0;
		incr = 1;
		delay = 0;
		reverse = 0;
		ext = 0;
	        iterations = 51;
		period = 0;
		duty = 0;
		//setup port A
		config_bus[`MEM1A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, shift, incr, delay, reverse, ext};
	        
	        //setup memory 0 for reading signals x1 and x2
		sela = `sdisabled;
		selb = `sdisabled;
		start = 43;
	        shift = 0;
		incr = 1;
		delay = 0;
		reverse = 0;
		ext = 0;
	        iterations = 51;
		period = 0;
		duty = 0;
		//setup port A
		config_bus[`MEM0A_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, sela, start, shift, incr, delay, reverse, ext};
	        
		start = 95;
		//setup port B
		config_bus[`MEM0B_CONFIG_OFFSET-1 -: `MEMP_CONFIG_BITS] = 
					{iterations, period, duty, selb, start, shift, incr, delay, reverse, ext};
	        
	        //setup alulite3 for adding x1 and x2 
		alu_fns = `ALU_ADD;
		sela = `smem0A;
		selb = `smem0B;
		
		config_bus[`ALU_LITE3_CONFIG_OFFSET-1 -: `ALU_LITE_CONFIG_BITS] = 
					{sela, selb, alu_fns};
	        
	        //setup alu0 for read signal y 
		alu_fns = `ALU_ADD;
		sela = `smem1A;
		selb = `salulite2;
		
		config_bus[`ALU0_CONFIG_OFFSET-1 -: `ALU_CONFIG_BITS] = 
					{sela, selb, alu_fns};

	        //setup alulite2 for reg constant 0
	        sela = `salulite2;
	        selb = `salulite2;
	        alu_fns = `ALU_LOGIC_AND;
	        
	        config_bus[`ALU_LITE2_CONFIG_OFFSET-1 -: `ALU_LITE_CONFIG_BITS] =
       				        {sela, selb, alu_fns};
	        
	        //init engine
		rw_addr = `ENG_CTRL_REG;
		rw_rnw = 0;
		rw_data_to_wr = `DATA_W'h001C0001; //init
	        rw_req = 1;
		
		#(3*clk_period) //because of the memory delay and config_reg_shadow load, the init_bit needs at least 3 clock ticks to reset functional units properly
		rw_data_to_wr = `DATA_W'h001C1182; //run 		
	        
	        #clk_period
	        rw_req = 0;
	        
	        //simulation time 20000 ns
	        #(35000-1632*clk_period-1)
	        
	        //read result
		rw_addr = `ENG_MEM1 + `DATA_W'h5E;
		rw_rnw = 1;
	        rw_req = 1;
	        
	        #(2*clk_period)
	        rw_req = 0;
	        
	        //print the result of test (passed if it passes the test, failed otherwise)
	        if(rw_data_to_rd == `DATA_W'h05366CE2)
	          $fwrite(fp,"Low Pass Filter second order test -> passed\n");
	        else
		  $fwrite(fp,"Low Pass Filter second order test -> failed\n");
	        
	        $fclose(fp);
	        
                $finish;
	        
	end
      
	always 
		#(clk_period/2) clk = ~clk;
        
        always @ (*) begin
	   x = uut.data_bus[`DATA_BITS - (`salulite3-1)*`DATA_W-1 -: `DATA_W];
	   y = uut.data_bus[`DATA_BITS - (`salu0-1)*`DATA_W-1 -: `DATA_W];
	   x_r = x;
	   y_r = y;

	   if ((uut.status&`DATA_W'h0000000B) == `DATA_W'h0000000B && d_thread == 1'b0) begin
	     result_thread = count;
	     d_thread = 1'b1;
	   end
	   
	   if ((uut.status&`DATA_W'h000000FF) == `DATA_W'h000000FF)
	     done = 1'b1;
	   else
	     done = 1'b0;
	end
        
        // Counter of clocks during threads
        always @ (posedge clk) begin
	   if ((uut.status&`DATA_W'h000000FF) != `DATA_W'h000000FF && done^done_1)
             count <= `DATA_W'h00000000;
	   else if (done == 1'b0)
	     count <= count + 1'b1;
	   done_1 <= done;
	end
		
endmodule

