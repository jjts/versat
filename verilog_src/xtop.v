/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

  Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Andre Lopes <andre.a.lopes@netcabo.pt>
             Guilherme Luz <gui_luz_93@hotmail.com>
             Joao Dias Lopes <joao.d.lopes91@gmail.com>
             Francisco Nunes <ftcnunes@gmail.com>
 
***************************************************************************** */

`timescale 1ns / 1ps

`include "xdefs.v"
`include "xmem_map.v"

module xtop #
            (
	     parameter integer C_S_AXI_ADDR_WIDTH = 32,
	     parameter integer C_S_AXI_DATA_WIDTH = 32,
	     parameter C_S_AXI_MIN_SIZE = 32'h000001FF,
	     parameter integer C_USE_WSTRB = 0,
	     parameter C_DPHASE_TIMEOUT = 8,
	     parameter C_BASEADDR = 32'hFFFFFFFF,
	     parameter C_HIGHADDR = 32'h00000000,
	     //parameter C_FAMILY = "virtex6",
	     parameter integer C_NUM_REG = 1,
	     parameter integer C_NUM_MEM = 1,
	     parameter integer C_SLV_AWIDTH = 32,
	     parameter integer C_SLV_DWIDTH = 32,
	     
	     parameter integer C_M_AXI_THREAD_ID_WIDTH = 1,
	     parameter integer C_M_AXI_ADDR_WIDTH = 32,
	     parameter integer C_M_AXI_DATA_WIDTH = 32,
	     //parameter integer C_M_AXI_AWUSER_WIDTH = 1,
	     //parameter integer C_M_AXI_ARUSER_WIDTH = 1,
	     //parameter integer C_M_AXI_WUSER_WIDTH = 1,
	     //parameter integer C_M_AXI_RUSER_WIDTH = 1,
	     //parameter integer C_M_AXI_BUSER_WIDTH = 1,
	     parameter integer C_M_AXI_SUPPORTS_WRITE = 1,
	     parameter integer C_M_AXI_SUPPORTS_READ = 1,
	     parameter integer C_INTERCONNECT_M_AXI_WRITE_ISSUING = 8,
	     //parameter C_M_AXI_TARGET = 'h00000000,
	     parameter integer C_OFFSET_WIDTH = 9
            )
            (
	     // interface select
`ifdef SPI_IF
	     // SPI slave interface to master CPU 
	     input 				       sclk,
	     input 				       ss,
	     input 				       mosi,
	     output 				       miso,
`endif
	     
`ifdef PAR_IF 
	     //parallel control interface to master CPU
	     input [`CTRL_REGF_ADDR_W-1:0] 	       par_addr,
	     input 				       par_we,
	     input [`DATA_W-1:0] 		       par_in,
	     output [`DATA_W-1:0] 		       par_out,
`endif
	     
`ifdef DMA_IF
	     //dma interface 
	     input 				       dma_req,
	     input 				       dma_rnw,
	     input [`INT_ADDR_W-1:0] 		       dma_addr,
	     input [`DATA_W-1:0] 		       dma_data_in,
	     output [`DATA_W-1:0] 		       dma_data_out,
`endif
	     
`ifdef AXI_IF
	     // AXI4 Lite
	     // System Signals
	     input wire 			       S_AXI_ACLK,
	     input wire 			       S_AXI_ARESETN,
	    
	     // Slave Interface Write Address Ports
	     input wire [C_S_AXI_ADDR_WIDTH-1:0]       S_AXI_AWADDR,
	     //input wire [3-1:0] 			  S_AXI_AWPROT,
	     input wire 			       S_AXI_AWVALID,
	     output wire 			       S_AXI_AWREADY,
	     
	     // Slave Interface Write Data Ports
	     input wire [C_S_AXI_DATA_WIDTH-1:0]       S_AXI_WDATA,
	     input wire [C_S_AXI_DATA_WIDTH/8-1:0]     S_AXI_WSTRB,
	     input wire 			       S_AXI_WVALID,
	     output wire 			       S_AXI_WREADY,
	     
	     // Slave Interface Write Response Ports
	     output wire [2-1:0] 		       S_AXI_BRESP,
	     output wire 			       S_AXI_BVALID,
	     input wire 			       S_AXI_BREADY,
	     
	     
	     // Slave Interface Read Address Ports
	     input wire [C_S_AXI_ADDR_WIDTH-1:0]       S_AXI_ARADDR,
	     //input wire [3-1:0] 			  S_AXI_ARPROT,
	     input wire 			       S_AXI_ARVALID,
	     output wire 			       S_AXI_ARREADY,
	     
	     // Slave Interface Read Data Ports
	     output wire [C_S_AXI_DATA_WIDTH-1:0]      S_AXI_RDATA,
	     output wire [2-1:0] 	      	       S_AXI_RRESP,
	     output wire 			       S_AXI_RVALID,
	     input wire 			       S_AXI_RREADY,
	     
	     // AXI4 Full
	     // System Signals
	     input wire 		       	       M_AXI_ACLK,
	     input wire 			       M_AXI_ARESETN,
	     
	     // Master Interface Write Address
	     output wire [C_M_AXI_THREAD_ID_WIDTH-1:0] M_AXI_AWID,
	     output wire [C_M_AXI_ADDR_WIDTH-1:0]      M_AXI_AWADDR,
	     output wire [8-1:0] 		       M_AXI_AWLEN,
	     output wire [3-1:0] 		       M_AXI_AWSIZE,
	     output wire [2-1:0] 		       M_AXI_AWBURST,
	     output wire 			       M_AXI_AWLOCK,
	     output wire [4-1:0] 		       M_AXI_AWCACHE,
	     output wire [3-1:0] 		       M_AXI_AWPROT,
	     // AXI3 output wire [4-1:0]                  M_AXI_AWREGION,
	     output wire [4-1:0] 		       M_AXI_AWQOS,
	     output wire 			       M_AXI_AWVALID,
	     input wire 			       M_AXI_AWREADY,
	     
	     // Master Interface Write Data
	     // AXI3 output wire [C_M_AXI_THREAD_ID_WIDTH-1:0]     M_AXI_WID,
	     output wire [C_M_AXI_DATA_WIDTH-1:0]      M_AXI_WDATA,
	     output wire [C_M_AXI_DATA_WIDTH/8-1:0]    M_AXI_WSTRB,
	     output wire 			       M_AXI_WLAST,
	     output wire 			       M_AXI_WVALID,
	     input wire 			       M_AXI_WREADY,
	     
	     // Master Interface Write Response
	     input wire [C_M_AXI_THREAD_ID_WIDTH-1:0]  M_AXI_BID,
	     input wire [2-1:0] 		       M_AXI_BRESP,
	     input wire 			       M_AXI_BVALID,
	     output wire 			       M_AXI_BREADY,
	     
	     // Master Interface Read Address
	     output wire [C_M_AXI_THREAD_ID_WIDTH-1:0] M_AXI_ARID,
	     output wire [C_M_AXI_ADDR_WIDTH-1:0]      M_AXI_ARADDR,
	     output wire [8-1:0] 		       M_AXI_ARLEN,
	     output wire [3-1:0] 		       M_AXI_ARSIZE,
	     output wire [2-1:0] 		       M_AXI_ARBURST,
	     output wire 		               M_AXI_ARLOCK,
	     output wire [4-1:0] 		       M_AXI_ARCACHE,
	     output wire [3-1:0] 		       M_AXI_ARPROT,
	     // AXI3 output wire [4-1:0] 		 M_AXI_ARREGION,
	     output wire [4-1:0] 		       M_AXI_ARQOS,
	     output wire 			       M_AXI_ARVALID,
	     input wire 			       M_AXI_ARREADY,
	     
	     // Master Interface Read Data 
	     input wire [C_M_AXI_THREAD_ID_WIDTH-1:0]  M_AXI_RID,
	     input wire [C_M_AXI_DATA_WIDTH-1:0]       M_AXI_RDATA,
	     input wire [2-1:0] 		       M_AXI_RRESP,
	     input wire 			       M_AXI_RLAST,
	     input wire 			       M_AXI_RVALID,
	     output wire 			       M_AXI_RREADY
`endif

`ifndef AXI_IF
	     input 				       clk,
             input 				       rst
`endif
	     );
 
`ifdef AXI_IF
   parameter integer USER_SLV_DWIDTH = C_S_AXI_DATA_WIDTH;
   
   parameter integer IPIF_SLV_DWIDTH = C_S_AXI_DATA_WIDTH;
   
   parameter ZERO_ADDR_PAD = 0;
   parameter USER_SLV_BASEADDR = C_BASEADDR;
   parameter USER_SLV_HIGHADDR = C_HIGHADDR;
   
   parameter integer USER_SLV_NUM_REG = 1;
   parameter integer USER_NUM_REG = USER_SLV_NUM_REG;
   parameter integer TOTAL_IPIF_CE = USER_NUM_REG;
   
   //------------------------------------------
   //-- Index for CS/CE
   //------------------------------------------
   
   parameter integer USER_SLV_CS_INDEX = 0;
`endif
   
   
   // Control bus
   wire 			  rw_req;
   wire 			  rw_rnw;
   wire [`INT_ADDR_W-1:0] 	  rw_addr;
   wire [`DATA_W-1:0] 		  rw_data_to_rd;
   wire [`DATA_W-1:0] 		  rw_data_to_wr;
   // rw bus address decoder signals
   wire 			  ctrl_regf_req;
   wire [`DATA_W-1:0] 		  ctrl_regf_data_to_rd;

   wire 			  div_req;
   wire [`DATA_W-1:0] 		  div_data_to_rd;

   wire 			  dma_req_int;
   
`ifndef DMA_IF
   wire 			  dma_req;
`endif
   wire [`DATA_W-1:0] 		  dma_data_to_rd;
   
   wire 			  prog_mem_req;
   wire [`DATA_W-1:0] 		  prog_mem_data_to_rd;
   
   wire 			  conf_mem_req;
   
   wire [`DATA_W-1:0] 		  eng_data_to_rd;

   // DMA bus address decoder signals
   wire 			  dma_prog_mem_req;
   wire [`DATA_W-1:0] 		  dma_data_out_prog;

   wire 			  dma_conf_mem_req;
   wire [`DATA_W-1:0] 		  dma_data_out_conf;

   wire 			  dma_eng_req;
   wire [`DATA_W-1:0] 		  dma_data_out_eng;

   // SPI backend interface
   wire 			  spi_we;
   wire [`CTRL_REGF_ADDR_W-1:0]   spi_addr;
   wire [`DATA_W-1:0] 		  spi_data_out;

   // Configuration bus
   wire [`CONFIG_BITS-1:0] config_bus;

   // Program memory - xctrl interface
   wire [`IADDR_W:0] 		  pc;
   wire 			  instr_valid;
   wire [`INSTR_W-1:0] 		  instruction;
   
   //parallel data (dma) interface
   wire 			  int_dma_req;
`ifndef DMA_IF
   wire 			  dma_rnw;
   wire [`INT_ADDR_W-1:0] 	  dma_addr;
   wire [`DATA_W-1:0] 		  dma_data_in;
   wire [`DATA_W-1:0] 		  dma_data_out;
`endif
   
   //parallel control interface to master CPU
`ifdef AXI_IF
   wire [`CTRL_REGF_ADDR_W-1:0]   par_addr;
   wire 			  par_we;
   wire [`DATA_W-1:0] 		  par_in;
   wire [`DATA_W-1:0] 		  par_out;
`endif
   
   //SPI - parallel input interface mux
   wire [`CTRL_REGF_ADDR_W-1:0]   par_addr_int;
   wire 			  par_we_int;
   wire [`DATA_W-1:0] 		  par_in_int;

`ifdef SPI_IF
   wire [`DATA_W-1:0] 		  par_out;
`endif 
   
`ifdef AXI_IF
   wire 			  clk;
   wire 			  rst;
`endif
   
   // Create SPI - parallel input interface mux
`ifdef SPI_IF 
   assign par_addr_int = spi_addr;
   assign par_we_int = spi_we;
   assign par_in_int = spi_data_out;
`else
   assign par_addr_int = par_addr;
   assign par_we_int = par_we;
   assign par_in_int = par_in;
`endif
   
`ifdef DMA_IF
   assign int_dma_req = dma_req;
`endif
   
`ifndef DMA_IF
   assign dma_req = dma_req_int;
`endif
   
`ifdef AXI_IF
   assign clk = M_AXI_ACLK;
   assign rst = ~M_AXI_ARESETN;
`endif
   
   // Instantiate the spi_slave
`ifdef SPI_IF 
   spi_slave spi_slave (
	       		.clk(clk),
	       		.rst(rst),
	       		.sclk(sclk), 
	       		.ss(ss), 
	       		.mosi(mosi), 
	       		.miso(miso),
			
	       		.data_in(par_out),
	       		.data_out(spi_data_out),
			.address(spi_addr),
			.we(spi_we)
			);
`endif

   
   // Instantiate the control reg_file
   xctr_regf ctrl_regf (
			.clk(clk),
			
			//external interface
			.ext_we(par_we_int),
			.ext_addr(par_addr_int),
			.ext_data_in(par_in_int),
			.ext_data_out(par_out),
			
			//internal interface
			.int_req(ctrl_regf_req),
			.int_rw_rnw(rw_rnw),
			.int_addr(rw_addr[`CTRL_REGF_ADDR_W-1:0]),
			.int_data_in(rw_data_to_wr),
			.int_data_out(ctrl_regf_data_to_rd)
			);
   
`ifdef AXI_IF
   // Instantiate the DMA
   xdma dma (
	     .clk(clk), 
	     .rst(rst),
	     
	     //DMA interface
	     .rw_req(dma_req),
	     .rw_rnw(rw_rnw),
	     .rw_addr(rw_addr),
	     .data_in(rw_data_to_wr),
	     .data_out(dma_data_to_rd),
	     
	     //external port
	     // Master Interface Write Address
	     .M_AXI_AWID(M_AXI_AWID),
	     .M_AXI_AWADDR(M_AXI_AWADDR),
	     .M_AXI_AWLEN(M_AXI_AWLEN),
	     .M_AXI_AWSIZE(M_AXI_AWSIZE),
	     .M_AXI_AWBURST(M_AXI_AWBURST),
	     .M_AXI_AWLOCK(M_AXI_AWLOCK),
	     .M_AXI_AWCACHE(M_AXI_AWCACHE),
	     .M_AXI_AWPROT(M_AXI_AWPROT),
	     .M_AXI_AWQOS(M_AXI_AWQOS),
	     .M_AXI_AWVALID(M_AXI_AWVALID),
	     .M_AXI_AWREADY(M_AXI_AWREADY),
	     
	     // Master Interface Write Data
	     .M_AXI_WDATA(M_AXI_WDATA),
	     .M_AXI_WSTRB(M_AXI_WSTRB),
	     .M_AXI_WLAST(M_AXI_WLAST),
	     .M_AXI_WVALID(M_AXI_WVALID),
	     .M_AXI_WREADY(M_AXI_WREADY),
	     
	     // Master Interface Write Response
	     .M_AXI_BID(M_AXI_BID),
	     .M_AXI_BRESP(M_AXI_BRESP),
	     .M_AXI_BVALID(M_AXI_BVALID),
	     .M_AXI_BREADY(M_AXI_BREADY),
	     
	     // Master Interface Read Address
	     .M_AXI_ARID(M_AXI_ARID),
	     .M_AXI_ARADDR(M_AXI_ARADDR),
	     .M_AXI_ARLEN(M_AXI_ARLEN),
	     .M_AXI_ARSIZE(M_AXI_ARSIZE),
	     .M_AXI_ARBURST(M_AXI_ARBURST),
	     .M_AXI_ARLOCK(M_AXI_ARLOCK),
	     .M_AXI_ARCACHE(M_AXI_ARCACHE),
	     .M_AXI_ARPROT(M_AXI_ARPROT),
	     .M_AXI_ARQOS(M_AXI_ARQOS),
	     .M_AXI_ARVALID(M_AXI_ARVALID),
	     .M_AXI_ARREADY(M_AXI_ARREADY),
	     
	     // Master Interface Read Data 
	     .M_AXI_RID(M_AXI_RID),
	     .M_AXI_RDATA(M_AXI_RDATA),
	     .M_AXI_RRESP(M_AXI_RRESP),
	     .M_AXI_RLAST(M_AXI_RLAST),
	     .M_AXI_RVALID(M_AXI_RVALID),
	     .M_AXI_RREADY(M_AXI_RREADY),
	     
	     //internal port
	     .int_req(int_dma_req),
	     .int_rnw(dma_rnw),
	     .int_addr(dma_addr),
	     .int_data_out(dma_data_in),
	     .int_data_in(dma_data_out)
	     );
   
   // Instantiate the AXI slave I/F
   xaxi_lite_slave axi_lite_slave (
`ifndef ASIC
				   .S_AXI_ACLK(S_AXI_ACLK),
`endif
`ifdef ASIC
				   .S_AXI_ACLK(M_AXI_ACLK),
`endif
				   .S_AXI_ARESETN(S_AXI_ARESETN),
				   
				   //external port
				   // Slave Interface Write Address Ports
				   .S_AXI_AWADDR(S_AXI_AWADDR),
				   .S_AXI_AWVALID(S_AXI_AWVALID),
				   .S_AXI_AWREADY(S_AXI_AWREADY),
				   
				   // Slave Interface Write Data Ports
				   .S_AXI_WDATA(S_AXI_WDATA),
				   .S_AXI_WSTRB(S_AXI_WSTRB),
				   .S_AXI_WVALID(S_AXI_WVALID),
				   .S_AXI_WREADY(S_AXI_WREADY),
				   
				   // Slave Interface Write Response Ports
				   .S_AXI_BRESP(S_AXI_BRESP),
				   .S_AXI_BVALID(S_AXI_BVALID),
				   .S_AXI_BREADY(S_AXI_BREADY),
				   
				   // Slave Interface Read Address Ports
				   .S_AXI_ARADDR(S_AXI_ARADDR),
				   .S_AXI_ARVALID(S_AXI_ARVALID),
				   .S_AXI_ARREADY(S_AXI_ARREADY),
				   
				   // Slave Interface Read Data Ports
				   .S_AXI_RDATA(S_AXI_RDATA),
				   .S_AXI_RRESP(S_AXI_RRESP),
				   .S_AXI_RVALID(S_AXI_RVALID),
				   .S_AXI_RREADY(S_AXI_RREADY),
				   
				   //internal port
				   .par_addr(par_addr),
				   .par_we(par_we),
				   .par_in(par_out),
				   .par_out(par_in)
				   );
`endif
   
   // Instantiate the controller
   xctrl controller (
		     .clk(clk), 
		     .rst(rst),
		     
		     // Program memory interface
		     .pc(pc),
		     .instr_valid(instr_valid),
		     .instruction(instruction),
		     
		     // Internal rw bus
		     .rw_req(rw_req), 
		     .rw_rnw(rw_rnw), 
		     .rw_addr(rw_addr),
		     .data_to_rd(rw_data_to_rd), 
		     .data_to_wr(rw_data_to_wr)
		     );

   // Instantiate the Control Bus address decoder
   xaddr_decoder addr_decoder (
			       .rw_addr(rw_addr), 
       			       .rw_req(rw_req),
			       .data_to_rd(rw_data_to_rd),
			       
			       .div_req(div_req),
			       .div_data_to_rd(div_data_to_rd),
			       
			       .dma_req(dma_req_int),
			       .dma_data_to_rd(dma_data_to_rd),
			       
			       .ctrl_regf_req(ctrl_regf_req),
			       .ctrl_regf_data_to_rd(ctrl_regf_data_to_rd),
			       
			       .eng_data_to_rd(eng_data_to_rd),
			       
			       .prog_mem_req(prog_mem_req),
			       .prog_mem_data_to_rd(prog_mem_data_to_rd),
			       
			       .conf_mem_req(conf_mem_req)
			       );

   // Instantiate DMA address decoder
   xdma_addr_decoder dma_addr_decoder (
				       .clk(clk),
				       .rst(rst),
				       
       				       .dma_req(int_dma_req),
				       .dma_addr(dma_addr), 
				       .dma_data_out(dma_data_out),
				       
				       .prog_mem_req(dma_prog_mem_req),
				       .prog_data_to_rd(dma_data_out_prog),

				       .eng_req(dma_eng_req),
				       .eng_data_to_rd(dma_data_out_eng),

				       .conf_mem_req(dma_conf_mem_req),
				       .conf_data_to_rd(dma_data_out_conf)
				       );
   
   // Instantiate the instruction memory   
   xprog_mem prog_mem (
		       .clk(clk),
		       .rst(rst),
		       
		       .pc(pc),
		       .instr_valid(instr_valid),
       		       .instruction(instruction),
		       
		       .rw_req(prog_mem_req),
       		       .rw_rnw(rw_rnw),
	 	       .rw_addr(rw_addr[`IADDR_W-1:0]),
	 	       .rw_data_in(rw_data_to_wr),
		       .rw_data_out(prog_mem_data_to_rd),
		       
		       .dma_req(dma_prog_mem_req),
		       .dma_rnw(dma_rnw),
		       .dma_addr(dma_addr[`IADDR_W-1:0]),
		       .dma_data_in(dma_data_in),
		       .dma_data_out(dma_data_out_prog)
		       
		       );

   // Instantiate the configuration memory
   xconf conf (
	       .clk(clk),
	       .rst(rst),
	       
	       // Control bus interface
	       .rw_req(conf_mem_req),
	       .rw_rnw(rw_rnw),
	       .rw_addr(rw_addr),
	       .rw_data(rw_data_to_wr[`DADDR_W-1:0]),
	       
  	       // DMA interface
	       .dma_req(dma_conf_mem_req),
	       .dma_rnw(dma_rnw),
	       .dma_addr(dma_addr[`CONF_ADDR_W+`N_CONF_SLICE_W-1:0]), 
	       .dma_data_in(dma_data_in), 
	       .dma_data_out(dma_data_out_conf),
	       
	       // Configuration for engine
    	       .conf_out(config_bus)
	       );
   
   // Instantiate the data plane engine
   xdata_eng data_eng (
		       .clk(clk),
		       .rst(rst),

		       // Control bus interface
		       .rw_req(rw_req),
		       .rw_rnw(rw_rnw), 
		       .rw_addr(rw_addr),
		       .rw_data_to_wr(rw_data_to_wr),
		       .rw_data_to_rd(eng_data_to_rd),

		       // Configuration bus input
		       .config_bus(config_bus),

		       // DMA interface
 		       .dma_req(dma_eng_req),
 		       .dma_rnw(dma_rnw),
 		       .dma_addr(dma_addr[`DADDR_W+1:0]),
		       .dma_data_in(dma_data_in),
	 	       .dma_data_out(dma_data_out_eng)
		       );

   // Instantiate the serial divider
   xdiv_wrapper div_wrapper (
			     .clk(clk),
			     .rst(rst),

			     // Control bus interface
			     .rw_req(div_req),
			     .rw_rnw(rw_rnw), 
			     .rw_addr(rw_addr[2:0]),
			     .rw_data_to_wr(rw_data_to_wr),
			     .rw_data_to_rd(div_data_to_rd)
			     );
   
			     
endmodule
