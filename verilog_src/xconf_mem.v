/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
            Joao Dias lopes <joao.d.lopes91@gmail.com>
            Francisco Nunes <ftcnunes@gmail.com>
 
 ***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module xconf_mem(
		 input 					  clk,
		 input 					  rst,
   
		 //dma port
		 input 					  dma_req,
		 input 					  dma_rnw,
		 input [`CONF_ADDR_W+`N_CONF_SLICE_W-1:0] dma_addr,
		 input [`DATA_W-1:0] 			  dma_data_in,
		 output reg [`DATA_W-1:0] 		  dma_data_out,

		 //configuration port
		 input 					  conf_req,
		 input 					  conf_rnw,
		 input [`CONF_ADDR_W-1:0] 		  conf_addr,
		 input [`CONFIG_BITS-1:0] 		  conf_data_in,
		 output [`CONFIG_BITS-1:0] 		  conf_data_out,
		 output reg 				  conf_ld
		 );

   // Slice enable array
   reg [`N_CONF_SLICES-1:0] 				  en_int;
   reg [`N_CONF_SLICES-1:0] 				  en_int_reg;

   // Data IO
   wire [`N_CONF_SLICES*`DATA_W-1:0] 			  data_in;
   wire [`N_CONF_SLICES*`DATA_W-1:0] 			  data_out;
   wire [`N_CONF_SLICES*`DATA_W-1:0] 			  dma_data_out_int;
   
   // Load configuration to xconf_reg
   always @ (posedge rst, posedge clk) begin 
      if(rst)
	conf_ld <= 1'b0;
      else 
	conf_ld <= conf_req & conf_rnw;
   end
   
   // Read/write data
   assign data_in = {conf_data_in,{(`DATA_W*`N_CONF_SLICES-`CONFIG_BITS){1'b0}}};
   assign conf_data_out = data_out[`N_CONF_SLICES*`DATA_W-1 -: `CONFIG_BITS];

   // Slice decoder
   reg [4:0] 						  i,k;

   always @ * begin : en_int_compute
      en_int = 0;
      
      if (dma_req) begin
	 for (i=0; i<`N_CONF_SLICES; i=i+1)
	   en_int[i] = ((i == dma_addr[`N_CONF_SLICE_W-1 : 0]) ? 1'b1: 1'b0);
      end else if (conf_req) begin
	 en_int = {`N_CONF_SLICES{1'b1}};
      end else begin
	 en_int = {`N_CONF_SLICES{1'b0}};
      end
   end

   always @ * begin : dma_data_output
      dma_data_out = `DATA_W'd0;
      
      for (k=0; k < `N_CONF_SLICES; k=k+1) begin
	 if (en_int_reg[k])
	   dma_data_out = dma_data_out_int[`N_CONF_SLICES*`DATA_W-k*`DATA_W-1 -: `DATA_W];
      end
   end

   always @ (posedge clk) begin : en_int_register
      en_int_reg = en_int;
   end
   
   // Slice array 
   genvar j;

   generate
      for (j=0; j < `N_CONF_SLICES; j=j+1) begin : conf_slice_array
	 
	 xconf_mem_slice xconf_slice (
				      .clk(clk),
				      .en(en_int[j]),
				      .dma_req(dma_req),
				      .dma_rnw(dma_rnw),
				      .conf_req(conf_req),
				      .conf_rnw(conf_rnw),
				      .dma_addr(dma_addr[`CONF_ADDR_W+`N_CONF_SLICE_W-1 -:`CONF_ADDR_W]),
				      .conf_addr(conf_addr),
				      .dma_data_in(dma_data_in),
				      .dma_data_out(dma_data_out_int[`N_CONF_SLICES*`DATA_W-j*`DATA_W-1 -:`DATA_W]),
				      .conf_data_in(data_in[`N_CONF_SLICES*`DATA_W-j*`DATA_W-1 -:`DATA_W]),
				      .conf_data_out(data_out[`N_CONF_SLICES*`DATA_W-j*`DATA_W-1 -:`DATA_W])
				      );
	 
      end
   endgenerate
endmodule
