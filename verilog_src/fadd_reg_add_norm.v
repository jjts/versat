`timescale 1ns / 1ps



module fadd_reg_add_norm(
    input clk, reset,
    
    input eop_add,
    input sign_add,
    input [7:0] exp_add,
    input [1:0] round_mode_add,
    input [27:0] sum_add,
    
    output reg eop_norm,
    output reg sign_norm,
    output reg [7:0] exp_norm,
    output reg [1:0] round_mode_norm,
    output reg [27:0] sum_norm  
    );
    
    always @ (posedge clk)
    begin
        if (reset) begin
            eop_norm <= 0;
            sign_norm <= 0;
            exp_norm <= 0;
            round_mode_norm <= 0;
            sum_norm <= 0;
        end
        else begin
            eop_norm <= eop_add;
            sign_norm <= sign_add;
            exp_norm <= exp_add;
            round_mode_norm <= round_mode_add;
            sum_norm <= sum_add;
        end
    end
    
endmodule
