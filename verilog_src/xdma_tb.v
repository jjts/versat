/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

  Copyright (C) 2014 Authors

  Author(s): Joao Dias Lopes <joao.d.lopes91@gmail.com>
 
***************************************************************************** */

`timescale 1ns/1ps

`include "xdefs.v"
`include "xmem_map.v"

module xdma_tb #
  (
   parameter integer C_M_AXI_THREAD_ID_WIDTH = 1,
   parameter integer C_M_AXI_ADDR_WIDTH = 32,
   parameter integer C_M_AXI_DATA_WIDTH = 32,
   parameter integer C_M_AXI_SUPPORTS_WRITE = 1,
   parameter integer C_M_AXI_SUPPORTS_READ = 1,
   parameter integer C_INTERCONNECT_M_AXI_WRITE_ISSUING = 8,
   parameter integer C_OFFSET_WIDTH = 9
   );

   //parameters 
   parameter clk_period = 40;
   parameter NTRANSF = 256;
   
   // System Signals
   reg 		     clk;
   reg 		     rst;
   
   //DMA interface
   reg 		         rw_req;
   reg 		         rw_rnw;
   reg [`INT_ADDR_W-1:0] rw_addr;
   reg [`DATA_W-1:0] 	 data_in;
   wire [`DATA_W-1:0] 	 data_out;
   
   // AXI4 Full
   // Master Interface Write Address
   wire [C_M_AXI_THREAD_ID_WIDTH-1:0] M_AXI_AWID;
   wire [C_M_AXI_ADDR_WIDTH-1:0]      M_AXI_AWADDR;
   wire [8-1:0] 		      M_AXI_AWLEN;
   wire [3-1:0] 		      M_AXI_AWSIZE;
   wire [2-1:0] 		      M_AXI_AWBURST;
   wire 			      M_AXI_AWLOCK;
   wire [4-1:0] 		      M_AXI_AWCACHE;
   wire [3-1:0] 		      M_AXI_AWPROT;
   wire [4-1:0] 		      M_AXI_AWQOS;
   wire 			      M_AXI_AWVALID;
   reg 				      M_AXI_AWREADY;
   
   // Master Interface Write Data
   wire [C_M_AXI_DATA_WIDTH-1:0]      M_AXI_WDATA;
   wire [C_M_AXI_DATA_WIDTH/8-1:0]    M_AXI_WSTRB;
   wire 			      M_AXI_WLAST;
   wire 			      M_AXI_WVALID;
   reg 				      M_AXI_WREADY;
   
   // Master Interface Write Response
   reg [C_M_AXI_THREAD_ID_WIDTH-1:0]  M_AXI_BID;
   reg [2-1:0] 			      M_AXI_BRESP;
   reg 				      M_AXI_BVALID;
   wire 			      M_AXI_BREADY;
   
   // Master Interface Read Address
   wire [C_M_AXI_THREAD_ID_WIDTH-1:0] M_AXI_ARID;
   wire [C_M_AXI_ADDR_WIDTH-1:0]      M_AXI_ARADDR;
   wire [8-1:0] 		      M_AXI_ARLEN;
   wire [3-1:0] 		      M_AXI_ARSIZE;
   wire [2-1:0] 		      M_AXI_ARBURST;
   wire [2-1:0] 		      M_AXI_ARLOCK;
   wire [4-1:0] 		      M_AXI_ARCACHE;
   wire [3-1:0] 		      M_AXI_ARPROT;
   wire [4-1:0] 		      M_AXI_ARQOS;
   wire 			      M_AXI_ARVALID;
   reg 				      M_AXI_ARREADY;
   
   // Master Interface Read Data 
   reg [C_M_AXI_THREAD_ID_WIDTH-1:0]  M_AXI_RID;
   reg [C_M_AXI_DATA_WIDTH-1:0]       M_AXI_RDATA;
   reg [2-1:0] 			      M_AXI_RRESP;
   reg 				      M_AXI_RLAST;
   reg 				      M_AXI_RVALID;
   wire 			      M_AXI_RREADY;

   //internal port
   wire 			      int_req;
   wire 			      int_rnw;
   wire [`INT_ADDR_W-1:0] 	      int_addr;
   wire [`DATA_W-1:0] 		      int_data_out;
   reg [`DATA_W-1:0] 		      int_data_in;
   
   integer 			      k;
   
   // Instantiate the control reg_file
   xdma uut (
	     // System Signals
	     .clk(clk),
	     .rst(rst),

	     //DMA interface
  	     .rw_req(rw_req),
   	     .rw_rnw(rw_rnw),
	     .rw_addr(rw_addr),
  	     .data_in(data_in),
   	     .data_out(data_out),
	     
	     // Master Interface Write Address
	     .M_AXI_AWID(M_AXI_AWID),
	     .M_AXI_AWADDR(M_AXI_AWADDR),
	     .M_AXI_AWLEN(M_AXI_AWLEN),
	     .M_AXI_AWSIZE(M_AXI_AWSIZE),
	     .M_AXI_AWBURST(M_AXI_AWBURST),
	     .M_AXI_AWLOCK(M_AXI_AWLOCK),
	     .M_AXI_AWCACHE(M_AXI_AWCACHE),
	     .M_AXI_AWPROT(M_AXI_AWPROT),
	     .M_AXI_AWQOS(M_AXI_AWQOS),
	     .M_AXI_AWVALID(M_AXI_AWVALID),
	     .M_AXI_AWREADY(M_AXI_AWREADY),
	     
	     // Master Interface Write Data
	     .M_AXI_WDATA(M_AXI_WDATA),
	     .M_AXI_WSTRB(M_AXI_WSTRB),
	     .M_AXI_WLAST(M_AXI_WLAST),
	     .M_AXI_WVALID(M_AXI_WVALID),
	     .M_AXI_WREADY(M_AXI_WREADY),
	     
	     // Master Interface Write Response
	     .M_AXI_BID(M_AXI_BID),
	     .M_AXI_BRESP(M_AXI_BRESP),
	     .M_AXI_BVALID(M_AXI_BVALID),
	     .M_AXI_BREADY(M_AXI_BREADY),
	     
	     // Master Interface Read Address
	     .M_AXI_ARID(M_AXI_ARID),
	     .M_AXI_ARADDR(M_AXI_ARADDR),
	     .M_AXI_ARLEN(M_AXI_ARLEN),
	     .M_AXI_ARSIZE(M_AXI_ARSIZE),
	     .M_AXI_ARBURST(M_AXI_ARBURST),
	     .M_AXI_ARLOCK(M_AXI_ARLOCK),
	     .M_AXI_ARCACHE(M_AXI_ARCACHE),
	     .M_AXI_ARPROT(M_AXI_ARPROT),
	     .M_AXI_ARQOS(M_AXI_ARQOS),
	     .M_AXI_ARVALID(M_AXI_ARVALID),
	     .M_AXI_ARREADY(M_AXI_ARREADY),
	     
	     // Master Interface Read Data 
	     .M_AXI_RID(M_AXI_RID),
	     .M_AXI_RDATA(M_AXI_RDATA),
	     .M_AXI_RRESP(M_AXI_RRESP),
	     .M_AXI_RLAST(M_AXI_RLAST),
	     .M_AXI_RVALID(M_AXI_RVALID),
	     .M_AXI_RREADY(M_AXI_RREADY),

	     //internal port
	     .int_req(int_req),
	     .int_rnw(int_rnw),
   	     .int_addr(int_addr),
   	     .int_data_out(int_data_out),
    	     .int_data_in(int_data_in)
	     );
   
   initial begin
      
`ifdef DEBUG
      $dumpfile("xdma.vcd");
      $dumpvars();
`endif
      
      rw_req = 0;
      rw_rnw = 1;
      rw_addr = `INT_ADDR_W'd0;
      data_in = `DATA_W'd0;
      M_AXI_AWREADY = 1;
      M_AXI_WREADY = 0;
      M_AXI_BID = 0;
      M_AXI_BRESP = 2'b00;
      M_AXI_BVALID = 0;
      M_AXI_ARREADY = 1;
      M_AXI_RID = 0;
      M_AXI_RDATA = `DATA_W'd0;
      M_AXI_RRESP = 2'b00;
      M_AXI_RLAST = 0;
      M_AXI_RVALID = 0;
      int_data_in = `DATA_W'd0;
      
      // Global reset of FPGA
      #100
 
      // Initialize Inputs
      clk = 0;
      rst = 0;
      
      // Global reset
      #(clk_period+1)
      rst = 1;
      
      #clk_period
      rst = 0;
      
      //
      // Configure DMA to copy data into versat
      //
      #clk_period;
      
      rw_req = 1;
      rw_rnw = 0;
      rw_addr = `DMA_EXT_ADDR_ADDR;
      data_in = `DATA_W'hA7400000;
      
      #clk_period;
      
      rw_rnw = 1;
      
      #clk_period;
      
      rw_rnw = 0;
      rw_addr = `DMA_INT_ADDR_ADDR;
      data_in = `DATA_W'h2000;

      #clk_period;

      rw_rnw = 1;
      
      #clk_period;
      
      rw_rnw = 0;
      rw_addr = `DMA_SIZE_ADDR;
      data_in = NTRANSF;

      #clk_period;
      
      rw_rnw = 1;
      
      #clk_period;
      
      rw_rnw = 0;
      rw_addr = `DMA_DIRECTION_ADDR;
      data_in = `DATA_W'd1;
      
      #clk_period;
      
      rw_rnw = 1;
      
      #clk_period;
      
      rw_req = 0;
      
      //
      // Emulate BUS AXI4 signals
      //
      #clk_period;
      
      M_AXI_ARREADY = 0;
      
      #clk_period;
      
      M_AXI_RVALID = 1;
      
      for (k = 0; k < NTRANSF; k=k+1) begin
	 M_AXI_RDATA = k;
	 
	 if (k == (NTRANSF-1))
	   M_AXI_RLAST = 1;
	 
	 #clk_period;
      end
      
      M_AXI_ARREADY = 1;
      M_AXI_RVALID = 0;
      M_AXI_RLAST = 0;
      
      //
      // Configure DMA to copy data from versat
      //
      #clk_period;
      
      rw_req = 1;
      rw_rnw = 0;
      rw_addr = `DMA_DIRECTION_ADDR;
      data_in = `DATA_W'd2;
      
      #clk_period;
      
      rw_rnw = 1;
      
      #clk_period;
      
      rw_req = 0;
      
      //
      // Emulate BUS AXI4 signals
      //
      #clk_period;
      
      M_AXI_AWREADY = 0;
      
      #clk_period;
      
      M_AXI_WREADY = 1;
      
      for (k = 0; k < (NTRANSF+2); k=k+1) begin
	 if (k > 0 && k < (NTRANSF+1))
	   int_data_in = k - 1;

	 if (k == (NTRANSF+1))
	   M_AXI_BVALID = 1;
	 
	 #clk_period;
      end
      
      M_AXI_BVALID = 0;
      M_AXI_AWREADY = 1;
      M_AXI_WREADY = 0;
      
      #clk_period;
      
      // Simulation time
      #clk_period $finish;
   end
	
   always 
     #(clk_period/2) clk = ~clk;
   
endmodule
