/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: 

 Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
            Joao Dias Lopes <joao.d.lopes91@gmail.com>
            Andre Lopes <andre.a.lopes@netcabo.pt>
            Francisco Nunes <ftcnunes@gmail.com>

  ***************************************************************************** */


/* 
 
Command word structure

 bit  meaning 
 0    init 
 1    run 
 2    bs0
 3    mult3

 4    mult2
 5    mult1
 6    mult0
 7    alulite3

 8    alulite2
 9    alulite1
10    alulite0
11    alu1

12    alu0
13    mem3b 
14    mem3a 
15    mem2b 
 -------
16    mem2a 
17    mem1b 
18    mem1a 
19    mem0b 
 
20    mem0a 

Status word structure  
 
 bit meaning
 0  mem0B done
 1  mem0A done
 2  mem1B done
 3  mem1A done

 4  mem2B done
 5  mem2A done
 6  mem3B done
 7  mem3A done
  */


`timescale 1ns / 1ps

`include "xdefs.v"
`include "xmem_map.v"

module xdata_eng (
		  input 		   clk,
		  input 		   rst, 

		  input [`CONFIG_BITS-1:0] config_bus,

		  // Controller interface
		  input 		   rw_req,
		  input 		   rw_rnw,
		  input [`INT_ADDR_W-1:0]  rw_addr,
		  input [`DATA_W-1:0] 	   rw_data_to_wr,
		  output reg [`DATA_W-1:0] rw_data_to_rd,

		  // DMA interface
 		  input 		   dma_req,
 		  input 		   dma_rnw,
 		  input [`DADDR_W+1:0] 	   dma_addr,
		  input [`DATA_W-1:0] 	   dma_data_in,
	          output reg [`DATA_W-1:0] dma_data_out
		  );
   
   wire [2*`Nmem-1:0] 			   mem_done;
   wire 				   run_bit, init_bit;
   reg 					   run_bit_reg, init_bit_reg;
   
   wire 				   ctrl_req;
   wire 				   status_req;
   wire [`DATA_W-1: 0] 			   status;
   
   reg [`Nmem-1:0] 			   dma_mem_req;
   wire [`Nmem-1:0] 			   mem_req;	
   wire [`N-4:0] 			   fu_regf_req;

   // All mems and fus contribute their outputs to this bus
   wire [`DATA_BITS-1:0] 		   data_bus;
   
   reg [`CONFIG_BITS-1:0] 		   config_reg_shadow;
   
   // Auxiliary signals
   reg 					   rw_req_reg;
   reg 					   rw_rnw_reg; 
   reg [`INT_ADDR_W-1:0] 		   rw_addr_reg;
   reg [`DATA_W-1:0] 			   rw_data_to_wr_reg;
   
   reg 					   dma_req_reg;
   reg 					   dma_rnw_reg;
   reg [`DADDR_W+1:0] 			   dma_addr_reg;
   reg [`DATA_W-1:0] 			   dma_data_in_reg;
   
   reg [1:0] 				   dma_addr_int;
   
   // ENG_STATUS_REG
   assign status = {{(`DATA_W-2*`Nmem){1'b0}}, mem_done};

   assign init_bit = ctrl_req & rw_data_to_wr_reg[0];
   assign run_bit = ctrl_req & rw_data_to_wr_reg[1];
   
   //assign special data bus entries: 0, 1 and rw_data_to_wr_reg
   assign data_bus[`DATA_BITS-1 -: `DATA_W] = `DATA_W'd0; //zero constant
   assign data_bus[`DATA_BITS-1-`DATA_W -: `DATA_W] = `DATA_W'd1; //one constant
   assign data_bus[`DATA_BITS-1-2*`DATA_W -: `DATA_W] = 0;

   // init and run registers
   always @ (posedge rst, posedge clk) begin
      if(rst) begin
	 init_bit_reg <= 1'b0;
	 run_bit_reg <= 1'b0;
      end
      else begin
	 init_bit_reg <= init_bit;
	 run_bit_reg <= run_bit;
      end
   end

   // register inputs
   always @ (posedge rst, posedge clk) begin
      if(rst) begin
	 rw_req_reg <= 1'b0;
	 rw_rnw_reg <= 1'b0; 
	 rw_addr_reg <= {`INT_ADDR_W{1'b0}};
	 rw_data_to_wr_reg <= {`DATA_W{1'b0}};
	 dma_req_reg <= 1'b0;
 	 dma_rnw_reg <= 1'b0;
 	 dma_addr_reg <= {(`DADDR_W+2){1'b0}};
	 dma_data_in_reg <= {`DATA_W{1'b0}};
      end
      else begin
	 rw_req_reg <= rw_req;
	 rw_rnw_reg <= rw_rnw;
	 rw_addr_reg <= rw_addr;
	 rw_data_to_wr_reg <= rw_data_to_wr;
	 dma_req_reg <= dma_req;
 	 dma_rnw_reg <= dma_rnw;
 	 dma_addr_reg <= dma_addr;
	 dma_data_in_reg <= dma_data_in;
      end
   end

   // configuration register
   always @ (posedge rst, posedge clk) begin
      if(rst) begin
	 config_reg_shadow <= {`CONFIG_BITS{1'b0}};
      end
      else if(init_bit) begin
	 config_reg_shadow <= config_bus;
      end
   end

   // dma address register
   always @ (posedge rst, posedge clk) begin
      if (rst) begin
	 dma_addr_int <= 2'b00;
      end else begin
	 dma_addr_int <= dma_addr_reg[`DADDR_W+1 -: 2];
      end
   end
   
   //dma address decoder
   //compute requests
   always @ * begin
      dma_mem_req = {`Nmem{1'b0}};
      case (dma_addr_reg[`DADDR_W+1 -: 2])
	2'd0 : dma_mem_req[0] = dma_req_reg;
	2'd1 : dma_mem_req[1] = dma_req_reg;
	2'd2 : dma_mem_req[2] = dma_req_reg;
	2'd3 : dma_mem_req[3] = dma_req_reg;
      endcase
   end
   //compute dma_data_out
   always @ * begin
      case (dma_addr_int)
	2'd0 : dma_data_out = data_bus[`DATA_BITS - (`smem0A - 1)*`DATA_W - 1 -: `DATA_W];
	2'd1 : dma_data_out = data_bus[`DATA_BITS - (`smem1A - 1)*`DATA_W - 1 -: `DATA_W];
	2'd2 : dma_data_out = data_bus[`DATA_BITS - (`smem2A - 1)*`DATA_W - 1 -: `DATA_W];
	2'd3 : dma_data_out = data_bus[`DATA_BITS - (`smem3A - 1)*`DATA_W - 1 -: `DATA_W];
      endcase
   end
   
   // Select read data
   always @ * begin : data_read_sel
      integer j;
      rw_data_to_rd = status;

      if(~status_req) begin
	 for (j=0; j < `Nmem; j= j+1)
	   if (mem_req[j])
	     rw_data_to_rd = data_bus[`DATA_BITS - (`smem0B+2*j-1)*`DATA_W -1 -: `DATA_W];	
	 for (j=0; j < (`N-3); j= j+1)
	   if (fu_regf_req[`N-4-j])
	     rw_data_to_rd = data_bus[`DATA_BITS - (j+3)*`DATA_W -1 -: `DATA_W];				
      end 
   end
   
   xeng_addr_decoder eng_addr_decoder(
				      .rw_req(rw_req_reg),
				      .rw_addr(rw_addr_reg),
				      .ctrl_req(ctrl_req),
				      .status_req(status_req),
				      .fu_regf_req(fu_regf_req),
				      .mem_req(mem_req)
      				      );
   
   // Instantiate the data memories
   genvar i;
   generate
      for (i=0; i < `Nmem; i=i+1) begin : mem_array
	 xmem mem (
		   .clk(clk),
		   .rst(rst),
		   .initA((init_bit_reg&rw_data_to_wr_reg[`mem_idx-2*i-1])),
		   .initB((init_bit_reg&rw_data_to_wr_reg[`mem_idx-2*i-2])),
		   .runA(run_bit_reg&rw_data_to_wr_reg[`mem_idx-2*i-1]),
		   .runB(run_bit_reg&rw_data_to_wr_reg[`mem_idx-2*i-2]),
		   .doneA(mem_done[2*i+1]),
		   .doneB(mem_done[2*i]),

		   // Controller interface
		   .rw_addrA_req(fu_regf_req[`N-`smem0A-2*i]),
		   .rw_addrB_req(fu_regf_req[`N-`smem0B-2*i]),
		   .rw_mem_req(mem_req[i]),
		   .rw_rnw(rw_rnw_reg),
		   .rw_addr(rw_addr_reg[`DADDR_W-1:0]),
		   .rw_data_to_wr(rw_data_to_wr_reg),
 		   
		   // DMA interface
		   .dma_addr(dma_addr_reg[`DADDR_W-1:0]),
		   .dma_rnw(dma_rnw_reg),
		   .dma_mem_req(dma_mem_req[i]),
		   
		   // Data IO
		   .dma_data_in(dma_data_in_reg),
		   .data_in_bus(data_bus),
		   .outA(data_bus[`DATA_BITS - (`smem0A+2*i-1)*`DATA_W-1 -: `DATA_W]),
		   .outB(data_bus[`DATA_BITS - (`smem0B+2*i-1)*`DATA_W-1 -: `DATA_W]),
		   
		   // Configuration data 
		   .config_bits(config_reg_shadow[`MEM0A_CONFIG_OFFSET-2*i*`MEMP_CONFIG_BITS-1 -: 2*`MEMP_CONFIG_BITS])
		   // Needs multiply 2 times (MEMP_CONFIG_BITS) because each memory has 2 ports (A and B)
		   );
      end
   endgenerate
   
   // Instantiate the ALUs
   genvar k;
   generate
      for (k=0; k < `Nalu; k=k+1) begin : add_array
	 xalu alu (
		   .clk(clk),
		   .rst(run_bit_reg & rw_data_to_wr_reg[`mem_idx - 2*`Nmem - k - 1]),

		   // Controller interface
		   .rw_req(fu_regf_req[`N - `salu0 - k]),
		   .rw_rnw(rw_rnw_reg),
		   .rw_data_to_wr(rw_data_to_wr_reg),

		   // Data IO
		   .data_in_bus(data_bus),
		   .alu_result(data_bus[`DATA_BITS - (`salu0 + k - 1)*`DATA_W - 1 -: `DATA_W]),
		   
		   // Configuration data
		   .configdata(config_reg_shadow[`ALU0_CONFIG_OFFSET - k*`ALU_CONFIG_BITS - 1 -: `ALU_CONFIG_BITS])
		   );
      end
   endgenerate
   
   // Instantiate the ALUs lite
   genvar l;
   generate
      for (l=0; l < `Nalu_lite; l=l+1) begin : add_lite_array
	 xalu_lite alu_lite (
			     .clk(clk),
			     .rst(run_bit_reg & rw_data_to_wr_reg[`mem_idx - 2*`Nmem - `Nalu - l - 1]),

			     // Controller interface
			     .rw_req(fu_regf_req[`N - `salulite0 - l]),
			     .rw_rnw(rw_rnw_reg),
			     .rw_data_to_wr(rw_data_to_wr_reg),

			     // Data IO
			     .data_in_bus(data_bus),
			     .alu_result(data_bus[`DATA_BITS - (`salulite0 + l - 1)*`DATA_W - 1 -: `DATA_W]),

			     // Configuration data
			     .configdata(config_reg_shadow[`ALU_LITE0_CONFIG_OFFSET - l*`ALU_LITE_CONFIG_BITS - 1 -: `ALU_LITE_CONFIG_BITS])
			     );
      end
   endgenerate
   
   // Instantiate the multipliers
   genvar m;
   generate
      for (m=0; m < `Nmul; m=m+1) begin : mult_array
	 xmult mult (
		     .clk(clk),
		     .rst(run_bit_reg & rw_data_to_wr_reg[`mem_idx - 2*`Nmem - `Nalu -`Nalu_lite - m - 1]),

		     // Controller interface
		     .rw_req(fu_regf_req[`N - `smul0 - m]),
		     .rw_rnw(rw_rnw_reg),
		     .rw_data_to_wr(rw_data_to_wr_reg),

		     // Data IO
		     .data_in_bus(data_bus),
		     .product(data_bus[`DATA_BITS - (`smul0 + m - 1)*`DATA_W - 1 -: `DATA_W]),
		     
		     // Configuration data
		     .configdata(config_reg_shadow[`MULT0_CONFIG_OFFSET - m*`MULT_CONFIG_BITS - 1 -: `MULT_CONFIG_BITS])
		     );
      end
   endgenerate
   
   // Instantiate the barrel shifters
   genvar n;
   generate
      for (n=0; n < `Nbs; n=n+1) begin : bs_array
	 xbs bs (
		 .clk(clk),
		 .rst(run_bit_reg & rw_data_to_wr_reg[`mem_idx - 2*`Nmem - `Nalu -`Nalu_lite - `Nmul - n - 1]),

		 // Controller interface
		 .rw_req(fu_regf_req[`N - `sbs0 - n]),
		 .rw_rnw(rw_rnw_reg),
		 .rw_data_to_wr(rw_data_to_wr_reg),

		 // Data IO
		 .data_in_bus(data_bus),
		 .data_out(data_bus[`DATA_BITS - (`sbs0 + n - 1)*`DATA_W - 1 -: `DATA_W]),
		 
		 // Configuration data
		 .configdata(config_reg_shadow[`BS0_CONFIG_OFFSET - n*`BS_CONFIG_BITS - 1 -: `BS_CONFIG_BITS])
		 );
      end
   endgenerate

endmodule
