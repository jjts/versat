/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: testbench to load the program and data for executing a simple 
               vector addition kernel and read back the result via parallel 
               interface.
 
               The testbench plays the role of a host CPU that has Versat as a 
               guest co-processor. Host and guest communicate by acessign the 
               same 16x32 register file R0 to R15.
                
               Registers used:
 
               R0: Address Register. Used by the host to place start memory 
                   addresses for reading writing or executing. Guest starts 
                   execution whenever this register is non zero.
 
               R14: Command Register. Used by the host to issue commands to the 
                    guest; used by the guest to indicate completion of the 
                    commands received
 
               R15: Data Register. Used by the host to place the data words to
                    write to the guest memory; used by the guest to place the data 
                    words requested by the host for reading
 
 
               Host commands:
 
               WRITE: host tells guest it wants to write the word placed in R15
                      to the memory position currently held in register B.
 
               READ: host tells guest it wants to read the word memory position 
                     currently held in register B, which should be placed in R15
                     by guest.
 
               FINISH: host tells guest it has no more words to read or write.
 
               RUN: host tells guest it wants to run the program starting at the 
                    address currently in R0
  
 
               Guest response:
 
               ACK: guest tells host it has performed the READ or WRITE commands.
 
 

   Copyright (C) 2014 Authors

  Author(s): Joao Dias Lopes <joao.d.lopes91@gmail.com>
             Jose T. de Sousa <jose.t.de.sousa@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps

`include "xdefs.v"
`include "xmem_map.v"


module xtop_dma_tb;

   //parameters 
   parameter clk_period = 40;

   //sizes
   parameter IMEM_SIZE = 2**`IADDR_W;
   parameter DMEM_SIZE = 2**`DADDR_W;
   
   reg clk;
   reg rst;
   
   //parallel interface
   reg 	[`CTRL_REGF_ADDR_W-1:0] par_addr;
   reg 				par_we;
   reg [`DATA_W-1:0] 		par_in;
   wire [`DATA_W-1:0] 		par_out;
   reg [`DATA_W-1:0] 		data_addr;
   
   //dma interface 
   reg 				dma_req;
   reg 				dma_rnw;
   reg [`INT_ADDR_W-1:0] 	dma_addr;
   reg [`DATA_W-1:0] 		dma_data_in;
   wire [`DATA_W-1:0] 		dma_data_out;
   

   reg [80*8:1] mystring;

   wire 	ctr_timer_en;
   integer 	i, k, z, fp, start_time, ctr_timer;
   
   // Data Bank
   reg [`INSTR_W-1:0] prog [IMEM_SIZE-1:0];
   reg [`DATA_W-1:0] data [DMEM_SIZE-1:0];

   
   // Instantiate the Unit Under Test (UUT)
   xtop uut (
	     .clk(clk), 
	     .rst(rst),
	     // parallel interface
	     .par_addr(par_addr),
	     .par_we(par_we),
	     .par_in(par_in),
	     .par_out(par_out),

	     .dma_req(dma_req),
	     .dma_rnw(dma_rnw),
	     .dma_addr(dma_addr),
	     .dma_data_in(dma_data_in),
	     .dma_data_out(dma_data_out)
	     );
   
   initial begin
      
`ifdef DEBUG
      $dumpfile("xtop_dma.vcd");
      $dumpvars();
`endif
      
      $readmemh("opcode.hex", prog, 0, (IMEM_SIZE-1));

      dma_req = 0;
      dma_rnw = 1;
      dma_addr = 0;
      dma_data_in = 0;
      
      // Global reset of FPGA
      #100
 
      // Initialize Inputs
      clk = 0;
      rst = 0;

      //parallel interface
      par_addr = 0;
      par_we = 0;
      par_in = 0;
      
      // Global reset
      #(clk_period+1)
      rst = 1;

      #clk_period
      rst = 0;
      
      #clk_period;
      
      //
      //load program
      //
      
      dma_req = 1;
      dma_rnw = 0;
      dma_addr = `INT_ADDR_W'h800;
      
      for (i=0; i<IMEM_SIZE; i = i+1) begin 
	 
	 dma_data_in = {{(`DATA_W-`INSTR_W){1'b0}},prog[i]};
	 #clk_period;
	 
	 dma_addr = dma_addr + 1;
      end
      
      dma_req = 0;
      dma_rnw = 1;
      
      #(2*clk_period);
      
      //
      //load data
      //
      
      dma_addr = `ENG_MEM_BASE;
      
      for (k = 0; k < `Nmem; k=k+1) begin
	 $sformat(mystring, "./xmem%1ddata.hex", k);
	 
	 $readmemh(mystring, data, 0, (DMEM_SIZE-1));
	 
	 dma_req = 1;
	 dma_rnw = 0;
	 
	 for (i=0; i<DMEM_SIZE; i = i+1) begin 
	    
	    dma_data_in = data[i];
	    #clk_period;

	    if(i < (DMEM_SIZE-1))
	      dma_addr = dma_addr + 1;
	 end
	 
	 dma_addr = dma_addr + 1;
	 dma_req = 0;
	 dma_rnw = 1;
	 
	 #(2*clk_period);
	 
      end
      
      //
      //run program
      //    
      #clk_period par_we = 1;
      par_addr = 14;
      par_in = `BR_RUN;
      
      #clk_period par_addr = 0;
      par_in = `PROG_MEM_BASE;
      
      start_time = $time;
      
      //wait for versat to return to bootrom 
      #clk_period par_we = 0;
      par_addr = 0;
      while(par_out != 0) #clk_period;
      
      $display("Total Clocks: %0d",($time-start_time)/clk_period);
      $display("Controller Clocks: %0d", ctr_timer);
      
      //
      //read data
      //
      
      dma_addr = `ENG_MEM_BASE;

      fp = $fopen("./xtop.out","w");
      
      for (k = 0; k < `Nmem; k=k+1) begin
	 dma_req = 1;
	 
	 for (i=0; i<(DMEM_SIZE + 2); i = i+1) begin 

	    #(clk_period/2);
	    
	    if (i > 1)
	      data[i-2] = dma_data_out;

	    #(clk_period/2);

	    if (i < DMEM_SIZE)
	      dma_addr = dma_addr + 1;

	    if(i >= (DMEM_SIZE-1))
	      dma_req = 0;
	 end

	 #(2*clk_period);
	 
	 for (z=0; z < DMEM_SIZE; z=z+1)
	   $fwrite(fp,"%8h\n",data[z]);
	 
	 $fwrite(fp,"\n");
      end
      
      $fclose(fp);
      
      // Simulation time
      #(10000) $finish;
   end

   //Measure unhidden controller time
   assign ctr_timer_en = (& uut.data_eng.mem_done) & (uut.pc >= ({1'b0,`ROM_BASE} + 2**`ROM_ADDR_W)) & uut.div_wrapper.ready;

   always @ (posedge clk) begin
      if(rst == 1)
	ctr_timer = 0;
      else if (ctr_timer_en)
	ctr_timer = ctr_timer+1;
   end
     
     
   always 
     #(clk_period/2) clk = ~clk;

   wire [`DATA_W-1:0]     r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15;
   wire [`DATA_W-1:0]     regA, regB, regC;
   wire [`IADDR_W:0] 	  pc;
   wire [`INSTR_W-1:0]    instr;
   wire [`INT_ADDR_W-1:0] rw_addr;
   wire 		  rw_req, rw_rnw;
   
   
   assign pc = uut.pc;
   assign instr = uut.instruction;
   assign regA = uut.controller.regA;
   assign regB = uut.controller.regB;
   assign regC = uut.controller.cs;
   assign rw_addr = uut.controller.rw_addr;
   assign rw_req = uut.controller.rw_req;
   assign rw_rnw = uut.controller.rw_rnw;

   assign r0 = uut.ctrl_regf.ctrl_reg_1[0];
   assign r1 = uut.ctrl_regf.ctrl_reg_1[1];
   assign r2 = uut.ctrl_regf.ctrl_reg_1[2];
   assign r3 = uut.ctrl_regf.ctrl_reg_1[3];
   assign r4 = uut.ctrl_regf.ctrl_reg_1[4];
   assign r5 = uut.ctrl_regf.ctrl_reg_1[5];
   assign r6 = uut.ctrl_regf.ctrl_reg_1[6];
   assign r7 = uut.ctrl_regf.ctrl_reg_1[7];
   assign r8 = uut.ctrl_regf.ctrl_reg_1[8];
   assign r9 = uut.ctrl_regf.ctrl_reg_1[9];
   assign r10 = uut.ctrl_regf.ctrl_reg_1[10];
   assign r11 = uut.ctrl_regf.ctrl_reg_1[11];
   assign r12 = uut.ctrl_regf.ctrl_reg_1[12];
   assign r13 = uut.ctrl_regf.ctrl_reg_1[13];
   assign r14 = uut.ctrl_regf.ctrl_reg_1[14];
   assign r15 = uut.ctrl_regf.ctrl_reg_1[15];
   
      	 
endmodule
