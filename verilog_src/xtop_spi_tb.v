/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Joao Dias Lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps

`include "xdefs.v"
`include "xmem_map.v"

`define ADDR_SPI_W 4 //Register width (2**4 = 16 registers, xctr_regf - change here)

module xtop_spi_tb;
   
   //parameters 
   parameter clk_period = 40;
   parameter sclk_period = clk_period*5;
   parameter prog_start = `PROG_MEM_BASE;
   parameter NSCLKS = 2*(`DATA_W+`ADDR_SPI_W+3); //Number of sclks that spi comunication needs to send/receive one word
   parameter NBITS = (`DATA_W+`ADDR_SPI_W+1); //Number of DATA_W bits + ADDR_SPI_W bits + rw_rnw bit
   parameter MEM_SIZE = 2**`DADDR_W; //Number of words that a data memory can have at maximum
   
   `include "./spi_tasks.v"
   
   // Inputs
   reg clk;
   reg rst;
   reg sclk;
   reg ss;
   reg mosi;
   
   // Outputs
   wire miso;
   
   // Memory Bank input
   reg [`DATA_W-1:0] mem [MEM_SIZE-1:0];

   // Memory Bank output
   reg [`DATA_W-1:0] data [MEM_SIZE-1:0];

   reg [`DATA_W-1:0] data_addr;

   reg [`DATA_W-1:0] data_output;

   reg [80*8:1] fileName;
   
   integer 	     k, fp, z;
   
   
   // Instantiate the Unit Under Test (UUT)
   xtop uut (
	     .clk(clk), 
	     .rst(rst),
	     
	     // SPI slave interface
	     .sclk(sclk), 
	     .ss(ss), 
	     .mosi(mosi), 
	     .miso(miso)
	     
	     );
   
   initial begin
      // Global reset of FPGA
      #100
      
`ifdef DEBUG
      $dumpfile("xtop_spi.vcd");
      $dumpvars(2,xtop_spi_tb);
`endif
      
      $readmemh("opcode.hex", mem, 0, (MEM_SIZE-1));
      
      // Initialize Inputs
      clk = 1;
      sclk = 1;
      rst = 0;
      ss = 1;
      
      // Global reset
      #(clk_period+1)
      rst = 1;

      #clk_period
      rst = 0;
      
      // Send Program by SPI
      #clk_period;
      #(sclk_period/2)
      
      send_file_spi(MEM_SIZE,prog_start);
      
      // Send Data by SPI
      // Initialize all memories
      data_addr = `ENG_MEM_BASE;
      
      for (k = 0; k < `Nmem; k=k+1) begin
	 $sformat(fileName, "./xmem%1ddata.hex", k);
	 
	 $readmemh(fileName, mem, 0, (MEM_SIZE-1));
	 
	 send_file_spi(MEM_SIZE,data_addr);
	 
	 data_addr = data_addr + MEM_SIZE; // switch memory
      end
      
      // Send run command to versat by SPI
      send_command_spi(`PROG_MEM_BASE,`BR_RUN);
      
      // Receive Result by SPI and print to a file
      data_addr = `ENG_MEM_BASE;
      
      fp = $fopen("./xtop.out","w");
      
      for (k = 0; k < `Nmem; k=k+1) begin
	 receive_file_spi(MEM_SIZE,data_addr);
	 
	 for (z=0; z < MEM_SIZE; z=z+1)
	   $fwrite(fp,"%8h\n",data[z]);
	 
	 $fwrite(fp,"\n");
	 
	 data_addr = data_addr + MEM_SIZE; // switch memory
      end
      
      $fclose(fp);
      
      // Simulation time
      #(1000) $finish;
   end
	
   always 
     #(clk_period/2) clk = ~clk;

   always 
     #(sclk_period/2) sclk = ~sclk;
      	 
endmodule
