/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: testbench to create the Versat dictionary
   Copyright (C) 2016 Authors

  Author(s): 
             Jose T. de Sousa <jose.t.de.sousa@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps

`include "xdefs.v"
`include "xmem_map.v"
`include "xaludefs.v"

module xdict_tb;
   
   integer fp;
   
   initial begin
      
      fp = $fopen("xdict.txt","w");
      
      $fwrite(fp,"{\n");
      
      $fwrite(fp, "\"R0\":%d,\n", `R0);
      $fwrite(fp, "\"R1\":%d,\n", `R1);
      $fwrite(fp, "\"R2\":%d,\n", `R2);
      $fwrite(fp, "\"R3\":%d,\n", `R3);
      $fwrite(fp, "\"R4\":%d,\n", `R4);
      $fwrite(fp, "\"R5\":%d,\n", `R5);
      $fwrite(fp, "\"R6\":%d,\n", `R6);
      $fwrite(fp, "\"R7\":%d,\n", `R7);
      $fwrite(fp, "\"R8\":%d,\n", `R8);
      $fwrite(fp, "\"R9\":%d,\n", `R9);
      $fwrite(fp, "\"R10\":%d,\n", `R10);
      $fwrite(fp, "\"R11\":%d,\n", `R11);
      $fwrite(fp, "\"R12\":%d,\n", `R12);
      $fwrite(fp, "\"R13\":%d,\n", `R13);
      $fwrite(fp, "\"R14\":%d,\n", `R14);
      $fwrite(fp, "\"R15\":%d,\n", `R15);
      
      $fwrite(fp, "\"BR_WRITE\":%d,\n", `BR_WRITE);
      $fwrite(fp, "\"BR_RUN\":%d,\n", `BR_RUN);
      $fwrite(fp, "\"BR_DONE\":%d,\n", `BR_DONE);
      $fwrite(fp, "\"BR_ACK\":%d,\n", `BR_ACK);
      $fwrite(fp, "\"BR_READ\":%d,\n", `BR_READ);
      
      $fwrite(fp, "\"DMA_EXT_ADDR_ADDR\":%d,\n", `DMA_EXT_ADDR_ADDR);
      $fwrite(fp, "\"DMA_INT_ADDR_ADDR\":%d,\n", `DMA_INT_ADDR_ADDR);
      $fwrite(fp, "\"DMA_SIZE_ADDR\":%d,\n", `DMA_SIZE_ADDR);
      $fwrite(fp, "\"DMA_DIRECTION_ADDR\":%d,\n", `DMA_DIRECTION_ADDR);
      $fwrite(fp, "\"DMA_START_ADDR\":%d,\n", `DMA_START_ADDR);
      $fwrite(fp, "\"DMA_STATUS_ADDR\":%d,\n", `DMA_STATUS_ADDR);
      
      $fwrite(fp, "\"DIV_START_ADDR\":%d,\n", `DIV_START_ADDR);
      $fwrite(fp, "\"DIV_READY_ADDR\":%d,\n", `DIV_READY_ADDR);
      $fwrite(fp, "\"DIV_DIVIDEND_ADDR\":%d,\n", `DIV_DIVIDEND_ADDR);
      $fwrite(fp, "\"DIV_DIVISOR_ADDR\":%d,\n", `DIV_DIVISOR_ADDR);
      $fwrite(fp, "\"DIV_QUOTIENT_ADDR\":%d,\n", `DIV_QUOTIENT_ADDR);
      $fwrite(fp, "\"DIV_REMAINDER_ADDR\":%d,\n", `DIV_REMAINDER_ADDR);
      $fwrite(fp, "\"DIV_TYPE_ADDR\":%d,\n", `DIV_TYPE_ADDR);
      
      $fwrite(fp, "\"ROM_BASE\":%d,\n", `ROM_BASE);
      $fwrite(fp, "\"PROG_MEM_BASE\":%d,\n", `PROG_MEM_BASE);
      
      $fwrite(fp, "\"CONF_MEM_BASE\":%d,\n", `CONF_MEM_BASE);
      $fwrite(fp, "\"CLEAR_CONFIG_ADDR\":%d,\n", `CLEAR_CONFIG_ADDR);
      
      $fwrite(fp, "\"ENG_CTRL_REG\":%d,\n", `ENG_CTRL_REG);
      $fwrite(fp, "\"ENG_STATUS_REG\":%d,\n", `ENG_STATUS_REG);
      
      $fwrite(fp, "\"ENG_MEM0\":%d,\n", `ENG_MEM0);
      $fwrite(fp, "\"ENG_MEM1\":%d,\n", `ENG_MEM1);
      $fwrite(fp, "\"ENG_MEM2\":%d,\n", `ENG_MEM2);
      $fwrite(fp, "\"ENG_MEM3\":%d,\n", `ENG_MEM3);
      
      $fwrite(fp, "\"ENG_FU_BASE\":%d,\n", `ENG_FU_BASE);
      
      $fwrite(fp, "\"MEM0A\":%d,\n", `MEM0A);
      $fwrite(fp, "\"MEM0B\":%d,\n", `MEM0B);
      $fwrite(fp, "\"MEM1A\":%d,\n", `MEM1A);
      $fwrite(fp, "\"MEM1B\":%d,\n", `MEM1B);
      $fwrite(fp, "\"MEM2A\":%d,\n", `MEM2A);
      $fwrite(fp, "\"MEM2B\":%d,\n", `MEM2B);
      $fwrite(fp, "\"MEM3A\":%d,\n", `MEM3A);
      $fwrite(fp, "\"MEM3B\":%d,\n", `MEM3B);
      
      $fwrite(fp, "\"ALU0\":%d,\n", `ALU0);
      $fwrite(fp, "\"ALU1\":%d,\n", `ALU1);
      $fwrite(fp, "\"ALU_LITE0\":%d,\n", `ALULITE0);
      $fwrite(fp, "\"ALU_LITE1\":%d,\n", `ALULITE1);
      $fwrite(fp, "\"ALU_LITE2\":%d,\n", `ALULITE2);
      $fwrite(fp, "\"ALU_LITE3\":%d,\n", `ALULITE3);
      
      $fwrite(fp, "\"MULT0\":%d,\n", `MULT0);      
      $fwrite(fp, "\"MULT1\":%d,\n", `MULT1);      
      $fwrite(fp, "\"MULT2\":%d,\n", `MULT2);      
      $fwrite(fp, "\"MULT3\":%d,\n", `MULT3);      
      $fwrite(fp, "\"BS0\":%d,\n", `BS0);      
      $fwrite(fp, "\"ENG_MEM_BASE\":%d,\n", `ENG_MEM_BASE);
      
      $fwrite(fp, "\"MEM_CONF_ITER_OFFSET\":%d,\n", `MEM_CONF_ITER_OFFSET);
      $fwrite(fp, "\"MEM_CONF_PER_OFFSET\":%d,\n", `MEM_CONF_PER_OFFSET);
      $fwrite(fp, "\"MEM_CONF_DUTY_OFFSET\":%d,\n", `MEM_CONF_DUTY_OFFSET);
      $fwrite(fp, "\"MEM_CONF_SELA_OFFSET\":%d,\n", `MEM_CONF_SELA_OFFSET);
      $fwrite(fp, "\"MEM_CONF_START_OFFSET\":%d,\n", `MEM_CONF_START_OFFSET);
      $fwrite(fp, "\"MEM_CONF_SHIFT_OFFSET\":%d,\n", `MEM_CONF_SHIFT_OFFSET);
      $fwrite(fp, "\"MEM_CONF_INCR_OFFSET\":%d,\n", `MEM_CONF_INCR_OFFSET);
      $fwrite(fp, "\"MEM_CONF_DELAY_OFFSET\":%d,\n", `MEM_CONF_DELAY_OFFSET);
      $fwrite(fp, "\"MEM_CONF_RVRS_OFFSET\":%d,\n", `MEM_CONF_RVRS_OFFSET);
      $fwrite(fp, "\"MEM_CONF_OFFSET\":%d,\n", `MEM_CONF_OFFSET);
      $fwrite(fp, "\"MEM_CONF_SHIFT_OFFSET\":%d,\n", `MEM_CONF_SHIFT_OFFSET);
      $fwrite(fp, "\"MEM_CONF_INCR_OFFSET\":%d,\n", `MEM_CONF_INCR_OFFSET);
      $fwrite(fp, "\"MEM_CONF_DELAY_OFFSET\":%d,\n", `MEM_CONF_DELAY_OFFSET);
      $fwrite(fp, "\"MEM_CONF_RVRS_OFFSET\":%d,\n", `MEM_CONF_RVRS_OFFSET);
      $fwrite(fp, "\"MEM_CONF_EXT_OFFSET\":%d,\n", `MEM_CONF_EXT_OFFSET);
      $fwrite(fp, "\"MEM_CONF_OFFSET\":%d,\n", `MEM_CONF_OFFSET);
      $fwrite(fp, "\"ALU_CONF_SELA_OFFSET\":%d,\n", `ALU_CONF_SELA_OFFSET);
      $fwrite(fp, "\"ALU_CONF_SELB_OFFSET\":%d,\n", `ALU_CONF_SELB_OFFSET);
      $fwrite(fp, "\"ALU_CONF_FNS_OFFSET\":%d,\n", `ALU_CONF_FNS_OFFSET);
      $fwrite(fp, "\"ALU_CONF_OFFSET\":%d,\n", `ALU_CONF_OFFSET);
      $fwrite(fp, "\"ALU_LITE_CONF_SELA_OFFSET\":%d,\n", `ALU_LITE_CONF_SELA_OFFSET);
      $fwrite(fp, "\"ALU_LITE_CONF_SELB_OFFSET\":%d,\n", `ALU_LITE_CONF_SELB_OFFSET);
      $fwrite(fp, "\"ALU_LITE_CONF_FNS_OFFSET\":%d,\n", `ALU_LITE_CONF_FNS_OFFSET);
      $fwrite(fp, "\"ALU_LITE_CONF_OFFSET\":%d,\n", `ALU_LITE_CONF_OFFSET);
      $fwrite(fp, "\"MUL_CONF_SELA_OFFSET\":%d,\n", `MUL_CONF_SELA_OFFSET);
      $fwrite(fp, "\"MUL_CONF_SELB_OFFSET\":%d,\n", `MUL_CONF_SELB_OFFSET);
      $fwrite(fp, "\"MUL_CONF_LONHI_OFFSET\":%d,\n", `MUL_CONF_LONHI_OFFSET);
      $fwrite(fp, "\"MUL_CONF_DIV2_OFFSET\":%d,\n", `MUL_CONF_DIV2_OFFSET);
      $fwrite(fp, "\"MUL_CONF_OFFSET\":%d,\n", `MUL_CONF_OFFSET);
      $fwrite(fp, "\"BS_CONF_SELA_OFFSET\":%d,\n", `BS_CONF_SELA_OFFSET);
      $fwrite(fp, "\"BS_CONF_SELB_OFFSET\":%d,\n", `BS_CONF_SELB_OFFSET);
      $fwrite(fp, "\"BS_CONF_LNA_OFFSET\":%d,\n", `BS_CONF_LNA_OFFSET);
      $fwrite(fp, "\"BS_CONF_LNR_OFFSET\":%d,\n", `BS_CONF_LNR_OFFSET);
      $fwrite(fp, "\"BS_CONF_OFFSET\":%d,\n", `BS_CONF_OFFSET);
      
      $fwrite(fp, "\"MEM0A_CONFIG_ADDR\":%d,\n", `MEM0A_CONFIG_ADDR);
      $fwrite(fp, "\"MEM0B_CONFIG_ADDR\":%d,\n", `MEM0B_CONFIG_ADDR);
      $fwrite(fp, "\"MEM1A_CONFIG_ADDR\":%d,\n", `MEM1A_CONFIG_ADDR);
      $fwrite(fp, "\"MEM1B_CONFIG_ADDR\":%d,\n", `MEM1B_CONFIG_ADDR);
      $fwrite(fp, "\"MEM2A_CONFIG_ADDR\":%d,\n", `MEM2A_CONFIG_ADDR);
      $fwrite(fp, "\"MEM2B_CONFIG_ADDR\":%d,\n", `MEM2B_CONFIG_ADDR);
      $fwrite(fp, "\"MEM3A_CONFIG_ADDR\":%d,\n", `MEM3A_CONFIG_ADDR);
      $fwrite(fp, "\"MEM3B_CONFIG_ADDR\":%d,\n", `MEM3B_CONFIG_ADDR);
      $fwrite(fp, "\"ALU0_CONFIG_ADDR\":%d,\n", `ALU0_CONFIG_ADDR);
      $fwrite(fp, "\"ALU1_CONFIG_ADDR\":%d,\n", `ALU1_CONFIG_ADDR);
      $fwrite(fp, "\"ALU_LITE0_CONFIG_ADDR\":%d,\n", `ALU_LITE0_CONFIG_ADDR);
      $fwrite(fp, "\"ALU_LITE1_CONFIG_ADDR\":%d,\n", `ALU_LITE1_CONFIG_ADDR);
      $fwrite(fp, "\"ALU_LITE2_CONFIG_ADDR\":%d,\n", `ALU_LITE2_CONFIG_ADDR);
      $fwrite(fp, "\"ALU_LITE3_CONFIG_ADDR\":%d,\n", `ALU_LITE3_CONFIG_ADDR);
      $fwrite(fp, "\"MULT0_CONFIG_ADDR\":%d,\n", `MULT0_CONFIG_ADDR);
      $fwrite(fp, "\"MULT1_CONFIG_ADDR\":%d,\n", `MULT1_CONFIG_ADDR);
      $fwrite(fp, "\"MULT2_CONFIG_ADDR\":%d,\n", `MULT2_CONFIG_ADDR);
      $fwrite(fp, "\"MULT3_CONFIG_ADDR\":%d,\n", `MULT3_CONFIG_ADDR);
      $fwrite(fp, "\"BS0_CONFIG_ADDR\":%d,\n", `BS0_CONFIG_ADDR);
      
      $fwrite(fp, "\"sdisabled\":%d,\n", `sdisabled);
      $fwrite(fp, "\"s0\":%d,\n", `s0);
      $fwrite(fp, "\"s1\":%d,\n", `s1);
      $fwrite(fp, "\"sdata_in\":%d,\n", `sdata_in);
      $fwrite(fp, "\"smem0A\":%d,\n", `smem0A);
      $fwrite(fp, "\"smem0B\":%d,\n", `smem0B);
      $fwrite(fp, "\"smem1A\":%d,\n", `smem1A);
      $fwrite(fp, "\"smem1B\":%d,\n", `smem1B);
      $fwrite(fp, "\"smem2A\":%d,\n", `smem2A);
      $fwrite(fp, "\"smem2B\":%d,\n", `smem2B);
      $fwrite(fp, "\"smem3A\":%d,\n", `smem3A);
      $fwrite(fp, "\"smem3B\":%d,\n", `smem3B);
      $fwrite(fp, "\"salu0\":%d,\n", `salu0);
      $fwrite(fp, "\"salu1\":%d,\n", `salu1);
      $fwrite(fp, "\"salulite0\":%d,\n", `salulite0);
      $fwrite(fp, "\"salulite1\":%d,\n", `salulite1);
      $fwrite(fp, "\"salulite2\":%d,\n", `salulite2);
      $fwrite(fp, "\"salulite3\":%d,\n", `salulite3);
      $fwrite(fp, "\"smul0\":%d,\n", `smul0);
      $fwrite(fp, "\"smul1\":%d,\n", `smul1);
      $fwrite(fp, "\"smul2\":%d,\n", `smul2);
      $fwrite(fp, "\"smul3\":%d,\n", `smul3);
      $fwrite(fp, "\"sbs0\":%d,\n", `sbs0);
      $fwrite(fp, "\"saddr\":%d,\n", `saddr);
      
      $fwrite(fp, "\"ALU_LOGIC_OR\":%d,\n", `ALU_LOGIC_OR);
      $fwrite(fp, "\"ALU_LOGIC_AND\":%d,\n", `ALU_LOGIC_AND);
      $fwrite(fp, "\"ALU_CMP_SIG\":%d,\n", `ALU_CMP_SIG);
      $fwrite(fp, "\"ALU_LOGIC_XOR\":%d,\n", `ALU_LOGIC_XOR);
      $fwrite(fp, "\"ALU_ADD\":%d,\n", `ALU_ADD);
      $fwrite(fp, "\"ALU_SUB\":%d,\n", `ALU_SUB);
      $fwrite(fp, "\"ALU_SEXT8\":%d,\n", `ALU_SEXT8);
      $fwrite(fp, "\"ALU_SEXT16\":%d,\n", `ALU_SEXT16);
      $fwrite(fp, "\"ALU_SHIFTR_ARTH\":%d,\n", `ALU_SHIFTR_ARTH);
      $fwrite(fp, "\"ALU_SHIFTR_LOG\":%d,\n", `ALU_SHIFTR_LOG);
      $fwrite(fp, "\"ALU_CMP_UNS\":%d,\n", `ALU_CMP_UNS);
      $fwrite(fp, "\"ALU_MUX\":%d,\n", `ALU_MUX);
      $fwrite(fp, "\"ALU_CLZ\":%d,\n", `ALU_CLZ);
      $fwrite(fp, "\"ALU_MAX\":%d,\n", `ALU_MAX);
      $fwrite(fp, "\"ALU_MIN\":%d,\n", `ALU_MIN);
      $fwrite(fp, "\"ALU_ABS\":%d,\n", `ALU_ABS);
      
      $fwrite(fp, "\"IMM_W\":%d,\n", `IMM_W);
      
      $fwrite(fp, "\"INSTR_W\":%d,\n", `INSTR_W);
      $fwrite(fp, "\"IADDR_W\":%d,\n", `IADDR_W);
      
      $fwrite(fp, "\"ROM_ADDR_W\":%d,\n", `ROM_ADDR_W);
      
      $fwrite(fp, "\"DELAY_SLOTS\":%d,\n", `DELAY_SLOTS);
      
      $fwrite(fp,"}\n"); 
      
      $fclose(fp);
   end
   
endmodule
