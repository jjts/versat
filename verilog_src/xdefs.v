/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: architecture parameters

 Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
            Joao Dias lopes <joao.d.lopes91@gmail.com>
            Andre Lopes <andre.a.lopes@netcabo.pt>
            Francisco Nunes <ftcnunes@gmail.com>

***************************************************************************** */

// Uncomment this line to use ASIC memories
//`define ASIC

// Uncomment this line to use Xilinx memories
//`define XILINX

// Uncomment this line to use Altera memories
//`define ALTERA

// Uncomment this line to use hard ROM
//`define ROM

// Uncomment this line to use AXI interface
//`define AXI_IF

// Uncomment this line to use DMA interface
//`define DMA_IF

// Uncomment this line to use Parallel interface
//`define PAR_IF

// Uncomment this line to use FPGA
//`define SPI_IF

// Data width
`define DATA_W 32 // bits

// Boot ROM data memory address width
`define ROM_ADDR_W 8 // 2**8 = 256 boot instructions

// 2**15 = 32k internal addresses; see memory map
`define INT_ADDR_W 15

// Data memory address width
`define DADDR_W 11 // 2**11 * 4 = 8kB each mem, 32kB for 4 data mems

// Instruction memory address width
`define IADDR_W 11 // 2**11 = 2048 instructions

// Control reg file
`define CTRL_REGF_ADDR_W 4

// Number of functional units
`define Nmem      4
`define Nalu      2
`define Nalu_lite 4
`define Nmul      4
`define Nbs       1
`define N_W       5 // 2**5 = 32: max number of inmux inputs
`define N         (2*`Nmem + `Nalu + `Nalu_lite + `Nmul + `Nbs + 3) // actual number of inmux inputs; 
// total = 22; note the 3 special inputs: 0, 1, data_in

`define DATA_BITS (`N *`DATA_W) //internal data bus

// Data bus selection codes
`define sdisabled 0
`define s0 	  (`sdisabled + 1) //1
`define s1     	  (`s0 + 1)        //2
`define sdata_in  (`s1 + 1)        //3
`define smem0A 	  (`sdata_in + 1)  //4
`define smem0B    (`smem0A + 1)    //5
`define smem1A 	  (`smem0B + 1)    //6
`define smem1B	  (`smem1A + 1)    //7
`define smem2A 	  (`smem1B + 1)    //8
`define smem2B	  (`smem2A + 1)    //9
`define smem3A 	  (`smem2B + 1)    //10
`define smem3B	  (`smem3A + 1)    //11
`define salu0 	  (`smem3B + 1)    //12
`define salu1 	  (`salu0 + 1)     //13
`define salulite0 (`salu1 + 1)     //14
`define salulite1 (`salulite0 + 1) //15
`define salulite2 (`salulite1 + 1) //16
`define salulite3 (`salulite2 + 1) //17
`define smul0 	  (`salulite3 + 1) //18
`define smul1 	  (`smul0 + 1)     //19
`define smul2 	  (`smul1 + 1)     //20
`define smul3 	  (`smul2 + 1)     //21
`define sbs0	  (`smul3 + 1)     //22
`define saddr	  31

// Index of init_bit and run_bit mask
`define mem_idx (2*`Nmem + `Nalu + `Nalu_lite + `Nmul + `Nbs + 2) //21

// Configuration memory 
`define CONF_ADDR_W    6 //means 2**6 configurations
`define N_CONF_SLICE_W 5 //means 2**5 = 32 conf mem slices, just 21 needed
`define N_CONF_SLICES  (2**`N_CONF_SLICE_W - 11) //21 slices

`define PERIOD_W 5  //LOG2 of max period and duty cicle

// MEM port config bits: SEL+START+INCR+DELAY+RNW+REVERSE+EXT
// iterations = `DADDR_W
// period = PERIOD_W
// duty = PERIOD_W
// input selection  bits = N_W
// start width = ADDR_W
// shift width = ADDR_W
// incr width = ADDR_W 
// delay width = PERIOD_W 
// reverse = 1 
// ext = 1 
`define MEMP_CONFIG_BITS (`N_W + 4*`DADDR_W + 3*`PERIOD_W + 1 + 1) //66

// ALU config bits:
// input selection  bits = 2 * N_W
`define ALU_FNS_W	4
`define ALU_CONFIG_BITS (2*`N_W + `ALU_FNS_W) //14

// ALU lite config bits:
// input selection  bits = 2 * N_W
`define ALU_LITE_FNS_W	3
`define ALU_LITE_CONFIG_BITS (2*`N_W + `ALU_LITE_FNS_W + 1) //14

// MULT config bits:
// input selection bits = 2 * N_W
// lonhi = 1 bit 
// div2 = 1 bit 
`define MULT_CONFIG_BITS (2*`N_W + 2) //12

// BS config bits:
// input selection bits = 2 * N_W
// lognarith = 1 bit 
// leftnright = 1 bit
`define BS_CONFIG_BITS (2*`N_W + 2) //12

//total config bits 
`define CONFIG_BITS (2*`Nmem*`MEMP_CONFIG_BITS + `Nalu*`ALU_CONFIG_BITS + `Nalu_lite*`ALU_LITE_CONFIG_BITS + `Nmul*`MULT_CONFIG_BITS + `Nbs*`BS_CONFIG_BITS)

`define MEM0A_CONFIG_OFFSET `CONFIG_BITS
`define MEM0B_CONFIG_OFFSET (`MEM0A_CONFIG_OFFSET           - `MEMP_CONFIG_BITS)
`define MEM1A_CONFIG_OFFSET (`MEM0B_CONFIG_OFFSET           - `MEMP_CONFIG_BITS) 
`define MEM1B_CONFIG_OFFSET (`MEM1A_CONFIG_OFFSET           - `MEMP_CONFIG_BITS) 
`define MEM2A_CONFIG_OFFSET (`MEM1B_CONFIG_OFFSET           - `MEMP_CONFIG_BITS)
`define MEM2B_CONFIG_OFFSET (`MEM2A_CONFIG_OFFSET           - `MEMP_CONFIG_BITS)
`define MEM3A_CONFIG_OFFSET (`MEM2B_CONFIG_OFFSET           - `MEMP_CONFIG_BITS)
`define MEM3B_CONFIG_OFFSET (`MEM3A_CONFIG_OFFSET           - `MEMP_CONFIG_BITS)
`define ALU0_CONFIG_OFFSET  (`MEM3B_CONFIG_OFFSET           - `MEMP_CONFIG_BITS)
`define ALU1_CONFIG_OFFSET  (`ALU0_CONFIG_OFFSET            - `ALU_CONFIG_BITS)
`define ALU_LITE0_CONFIG_OFFSET  (`ALU1_CONFIG_OFFSET       - `ALU_CONFIG_BITS)
`define ALU_LITE1_CONFIG_OFFSET  (`ALU_LITE0_CONFIG_OFFSET  - `ALU_LITE_CONFIG_BITS)
`define ALU_LITE2_CONFIG_OFFSET  (`ALU_LITE1_CONFIG_OFFSET  - `ALU_LITE_CONFIG_BITS)
`define ALU_LITE3_CONFIG_OFFSET  (`ALU_LITE2_CONFIG_OFFSET  - `ALU_LITE_CONFIG_BITS)
`define MULT0_CONFIG_OFFSET (`ALU_LITE3_CONFIG_OFFSET       - `ALU_LITE_CONFIG_BITS)
`define MULT1_CONFIG_OFFSET (`MULT0_CONFIG_OFFSET           - `MULT_CONFIG_BITS)
`define MULT2_CONFIG_OFFSET (`MULT1_CONFIG_OFFSET           - `MULT_CONFIG_BITS)
`define MULT3_CONFIG_OFFSET (`MULT2_CONFIG_OFFSET           - `MULT_CONFIG_BITS)
`define BS0_CONFIG_OFFSET   (`MULT3_CONFIG_OFFSET           - `MULT_CONFIG_BITS)

// Boot ROM commands
// Host commands
`define BR_WRITE `DATA_W'h0
`define BR_READ `DATA_W'h4
`define BR_DONE `DATA_W'h2
`define BR_RUN `DATA_W'h1
// Slave response
`define BR_ACK `DATA_W'h3

// Instructions
// Instruction fields
`define OPCODESZ 4
`define INSTR_W 20
`define IMM_W (`INSTR_W-`OPCODESZ) //16 = DATA_W/2

`define DELAY_SLOTS 2 // Assembler info
`define DELAY_INSTR   // Verilog info
