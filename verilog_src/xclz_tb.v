`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   14:08:16 08/06/2014
// Design Name:   xclz
// Module Name:   C:/Dokandre/School_Stuff/Estagio_INESC/versat/verilog_src/xclz_tb.v
// Project Name:  versat_proj
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: xclz
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module xclz_tb;

	// Inputs
	reg [31:0] data_in;

	// Outputs
	wire [5:0] data_out;

	// Instantiate the Unit Under Test (UUT)
	xclz uut (
		.data_in(data_in), 
		.data_out(data_out)
	);
	integer i;

	initial begin
		// Initialize Inputs
		data_in = 32'b10000000000000000000000000000000;

		// Wait 100 ns for global reset to finish
		#100;
        for(i=0; i<32; i=i+1) begin
			data_in <= data_in>>1;
			#100;
		  end
		// Add stimulus here

	end
      
endmodule

