/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: SPI tasks

 Copyright (C) 2014 Authors

 Author(s): Joao Dias lopes <joao.d.lopes91@gmail.com>

 ***************************************************************************** */

//Send data by SPI
task send_data_spi;
   input [`ADDR_SPI_W-1:0] addr;
   input [`DATA_W-1:0] 	   data;
   
   integer 		   j;

   begin
      for (j = 0; j <= NBITS; j=j+1) begin
	    
	 if (j > NBITS-`DATA_W) begin // send data
	    ss = 0;
	    mosi = data[`DATA_W-(j-(NBITS-`DATA_W+1))-1];
	 end else begin // send rw_rnw
	    if (j == 0)
	      mosi = 0;
	    else if (j == NBITS-`DATA_W) begin // bit between address and data
	       ss = 1;
	    end else // send address
	      mosi = addr[`ADDR_SPI_W-j];
	 end

	 #sclk_period;
	 
      end
   end
endtask

//Receive data from SPI
task receive_data_spi;
   input [`ADDR_SPI_W-1:0]  addr;
   output reg [`DATA_W-1:0] data;
   
   integer 		   j;

   begin
      for (j = 0; j <= NBITS; j=j+1) begin
	 
	 if (j > NBITS-`DATA_W) begin // receive data from addr
	    data[`DATA_W-(j-(NBITS-`DATA_W+1))-1] = miso;
	 end else begin
	    if (j == 0) // send rw_rnw
	      mosi = 1;
	    else if (j == NBITS-`DATA_W) begin // bit between address and data
	       ss = 1;
	    end else // send address
	      mosi = addr[`ADDR_SPI_W-j];
	 end
	 
	 #sclk_period;
	 
	 if (j == NBITS-`DATA_W) begin
	    ss = 0;
	    #(sclk_period/2);
	 end
      end

      #(sclk_period/2);
   end
endtask

task send_command_spi;
   input [`DATA_W-1:0] addr_start;
   input [`DATA_W-1:0] command;
   
   reg [`ADDR_SPI_W-1:0] addr;
   reg 			 nop_3;
   reg [`DATA_W-1:0] 	 data_miso;

   begin
      ss = 0;
      
      addr = `ADDR_SPI_W'hE;
      
      // Write command to R14
      send_data_spi(addr,command);
      
      ss = 1;

      #sclk_period
	 
      ss = 0;

      addr = `ADDR_SPI_W'h0;
      
      if (addr_start != `DATA_W'h00000000) begin
	 // Write addr_start to R0
	 send_data_spi(addr,addr_start);
	 
	 ss = 1;
	 
	 #sclk_period
	    
	 ss = 0;
      end
      
      if (command != `BR_READ && command != `BR_WRITE) begin
	 nop_3 = 1'b0;
	 
	 while (nop_3 == 1'b0) begin // wait for R0 = 0
	    receive_data_spi(addr,data_miso);
	    
	    if (data_miso == `DATA_W'h00000000) begin // R0 = 0?
	       nop_3 = 1'b1;
	    end
	    
	    ss = 1;
	    
	    #sclk_period
	    
	    ss = 0;
	 end
      end
      
      ss = 1;
      #sclk_period;
   end
endtask

// Send Program by SPI
task send_file_spi;
   input [`DATA_W-1:0] mem_size;
   input [`DATA_W-1:0] prog_start;
   
   reg [`ADDR_SPI_W-1:0] addr;
   reg 			 nop_3;
   reg [`DATA_W-1:0] 	 data_miso;
   
   integer 		 i;
   
   begin
      ss = 0;
      
      addr = `ADDR_SPI_W'hF;
      
      // Write first instruction to R15
      send_data_spi(addr,mem[0]);
      
      ss = 1;
      
      #sclk_period
      // Send WRITE command to versat
      send_command_spi(prog_start,`BR_WRITE);
      
      ss = 0;
      
      // Send the rest of program instructions
      for (i = 1; i < mem_size; i=i+1) begin
	 ss = 0;
	 
	 addr = `ADDR_SPI_W'hF;
	 
	 // Write instruction to R15
	 send_data_spi(addr,mem[i]);
	 
	 ss = 1;
	 
	 #sclk_period
	 
	 ss = 0;
	 
	 addr = `ADDR_SPI_W'hE;
	 
	 // Send WRITE command to versat
	 send_data_spi(addr,`BR_WRITE);
	 
	 ss = 1;
	 
	 #sclk_period
	 
	 ss = 0;
	 
	 nop_3 = 1'b0;
	 
	 while (nop_3 == 1'b0) begin //wait for ACK
	    receive_data_spi(addr,data_miso);
	    
	    if (data_miso == `BR_ACK) begin // ACK?
	       nop_3 = 1'b1;
	    end
	    
	    ss = 1;

	    #sclk_period
	    
	    ss = 0;
	 end
      end
      
      ss = 1;
      
      #sclk_period
      // Send FINISH command to versat
      send_command_spi(`DATA_W'h00000000,`BR_DONE);
      
   end
endtask

task receive_file_spi;
   input [`DATA_W-1:0]	       num_words;
   input [`DATA_W-1:0] 	       data_start;

   reg [`ADDR_SPI_W-1:0] addr;
   reg 			 nop_3;
   reg [`DATA_W-1:0] 	 data_miso;
   
   integer 		 i;
   
   begin
      ss = 0;
      
      addr = `ADDR_SPI_W'hE;
      
      // Send READ command to versat
      send_command_spi(data_start,`BR_READ);
      
      ss = 0;
      
      // Receive data
      for (i = 0; i < num_words; i=i+1) begin
	 nop_3 = 1'b0;
	 
	 addr = `ADDR_SPI_W'hE;
	 
	 while (nop_3 == 1'b0) begin // wait for R14 be different READ
	    receive_data_spi(addr,data_miso);
	    
	    if (data_miso != `BR_READ) begin // READ?
	       nop_3 = 1'b1;
	    end
	    
	    ss = 1;
	    
	    #sclk_period
	    
	    ss = 0;
	 end
	 
	 addr = `ADDR_SPI_W'hF;
	 
	 // Read data from R15
	 receive_data_spi(addr,data_miso);
	 data[i] = data_miso;
	 
	 ss = 1;
	 
	 #sclk_period
	 
	 ss = 0;
	 
	 addr = `ADDR_SPI_W'hE;
	 
	 // Write 0 to R14 (clear R14)
	 send_data_spi(addr,`DATA_W'h00000000);
	 
	 ss = 1;
	 
	 #sclk_period
	 
	 ss = 0;
      end
      
      ss = 1;
      
      #sclk_period
      // Send FINISH command to versat
      send_command_spi(`DATA_W'h00000000,`BR_DONE);
      
   end
endtask
