`timescale 1ns / 1ps


module fadd_add(
    input  eop,
    input  [23:0] mag_big,
    input  [26:0] mag_small,
    output [27:0] mag_add
    );
    
    wire [27:0] op_big, op_small, op_inv;
    
    assign op_big = {1'b0, mag_big, 3'b000}; 
    assign op_small = {1'b0, mag_small};
    
    assign op_inv = eop ? ~op_small : op_small;
    
    assign mag_add = op_big + op_inv + eop;
    
    
endmodule
