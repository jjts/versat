/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: 

 Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>

 ***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"


module xmem_wrapper2048x32 (
			     //control
			     input 		  clk,

			     //port A
			     input 		  enA,
			     input 		  wrA,
			     input [`DADDR_W-1:0] addrA,
			     input [`DATA_W-1:0]  data_inA,
			     output [`DATA_W-1:0] data_outA,

			     //port B
			     input 		  enB,
			     input 		  wrB,
			     input [`DADDR_W-1:0] addrB,
			     input [`DATA_W-1:0]  data_inB,
			     output [`DATA_W-1:0] data_outB
	    );


 function [`DADDR_W-1:0] reverseAddrBits;    
      input [`DADDR_W-1:0] 		    word;             
      integer 				    i;
      
      begin
	 for (i=0; i < `DADDR_W; i=i+1)
	   reverseAddrBits[i]=word[`DADDR_W-1 - i];         
      end                           
   endfunction    

 function [`DATA_W-1:0] reverseDataBits;    
      input [`DATA_W-1:0] 		    word;             
      integer 				    i;
      
      begin
	 for (i=0; i < `DATA_W; i=i+1)
	   reverseDataBits[i]=word[`DATA_W-1 - i];         
      end                           
   endfunction    
   
wire
DOA0,DOA1,DOA2,DOA3,DOA4,DOA5,DOA6,DOA7,DOA8,DOA9,DOA10,DOA11,DOA12,
DOA13,DOA14,DOA15,DOA16,DOA17,DOA18,DOA19,
DOA20,DOA21,DOA22,DOA23,DOA24,DOA25,DOA26,
DOA27,DOA28,DOA29,DOA30,DOA31,DOB0,DOB1,
DOB2,DOB3,DOB4,DOB5,DOB6,DOB7,DOB8,DOB9,
DOB10,DOB11,DOB12,DOB13,DOB14,DOB15,DOB16,
DOB17,DOB18,DOB19,DOB20,DOB21,DOB22,DOB23,
DOB24,DOB25,DOB26,DOB27,DOB28,DOB29,DOB30,
DOB31,CKA,CKB,OEA,OEB;

reg A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,
B5,B6,B7,B8,B9,B10,DIA0,DIA1,DIA2,DIA3,DIA4,DIA5,DIA6,
DIA7,DIA8,DIA9,DIA10,DIA11,DIA12,DIA13,
DIA14,DIA15,DIA16,DIA17,DIA18,DIA19,DIA20,
DIA21,DIA22,DIA23,DIA24,DIA25,DIA26,DIA27,
DIA28,DIA29,DIA30,DIA31,DIB0,DIB1,DIB2,
DIB3,DIB4,DIB5,DIB6,DIB7,DIB8,DIB9,DIB10,
DIB11,DIB12,DIB13,DIB14,DIB15,DIB16,DIB17,
DIB18,DIB19,DIB20,DIB21,DIB22,DIB23,DIB24,
DIB25,DIB26,DIB27,DIB28,DIB29,DIB30,DIB31,CSA,CSB,WEAN,WEBN;
   

   
   //port A
   assign CKA = clk;
   assign OEA = 1'b1;

   assign data_outA = reverseDataBits(
		       {DOA0,DOA1,DOA2,DOA3,DOA4,DOA5,DOA6,DOA7,DOA8,DOA9,DOA10,DOA11,
		       DOA12,DOA13,DOA14,DOA15,DOA16,DOA17,DOA18,DOA19,DOA20,DOA21,
		       DOA22,DOA23,DOA24,DOA25,DOA26,DOA27,DOA28,DOA29,DOA30,DOA31});

   always @ * begin 
      //CSA = #1 enA;
      CSA = 1'b1;
      WEAN = #1 ~wrA;
      {A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10} = #1 reverseAddrBits(addrA);

      {DIA0,DIA1,DIA2,DIA3,DIA4,DIA5,DIA6,DIA7,DIA8,DIA9,DIA10,DIA11,DIA12,DIA13,
       DIA14,DIA15,DIA16,DIA17,DIA18,DIA19,DIA20,DIA21,DIA22,DIA23,DIA24,DIA25,
       DIA26,DIA27, DIA28,DIA29,DIA30,DIA31} =
					       reverseDataBits(data_inA);
   end 

   //port B
   assign CKB = clk;
   assign OEB = 1'b1;

   assign data_outB = reverseDataBits(
                       {DOB0,DOB1,DOB2,DOB3,DOB4,DOB5,DOB6,DOB7,DOB8,DOB9,DOB10,DOB11,
                       DOB12,DOB13,DOB14,DOB15,DOB16,DOB17,DOB18,DOB19,DOB20,DOB21,
                       DOB22,DOB23,DOB24,DOB25,DOB26,DOB27,DOB28,DOB29,DOB30,DOB31});

   always @ * begin 
      //CSB = #1 enB;
      CSB = 1'b1;
      WEBN = #1 ~wrB;
      {B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10} = #1 reverseAddrBits(addrB);
      {DIB0,DIB1,DIB2, DIB3,DIB4,DIB5,DIB6,DIB7,DIB8,DIB9,DIB10,
       DIB11,DIB12,DIB13,DIB14,DIB15,DIB16,DIB17,
       DIB18,DIB19,DIB20,DIB21,DIB22,DIB23,DIB24,
       DIB25,DIB26,DIB27,DIB28,DIB29,DIB30,DIB31} =
						    reverseDataBits(data_inB);
   end
   
 SJHD130_2048X32X1CM4 asic_mem (
				.A0(A0),.A1(A1),.A2(A2),.A3(A3),.A4(A4),.A5(A5),.A6(A6),.A7(A7),.A8(A8),.A9(A9),.A10(A10),.B0(B0),.B1(B1),.B2(B2),.B3(B3),.B4(B4),
				.B5(B5),.B6(B6),.B7(B7),.B8(B8),.B9(B9),.B10(B10),.DOA0(DOA0),.DOA1(DOA1),.DOA2(DOA2),.DOA3(DOA3),.DOA4(DOA4),
				.DOA5(DOA5),.DOA6(DOA6),.DOA7(DOA7),.DOA8(DOA8),.DOA9(DOA9),.DOA10(DOA10),.DOA11(DOA11),.DOA12(DOA12),
				.DOA13(DOA13),.DOA14(DOA14),.DOA15(DOA15),.DOA16(DOA16),.DOA17(DOA17),.DOA18(DOA18),.DOA19(DOA19),.DOA20(DOA20),
				.DOA21(DOA21),.DOA22(DOA22),.DOA23(DOA23),.DOA24(DOA24),.DOA25(DOA25),.DOA26(DOA26),.DOA27(DOA27),.DOA28(DOA28),
				.DOA29(DOA29),.DOA30(DOA30),.DOA31(DOA31),.DOB0(DOB0),.DOB1(DOB1),.DOB2(DOB2),.DOB3(DOB3),.DOB4(DOB4),.DOB5(DOB5),
				.DOB6(DOB6),.DOB7(DOB7),.DOB8(DOB8),.DOB9(DOB9),.DOB10(DOB10),.DOB11(DOB11),.DOB12(DOB12),.DOB13(DOB13),.DOB14(DOB14),
				.DOB15(DOB15),.DOB16(DOB16),.DOB17(DOB17),.DOB18(DOB18),.DOB19(DOB19),.DOB20(DOB20),.DOB21(DOB21),.DOB22(DOB22),.DOB23(DOB23),
				.DOB24(DOB24),.DOB25(DOB25),.DOB26(DOB26),.DOB27(DOB27),.DOB28(DOB28),.DOB29(DOB29),.DOB30(DOB30),.DOB31(DOB31),.DIA0(DIA0),
				.DIA1(DIA1),.DIA2(DIA2),.DIA3(DIA3),.DIA4(DIA4),.DIA5(DIA5),.DIA6(DIA6),.DIA7(DIA7),.DIA8(DIA8),.DIA9(DIA9),.DIA10(DIA10),
				.DIA11(DIA11),.DIA12(DIA12),.DIA13(DIA13),.DIA14(DIA14),.DIA15(DIA15),.DIA16(DIA16),.DIA17(DIA17),.DIA18(DIA18),.DIA19(DIA19),
				.DIA20(DIA20),.DIA21(DIA21),.DIA22(DIA22),.DIA23(DIA23),.DIA24(DIA24),.DIA25(DIA25),.DIA26(DIA26),.DIA27(DIA27),.DIA28(DIA28),
				.DIA29(DIA29),.DIA30(DIA30),.DIA31(DIA31),.DIB0(DIB0),.DIB1(DIB1),.DIB2(DIB2),.DIB3(DIB3),.DIB4(DIB4),.DIB5(DIB5),.DIB6(DIB6),
				.DIB7(DIB7),.DIB8(DIB8),.DIB9(DIB9),.DIB10(DIB10),.DIB11(DIB11),.DIB12(DIB12),.DIB13(DIB13),.DIB14(DIB14),.DIB15(DIB15),.DIB16(DIB16),
				.DIB17(DIB17),.DIB18(DIB18),.DIB19(DIB19),.DIB20(DIB20),.DIB21(DIB21),.DIB22(DIB22),.DIB23(DIB23),.DIB24(DIB24),.DIB25(DIB25),
				.DIB26(DIB26),.DIB27(DIB27),.DIB28(DIB28),.DIB29(DIB29),.DIB30(DIB30),.DIB31(DIB31),
				.WEAN(WEAN),.WEBN(WEBN),.CKA(CKA),.CKB(CKB),.CSA(CSA),.CSB(CSB),.OEA(OEA),.OEB(OEB)
				);
   
endmodule
