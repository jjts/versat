/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: 

 Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
            Joao Dias Lopes <joao.d.lopes91@gmail.com>

 ***************************************************************************** */
`timescale 1ns / 1ps

`include "xdefs.v"
`include "xaludefs.v"

module xalu (
	     // Control 
	     input 			     clk,
	     input 			     rst, 

             // Controller interface
	     input 			     rw_req,
	     input 			     rw_rnw,
	     input [`DATA_W-1:0] 	     rw_data_to_wr,

	     // Data
	     input [`N*`DATA_W-1:0] 	     data_in_bus,
	     output reg signed [`DATA_W-1:0] alu_result,
	     
	     // Config data
	     input [`ALU_CONFIG_BITS - 1:0]  configdata	
	     );
   
   reg 					     rst_reg;
   
   reg [`DATA_W:0] 			     ai;
   reg [`DATA_W:0] 			     bz;
   wire [`DATA_W:0] 			     temp_adder;
   wire [5:0] 				     data_out_clz_i;
   reg 					     cin;

   reg signed [`DATA_W-1:0] 		     alu_result_int;

   wire [`N_W-1: 0] 			     sela;
   wire [`N_W-1: 0] 			     selb;

   wire [`DATA_W-1:0] 			     op_a;
   wire [`DATA_W-1:0] 			     op_b;
   reg 					     op_a_msb;
   reg 					     op_b_msb;
   reg [`DATA_W-1:0] 			     op_a_reg;
   reg [`DATA_W-1:0] 			     op_b_reg;
   wire [`ALU_FNS_W-1:0] 		     fns;
   
   wire 				     enabledA, enabledB;
   reg 					     enabledA_reg, enabledB_reg;

   // Unpack config data
   assign sela = configdata[`ALU_CONFIG_BITS-1 -: `N_W];
   assign selb = configdata[`ALU_CONFIG_BITS-`N_W-1 -: `N_W];
   assign fns = configdata[`ALU_FNS_W-1 : 0];

   // Input selection 
   xinmux muxa (
		.sel(sela),
		.data_in_bus(data_in_bus),
		.data_out(op_a),
		.enabled(enabledA)
		);
   
   xinmux muxb (
		.sel(selb),
		.data_in_bus(data_in_bus),
		.data_out(op_b),
		.enabled(enabledB)		
		);
   
   always @ (posedge clk)
     rst_reg <= rst;
   
   always @ (posedge clk)
     if (rst_reg) begin
	op_b_reg <= `DATA_W'h00000000;
	op_a_reg <= `DATA_W'h00000000;
	enabledA_reg <= 0;
	enabledB_reg <= 0;
     end else begin
	op_b_reg <= op_b;
	op_a_reg <= op_a;
	enabledA_reg <= enabledA;
	enabledB_reg <= enabledB;
     end // else: !if(rst)
   
   // Computes alu_result_int
   always @ * begin
      
      alu_result_int = temp_adder[31:0] ;

      case (fns)
	`ALU_LOGIC_OR : begin
	   alu_result_int = op_a_reg | op_b_reg;
	end
	`ALU_LOGIC_AND : begin
	   alu_result_int = op_a_reg & op_b_reg;
	end
	`ALU_LOGIC_XOR : begin
	   alu_result_int = op_a_reg ^ op_b_reg;
	end 	
	`ALU_SEXT8 : begin      
	   alu_result_int = {{24{op_a_reg[7]}}, op_a_reg[7:0]};
	end
	`ALU_SEXT16 : begin
	   alu_result_int = {{16{op_a_reg[15]}}, op_a_reg[15:0]};  
	end
	`ALU_SHIFTR_ARTH : begin
	   alu_result_int = {op_a_reg[31] ,op_a_reg[31:1] };
	end
	`ALU_SHIFTR_LOG : begin
	   alu_result_int = {1'b 0,op_a_reg[31:1] };
	end
	`ALU_CMP_SIG : begin
	   alu_result_int[31] = temp_adder[32] ;
	end
	`ALU_CMP_UNS : begin
	   alu_result_int[31] = temp_adder[32] ;
	end
	`ALU_MUX : begin
	   alu_result_int = op_b_reg;
	   
	   if(op_a_reg[31])
	     alu_result_int = `DATA_W'b0;
	end
	`ALU_SUB : begin
	end
	`ALU_ADD : begin
	end
	`ALU_CLZ : begin
           alu_result_int = {{26{1'b0}},data_out_clz_i};
	end
	`ALU_MAX : begin
           if (temp_adder[32] == 1'b 0) begin
              alu_result_int = op_b_reg;
           end
           else begin
              alu_result_int = op_a_reg;
           end
	end
	`ALU_MIN : begin
           if (temp_adder[32] == 1'b 0) begin
              alu_result_int = op_a_reg;
           end
           else begin
              alu_result_int = op_b_reg;
           end
	end
	default : begin
	end
      endcase // case (fns)
   end

   // Computes temp_adder
   assign temp_adder = ((bz & ({op_b_msb,op_b_reg}))) + ((ai ^ ({op_a_msb,op_a_reg}))) + {{32{1'b0}},cin};

   // Compute ai, cin, bz
   always @ * begin
      cin = 1'b 0;
      ai = {33{1'b0}}; // will invert op_a_reg if set to all ones
      bz = {33{1'b1}}; // will zero op_b_reg if set to all zeros

      op_a_msb = 1'b0;
      op_b_msb = 1'b0;
      
      case(fns)
	`ALU_CMP_SIG : begin
	   ai = {33{1'b1}};
	   cin = 1'b 1;
	   op_a_msb = op_a_reg[31];
	   op_b_msb = op_b_reg[31];
	end
	`ALU_CMP_UNS : begin
	   ai = {33{1'b1}};
	   cin = 1'b 1;
	end
	`ALU_SUB : begin
	   ai = {33{1'b1}};
	   cin = 1'b 1;
	end
	`ALU_MAX : begin
           ai = {33{1'b1}};
           cin = 1'b 1;
	   op_a_msb = op_a_reg[31];
	   op_b_msb = op_b_reg[31];
	end
	`ALU_MIN : begin
           ai = {33{1'b1}};
           cin = 1'b 1;
	   op_a_msb = op_a_reg[31];
	   op_b_msb = op_b_reg[31];
	end
	`ALU_ABS : begin
           if (op_a_reg[31]  == 1'b 1) begin
              // ra is negative
              ai = {33{1'b1}};
              cin = 1'b 1;
           end
           bz = {33{1'b0}};
	end
	default : begin
	end
      endcase
   end

   // Count leading zeros
   xclz clz (
             .data_in(op_a_reg),
	     .data_out(data_out_clz_i)
	     );

   always @ (posedge clk)
     if (rst_reg)
       alu_result <= `DATA_W'h00000000;
     else begin
	if (rw_req & ~rw_rnw)
	  alu_result <= rw_data_to_wr;
	else if (enabledA_reg & enabledB_reg)
	  alu_result <= alu_result_int;
     end
   
endmodule
