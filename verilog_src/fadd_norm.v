`timescale 1ns / 1ps


module fadd_norm(
    input eop,
    input [1:0] round,
    input sign,
    input [7:0] exp,
    input [27:0] sum,
    output [31:0] result 
    );
    
   reg [4:0] zeros;
   reg [7:0] exp_norm;
   reg [27:0] sum_norm;
   wire plus_one;
   wire [24:0]sum_round;
   wire [7:0] exp_round;
   
    // Counting leading zeros
    always @ (*)
    begin
        if (sum[26]) zeros = 5'd0;
        else if (sum[25]) zeros = 5'd1;
        else if (sum[24]) zeros = 5'd2;
        else if (sum[23]) zeros = 5'd3;
        else if (sum[22]) zeros = 5'd4;
        else if (sum[21]) zeros = 5'd5;
        else if (sum[20]) zeros = 5'd6;
        else if (sum[19]) zeros = 5'd7;
        else if (sum[18]) zeros = 5'd8;
        else if (sum[17]) zeros = 5'd9;
        else if (sum[16]) zeros = 5'd10;
        else if (sum[15]) zeros = 5'd11;
        else if (sum[14]) zeros = 5'd12;
        else if (sum[13]) zeros = 5'd13;
        else if (sum[12]) zeros = 5'd14;
        else if (sum[11]) zeros = 5'd15;
        else if (sum[10]) zeros = 5'd16;
        else if (sum[9])  zeros = 5'd17;
        else if (sum[8])  zeros = 5'd18;
        else if (sum[7])  zeros = 5'd19;
        else if (sum[6])  zeros = 5'd20;
        else if (sum[5])  zeros = 5'd21;
        else if (sum[4])  zeros = 5'd22;
        else if (sum[3])  zeros = 5'd23;
        else if (sum[2])  zeros = 5'd24;
        else if (sum[1])  zeros = 5'd25;
        else zeros = 5'd26;
    end   
   
    // normalization
    always @ (*)
    begin
        if (sum[27] == 1 && eop == 0) begin 
            sum_norm = sum >> 1;
            exp_norm = exp + 1;
        end
        else begin
            sum_norm = sum << zeros;
            exp_norm = exp - zeros;
        end
    end


    //--------------------------------------------------------------------------
    // Rounding
    //--------------------------------------------------------------------------
    
    assign plus_one =  ~round[1] & ~round[0] & sum_norm[2] & (sum_norm[1] | sum_norm[0])
                       |
                       ~round[1] & ~round[0] & sum_norm[2] & ~sum_norm[1] & ~sum_norm[0] & sum_norm[3]
                       |
                       ~round[1] & round[0] & (sum_norm[2] | sum_norm[1] | sum_norm[0]) & sign
                       |
                       round[1] & ~round[0] & (sum_norm[2] | sum_norm[1] | sum_norm[0]) & ~sign;
                    
    assign sum_round = {1'b0, sum_norm[26:3]} + plus_one;
    assign exp_round = sum_round[24] ? exp_norm + 1 : exp_norm;

    // output
    assign result = {sign, exp_round, sum_round[22:0]};    


endmodule
