`timescale 1ns / 1ps



module fadd(
    input  clk, reset,
    input  operation,
    input  [1:0] round_mode,
    input  [31:0] a,
    input  [31:0] b,    
    output [31:0] result
    );
    
    
    wire eop_align, eop_add, eop_norm;
    wire sign_align, sign_add, sign_norm;
    wire [7:0] exp_align, exp_add, exp_norm;
    wire [23:0] mag_big_align, mag_big_add;
    wire [26:0] mag_small_align, mag_small_add;
    wire [27:0] sum_add, sum_norm;
    wire [1:0] round_mode_add, round_mode_norm;
    
    
    fadd_align align(operation, a, b, eop_align, sign_align, exp_align, mag_big_align, mag_small_align);
    
    fadd_reg_align_add ppl_align_add(clk, reset, eop_align, sign_align, round_mode, exp_align, mag_big_align, mag_small_align,
                                            eop_add, sign_add, round_mode_add, exp_add, mag_big_add, mag_small_add);
    
    fadd_add add(eop_add, mag_big_add, mag_small_add, sum_add);
    
    fadd_reg_add_norm ppl_add_norm(clk, reset, eop_add, sign_add, exp_add, round_mode_add, sum_add,
                                          eop_norm, sign_norm, exp_norm, round_mode_norm, sum_norm);
    
    fadd_norm norm(eop_norm, round_mode_norm, sign_norm, exp_norm, sum_norm, result);
    
    
    
endmodule
