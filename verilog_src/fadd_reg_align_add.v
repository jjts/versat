`timescale 1ns / 1ps


module fadd_reg_align_add(
    input clk, reset,
    input eop_align, sign_align,
    input [1:0] round_mode_align,
    input [7:0] exp_align,
    input [23:0] mag_big_align,
    input [26:0] mag_small_align,
    output reg eop_add, sign_add,
    output reg [1:0] round_mode_add,
    output reg [7:0] exp_add,
    output reg [23:0] mag_big_add,
    output reg [26:0] mag_small_add    
    );
    
    always @ (posedge clk)
    begin
        if (reset) begin
            eop_add <= 0;
            sign_add <= 0;
            round_mode_add <= 0;
            exp_add <= 0;
            mag_big_add <= 0;
            mag_small_add <= 0;
        end
        else begin
            eop_add <= eop_align;
            sign_add <= sign_align;
            round_mode_add <= round_mode_align;
            exp_add <= exp_align;
            mag_big_add <= mag_big_align;
            mag_small_add <= mag_small_align;
        end    
    end
    
endmodule
