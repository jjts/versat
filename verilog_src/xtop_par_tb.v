/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: testbench to load the program and data for executing a simple 
               vector addition kernel and read back the result via parallel 
               interface.
 
               The testbench plays the role of a host CPU that has Versat as a 
               guest co-processor. Host and guest communicate by acessign the 
               same 16x32 register file R0 to R15.
                
               Registers used:
 
               R0: Address Register. Used by the host to place start memory 
                   addresses for reading writing or executing. Guest starts 
                   execution whenever this register is non zero.
 
               R14: Command Register. Used by the host to issue commands to the 
                    guest; used by the guest to indicate completion of the 
                    commands received
 
               R15: Data Register. Used by the host to place the data words to
                    write to the guest memory; used by the guest to place the data 
                    words requested by the host for reading
 
 
               Host commands:
 
               WRITE: host tells guest it wants to write the word placed in R15
                      to the memory position currently held in register B.
 
               READ: host tells guest it wants to read the word memory position 
                     currently held in register B, which should be placed in R15
                     by guest.
 
               FINISH: host tells guest it has no more words to read or write.
 
               RUN: host tells guest it wants to run the program starting at the 
                    address currently in R0
  
 
               Guest response:
 
               ACK: guest tells host it has performed the READ or WRITE commands.
 
 

   Copyright (C) 2014 Authors

  Author(s): Joao Dias Lopes <joao.d.lopes91@gmail.com>
             Jose T. de Sousa <jose.t.de.sousa@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps

`include "xdefs.v"
`include "xmem_map.v"


module xtop_par_tb;

   //parameters 
   parameter clk_period = 40;
   
   //sizes
   parameter IMEM_SIZE = 2**`IADDR_W;
   parameter DMEM_SIZE = 2**`DADDR_W;
   
   reg clk;
   reg rst;
   
   //parallel interface
   reg 	[`CTRL_REGF_ADDR_W-1:0] par_addr;
   reg 				par_we;
   reg [`DATA_W-1:0] 		par_in;
   wire [`DATA_W-1:0] 		par_out;
   reg [`DATA_W-1:0] 		data_addr;

   reg [80*8:1] fileName;
   
   integer 			i, k, z, fp;
   
   // Data Bank
   reg [`INSTR_W-1:0] prog [IMEM_SIZE-1:0];
   reg [`DATA_W-1:0] data [DMEM_SIZE-1:0];
   
   
   // Instantiate the Unit Under Test (UUT)
   xtop uut (
	     .clk(clk), 
	     .rst(rst),
	     
	     // parallel interface
	     .par_addr(par_addr),
	     .par_we(par_we),
	     .par_in(par_in),
	     .par_out(par_out)
	     );
   
   initial begin

`ifdef DEBUG
      $dumpfile("xtop_par.vcd");
      $dumpvars();
`endif
      
      $readmemh("opcode.hex", prog, 0, (IMEM_SIZE-1));
      
      // Global reset of FPGA
      #100
 
      // Initialize Inputs
      clk = 0;
      rst = 0;

      //parallel interface
      par_addr = 0;
      par_we = 0;
      par_in = 0;
      
      // Global reset
      #(clk_period+1)
      rst = 1;

      #clk_period
      rst = 0;

      //
      //load program
      //

      #clk_period par_addr = 15;
      par_in = prog[0];
      par_we = 1;
      
      #clk_period par_addr = 14;
      par_in = `BR_WRITE;
      
      #clk_period par_addr = 0;
      par_in = `PROG_MEM_BASE;

      #clk_period par_addr = 14;
      for (i=1; i<DMEM_SIZE; i = i+1) begin 
	 par_we = 0; 
	 
	 while(par_out != `BR_ACK) #clk_period;
	 
	 #clk_period par_we = 1;
	 par_addr = 15;
	 par_in = prog[i];
	 
	 #clk_period par_addr = 14;
	 par_in = `BR_WRITE;
	 #clk_period;
      end

      par_we = 0; 
      while(par_out != `BR_ACK) #clk_period;

      #clk_period par_we = 1;
      par_addr = 14;
      par_in = `BR_DONE;

      //wait for versat to return to bootrom 
      #clk_period par_we = 0;
      par_addr = 0;
      while(par_out != 0) #clk_period;

      //
      //load data
      //
      data_addr = `ENG_MEM_BASE;
      
      for (k = 0; k < `Nmem; k=k+1) begin
	 $sformat(fileName, "./xmem%1ddata.hex", k);
	 
	 $readmemh(fileName, data, 0, (DMEM_SIZE-1));
	 
	 #clk_period par_addr = 15;
	 par_in = data[0];
	 par_we = 1;
	 
	 #clk_period par_addr = 14;
	 par_in = `BR_WRITE;
	 
	 #clk_period par_addr = 0;
	 par_in = data_addr;
	 
	 #clk_period par_addr = 14;
	 for (i=1; i<DMEM_SIZE; i = i+1) begin 
	    par_we = 0; 
	    while(par_out != `BR_ACK) #clk_period;
	    
	    #clk_period par_we = 1;
	    par_addr = 15;
	    par_in = data[i];
	    
	    #clk_period par_addr = 14;
	    par_in = `BR_WRITE;
	    #clk_period;
	 end

	 par_we = 0; 
	 while(par_out != `BR_ACK) #clk_period;
	 
	 #clk_period par_we = 1;
	 par_addr = 14;
	 par_in = `BR_DONE;
	 
	 //wait for versat to return to bootrom 
	 #clk_period par_we = 0;
	 par_addr = 0;
	 while(par_out != 0) #clk_period;

	 data_addr = data_addr + DMEM_SIZE; // switch memory
      end

      //
      //run program
      //    
      #clk_period par_we = 1;
      par_addr = 14;
      par_in = `BR_RUN;
      
      #clk_period par_addr = 0;
      par_in = `PROG_MEM_BASE;
      
      //wait for versat to return to bootrom 
      #clk_period par_we = 0;
      par_addr = 0;
      while(par_out != 0) #clk_period;

      //
      //read data
      //
      data_addr = `ENG_MEM_BASE;

      fp = $fopen("./xtop.out","w");
      
      for (k = 0; k < `Nmem; k=k+1) begin
	 #clk_period par_we = 1;
	 par_addr = 14;
	 par_in = `BR_READ;
	 
	 #clk_period par_addr = 0;
	 par_in = data_addr;

	 #clk_period par_addr = 14;
	 for (i=0; i<DMEM_SIZE; i = i+1) begin 
	    par_we = 0; 
	    
	    while(par_out != `BR_ACK) #clk_period;
	    
	    #clk_period par_addr = 15;
	    
	    #clk_period par_we = 1;
	    data[i] = par_out;
	    par_addr = 14;
	    par_in = `BR_READ;
	    #clk_period;
	 end
	 
	 #clk_period par_addr = 14;
	 par_in = `BR_DONE;
	 
	 //wait for versat to return to bootrom 
	 #clk_period par_we = 0;
	 par_addr = 0;
	 while(par_out != 0) #clk_period;

	 for (z=0; z < DMEM_SIZE; z=z+1)
	   $fwrite(fp,"%8h\n",data[z]);
	 
	 $fwrite(fp,"\n");

	 data_addr = data_addr + DMEM_SIZE; // switch memory
      end
      
      $fclose(fp);
      
      // Simulation time
      #(10000) $finish;
   end
	
   always 
     #(clk_period/2) clk = ~clk;
      	 
endmodule
