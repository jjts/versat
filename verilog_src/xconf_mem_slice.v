/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias lopes <joao.d.lopes91@gmail.com>
             Francisco Nunes <ftcnunes@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module xconf_mem_slice (
			input 			 clk,
			input 			 en,

			// Flags
			input 			 dma_req,
			input 			 dma_rnw,
			input 			 conf_req,
			input 			 conf_rnw,

			// Adresses
			input [`CONF_ADDR_W-1:0] dma_addr,
			input [`CONF_ADDR_W-1:0] conf_addr,

			// Data in/out
			input [`DATA_W-1:0] 	 dma_data_in,
			output [`DATA_W-1:0] 	 dma_data_out,
			input [`DATA_W-1:0] 	 conf_data_in,
			output [`DATA_W-1:0] 	 conf_data_out
			);

`ifndef ASIC
   reg [`DATA_W-1:0] 				 conf_mem_slice [2**`CONF_ADDR_W-1:0];

   reg [`DATA_W-1:0] 				 dma_data_out_int;
   reg [`DATA_W-1:0] 				 conf_data_out_int;
`else
   wire 					 en_int;
   wire 					 we;
   
   wire [`CONF_ADDR_W-1:0] 			 addr;
   
   wire [`DATA_W-1:0] 				 data_in;
   wire [`DATA_W-1:0] 				 data_out;

   wire [`DATA_W-1:0] 				 dma_data_out_int;
   wire [`DATA_W-1:0] 				 conf_data_out_int;
`endif
   
   wire 					 en_dma;
   wire 					 en_conf;
   
   wire 					 wr_dma;
   wire 					 wr_conf;

   assign dma_data_out = dma_data_out_int;
   assign conf_data_out = conf_data_out_int;
   
`ifndef ALTERA // XILINX & ASIC
   assign en_dma = en & dma_req;
   assign en_conf = en & conf_req;
   
   assign wr_dma = ~dma_rnw;
   assign wr_conf = ~conf_rnw;
`else
   assign wr_dma = en & dma_req & ~dma_rnw;
   assign wr_conf = en & conf_req & ~conf_rnw;
`endif

`ifdef ASIC
   assign en_int = (dma_req)? en_dma : en_conf;
   assign we = (dma_req)? wr_dma : wr_conf;
   
   assign addr = (dma_req)? dma_addr : conf_addr;
   
   assign data_in = (dma_req)? dma_data_in : conf_data_in;
   
   assign dma_data_out_int = data_out;
   assign conf_data_out_int = data_out;
`endif
   
`ifdef XILINX
   always @ (posedge clk) begin : conf_reg_rw_dma
      if (en_dma) begin
	 if (wr_dma)
	   conf_mem_slice[dma_addr] <= dma_data_in;
	 dma_data_out_int <= conf_mem_slice[dma_addr];
      end
   end

   always @ (posedge clk) begin : conf_reg_rw
      if (en_conf) begin
	 if (wr_conf)
	   conf_mem_slice[conf_addr] <= conf_data_in;
	 conf_data_out_int <= conf_mem_slice[conf_addr];
      end
   end
`endif // !`ifdef XILINX
   
`ifdef ALTERA
   always @ (posedge clk) begin : conf_reg_rw_dma
      if (wr_dma)
	conf_mem_slice[dma_addr] <= dma_data_in;
      dma_data_out_int <= conf_mem_slice[dma_addr];
   end

   always @ (posedge clk) begin : conf_reg_rw
      if (wr_conf)
	conf_mem_slice[conf_addr] <= conf_data_in;
      conf_data_out_int <= conf_mem_slice[conf_addr];
   end
`endif // !`ifdef ALTERA
   
`ifdef ASIC
   
   xmem_wrapper64x32 memory_wrapper (
				     .clk(clk),
      
				     .en(en_int),
				     .wr(we),
				     .addr(addr),
				     .data_in(data_in),
				     .data_out(data_out)		
				     );
`endif //  `ifdef ASIC
   
endmodule
