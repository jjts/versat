/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module xboot_rom(
		 input 			  clk,
		 input [`ROM_ADDR_W-1:0]  addr,
		 output reg [`INSTR_W-1:0] data
		 );

`ifndef ROM
   reg [`INSTR_W-1:0] 			  mem [2**`ROM_ADDR_W-1:0];
`endif
   
   wire 				  en;

   assign en = 1'b1;
   
`ifdef ROM
   always @(posedge clk)
      if (en)
         case (addr)
           `ROM_ADDR_W'h00: data <= `INSTR_W'h80000;
	   `ROM_ADDR_W'h01: data <= `INSTR_W'h10000;
	   `ROM_ADDR_W'h02: data <= `INSTR_W'h00000;
	   `ROM_ADDR_W'h03: data <= `INSTR_W'h4fffd;
	   `ROM_ADDR_W'h04: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h05: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h06: data <= `INSTR_W'h80001;
	   `ROM_ADDR_W'h07: data <= `INSTR_W'h1000d;
	   `ROM_ADDR_W'h08: data <= `INSTR_W'h0000e;
	   `ROM_ADDR_W'h09: data <= `INSTR_W'hd000d;
	   `ROM_ADDR_W'h0a: data <= `INSTR_W'h40023;
	   `ROM_ADDR_W'h0b: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h0c: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h0d: data <= `INSTR_W'h80004;
	   `ROM_ADDR_W'h0e: data <= `INSTR_W'h1000d;
	   `ROM_ADDR_W'h0f: data <= `INSTR_W'h0000e;
	   `ROM_ADDR_W'h10: data <= `INSTR_W'hd000d;
	   `ROM_ADDR_W'h11: data <= `INSTR_W'h40021;
	   `ROM_ADDR_W'h12: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h13: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h14: data <= `INSTR_W'h00000;
	   `ROM_ADDR_W'h15: data <= `INSTR_W'h10040;
	   `ROM_ADDR_W'h16: data <= `INSTR_W'h80003;
	   `ROM_ADDR_W'h17: data <= `INSTR_W'h1000d;
	   `ROM_ADDR_W'h18: data <= `INSTR_W'h0000e;
	   `ROM_ADDR_W'h19: data <= `INSTR_W'hd000d;
	   `ROM_ADDR_W'h1a: data <= `INSTR_W'h4fffa;
	   `ROM_ADDR_W'h1b: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h1c: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h1d: data <= `INSTR_W'h80002;
	   `ROM_ADDR_W'h1e: data <= `INSTR_W'h1000d;
	   `ROM_ADDR_W'h1f: data <= `INSTR_W'h0000e;
	   `ROM_ADDR_W'h20: data <= `INSTR_W'hd000d;
	   `ROM_ADDR_W'h21: data <= `INSTR_W'h4ffdd;
	   `ROM_ADDR_W'h22: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h23: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h24: data <= `INSTR_W'h0000f;
	   `ROM_ADDR_W'h25: data <= `INSTR_W'h30000;
	   `ROM_ADDR_W'h26: data <= `INSTR_W'h00040;
	   `ROM_ADDR_W'h27: data <= `INSTR_W'hc0001;
	   `ROM_ADDR_W'h28: data <= `INSTR_W'h10040;
	   `ROM_ADDR_W'h29: data <= `INSTR_W'h80003;
	   `ROM_ADDR_W'h2a: data <= `INSTR_W'h1000e;
	   `ROM_ADDR_W'h2b: data <= `INSTR_W'h80000;
	   `ROM_ADDR_W'h2c: data <= `INSTR_W'h4ffe8;
	   `ROM_ADDR_W'h2d: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h2e: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h2f: data <= `INSTR_W'h00000;
	   `ROM_ADDR_W'h30: data <= `INSTR_W'h10040;
	   `ROM_ADDR_W'h31: data <= `INSTR_W'h70000;
	   `ROM_ADDR_W'h32: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h33: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h34: data <= `INSTR_W'h00000;
	   `ROM_ADDR_W'h35: data <= `INSTR_W'h10040;
	   `ROM_ADDR_W'h36: data <= `INSTR_W'h80003;
	   `ROM_ADDR_W'h37: data <= `INSTR_W'h1000d;
	   `ROM_ADDR_W'h38: data <= `INSTR_W'h0000e;
	   `ROM_ADDR_W'h39: data <= `INSTR_W'hd000d;
	   `ROM_ADDR_W'h3a: data <= `INSTR_W'h4fffa;
	   `ROM_ADDR_W'h3b: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h3c: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h3d: data <= `INSTR_W'h80002;
	   `ROM_ADDR_W'h3e: data <= `INSTR_W'h1000d;
	   `ROM_ADDR_W'h3f: data <= `INSTR_W'h0000e;
	   `ROM_ADDR_W'h40: data <= `INSTR_W'hd000d;
	   `ROM_ADDR_W'h41: data <= `INSTR_W'h4ffbd;
	   `ROM_ADDR_W'h42: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h43: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h44: data <= `INSTR_W'h20000;
	   `ROM_ADDR_W'h45: data <= `INSTR_W'h20000;
	   `ROM_ADDR_W'h46: data <= `INSTR_W'h20000;
	   `ROM_ADDR_W'h47: data <= `INSTR_W'h1000f;
	   `ROM_ADDR_W'h48: data <= `INSTR_W'h00040;
	   `ROM_ADDR_W'h49: data <= `INSTR_W'hc0001;
	   `ROM_ADDR_W'h4a: data <= `INSTR_W'h10040;
	   `ROM_ADDR_W'h4b: data <= `INSTR_W'h80003;
	   `ROM_ADDR_W'h4c: data <= `INSTR_W'h1000e;
	   `ROM_ADDR_W'h4d: data <= `INSTR_W'h80000;
	   `ROM_ADDR_W'h4e: data <= `INSTR_W'h4ffe6;
	   `ROM_ADDR_W'h4f: data <= `INSTR_W'hc0000;
	   `ROM_ADDR_W'h50: data <= `INSTR_W'hc0000;
           default: data <= `INSTR_W'hc0000;
         endcase
`else
   initial begin
      $readmemh("./boot_rom.hex",mem,0,2**`ROM_ADDR_W-1);
   end

   always @ (posedge clk) begin
      if (en)
	data <= mem[addr];  
   end
`endif
   
endmodule
