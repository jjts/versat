/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias Lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module xmult (
	      input clk,
	      input rst,
	
              //controller interface
              input rw_req,
	      input rw_rnw,
	      input [`DATA_W-1:0] rw_data_to_wr,
	      
	      //data 
	      input [`N*`DATA_W-1:0] data_in_bus,
              output reg signed [`DATA_W-1:0] product,
	      
	      //config 
	      input [`MULT_CONFIG_BITS-1:0] configdata
	      );

   wire [`DATA_W-1:0] 			    res11;
   wire [`DATA_W-1:0] 			    res12;
   wire [`DATA_W-1:0] 			    res21;
   wire [`DATA_W-1:0] 			    res22;

   wire [2*`DATA_W-1:0] 		    res2;
   wire [`DATA_W-1:0] 			    res3;
        
   reg [3*`DATA_W/2-1:0] 		    res1_reg;
   reg signed [2*`DATA_W-1:0] 		    res1_reg2;
        
   //data
   wire signed [`DATA_W-1:0] 		    op_a;
   wire signed [`DATA_W-1:0] 		    op_b;
   
   wire [`DATA_W-1:0] 			    op_a_u;
   wire [`DATA_W-1:0] 			    op_b_u;
   reg [`DATA_W-1:0] 			    op_a_u_reg;
   reg [`DATA_W-1:0] 			    op_b_u_reg;
   reg 					    op_a_s;
   reg 					    op_b_s;
   
   //config data
   wire [`N_W-1: 0] 			    sela;
   wire [`N_W-1: 0] 			    selb;
   wire 				    lonhi;
   wire 				    div2;
	
   wire 				    enabledA, enabledB;

   //unpack config bits 
   assign sela = configdata[`MULT_CONFIG_BITS-1 -: `N_W];
   assign selb = configdata[`MULT_CONFIG_BITS-1-`N_W -: `N_W];
   assign lonhi = configdata[1];
   assign div2 = configdata[0];
   
   //input selection 
   xinmux muxa (
		.sel(sela),
		.data_in_bus(data_in_bus),
		.data_out(op_a),
		.enabled(enabledA)
		);
	
   xinmux muxb (
		.sel(selb),
		.data_in_bus(data_in_bus),
		.data_out(op_b),
		.enabled(enabledB)		
		);

   assign op_a_u = op_a[`DATA_W-1]? -op_a : op_a;
   assign op_b_u = op_b[`DATA_W-1]? -op_b : op_b;
   
   always @ (posedge rst, posedge clk) 
     if (rst) begin
	res1_reg <= {2*`DATA_W{1'b0}};
	res1_reg2 <= {2*`DATA_W{1'b0}};
	op_a_u_reg <= {`DATA_W{1'b0}};
	op_b_u_reg <= {`DATA_W{1'b0}};
	op_a_s <= 1'b0;
	op_b_s <= 1'b0;	
     end else begin
	op_a_u_reg <= op_a_u;
	op_b_u_reg <= op_b_u;
	op_a_s <= op_a[`DATA_W-1];
	op_b_s <= op_b[`DATA_W-1];	
	res1_reg <= res11+{res21,{`DATA_W/2{1'b0}}};
	res1_reg2 <= (op_a_s^op_b_s)?
		     -({res22,{`DATA_W{1'b0}}}+{res12,{`DATA_W/2{1'b0}}}+res1_reg) :
		     {res22,{`DATA_W{1'b0}}}+{res12,{`DATA_W/2{1'b0}}}+res1_reg;
     end
        
   assign res11 = op_a_u[`DATA_W/2-1:0] * op_b_u[`DATA_W/2-1:0];
   assign res21 = op_a_u[`DATA_W-1:`DATA_W/2] * op_b_u[`DATA_W/2-1:0];
   assign res12 = op_a_u_reg[`DATA_W/2-1:0] * op_b_u_reg[`DATA_W-1:`DATA_W/2];
   assign res22 = op_a_u_reg[`DATA_W-1:`DATA_W/2] * op_b_u_reg[`DATA_W-1:`DATA_W/2];
   
   assign res2 = (div2 == 1'b0)? res1_reg2<<1 : res1_reg2;
   assign res3 = (lonhi == 1'b1)? res2[`DATA_W-1:0] : res2[2*`DATA_W-1:`DATA_W];

   always @ (posedge rst, posedge clk) 
     if (rst)
       product <= `DATA_W'h00000000;
     else begin
	if (rw_req & ~rw_rnw)
	  product <= rw_data_to_wr;
	else if(enabledA & enabledB)
	  product <= res3;
     end
endmodule
