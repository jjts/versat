/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: AXI tasks

 Copyright (C) 2014 Authors

 Author(s): Joao Dias lopes <joao.d.lopes91@gmail.com>

 ***************************************************************************** */

//Write data to AXI4 lite slave
task write_data_axi;
   input [`DATA_W-1:0] addr;
   input [`DATA_W-1:0] data;

   begin
      #clk_period;
      
      S_AXI_AWADDR = (addr*4);
      S_AXI_AWVALID = 1'b1;
      
      #clk_period;
      
      S_AXI_WDATA = data;
      S_AXI_WVALID = 1'b1;
      
      #clk_period;
      
      S_AXI_AWVALID = 1'b0;
      S_AXI_WVALID = 1'b0;
      S_AXI_BREADY = 1'b1;

      #clk_period;
      
      S_AXI_BREADY = 1'b0;
      
      #clk_period;
   end
endtask

//Read data from AXI4 lite slave
task read_data_axi;
   input [`DATA_W-1:0]      addr;
   output reg [`DATA_W-1:0] data;

   begin
      #clk_period;
      
      S_AXI_ARADDR = (addr*4);
      S_AXI_ARVALID = 1'b1;
      
      #clk_period;
      
      S_AXI_ARVALID = 1'b0;
      S_AXI_RREADY = 1'b1;
      
      #clk_period;

      data = S_AXI_RDATA;
      S_AXI_RREADY = 1'b0;
      
      #clk_period;
   end
endtask

//Configure DMA by AXI4 lite slave
task dma_conf_axi;
   input [`DATA_W-1:0] ext_addr;
   input [`DATA_W-1:0] int_addr;
   input [`DATA_W-1:0] size;
   input [`DATA_W-1:0] direction;
   
   reg [`DATA_W-1:0]   addr;
   reg [`DATA_W-1:0]   data;
   
   begin
      
      // Write write command into R14
      addr = `DATA_W'd14;
      data = `BR_WRITE;
      
      write_data_axi(addr,data);
      
      // Write external address into R15
      addr = `DATA_W'd15;
      data = ext_addr;
      
      write_data_axi(addr,data);

      // Write DMA external address into R0
      addr = `DATA_W'd0;
      data = `DMA_EXT_ADDR_ADDR;
      
      write_data_axi(addr,data);
      
      // Wait for versat response
      addr = `DATA_W'd14;
      data = `DATA_W'd0;
      
      while (data != `BR_ACK) begin
	 read_data_axi(addr,data);
	 
	 #clk_period;
      end
      
      // Write internal address into R15
      addr = `DATA_W'd15;
      data = int_addr;
      
      write_data_axi(addr,data);
      
      // Write write command into R14
      addr = `DATA_W'd14;
      data = `BR_WRITE;
      
      write_data_axi(addr,data);

      // Wait for versat response
      addr = `DATA_W'd14;
      data = `DATA_W'd0;
      
      while (data != `BR_ACK) begin
	 read_data_axi(addr,data);
	 
	 #clk_period;
      end
      
      // Write transfer size into R15
      addr = `DATA_W'd15;
      data = size;
      
      write_data_axi(addr,data);
      
      // Write write command into R14
      addr = `DATA_W'd14;
      data = `BR_WRITE;
      
      write_data_axi(addr,data);
      
      // Wait for versat response
      addr = `DATA_W'd14;
      data = `DATA_W'd0;
      
      while (data != `BR_ACK) begin
	 read_data_axi(addr,data);
	 
	 #clk_period;
      end
      
      // Write transfer direction into R15
      addr = `DATA_W'd15;
      data = direction;
      
      write_data_axi(addr,data);
      
      // Write write command into R14
      addr = `DATA_W'd14;
      data = `BR_WRITE;
      
      write_data_axi(addr,data);
      
      // Wait for versat response
      addr = `DATA_W'd14;
      data = `DATA_W'd0;
      
      while (data != `BR_ACK) begin
	 read_data_axi(addr,data);
	 
	 #clk_period;
      end
      
      // Write write command into R14
      addr = `DATA_W'd14;
      data = `BR_WRITE;
      
      write_data_axi(addr,data);
      
      // Wait for versat response
      addr = `DATA_W'd14;
      data = `DATA_W'd0;
      
      while (data != `BR_ACK) begin
	 read_data_axi(addr,data);
	 
	 #clk_period;
      end
      
      // Write done into R14
      addr = `DATA_W'd14;
      data = `BR_DONE;
      
      write_data_axi(addr,data);
      
      // Wait for versat response
      addr = `DATA_W'd0;
      data = `DATA_W'd1;
      
      while (data != `DATA_W'd0) begin
	 read_data_axi(addr,data);
	 
	 #clk_period;
      end
      
   end
endtask