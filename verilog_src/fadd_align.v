`timescale 1ns / 1ps



module fadd_align(
    input  operation,
    input  [31:0] a,
    input  [31:0] b,
    output eop, sign,
    output [7:0] exp,
    output [23:0] mag_big,
    output [26:0] mag_small
    );
    
    
    wire [31:0] num_big, num_small;
    wire [23:0] mag_big_swap, mag_small_swap;
    wire [7:0]  exp_big, exp_small, exp_big_swap, exp_small_swap, exp_dif;
    wire [49:0] mag_small_shift;
    wire [26:0] mag_small_align;
    wire swap, sticky_bit, hidden_big, hidden_small;
     
    
    
    assign swap = (a[30:0] >= b[30:0]);
    
    assign num_big = swap ? b : a;
    assign num_small = swap ? a : b;
    
    // hidden bit  
    assign hidden_big = |num_big[30:23];
    assign hidden_small = |num_small[30:23];
         
    // prepend hidden bit
    assign mag_big_swap = {hidden_big, num_big[22:0]};
    assign mag_small_swap = {hidden_small, num_small[22:0]};
    
    assign exp_big_swap = num_big[30:23];
    assign exp_small_swap = num_small[30_23];
     
    // denormals 
    assign exp_big = (hidden_big == 0) ? exp_big_swap + 1 : exp_big_swap;
    assign exp_small = (hidden_small == 0) ? exp_small_swap + 1 : exp_small_swap;
     
    // exponent difference
    assign exp_dif = exp_big - exp_small;
          
    // shift smaller operand
    assign mag_small_shift = (exp_dif >= 26) ? {26'b0, mag_small_swap} : {mag_small_swap, 26'd0} >> exp_dif;
    assign sticky_bit = |mag_small_shift[23:0];
    assign mag_small_align = {mag_small_shift[49:24], sticky_bit};
    
     
    // magnitudes 
    assign mag_big = mag_big_swap;
    assign mag_small = mag_small_align;  
     
    
    // exponent
    assign exp = exp_big;
    
     
    // sign
    assign sign = swap ? operation ^ b[31] : a[31]; 
        
        
    // effective operation
    assign eop = operation ^ a[31] ^ b[31];  
     
     
    
         
     
endmodule
