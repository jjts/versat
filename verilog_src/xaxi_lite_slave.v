/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

  Copyright (C) 2014 Authors

  Author(s): Joao Dias Lopes <joao.d.lopes91@gmail.com>
 
***************************************************************************** */

`timescale 1ns/1ps

`include "xdefs.v"

module xaxi_lite_slave #
  (
   parameter integer C_S_AXI_ADDR_WIDTH = 32,
   parameter integer C_S_AXI_DATA_WIDTH = 32,
   parameter C_S_AXI_MIN_SIZE = 32'h000001FF,
   parameter integer C_USE_WSTRB = 0,
   parameter C_DPHASE_TIMEOUT = 8,
   parameter C_BASEADDR = 32'hFFFFFFFF,
   parameter C_HIGHADDR = 32'h00000000,
   //parameter C_FAMILY = "virtex6",
   parameter integer C_NUM_REG = 1,
   parameter integer C_NUM_MEM = 1,
   parameter integer C_SLV_AWIDTH = 32,
   parameter integer C_SLV_DWIDTH = 32
   )
   (
    //external port
    // System Signals
    input wire 				  S_AXI_ACLK,
    input wire 				  S_AXI_ARESETN,
    
    // Slave Interface Write Address Ports
    input wire [C_S_AXI_ADDR_WIDTH-1:0]   S_AXI_AWADDR,
    //input wire [3-1:0] 			  S_AXI_AWPROT,
    input wire 				  S_AXI_AWVALID,
    output reg 				  S_AXI_AWREADY,
    
    // Slave Interface Write Data Ports
    input wire [C_S_AXI_DATA_WIDTH-1:0]   S_AXI_WDATA,
    input wire [C_S_AXI_DATA_WIDTH/8-1:0] S_AXI_WSTRB,
    input wire 				  S_AXI_WVALID,
    output reg 				  S_AXI_WREADY,
    
    // Slave Interface Write Response Ports
    output wire [2-1:0] 		  S_AXI_BRESP,
    output reg 				  S_AXI_BVALID,
    input wire 				  S_AXI_BREADY,
    
    
    // Slave Interface Read Address Ports
    input wire [C_S_AXI_ADDR_WIDTH-1:0]   S_AXI_ARADDR,
    //input wire [3-1:0] 			  S_AXI_ARPROT,
    input wire 				  S_AXI_ARVALID,
    output reg 				  S_AXI_ARREADY,
    
    // Slave Interface Read Data Ports
    output wire [C_S_AXI_DATA_WIDTH-1:0]  S_AXI_RDATA,
    output wire [2-1:0] 		  S_AXI_RRESP,
    output reg 				  S_AXI_RVALID,
    input wire 				  S_AXI_RREADY,
    
    //internal port
    output [`CTRL_REGF_ADDR_W-1:0] 	  par_addr,
    output  				  par_we,
    input [`DATA_W-1:0] 		  par_in,
    output [`DATA_W-1:0] 		  par_out
    );
   
   
   parameter integer USER_SLV_DWIDTH = C_S_AXI_DATA_WIDTH;
   
   parameter integer IPIF_SLV_DWIDTH = C_S_AXI_DATA_WIDTH;
   
   parameter ZERO_ADDR_PAD = 0;
   parameter USER_SLV_BASEADDR = C_BASEADDR;
   parameter USER_SLV_HIGHADDR = C_HIGHADDR;
   
   parameter integer USER_SLV_NUM_REG = 1;
   parameter integer USER_NUM_REG = USER_SLV_NUM_REG;
   parameter integer TOTAL_IPIF_CE = USER_NUM_REG;
   
   //------------------------------------------
   //-- Index for CS/CE
   //------------------------------------------
   
   parameter integer USER_SLV_CS_INDEX = 0;
   
   parameter IDLE=2'h0, WRITE=2'h1, READ=2'h2, W_RESPONSE=2'h3;
   
   wire 				  clk;
   wire 				  rst;
   
   reg [2-1:0] 				  state;
   reg [2-1:0] 				  state_nxt;
   
   assign clk = S_AXI_ACLK;
   assign rst = ~S_AXI_ARESETN;
   
   //Discart 2 less significant bits and just use CTRL_REGF_ADDR_W bits
   assign par_addr = S_AXI_AWVALID? S_AXI_AWADDR[`CTRL_REGF_ADDR_W+2-1 -: `CTRL_REGF_ADDR_W] : S_AXI_ARADDR[`CTRL_REGF_ADDR_W+2-1 -: `CTRL_REGF_ADDR_W];
   assign par_we = S_AXI_AWVALID & S_AXI_WVALID;
   assign par_out = S_AXI_WDATA;
   assign S_AXI_RDATA = par_in;
   
   //Response is always OK
   assign S_AXI_BRESP = 2'b00;
   assign S_AXI_RRESP = 2'b00;
   
   //State register
   always @ (posedge rst, posedge clk)
     if (rst) begin 
	state <= 2'b00;
     end else begin
	state <= state_nxt;
     end
   
   //State machine
   always @ * begin
      state_nxt = state;
      S_AXI_AWREADY = 1'b0;
      S_AXI_ARREADY = 1'b0;
      S_AXI_BVALID = 1'b0;
      S_AXI_RVALID = 1'b0;
      S_AXI_WREADY = 1'b0;
      
      case (state)
	IDLE: begin
	   if (S_AXI_AWVALID)
	     state_nxt = WRITE;
	   else if (S_AXI_ARVALID)
	     state_nxt = READ;
	end
	WRITE: begin
	   if (par_we)
	     state_nxt = W_RESPONSE;
	   S_AXI_AWREADY = 1'b1;
	   S_AXI_WREADY = 1'b1;
	end
	READ: begin
	   if (S_AXI_RREADY)
	     state_nxt = IDLE;
	   S_AXI_ARREADY = 1'b1;
	   S_AXI_RVALID = 1'b1;
	end
	W_RESPONSE: begin
	   if (S_AXI_BREADY)
	     state_nxt = IDLE;
	   S_AXI_BVALID = 1'b1;
	end
      endcase
   end
   
endmodule
