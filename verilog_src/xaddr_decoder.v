/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: 

 Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>

 ***************************************************************************** */

`timescale 1ns / 1ps

`include "xdefs.v"
`include "xmem_map.v"

module xaddr_decoder (
		      input 		       rw_req,
		      input [`INT_ADDR_W-1:0]  rw_addr,
		      
		      input [`DATA_W-1:0]      ctrl_regf_data_to_rd,
		      input [`DATA_W-1:0]      eng_data_to_rd,
		      input [`DATA_W-1:0]      div_data_to_rd,
		      input [`DATA_W-1:0]      dma_data_to_rd,
		      input [`DATA_W-1:0]      prog_mem_data_to_rd,
		      
		      output reg 	       div_req,
		      output reg 	       dma_req,
		      output reg 	       prog_mem_req,
		      output reg 	       conf_mem_req,
		      output reg 	       ctrl_regf_req,
		      output reg [`DATA_W-1:0] data_to_rd
		     );

   reg 					       rw_req_delayed;
   
   always @ * begin 
      ctrl_regf_req = 1'b0;
      conf_mem_req = 1'b0;
      prog_mem_req = 1'b0;
      div_req = 1'b0;
      dma_req = 1'b0;

      rw_req_delayed = #1 rw_req;
      
      if (`CTRL_REGF_BASE == (rw_addr & ({`INT_ADDR_W{1'b1}}<<`CTRL_REGF_ADDR_W))) begin
	 ctrl_regf_req = rw_req;
         data_to_rd = ctrl_regf_data_to_rd;
      end else if (`DMA_BASE == (rw_addr & ({`INT_ADDR_W{1'b1}}<<`PERIPH_ADDR_W))) begin
	 dma_req = rw_req;
	 data_to_rd = dma_data_to_rd;
      end else if (`DIV_BASE == (rw_addr & ({`INT_ADDR_W{1'b1}}<<`PERIPH_ADDR_W))) begin
	 div_req = rw_req;
	 data_to_rd = div_data_to_rd;
      end else if (`PROG_MEM_BASE == (rw_addr & ({`INT_ADDR_W{1'b1}}<<`IADDR_W))) begin
         prog_mem_req = rw_req;
         data_to_rd = prog_mem_data_to_rd;
      end else if (`CONF_MEM_BASE == (rw_addr & ({`INT_ADDR_W{1'b1}}<<`CONF_MEM_ADDR_W))) begin
	 conf_mem_req = rw_req;
	 data_to_rd = `DATA_W'd0;
      end else if (`ENG_BASE == (rw_addr & ({`INT_ADDR_W{1'b1}}<<(`ENG_ADDR_W-1))) ) begin
         data_to_rd = eng_data_to_rd;
      end else if (`ENG_MEM_BASE == (rw_addr & ({`INT_ADDR_W{1'b1}}<<(`ENG_ADDR_W-1))) ) begin
         data_to_rd = eng_data_to_rd;
      end else if ( (rw_req &rw_req_delayed) == 1'b1) begin
	 $display("warning: requested unmapped address %x at time %f", rw_addr, $time);
      end
   end
   
endmodule
