//Description: performs unsigned  integer division
`timescale 1ns / 1ps
`include "xdefs.v"
`define WITH_REM
`define CLOG2_NBITS 5  // ceiling of log2 of NBITS

module xdiv(
	    input 		clk,
	    input 		clk_en,
	    input 		rst,
	    input 		div_type,//0-unsigned, 1-signed
	    input 		start,

	    input [`DATA_W-1:0] 	dividend,
	    input [`DATA_W-1:0] 	divisor,
	    output [`DATA_W-1:0] quotient,
	   `ifdef WITH_REM
	    output [`DATA_W-1:0] remainder,
	   `endif
	    output 		ready
	    );


   reg 		       run_reg, run_nxt;

`ifdef WITH_REM   
   reg [2*`DATA_W-1:0]  dividend_quotient_reg, dividend_quotient_nxt;
`endif

`ifndef WITH_REM   
   reg [2*`DATA_W-2:0]  dividend_quotient_reg, dividend_quotient_nxt;
`endif

   
   reg [`DATA_W-1:0]    divisor_reg, divisor_nxt;

   wire [`DATA_W-1:0]   subtraction;

   reg [`CLOG2_NBITS-1:0] count, count_nxt;

   wire 		  quotient_sign;

`ifdef WITH_REM
   wire 		  remainder_sign;
`endif

   assign ready = ~run_reg;
   assign quotient_sign = ((dividend[`DATA_W-1] ^ divisor[`DATA_W-1])) & div_type;
`ifdef WITH_REM
   assign remainder_sign = dividend[`DATA_W-1]  & div_type;  
   assign remainder = dividend_quotient_reg[2*`DATA_W-1:`DATA_W];
`endif
   assign quotient = dividend_quotient_reg[`DATA_W-1:0];
   assign subtraction = dividend_quotient_reg[2*`DATA_W-2:`DATA_W-1] - divisor_reg;

   always @ * begin
      run_nxt = run_reg;
      count_nxt = count + 1'b1;
      dividend_quotient_nxt = dividend_quotient_reg;
      divisor_nxt = divisor_reg;

      if(run_reg == 1'b 0) begin //not yet runninng 
	 if(start == 1'b 1) begin
	    count_nxt = 0;
            run_nxt = 1'b 1;
            dividend_quotient_nxt = 0;
            dividend_quotient_nxt[`DATA_W-1:0] = dividend;
	    if (dividend[`DATA_W-1] & div_type)
	      dividend_quotient_nxt[`DATA_W-1:0] = ~dividend+1'b1;
	    divisor_nxt = divisor;
	    if(divisor[`DATA_W-1] & div_type)
	      divisor_nxt = ~divisor+1'b1;	    
	 end
      end
      else begin //running 
	 if(subtraction[`DATA_W-1] == 1'b 1) 
`ifdef WITH_REM
	    //shift left, insert a 0 as quotient bit
            dividend_quotient_nxt = {dividend_quotient_reg[2*`DATA_W-2:0],1'b 0};
	 else 
	    //subtract divisor, shift left, insert a 1 as quotient bit
            dividend_quotient_nxt = {subtraction[`DATA_W-1:0],dividend_quotient_reg[`DATA_W-2:0],1'b 1};
`endif
`ifndef WITH_REM
	    //shift left, insert a 0 as quotient bit
            dividend_quotient_nxt = {dividend_quotient_reg[2*`DATA_W-3:0],1'b 0};
	 else 
	    //subtract divisor, shift left, insert a 1 as quotient bit
            dividend_quotient_nxt = {subtraction[`DATA_W-2:0],dividend_quotient_reg[`DATA_W-2:0],1'b 1};
`endif	 
	 //compute last iteration and sign of quotient and remainder
	 if((count == `CLOG2_NBITS'(`DATA_W - 1))) begin
//	 if((count == (`DATA_W - 1))) begin
            run_nxt = 1'b 0;
	    
	    if(quotient_sign)
	      if(subtraction[`DATA_W-1]) 
		//insert a 0 as quotient bit
		dividend_quotient_nxt[`DATA_W-1:0] = ~{dividend_quotient_reg[`DATA_W-2:0],1'b 0}+1;
	      else 
		//insert a 1 as quotient bit
		dividend_quotient_nxt[`DATA_W-1:0] = ~{dividend_quotient_reg[`DATA_W-2:0],1'b 1}+1;
	    else
	      if(subtraction[`DATA_W-1]) 
		//insert a 0 as quotient bit
		dividend_quotient_nxt[`DATA_W-1:0] = {dividend_quotient_reg[`DATA_W-2:0],1'b 0};
	      else 
		//insert a 1 as quotient bit
		dividend_quotient_nxt[`DATA_W-1:0] = {dividend_quotient_reg[`DATA_W-2:0],1'b 1};

`ifdef WITH_REM
	    if(remainder_sign)
	      if(subtraction[`DATA_W-1]) 
		dividend_quotient_nxt[2*`DATA_W-1:`DATA_W] = ~{dividend_quotient_reg[2*`DATA_W-2 -:`DATA_W]}+1;
	      else 
		dividend_quotient_nxt[2*`DATA_W-1:`DATA_W] = ~subtraction+1;
	    else 
	      if(subtraction[`DATA_W-1]) 
		dividend_quotient_nxt[2*`DATA_W-1:`DATA_W] = {dividend_quotient_reg[2*`DATA_W-2 -:`DATA_W]};
	      else 
		dividend_quotient_nxt[2*`DATA_W-1:`DATA_W] = subtraction;
`endif
	 end
      end
   end

   always @(posedge rst, posedge clk) begin
      if(rst)
	run_reg <= 0;
      else if (clk_en == 1'b1) begin 
	 count <= count_nxt;
	 run_reg <= run_nxt;
	 divisor_reg <= divisor_nxt;
	 dividend_quotient_reg <= dividend_quotient_nxt;
      end 
   end

endmodule
