/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */        

// ALU functions
`define ALU_ADD           4'd0
`define ALU_SUB           4'd1
`define ALU_CMP_SIG       4'd2    
`define ALU_MUX   	  4'd3
`define ALU_MAX       	  4'd4 		
`define ALU_MIN       	  4'd5
`define ALU_LOGIC_OR      4'd6
`define ALU_LOGIC_AND     4'd7
`define ALU_CMP_UNS       4'd8
`define ALU_LOGIC_XOR     4'd9
`define ALU_SEXT8         4'd10
`define ALU_SEXT16        4'd11
`define ALU_SHIFTR_ARTH   4'd12
`define ALU_SHIFTR_LOG    4'd13
`define ALU_CLZ           4'd14
`define ALU_ABS        	  4'd15
