/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps

`include "xdefs.v"
`include "xmem_map.v"

module xprog_mem(
		 input 			   clk,
		 input 			   rst,

		 input [`IADDR_W:0] 	   pc,
		 output 		   instr_valid,
		 output reg [`INSTR_W-1:0] instruction,

		 input 			   rw_req,
		 input 			   rw_rnw,
	 	 input [`IADDR_W-1:0] 	   rw_addr,
	 	 input [`DATA_W-1:0] 	   rw_data_in,
		 output [`DATA_W-1:0] 	   rw_data_out,

		 input 			   dma_req,
		 input 			   dma_rnw,
		 input [`IADDR_W-1:0] 	   dma_addr,
		 input [`DATA_W-1:0] 	   dma_data_in,
		 output [`DATA_W-1:0] 	   dma_data_out
		 );
   
   wire [`INSTR_W-1:0] 			   data_rom;
   wire [`INSTR_W-1:0] 			   data_mem;
   
   reg [`INSTR_W-1:0] 			   instruction_int;
   
   wire [`IADDR_W:0] 			   pc_rom;
   wire [`IADDR_W:0] 			   pc_instr_mem;
   
   wire 				   dma_we;
   
   assign dma_we = dma_req & ~dma_rnw;
   
   assign pc_rom = pc & ~`ROM_BASE;
   assign pc_instr_mem = pc & ~`PROG_MEM_BASE;
   
   assign instr_valid = 1'b1;
   
   always @ * begin
      instruction = `INSTR_W'd0;
      
      if ({1'b0,`ROM_BASE} == (pc & ({(`IADDR_W+1){1'b1}} << `ROM_ADDR_W))) begin
	 instruction = data_rom;
      end else if ({1'b0,`PROG_MEM_BASE} == (pc & ({(`IADDR_W+1){1'b1}} << `IADDR_W))) begin
	 instruction = data_mem;
      end else begin
	 $display("warning: program counter %x address invalid memory position at time %d ns", pc, $time);
      end
   end
   
   xboot_rom boot_rom(
		      .clk(clk),
		      .addr(pc_rom[`ROM_ADDR_W-1:0]),
		      .data(data_rom)
		      );
   
   xinstr_mem instr_mem(
			.clk(clk),
			.rst(rst),
			
			.pc(pc_instr_mem[`IADDR_W-1:0]),
			.instruction(data_mem),
			
	 		.rw_req(rw_req),
	 		.rw_rnw(rw_rnw),
	 		.rw_addr(rw_addr),
	 		.rw_data_in(rw_data_in),
			.rw_data_out(rw_data_out),

			.dma_we(dma_we),
			.dma_addr(dma_addr),
			.dma_data_in(dma_data_in),
			.dma_data_out(dma_data_out)
			);
   
endmodule
