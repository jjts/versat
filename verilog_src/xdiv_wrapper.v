/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: serial divider wrapper 
 
  Address map:
   0: start 
   1: ready
   2: dividend
   3: divisor
   4: quotient 
   5: remainder 
   6: type (0-unsigned, 1-signed)
 
   Copyright (C) 2016 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
 
 ***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module xdiv_wrapper
  
  (
   input 		    clk,
   input 		    rst,
	    
   //control bus interface
   input 		    rw_req,
   input 		    rw_rnw,
   input [2:0] 		    rw_addr,
   input [`DATA_W-1:0] 	    rw_data_to_wr,
   output reg [`DATA_W-1:0] rw_data_to_rd);
   
   reg 			    div_type;
   reg [`DATA_W-1:0] 	    dividend, divisor;
   reg [`DATA_W-1:0] 	    quotient, remainder;
   wire [`DATA_W-1:0] 	    quotient_int, remainder_int;
   
   wire 	    start;
   wire 	    ready;
   reg 		    ready_reg;
   
   //instantiate serial divider
   xdiv div (
	     .clk(clk),
	     .clk_en(1'b1),
	     .rst(rst),
	     .div_type(div_type),
	     .start(start),

	     .dividend(dividend),
	     .divisor(divisor),
	     .quotient(quotient_int),
	     .remainder(remainder_int),
    
	     .ready(ready)
	     );

   //start division
   assign start = (rw_req == 1'b1 && rw_addr == 2'd0);
   
   //read process
   always @* begin
	if (rw_addr == 3'd4)
	  rw_data_to_rd = quotient;
	else if (rw_addr == 3'd5)
	  rw_data_to_rd = remainder;
	else
	  rw_data_to_rd = {{(`DATA_W-1){1'b0}},ready};
   end
   
   always @ (posedge clk) begin
      ready_reg <= ready;
      if(ready & ~ready_reg) begin
	 quotient <= quotient_int;
	 remainder <= remainder_int; 
      end 
      //write process
      if(rw_req & ~rw_rnw) begin
	 if(rw_addr == 3'd2)
	   dividend <= rw_data_to_wr;
	 else if (rw_addr == 3'd3)
	   divisor <= rw_data_to_wr;
	 else if (rw_addr == 3'd6)
	   div_type <= rw_data_to_wr[0];
      end
   end
endmodule
