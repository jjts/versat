/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: 

 Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
            Andre Lopes <andre.a.lopes@netcabo.pt>
            Francisco Nunes <ftcnunes@gmail.com>
 ***************************************************************************** */

`timescale 1ns / 1ps

`include "xdefs.v"
`include "xmem_map.v"

module xeng_addr_decoder (
			  input 		  rw_req,
			  input [`INT_ADDR_W-1:0] rw_addr,

			  output reg 		  ctrl_req,
			  output reg 		  status_req,
			  output reg [`N-4:0] 	  fu_regf_req,
			  output reg [`Nmem-1:0]  mem_req
			  );

   always @ * begin : addr_decoder

      reg [`INT_ADDR_W-1:0] i;

      ctrl_req = 1'b0;
      status_req = 1'b0;
      mem_req = `Nmem'b0;
      fu_regf_req = {`N-3{1'b0}};

      if (rw_addr == `ENG_CTRL_REG)
	ctrl_req = rw_req;
      else if (rw_addr == `ENG_STATUS_REG) 
	status_req = rw_req;
      else if (rw_addr < `ENG_MEM_BASE) begin
	 for (i = 0; i < (`N-3); i = i + 1)
	   fu_regf_req[`N-4-i] = (rw_addr == (i + `ENG_FU_BASE)) ? rw_req : 1'b0;
      end else begin
	 if (rw_addr < `ENG_MEM_BASE + 2**`DADDR_W)
	   mem_req[0] = rw_req;
	 else if (rw_addr < `ENG_MEM_BASE + 2*2**`DADDR_W)
	   mem_req[1] = rw_req;
	 else if (rw_addr < `ENG_MEM_BASE + 3*2**`DADDR_W)
	   mem_req[2] = rw_req;
	 else if (rw_addr < `ENG_MEM_BASE + 4*2**`DADDR_W)
	   mem_req[3] = rw_req;
      end
   end

endmodule
