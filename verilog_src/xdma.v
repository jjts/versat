/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

 Author(s): Joao Dias lopes <joao.d.lopes91@gmail.com>
 
 ***************************************************************************** */

`timescale 1ns / 1ps

`include "xdefs.v"
`include "xmem_map.v"

`define SIZE_W	   8

module xdma #
            (
	     parameter integer C_M_AXI_THREAD_ID_WIDTH = 1,
	     parameter integer C_M_AXI_ADDR_WIDTH = 32,
	     parameter integer C_M_AXI_DATA_WIDTH = 32,
	     //parameter integer C_M_AXI_AWUSER_WIDTH = 1,
	     //parameter integer C_M_AXI_ARUSER_WIDTH = 1,
	     //parameter integer C_M_AXI_WUSER_WIDTH = 1,
	     //parameter integer C_M_AXI_RUSER_WIDTH = 1,
	     //parameter integer C_M_AXI_BUSER_WIDTH = 1,
	     parameter integer C_M_AXI_SUPPORTS_WRITE = 1,
	     parameter integer C_M_AXI_SUPPORTS_READ = 1,
	     parameter integer C_INTERCONNECT_M_AXI_WRITE_ISSUING = 8,
	     //parameter C_M_AXI_TARGET = 'h00000000,
	     parameter integer C_OFFSET_WIDTH = 9
            )
            (
	    input 				      clk,
	    input 				      rst,
	    
	    //r/w interface
	    input 				      rw_req,
	    input 				      rw_rnw,
	    input [`INT_ADDR_W-1:0] 		      rw_addr,
	    input [`DATA_W-1:0] 		      data_in,
	    output reg [`DATA_W-1:0] 		      data_out,
	    
	    //external port
	    output wire [C_M_AXI_THREAD_ID_WIDTH-1:0] M_AXI_AWID,
	    output wire [C_M_AXI_ADDR_WIDTH-1:0]      M_AXI_AWADDR,
	    output wire [8-1:0] 		      M_AXI_AWLEN,
	    output wire [3-1:0] 		      M_AXI_AWSIZE,
	    output wire [2-1:0] 		      M_AXI_AWBURST,
	    output wire 			      M_AXI_AWLOCK,
	    output wire [4-1:0] 		      M_AXI_AWCACHE,
	    output wire [3-1:0] 		      M_AXI_AWPROT,
	    // AXI3 output wire [4-1:0]                  M_AXI_AWREGION,
	    output wire [4-1:0] 		      M_AXI_AWQOS,
	    //output wire [C_M_AXI_AWUSER_WIDTH-1:0]    M_AXI_AWUSER,*
	    output reg 				      M_AXI_AWVALID,
	    input wire 				      M_AXI_AWREADY,
    
	    // Master Interface Write Data
	    // AXI3 output wire [C_M_AXI_THREAD_ID_WIDTH-1:0]     M_AXI_WID,
	    output wire [C_M_AXI_DATA_WIDTH-1:0]      M_AXI_WDATA,
	    output wire [C_M_AXI_DATA_WIDTH/8-1:0]    M_AXI_WSTRB,
	    output reg 				      M_AXI_WLAST,
	    //output wire [C_M_AXI_WUSER_WIDTH-1:0]     M_AXI_WUSER,*
	    output reg 				      M_AXI_WVALID,
	    input wire 				      M_AXI_WREADY,
    
	    // Master Interface Write Response
	    input wire [C_M_AXI_THREAD_ID_WIDTH-1:0]  M_AXI_BID,
	    input wire [2-1:0] 			      M_AXI_BRESP,
	    //input wire [C_M_AXI_BUSER_WIDTH-1:0]      M_AXI_BUSER,*
	    input wire 				      M_AXI_BVALID,
	    output reg 				      M_AXI_BREADY,
    
	    // Master Interface Read Address
	    output wire [C_M_AXI_THREAD_ID_WIDTH-1:0] M_AXI_ARID,
	    output wire [C_M_AXI_ADDR_WIDTH-1:0]      M_AXI_ARADDR,
	    output wire [8-1:0] 		      M_AXI_ARLEN,
	    output wire [3-1:0] 		      M_AXI_ARSIZE,
	    output wire [2-1:0] 		      M_AXI_ARBURST,
	    output wire  		              M_AXI_ARLOCK,
	    output wire [4-1:0] 		      M_AXI_ARCACHE,
	    output wire [3-1:0] 		      M_AXI_ARPROT,
	    // AXI3 output wire [4-1:0] 		 M_AXI_ARREGION,
	    output wire [4-1:0] 		      M_AXI_ARQOS,
	    //output wire [C_M_AXI_ARUSER_WIDTH-1:0]    M_AXI_ARUSER,*
	    output reg 				      M_AXI_ARVALID,
	    input wire 				      M_AXI_ARREADY,
	    
	    // Master Interface Read Data 
	    input wire [C_M_AXI_THREAD_ID_WIDTH-1:0]  M_AXI_RID,
	    input wire [C_M_AXI_DATA_WIDTH-1:0]       M_AXI_RDATA,
	    input wire [2-1:0] 			      M_AXI_RRESP,
	    input wire 				      M_AXI_RLAST,
	    //input wire [C_M_AXI_RUSER_WIDTH-1:0]      M_AXI_RUSER,*
	    input wire 				      M_AXI_RVALID,
	    output reg 				      M_AXI_RREADY,
	    
	    //internal port
	    output reg 				      int_req,
	    output reg 				      int_rnw,
	    output [`INT_ADDR_W-1:0] 		      int_addr,
	    output [`DATA_W-1:0] 		      int_data_out,
	    input [`DATA_W-1:0] 		      int_data_in
	    );
   
   parameter IDLE=3'h0, STANDBY=3'h1,EXT2INT=3'h2,INT2EXT=3'h3,W_RESPONSE=3'h4;
   
   wire 			     we;
   
   wire [2-1:0] 		     status;
   reg 				     status_int;
   
   reg 				     init;
   wire 			     start;
   
   reg [`DATA_W-1:0] 		     ext_addr_reg, ext_addr_reg2;
   reg [`INT_ADDR_W-1:0] 	     int_addr_reg, int_addr_reg2;
   reg [`SIZE_W-1:0] 		     size_reg, size_reg2;
   reg [2-1:0] 			     direction_reg, direction_reg2;
   
   reg [`SIZE_W:0] 		     counter_int;
   reg [`SIZE_W:0] 		     counter_int_nxt;
   
   reg [3-1:0] 			     state;
   reg [3-1:0] 			     state_nxt;
   
   reg 				     error;
   reg 				     error_nxt;

   reg 				     M_AXI_WVALID_int, M_AXI_WVALID_reg;
   reg 				     M_AXI_WLAST_int, M_AXI_WLAST_reg;
   
   //Constant signals
   assign M_AXI_ARID = 1'b0;
   assign M_AXI_ARLOCK = 1'b0;
   assign M_AXI_ARCACHE = 4'h2;
   assign M_AXI_ARPROT = 3'b010;
   assign M_AXI_ARQOS = 4'h0;
   
   assign M_AXI_AWID = 1'b0;
   assign M_AXI_AWLOCK = 1'b0;
   assign M_AXI_AWCACHE = 4'h2;
   assign M_AXI_AWPROT = 3'b010;
   assign M_AXI_AWQOS = 4'h0;
   
   assign M_AXI_ARADDR = ext_addr_reg2;
   assign M_AXI_ARLEN = size_reg2;
   assign M_AXI_ARSIZE = 3'b010;
   assign M_AXI_ARBURST = 2'b01;
   assign int_data_out = M_AXI_RDATA;
   
   assign M_AXI_AWADDR = ext_addr_reg2;
   assign M_AXI_AWLEN = size_reg2;
   assign M_AXI_AWSIZE = 3'b010;
   assign M_AXI_AWBURST = 2'b01;
   assign M_AXI_WSTRB = 4'hF;
   
   assign int_addr = int_addr_reg2 + counter_int;
   
   assign we = ~rw_rnw & rw_req;
   assign status = {error,status_int};
   
   assign start = (we == 1'b1 && rw_addr == `DMA_START_ADDR);
   
   //Command Registers
   always @ (posedge rst, posedge clk)
     if (rst) begin
	ext_addr_reg <= `DATA_W'h0;
	int_addr_reg <= `INT_ADDR_W'h0;
	size_reg <= `SIZE_W'h0;
	direction_reg <= 2'h0;
     end else begin
	
	case (rw_addr)
	  
	  `DMA_EXT_ADDR_ADDR: begin
	     if (we)
	       ext_addr_reg <= data_in;
	     
	     data_out <= ext_addr_reg;
	  end
	  `DMA_INT_ADDR_ADDR: begin
	     if (we)
	       int_addr_reg <= data_in[`INT_ADDR_W-1:0];
	     
	     data_out <= {{(`DATA_W-`INT_ADDR_W){1'b0}},int_addr_reg};
	  end
	  `DMA_SIZE_ADDR: begin
	     if (we)
	       size_reg <= data_in[`SIZE_W-1:0] - 1'b1;
	     
	     data_out <= {{(`DATA_W-`SIZE_W){1'b0}},size_reg};
	  end
	  `DMA_DIRECTION_ADDR: begin
	     if (we)
	       direction_reg <= data_in[2-1:0];
	     
	     data_out <= {{(`DATA_W-2){1'b0}},direction_reg};
	  end
	  `DMA_STATUS_ADDR:
	    data_out <= {{(`DATA_W-2){1'b0}},status};
	  
	  default:
	    data_out <= {{(`DATA_W-2){1'b0}},status};
	  
	endcase
     end

   //Shadow registers
   always @ (posedge rst, posedge clk)
     if (rst) begin
	ext_addr_reg2 <= `DATA_W'h0;
	int_addr_reg2 <= `INT_ADDR_W'h0;
	size_reg2 <= `SIZE_W'h0;
	direction_reg2 <= 2'h0;
     end else if(start) begin
	ext_addr_reg2 <= ext_addr_reg;
	int_addr_reg2 <= int_addr_reg;
	size_reg2 <= size_reg;
	direction_reg2 <= direction_reg;
     end

   assign M_AXI_WDATA = int_data_in;

   //delays
   always @ (posedge rst, posedge clk)
     if (rst) begin
	M_AXI_WVALID_reg <= 0;
	M_AXI_WVALID <= 0;
	
	M_AXI_WLAST_reg <= 0;
	M_AXI_WLAST <= 0;
     end else begin
	M_AXI_WVALID_reg <= M_AXI_WVALID_int;
	M_AXI_WVALID <= M_AXI_WVALID_reg;
	
	M_AXI_WLAST_reg <= M_AXI_WLAST_int;
	M_AXI_WLAST <= M_AXI_WLAST_reg;
     end
   
   //Counter registers
   always @ (posedge clk)
     if (init) begin
	counter_int <= {(`SIZE_W+1){1'b0}};
     end else begin
	counter_int <= counter_int_nxt;
     end
   
   //Error register
   always @ (posedge rst, posedge clk)
     if (rst) begin 
	error <= 1'b0;
     end else begin
	error <= error_nxt;
     end
   
   //State register
   always @ (posedge rst, posedge clk)
     if (rst) begin 
	state <= 3'b00;
     end else begin
	state <= state_nxt;
     end
   
   //State machine
   always @ * begin
      state_nxt = state;
      status_int = 1'b0;
      
      init = 1'b0;
      
      int_req = 1'b0;
      int_rnw = 1'b1;
      
      M_AXI_ARVALID = 1'b0;
      M_AXI_AWVALID = 1'b0;
      M_AXI_RREADY = 1'b0;
      M_AXI_WVALID_int = 1'b0;
      M_AXI_WLAST_int = 1'b0;
      M_AXI_BREADY = 1'b1;
      
      error_nxt = error;
      
      counter_int_nxt = counter_int;
      
      case (state)
	IDLE: begin
	   if (start == 1'b1) begin
	      init = 1'b1;
	      state_nxt = STANDBY;
	   end
	end
	STANDBY: begin
	   if (direction_reg2 == 2'b01) begin
	      if (M_AXI_ARREADY == 1'b1)
		state_nxt = EXT2INT;
	      
	      M_AXI_ARVALID = 1'b1;
	   end else if (direction_reg2 == 2'b10) begin
	      if (M_AXI_AWREADY == 1'b1)
		state_nxt = INT2EXT;
	      
	      M_AXI_AWVALID = 1'b1;
	   end
	   
	   status_int = 1'b1;
	end
	EXT2INT: begin
	   if (counter_int == {{1'b0},size_reg2}) begin
	      if (M_AXI_RLAST == 1'b1)
		error_nxt = 1'b0;
	      else
		error_nxt = 1'b1;
	      
	      state_nxt = IDLE;
	   end
	   
	   M_AXI_RREADY = 1'b1;
	   
	   if (M_AXI_RVALID == 1'b1) begin
	      int_req = 1'b1;
	      int_rnw = 1'b0;
	      
	      counter_int_nxt = counter_int + 1'b1;
	   end
	   
	   status_int = 1'b1;
	end
	INT2EXT: begin
	   
	   if (counter_int == {{1'b0},size_reg2}) begin
	      M_AXI_WLAST_int = 1'b1;
	      
	      state_nxt = W_RESPONSE;
	   end
	   
	   if (M_AXI_WREADY == 1'b1) begin
	      M_AXI_WVALID_int = 1'b1;
	      
	      int_req = 1'b1;
	      
	      counter_int_nxt = counter_int + 1'b1;
	   end
	   
	   status_int = 1'b1;
	end
	W_RESPONSE: begin
	   
	   if (M_AXI_BVALID == 1'b1) begin
	      if (M_AXI_BRESP == 2'b00)
		error_nxt = 1'b0;
	      else
		error_nxt = 1'b1;
	      
	      state_nxt = IDLE;
	   end
	   
	   status_int = 1'b1;
	end
      endcase
   end
   
endmodule
