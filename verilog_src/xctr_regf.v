/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: 

 Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>

 ***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module xctr_regf (
		  input 			clk,

		  input 			ext_we,
		  input [`CTRL_REGF_ADDR_W-1:0] ext_addr,
		  input [`DATA_W-1:0] 		ext_data_in,
		  output [`DATA_W-1:0] 		ext_data_out,

		  input 			int_req,
		  input 			int_rw_rnw,
		  input [`CTRL_REGF_ADDR_W-1:0] int_addr,
		  input [`DATA_W-1:0] 		int_data_in,
		  output [`DATA_W-1:0] 		int_data_out
		  );


   // Implementation as 2-port distributed RAM needs two reg files
   reg [`DATA_W-1:0] 				ctrl_reg_1 [2**`CTRL_REGF_ADDR_W-1:0];
   reg [`DATA_W-1:0] 				ctrl_reg_2 [2**`CTRL_REGF_ADDR_W-1:0];

   wire 					int_we;
   wire [`DATA_W-1:0] 				data_in;
   wire [`CTRL_REGF_ADDR_W-1:0] 		addr;
   wire 					wr_en;

   assign int_we = int_req & ~int_rw_rnw;
   assign wr_en = ext_we | int_we;
   assign addr = (ext_we == 1'b1) ? ext_addr : int_addr;
   assign data_in = (ext_we == 1'b1) ? ext_data_in : int_data_in;

   assign ext_data_out = ctrl_reg_1[ext_addr];
   assign int_data_out = ctrl_reg_2[int_addr];

   always @ (posedge clk) begin
      if (wr_en) begin
	 ctrl_reg_1[addr] <= data_in;
	 ctrl_reg_2[addr] <= data_in;
      end
   end

endmodule
