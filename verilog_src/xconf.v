/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias lopes <joao.d.lopes91@gmail.com>
             Francisco Nunes <ftcnunes@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps

`include "xdefs.v"
`include "xmem_map.v"

module xconf (
	      input 				       clk,
	      input 				       rst,

	      // RW bus interface
	      input 				       rw_req,
	      input 				       rw_rnw,
	      input [`INT_ADDR_W-1:0] 		       rw_addr,
	      input [`DADDR_W-1:0] 		       rw_data,

	      // dma interface to conf memory
    	      input 				       dma_req,
	      input 				       dma_rnw,
	      input [`CONF_ADDR_W+`N_CONF_SLICE_W-1:0] dma_addr,
	      input [`DATA_W-1:0] 		       dma_data_in,
	      output reg [`DATA_W-1:0] 		       dma_data_out,
	      // configuration output to data engine
	      output [`CONFIG_BITS-1:0] 	       conf_out
	      );

   wire 					       conf_ld;
   wire [`CONFIG_BITS-1:0] 			       conf_from_mem;
   wire [`CONFIG_BITS-1:0] 			       conf_to_mem;

   reg  					       conf_reg_req;
   reg  					       conf_mem_req;

   wire [`DATA_W-1:0] 				       dma_data_out_int;

   //assign output
   assign conf_out = conf_to_mem;
   
   //decode address
   always @ * begin
      conf_mem_req = 1'b0;
      conf_reg_req = 1'b0;
      if (rw_req == 1'b1)  
	 if (rw_addr < `CLEAR_CONFIG_ADDR) begin
	    conf_mem_req = 1'b1;
	    conf_reg_req = 1'b0;
	 end else begin
	    conf_mem_req = 1'b0;
	    conf_reg_req = 1'b1;
	 end 
   end
   
   //delay
   always @ (posedge rst, posedge clk) 
     if (rst) begin
	dma_data_out <= `DATA_W'd0;
     end else begin
	dma_data_out <= dma_data_out_int;
     end
   
   //instantiate configuration register 
   xconf_reg xconf_reg (
			.clk(clk),
			.rst(rst),

			.conf_req(conf_reg_req),
			.conf_rnw(rw_rnw),
			.conf_addr(rw_addr),
			.conf_word(rw_data),

			.conf_in(conf_from_mem),
			.conf_ld(conf_ld),
			.conf_out(conf_to_mem)

			);

   //instantiate configuration memory
   xconf_mem conf_mem (
		       .clk(clk),
		       .rst(rst),

		       .dma_req(dma_req),
		       .dma_rnw(dma_rnw),
		       .dma_addr(dma_addr),
		       .dma_data_in(dma_data_in),
		       .dma_data_out(dma_data_out_int),

		       .conf_req(conf_mem_req),
		       .conf_rnw(rw_rnw),
		       .conf_addr(rw_addr[`CONF_ADDR_W-1:0]),

		       .conf_data_in(conf_to_mem),
		       .conf_data_out(conf_from_mem),
		       .conf_ld(conf_ld)
		       );

endmodule
