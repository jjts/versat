/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: 

 Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
            Joao Dias lopes <joao.d.lopes91@gmail.com>
 
 ***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"

module xinstr_mem (
		   // Control 
		   input 		    clk,
		   input 		    rst,
		   
		   // Controller interface
		   input [`IADDR_W-1:0]     pc,
		   output [`INSTR_W-1:0]    instruction,
		   
		   input 		    rw_req,
		   input 		    rw_rnw,
		   input [`IADDR_W-1:0]     rw_addr,
		   input [`DATA_W-1:0] 	    rw_data_in,
		   output [`DATA_W-1:0]     rw_data_out,
		   
		   input 		    dma_we,
		   input [`IADDR_W-1:0]     dma_addr,
		   input [`DATA_W-1:0] 	    dma_data_in,
		   output reg [`DATA_W-1:0] dma_data_out
		   );
   
`ifndef ASIC
   reg [`INSTR_W-1:0] 			    mem [2**`IADDR_W-1:0];
   reg [`INSTR_W-1:0] 			    instruction_int;
   wire [`INSTR_W-1:0] 			    rw_data_in_int;
   wire [`INSTR_W-1:0] 			    dma_data_in_int;
   wire [`INSTR_W-1:0] 			    data_in;
   reg [`INSTR_W-1:0] 			    data_out_int;
`else
   wire [`DATA_W-1:0] 			    instruction_int;
   wire [`DATA_W-1:0] 			    rw_data_in_int;
   wire [`DATA_W-1:0] 			    dma_data_in_int;
   wire [`DATA_W-1:0] 			    data_in;
   wire [`DATA_W-1:0] 			    data_out_int;
`endif
   
   wire [`DATA_W-1:0] 			    data_out_int2;
   
   wire [`IADDR_W-1:0] 			    addr;
   
   wire 				    we, wnr, wrA, en;
   
   wire [`DATA_W-1:0] 			    data_inA;
   
   assign en = 1'b1;
   assign wrA = 1'b0;
   
   assign data_inA = `DATA_W'd0;
   
   assign wnr = rw_req & ~rw_rnw;
   
   assign addr = (rw_req)? rw_addr : dma_addr;
   assign we = (rw_req)? wnr : dma_we;
   
`ifndef ASIC
   assign instruction = instruction_int;
   assign rw_data_in_int = rw_data_in[`INSTR_W-1:0];
   assign dma_data_in_int = dma_data_in[`INSTR_W-1:0];
   assign data_out_int2 = {{(`DATA_W-`INSTR_W){data_out_int[`INSTR_W-1]}},data_out_int};
   assign rw_data_out = {{(`DATA_W-`INSTR_W){data_out_int[`INSTR_W-1]}},data_out_int};
`else
   assign instruction = instruction_int[`INSTR_W-1:0];
   assign rw_data_in_int = rw_data_in;
   assign dma_data_in_int = dma_data_in;
   assign data_out_int2 = data_out_int;
   assign rw_data_out = data_out_int;
`endif
   
   assign data_in = (rw_req)? rw_data_in_int : dma_data_in_int;
   
   always @ (posedge rst, posedge clk) 
     if (rst) begin
	dma_data_out <= `DATA_W'd0;
     end else begin
	dma_data_out <= data_out_int2;
     end
   
`ifdef XILINX
   //instruction port
   always @(posedge clk) begin			
      if (en) begin
	 instruction_int <= mem[pc];
      end
   end
   //data port
   always @(posedge clk) begin			
      if (en) begin
	 if (we)
           mem[addr] <= data_in;
	 data_out_int <= mem[addr];
      end
   end
`endif // !`ifdef XILINX
   
`ifdef ALTERA
   //instruction port
   always @(posedge clk) begin
      instruction_int <= mem[pc];
   end   
   //data port
   always @(posedge clk) begin			
      if (we)
        mem[addr] <= data_in;
      data_out_int <= mem[addr];
   end
`endif // !`ifdef ALTERA

`ifdef ASIC
   
   xmem_wrapper2048x32 memory_wrapper (
				       .clk(clk),
				       
				       //instruction port
				       .enA(en),
				       .wrA(wrA),
				       .addrA(pc),
				       .data_inA(data_inA),
				       .data_outA(instruction_int),
				       
				       //data port
				       .enB(en),
				       .wrB(we),
				       .addrB(addr),
				       .data_inB(data_in),
				       .data_outB(data_out_int)
				       );
`endif //  `ifdef ASIC
   
endmodule
