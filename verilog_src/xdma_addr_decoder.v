/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: 

 Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>

 ***************************************************************************** */

`timescale 1ns / 1ps

`include "xdefs.v"
`include "xmem_map.v"

module xdma_addr_decoder (
		      input 		       clk,
		      input 		       rst,
		      
		      input 		       dma_req,
		      input [`INT_ADDR_W-1:0]  dma_addr,
		      output reg [`DATA_W-1:0] dma_data_out,

		      output reg 	       prog_mem_req, 
		      input [`DATA_W-1:0]      prog_data_to_rd,

		      output reg 	       eng_req,
		      input [`DATA_W-1:0]      eng_data_to_rd,

		      output reg 	       conf_mem_req, 
		      input [`DATA_W-1:0]      conf_data_to_rd

		     );
   
   reg [`INT_ADDR_W-1:0] 		       dma_addr_reg;
   reg [`INT_ADDR_W-1:0] 		       dma_addr_int;
   
   // dma address register
   always @ (posedge rst, posedge clk) begin
      if (rst) begin
	 dma_addr_reg <= `INT_ADDR_W'd0;
	 dma_addr_int <= `INT_ADDR_W'd0;
      end else begin
	 dma_addr_reg <= dma_addr;
	 dma_addr_int <= dma_addr_reg;
      end
   end
   
   //compute requests
   always @ * begin 

      conf_mem_req = 1'b0;
      eng_req = 1'b0;
      prog_mem_req = 1'b0;

      if (`PROG_MEM_BASE == (dma_addr & ({`INT_ADDR_W{1'b1}}<<`IADDR_W))) begin
	 prog_mem_req = dma_req;
      end else if (`CONF_MEM_BASE == (dma_addr & ({`INT_ADDR_W{1'b1}}<<`CONF_MEM_ADDR_W))) begin
         conf_mem_req = dma_req;
      end else if (`ENG_MEM_BASE == (dma_addr & ({`INT_ADDR_W{1'b1}}<<(`ENG_ADDR_W-1)))) begin
	 eng_req = dma_req;
      end else begin
	 $display("warning: requested unmapped dma address %x at time %f", dma_addr, $time);
      end
   end 

   //compute dma_data_out
   always @ * begin 

      dma_data_out = 0;
      
      if (`PROG_MEM_BASE == (dma_addr_int & ({`INT_ADDR_W{1'b1}}<<`IADDR_W))) begin
	 dma_data_out = prog_data_to_rd;	   
      end else if (`CONF_MEM_BASE == (dma_addr_int & ({`INT_ADDR_W{1'b1}}<<`CONF_MEM_ADDR_W))) begin
	 dma_data_out = conf_data_to_rd;
      end else if (`ENG_MEM_BASE == (dma_addr_int & ({`INT_ADDR_W{1'b1}}<<(`ENG_ADDR_W-1)))) begin
	 dma_data_out = eng_data_to_rd;
      end else begin
         $display("warning: requested unmapped dma address %x at time %f", dma_addr_int, $time);
      end
   end 

endmodule
