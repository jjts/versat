/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: 

 Copyright (C) 2014 Authors

 Author(s): Joao Dias lopes <joao.d.lopes91@gmail.com>

 ***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"


module xmem_wrapper64x32 (
			  //control
			  input 		   clk,
			  
			  //port
			  input 		   en,
			  input 		   wr,
			  input [`CONF_ADDR_W-1:0] addr,
			  input [`DATA_W-1:0] 	   data_in,
			  output [`DATA_W-1:0] 	   data_out
	    );


   function [`CONF_ADDR_W-1:0] reverseAddrBits;    
      input [`CONF_ADDR_W-1:0] 			   word;             
      integer 					   i;
      
      begin
	 for (i=0; i < `CONF_ADDR_W; i=i+1)
	   reverseAddrBits[i]=word[`CONF_ADDR_W-1 - i];         
      end                           
   endfunction    

   function [`DATA_W-1:0] reverseDataBits;    
      input [`DATA_W-1:0] 		    word;             
      integer 				    i;
      
      begin
	 for (i=0; i < `DATA_W; i=i+1)
	   reverseDataBits[i]=word[`DATA_W-1 - i];         
      end                           
   endfunction    
   
   wire DO0,DO1,DO2,DO3,DO4,DO5,DO6,DO7,DO8,DO9,DO10,DO11,DO12,
	DO13,DO14,DO15,DO16,DO17,DO18,DO19,
	DO20,DO21,DO22,DO23,DO24,DO25,DO26,
	DO27,DO28,DO29,DO30,DO31,CK,OE;
   

   reg A0,A1,A2,A3,A4,A5,DI0,DI1,DI2,DI3,DI4,DI5,DI6,
	DI7,DI8,DI9,DI10,DI11,DI12,DI13,
	DI14,DI15,DI16,DI17,DI18,DI19,DI20,
	DI21,DI22,DI23,DI24,DI25,DI26,DI27,
	DI28,DI29,DI30,DI31,CSB,WEB;
   
   assign CK = clk;
   assign OE = 1'b1;

   assign data_out = reverseDataBits(
				     {DO0,DO1,DO2,DO3,DO4,DO5,DO6,DO7,DO8,DO9,DO10,DO11,
				      DO12,DO13,DO14,DO15,DO16,DO17,DO18,DO19,DO20,DO21,
				      DO22,DO23,DO24,DO25,DO26,DO27,DO28,DO29,DO30,DO31});
   
   always @ * begin 
      CSB = #1 0;
      WEB = #1 ~(wr&en);
      {A0,A1,A2,A3,A4,A5} = #1 reverseAddrBits(addr);
      {DI0,DI1,DI2,DI3,DI4,DI5,DI6,DI7,DI8,DI9,DI10,DI11,DI12,DI13,
	   DI14,DI15,DI16,DI17,DI18,DI19,DI20,DI21,DI22,DI23,DI24,DI25,
	   DI26,DI27,DI28,DI29,DI30,DI31} = #1 reverseDataBits(data_in);
   end
   
   SEHD130_64X32X1CM4 asic_mem (
				.A0(A0),.A1(A1),.A2(A2),.A3(A3),.A4(A4),.A5(A5),.DO0(DO0),.DO1(DO1),.DO2(DO2),.DO3(DO3),.DO4(DO4),.DO5(DO5),.DO6(DO6),
				.DO7(DO7),.DO8(DO8),.DO9(DO9),.DO10(DO10),.DO11(DO11),.DO12(DO12),.DO13(DO13),.DO14(DO14),.DO15(DO15),
				.DO16(DO16),.DO17(DO17),.DO18(DO18),.DO19(DO19),.DO20(DO20),.DO21(DO21),.DO22(DO22),.DO23(DO23),
				.DO24(DO24),.DO25(DO25),.DO26(DO26),.DO27(DO27),.DO28(DO28),.DO29(DO29),.DO30(DO30),.DO31(DO31),
				.DI0(DI0),.DI1(DI1),.DI2(DI2),.DI3(DI3),.DI4(DI4),.DI5(DI5),.DI6(DI6),.DI7(DI7),.DI8(DI8),.DI9(DI9),
				.DI10(DI10),.DI11(DI11),.DI12(DI12),.DI13(DI13),.DI14(DI14),.DI15(DI15),.DI16(DI16),.DI17(DI17),
				.DI18(DI18),.DI19(DI19),.DI20(DI20),.DI21(DI21),.DI22(DI22),.DI23(DI23),.DI24(DI24),.DI25(DI25),
				.DI26(DI26),.DI27(DI27),.DI28(DI28),.DI29(DI29),.DI30(DI30),.DI31(DI31),.CK(CK),.WEB(WEB),.CSB(CSB),.OE(OE)
				);
   
endmodule
