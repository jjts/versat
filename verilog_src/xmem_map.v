/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: memory map as viewed from the xctrl block

   Copyright (C) 2014 Authors

  Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
             Joao Dias lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

//Index
`define CTRL_REGF_BASE `INT_ADDR_W'd16
`define DMA_BASE `INT_ADDR_W'd128
`define DIV_BASE `INT_ADDR_W'd256
`define PERIPH_ADDR_W 3
`define ROM_BASE `IADDR_W'd1024
`define PROG_MEM_BASE `INT_ADDR_W'd2048
`define CONF_MEM_BASE `INT_ADDR_W'd4096
`define CONF_MEM_ADDR_W 11
`define ENG_BASE `INT_ADDR_W'd8192
`define ENG_ADDR_W (`DADDR_W+3)

//Control register file
`define R0 `CTRL_REGF_BASE
`define R1 (`R0+1'b1)
`define R2 (`R1+1'b1)
`define R3 (`R2+1'b1)
`define R4 (`R3+1'b1)
`define R5 (`R4+1'b1)
`define R6 (`R5+1'b1)
`define R7 (`R6+1'b1)
`define R8 (`R7+1'b1)
`define R9 (`R8+1'b1)
`define R10 (`R9+1'b1)
`define R11 (`R10+1'b1)
`define R12 (`R11+1'b1)
`define R13 (`R12+1'b1)
`define R14 (`R13+1'b1)
`define R15 (`R14+1'b1)

//DMA
`define DMA_EXT_ADDR_ADDR `DMA_BASE
`define DMA_INT_ADDR_ADDR (`DMA_EXT_ADDR_ADDR+1'b1)
`define DMA_SIZE_ADDR (`DMA_INT_ADDR_ADDR+1'b1)
`define DMA_DIRECTION_ADDR (`DMA_SIZE_ADDR+1'b1)
`define DMA_START_ADDR (`DMA_DIRECTION_ADDR+1'b1)
`define DMA_STATUS_ADDR (`DMA_START_ADDR+1'b1)

//DIV
`define  DIV_START_ADDR `DIV_BASE
`define  DIV_READY_ADDR (`DIV_START_ADDR+1'b1)
`define  DIV_DIVIDEND_ADDR (`DIV_READY_ADDR+1'b1)
`define  DIV_DIVISOR_ADDR (`DIV_DIVIDEND_ADDR+1'b1)
`define  DIV_QUOTIENT_ADDR (`DIV_DIVISOR_ADDR+1'b1)
`define  DIV_REMAINDER_ADDR (`DIV_QUOTIENT_ADDR+1'b1)
`define  DIV_TYPE_ADDR (`DIV_REMAINDER_ADDR+1'b1)

//Configuration module

//MEM configuration offsets
`define MEM_CONF_ITER_OFFSET `INT_ADDR_W'd0
`define MEM_CONF_PER_OFFSET `INT_ADDR_W'd1
`define MEM_CONF_DUTY_OFFSET `INT_ADDR_W'd2
`define MEM_CONF_SELA_OFFSET `INT_ADDR_W'd3
`define MEM_CONF_START_OFFSET `INT_ADDR_W'd4
`define MEM_CONF_SHIFT_OFFSET `INT_ADDR_W'd5
`define MEM_CONF_INCR_OFFSET `INT_ADDR_W'd6
`define MEM_CONF_DELAY_OFFSET `INT_ADDR_W'd7
`define MEM_CONF_RVRS_OFFSET `INT_ADDR_W'd8
`define MEM_CONF_EXT_OFFSET `INT_ADDR_W'd9
`define MEM_CONF_OFFSET `INT_ADDR_W'd10
//ALU configuration offsets
`define ALU_CONF_SELA_OFFSET `INT_ADDR_W'd0
`define ALU_CONF_SELB_OFFSET `INT_ADDR_W'd1
`define ALU_CONF_FNS_OFFSET `INT_ADDR_W'd2
`define ALU_CONF_OFFSET `INT_ADDR_W'd3
//ALU lite configuration offsets
`define ALU_LITE_CONF_SELA_OFFSET `INT_ADDR_W'd0
`define ALU_LITE_CONF_SELB_OFFSET `INT_ADDR_W'd1
`define ALU_LITE_CONF_FNS_OFFSET `INT_ADDR_W'd2
`define ALU_LITE_CONF_OFFSET `INT_ADDR_W'd3
//MULT configuration offsets
`define MUL_CONF_SELA_OFFSET `INT_ADDR_W'd0
`define MUL_CONF_SELB_OFFSET `INT_ADDR_W'd1
`define MUL_CONF_LONHI_OFFSET `INT_ADDR_W'd2
`define MUL_CONF_DIV2_OFFSET `INT_ADDR_W'd3
`define MUL_CONF_OFFSET `INT_ADDR_W'd4
//BS configuration offsets
`define BS_CONF_SELA_OFFSET `INT_ADDR_W'd0
`define BS_CONF_SELB_OFFSET `INT_ADDR_W'd1
`define BS_CONF_LNA_OFFSET `INT_ADDR_W'd2
`define BS_CONF_LNR_OFFSET `INT_ADDR_W'd3
`define BS_CONF_OFFSET `INT_ADDR_W'd4

// configuration memory is at `CONF_MEM_BASE

//configuration register
`define CONF_REG_BASE (`CONF_MEM_BASE + `INT_ADDR_W'd1024)
`define CLEAR_CONFIG_ADDR (`CONF_REG_BASE - `INT_ADDR_W'd1)
`define MEM0A_CONFIG_ADDR `CONF_REG_BASE
`define MEM0B_CONFIG_ADDR (`MEM0A_CONFIG_ADDR+`MEM_CONF_OFFSET)
`define MEM1A_CONFIG_ADDR (`MEM0B_CONFIG_ADDR+`MEM_CONF_OFFSET) 
`define MEM1B_CONFIG_ADDR (`MEM1A_CONFIG_ADDR+`MEM_CONF_OFFSET) 
`define MEM2A_CONFIG_ADDR (`MEM1B_CONFIG_ADDR+`MEM_CONF_OFFSET)
`define MEM2B_CONFIG_ADDR (`MEM2A_CONFIG_ADDR+`MEM_CONF_OFFSET)
`define MEM3A_CONFIG_ADDR (`MEM2B_CONFIG_ADDR+`MEM_CONF_OFFSET)
`define MEM3B_CONFIG_ADDR (`MEM3A_CONFIG_ADDR+`MEM_CONF_OFFSET)
`define ALU0_CONFIG_ADDR  (`MEM3B_CONFIG_ADDR+`MEM_CONF_OFFSET)
`define ALU1_CONFIG_ADDR  (`ALU0_CONFIG_ADDR+`ALU_CONF_OFFSET)
`define ALU_LITE0_CONFIG_ADDR  (`ALU1_CONFIG_ADDR+`ALU_CONF_OFFSET)
`define ALU_LITE1_CONFIG_ADDR  (`ALU_LITE0_CONFIG_ADDR+`ALU_LITE_CONF_OFFSET)
`define ALU_LITE2_CONFIG_ADDR  (`ALU_LITE1_CONFIG_ADDR+`ALU_LITE_CONF_OFFSET)
`define ALU_LITE3_CONFIG_ADDR  (`ALU_LITE2_CONFIG_ADDR+`ALU_LITE_CONF_OFFSET)
`define MULT0_CONFIG_ADDR (`ALU_LITE3_CONFIG_ADDR+`ALU_LITE_CONF_OFFSET)
`define MULT1_CONFIG_ADDR (`MULT0_CONFIG_ADDR+`MUL_CONF_OFFSET)
`define MULT2_CONFIG_ADDR (`MULT1_CONFIG_ADDR+`MUL_CONF_OFFSET)
`define MULT3_CONFIG_ADDR (`MULT2_CONFIG_ADDR+`MUL_CONF_OFFSET)
`define BS0_CONFIG_ADDR (`MULT3_CONFIG_ADDR+`MUL_CONF_OFFSET)


//Data engine
`define ENG_CTRL_REG `ENG_BASE
`define ENG_STATUS_REG `ENG_BASE+1'b1
`define ENG_FU_BASE (`ENG_BASE+2**`N_W) 

//Data engine: FUs
`define MEM0A `ENG_FU_BASE
`define MEM0B (`MEM0A+1)
`define MEM1A (`MEM0B+1)
`define MEM1B (`MEM1A+1)
`define MEM2A (`MEM1B+1)
`define MEM2B (`MEM2A+1)
`define MEM3A (`MEM2B+1)
`define MEM3B (`MEM3A+1)
`define ALU0 (`MEM3B+1)
`define ALU1 (`ALU0+1)
`define ALULITE0 (`ALU1+1)
`define ALULITE1 (`ALULITE0+1)
`define ALULITE2 (`ALULITE1+1)
`define ALULITE3 (`ALULITE2+1)
`define MULT0 (`ALULITE3+1)
`define MULT1 (`MULT0+1)
`define MULT2 (`MULT1+1)
`define MULT3 (`MULT2+1)
`define BS0 (`MULT3+1)

//Data engine memories
`define ENG_MEM_BASE 2*`ENG_BASE
`define ENG_MEM0 `ENG_MEM_BASE
`define ENG_MEM1 `ENG_MEM0+`INT_ADDR_W'd2048
`define ENG_MEM2 `ENG_MEM1+`INT_ADDR_W'd2048
`define ENG_MEM3 `ENG_MEM2+`INT_ADDR_W'd2048
