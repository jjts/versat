/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

  Copyright (C) 2014 Authors

  Author(s): Joao Dias Lopes <joao.d.lopes91@gmail.com>
 
***************************************************************************** */

`timescale 1ns/1ps

`include "xdefs.v"
`include "xmem_map.v"

module xtop_axi_tb #
  (
   parameter integer C_S_AXI_ADDR_WIDTH = 32,
   parameter integer C_S_AXI_DATA_WIDTH = 32,
   parameter C_S_AXI_MIN_SIZE = 32'h000001FF,
   parameter integer C_USE_WSTRB = 0,
   parameter C_DPHASE_TIMEOUT = 8,
   parameter C_BASEADDR = 32'hFFFFFFFF,
   parameter C_HIGHADDR = 32'h00000000,
   parameter integer C_NUM_REG = 1,
   parameter integer C_NUM_MEM = 1,
   parameter integer C_SLV_AWIDTH = 32,
   parameter integer C_SLV_DWIDTH = 32,
   
   parameter integer C_M_AXI_THREAD_ID_WIDTH = 1,
   parameter integer C_M_AXI_ADDR_WIDTH = 32,
   parameter integer C_M_AXI_DATA_WIDTH = 32,
   parameter integer C_M_AXI_SUPPORTS_WRITE = 1,
   parameter integer C_M_AXI_SUPPORTS_READ = 1,
   parameter integer C_INTERCONNECT_M_AXI_WRITE_ISSUING = 8,
   parameter integer C_OFFSET_WIDTH = 9
   );

   //parameters 
   parameter clk_period = 6;
   parameter clk_period_div2 = 3;
   //parameter NTRANSF = 256;
   parameter IMEM_SIZE = 2**`IADDR_W;
   parameter DDR_SIZE = 41*2**20;
   
   // System Signals
   reg 		     clk;
   reg 		     rst;

   // AXI4 Lite
   // Slave Interface Write Address Ports
   reg [C_S_AXI_ADDR_WIDTH-1:0] S_AXI_AWADDR;
   reg 				S_AXI_AWVALID;
   wire 			S_AXI_AWREADY;
   
   // Slave Interface Write Data Ports
   reg [C_S_AXI_DATA_WIDTH-1:0]   S_AXI_WDATA;
   reg [C_S_AXI_DATA_WIDTH/8-1:0] S_AXI_WSTRB;
   reg 				  S_AXI_WVALID;
   wire 			  S_AXI_WREADY;
   
   // Slave Interface Write Response Ports
   wire [2-1:0] 		  S_AXI_BRESP;
   wire 			  S_AXI_BVALID;
   reg 				  S_AXI_BREADY;
   
   
   // Slave Interface Read Address Ports
   reg [C_S_AXI_ADDR_WIDTH-1:0]   S_AXI_ARADDR;
   reg 				  S_AXI_ARVALID;
   wire 			  S_AXI_ARREADY;
   
   // Slave Interface Read Data Ports
   wire [C_S_AXI_DATA_WIDTH-1:0]  S_AXI_RDATA;
   wire [2-1:0] 		  S_AXI_RRESP;
   wire 			  S_AXI_RVALID;
   reg 				  S_AXI_RREADY;
   
   // AXI4 Full
   // Master Interface Write Address
   wire [C_M_AXI_THREAD_ID_WIDTH-1:0] M_AXI_AWID;
   wire [C_M_AXI_ADDR_WIDTH-1:0]      M_AXI_AWADDR;
   wire [8-1:0] 		      M_AXI_AWLEN;
   wire [3-1:0] 		      M_AXI_AWSIZE;
   wire [2-1:0] 		      M_AXI_AWBURST;
   wire 			      M_AXI_AWLOCK;
   wire [4-1:0] 		      M_AXI_AWCACHE;
   wire [3-1:0] 		      M_AXI_AWPROT;
   wire [4-1:0] 		      M_AXI_AWQOS;
   wire 			      M_AXI_AWVALID;
   reg 				      M_AXI_AWREADY;
   
   // Master Interface Write Data
   wire [C_M_AXI_DATA_WIDTH-1:0]      M_AXI_WDATA;
   wire [C_M_AXI_DATA_WIDTH/8-1:0]    M_AXI_WSTRB;
   wire 			      M_AXI_WLAST;
   wire 			      M_AXI_WVALID;
   reg 				      M_AXI_WREADY;
   
   // Master Interface Write Response
   reg [C_M_AXI_THREAD_ID_WIDTH-1:0]  M_AXI_BID;
   reg [2-1:0] 			      M_AXI_BRESP;
   reg 				      M_AXI_BVALID;
   wire 			      M_AXI_BREADY;
   
   // Master Interface Read Address
   wire [C_M_AXI_THREAD_ID_WIDTH-1:0] M_AXI_ARID;
   wire [C_M_AXI_ADDR_WIDTH-1:0]      M_AXI_ARADDR;
   wire [8-1:0] 		      M_AXI_ARLEN;
   wire [3-1:0] 		      M_AXI_ARSIZE;
   wire [2-1:0] 		      M_AXI_ARBURST;
   wire 			      M_AXI_ARLOCK;
   wire [4-1:0] 		      M_AXI_ARCACHE;
   wire [3-1:0] 		      M_AXI_ARPROT;
   wire [4-1:0] 		      M_AXI_ARQOS;
   wire 			      M_AXI_ARVALID;
   reg 				      M_AXI_ARREADY;
   
   // Master Interface Read Data 
   reg [C_M_AXI_THREAD_ID_WIDTH-1:0]  M_AXI_RID;
   reg [C_M_AXI_DATA_WIDTH-1:0]       M_AXI_RDATA;
   reg [2-1:0] 			      M_AXI_RRESP;
   reg 				      M_AXI_RLAST;
   reg 				      M_AXI_RVALID;
   wire 			      M_AXI_RREADY;

   `include "axi_tasks.v"
   
   // DMA parameters
   reg [`DATA_W-1:0] ext_addr;
   reg [`DATA_W-1:0] int_addr;
   reg [`DATA_W-1:0] size;
   reg [`DATA_W-1:0] direction;

   reg [`DATA_W-1:0]   addr;
   reg [`DATA_W-1:0]   data_in;
   
   reg [80*8:1] mem_str;
   
   wire 	ctr_timer_en;
   integer 	i, k, mem_used, fp, start_time, ctr_timer, NTRANSF;
   reg 		FINISH;
   

   // Data Bank
   reg [`INSTR_W-1:0] prog [IMEM_SIZE-1:0];
   reg [`DATA_W-1:0] data [DDR_SIZE-1:0];
   
   // Instantiate the control reg_file
   xtop uut (
	     // AXI4 Lite
	     // System Signals
	     .S_AXI_ACLK(clk),
	     .S_AXI_ARESETN(~rst),
	     
	     // Slave Interface Write Address Ports
	     .S_AXI_AWADDR(S_AXI_AWADDR),
	     .S_AXI_AWVALID(S_AXI_AWVALID),
	     .S_AXI_AWREADY(S_AXI_AWREADY),
	     
	     // Slave Interface Write Data Ports
	     .S_AXI_WDATA(S_AXI_WDATA),
	     .S_AXI_WSTRB(S_AXI_WSTRB),
	     .S_AXI_WVALID(S_AXI_WVALID),
	     .S_AXI_WREADY(S_AXI_WREADY),
	     
	     // Slave Interface Write Response Ports
	     .S_AXI_BRESP(S_AXI_BRESP),
	     .S_AXI_BVALID(S_AXI_BVALID),
	     .S_AXI_BREADY(S_AXI_BREADY),
	     
	     
	     // Slave Interface Read Address Ports
	     .S_AXI_ARADDR(S_AXI_ARADDR),
	     .S_AXI_ARVALID(S_AXI_ARVALID),
	     .S_AXI_ARREADY(S_AXI_ARREADY),
	     
	     // Slave Interface Read Data Ports
	     .S_AXI_RDATA(S_AXI_RDATA),
	     .S_AXI_RRESP(S_AXI_RRESP),
	     .S_AXI_RVALID(S_AXI_RVALID),
	     .S_AXI_RREADY(S_AXI_RREADY),
	     
	     // AXI4 Full
	     // System Signals
	     .M_AXI_ACLK(clk),
	     .M_AXI_ARESETN(~rst),
	     
	     // Master Interface Write Address
	     .M_AXI_AWID(M_AXI_AWID),
	     .M_AXI_AWADDR(M_AXI_AWADDR),
	     .M_AXI_AWLEN(M_AXI_AWLEN),
	     .M_AXI_AWSIZE(M_AXI_AWSIZE),
	     .M_AXI_AWBURST(M_AXI_AWBURST),
	     .M_AXI_AWLOCK(M_AXI_AWLOCK),
	     .M_AXI_AWCACHE(M_AXI_AWCACHE),
	     .M_AXI_AWPROT(M_AXI_AWPROT),
	     .M_AXI_AWQOS(M_AXI_AWQOS),
	     .M_AXI_AWVALID(M_AXI_AWVALID),
	     .M_AXI_AWREADY(M_AXI_AWREADY),
	     
	     // Master Interface Write Data
	     .M_AXI_WDATA(M_AXI_WDATA),
	     .M_AXI_WSTRB(M_AXI_WSTRB),
	     .M_AXI_WLAST(M_AXI_WLAST),
	     .M_AXI_WVALID(M_AXI_WVALID),
	     .M_AXI_WREADY(M_AXI_WREADY),
	     
	     // Master Interface Write Response
	     .M_AXI_BID(M_AXI_BID),
	     .M_AXI_BRESP(M_AXI_BRESP),
	     .M_AXI_BVALID(M_AXI_BVALID),
	     .M_AXI_BREADY(M_AXI_BREADY),
	     
	     // Master Interface Read Address
	     .M_AXI_ARID(M_AXI_ARID),
	     .M_AXI_ARADDR(M_AXI_ARADDR),
	     .M_AXI_ARLEN(M_AXI_ARLEN),
	     .M_AXI_ARSIZE(M_AXI_ARSIZE),
	     .M_AXI_ARBURST(M_AXI_ARBURST),
	     .M_AXI_ARLOCK(M_AXI_ARLOCK),
	     .M_AXI_ARCACHE(M_AXI_ARCACHE),
	     .M_AXI_ARPROT(M_AXI_ARPROT),
	     .M_AXI_ARQOS(M_AXI_ARQOS),
	     .M_AXI_ARVALID(M_AXI_ARVALID),
	     .M_AXI_ARREADY(M_AXI_ARREADY),
	     
	     // Master Interface Read Data 
	     .M_AXI_RID(M_AXI_RID),
	     .M_AXI_RDATA(M_AXI_RDATA),
	     .M_AXI_RRESP(M_AXI_RRESP),
	     .M_AXI_RLAST(M_AXI_RLAST),
	     .M_AXI_RVALID(M_AXI_RVALID),
	     .M_AXI_RREADY(M_AXI_RREADY)
	     );
   
   initial begin
      
      // Global reset of FPGA
      #100
      
`ifdef DEBUG
      $dumpfile("xtop_axi.vcd");
      $dumpvars();
`endif
      
      S_AXI_AWADDR = 0;
      S_AXI_AWVALID = 0;
      S_AXI_WDATA = 0;
      S_AXI_WSTRB = 0;
      S_AXI_WVALID = 0;
      S_AXI_BREADY = 0;
      S_AXI_ARADDR = 0;
      S_AXI_ARVALID = 0;
      S_AXI_RREADY = 0;
      
      M_AXI_AWREADY = 0;
      M_AXI_WREADY = 0;
      M_AXI_BID = 0;
      M_AXI_BRESP = 2'b00;
      M_AXI_BVALID = 0;
      M_AXI_ARREADY = 0;
      M_AXI_RID = 0;
      M_AXI_RDATA = `DATA_W'd0;
      M_AXI_RRESP = 2'b00;
      M_AXI_RLAST = 0;
      M_AXI_RVALID = 0;
      
      // Initialize Inputs
      clk = 0;
      rst = 0;
      
      // Global reset
      #(clk_period_div2+0.3)
      rst = 1;
      
      #clk_period
      rst = 0;

      NTRANSF = 256;

      FINISH = 0;
      
      //
      // Load program in prog mem
      //
      
      ext_addr = `DATA_W'hA7400000;
      int_addr = `DATA_W'h800;
      size = NTRANSF;
      direction = `DATA_W'd1;
      
      $readmemh("./opcode.hex", prog, 0, (IMEM_SIZE-1));
      
      for (i=0; i<(IMEM_SIZE/NTRANSF); i = i+1) begin 
	 dma_conf_axi(ext_addr,int_addr,size,direction);
	 
	 //
	 // Emulate BUS AXI4 signals
	 //
	 
	 M_AXI_ARREADY = 1;
	 
	 #clk_period;
	 
	 M_AXI_ARREADY = 0;
	 
	 #(6*clk_period);
	 
	 M_AXI_RVALID = 1;
	 
	 for (k = 0; k < NTRANSF; k=k+1) begin
	    M_AXI_RDATA = prog[i*NTRANSF+k];
	    
	    if (k == (NTRANSF-1))
	      M_AXI_RLAST = 1;
	    
	    #clk_period;
	 end
	 
	 M_AXI_ARREADY = 1;
	 M_AXI_RVALID = 0;
	 M_AXI_RLAST = 0;
	 
	 int_addr = int_addr + NTRANSF;
      end

      //
      // Load data in external memory
      //
      
      fp = $fopen("./mem_in.hex","r");
      
      for(k=0; !$feof(fp); k=k+1) begin
	 if($fscanf(fp, "%x\n", data[k]) == 0) begin
	    $display("error: could not read \"mem_in.hex\" file.");
	    $finish;
	 end
      end
      
      $fclose(fp);
      
      mem_used = k;
      
      // Initialize registers
      for (k=mem_used-1; (mem_used-k) < 16; k=k-1) begin
	 addr = `DATA_W'd 16-(mem_used-k);
	 data_in = data[k];	 
	 
	 write_data_axi(addr,data_in);
      end
      
      //
      // Run program
      //
      
      // Write run command into R14
      addr = `DATA_W'd14;
      data_in = `BR_RUN;
      
      write_data_axi(addr,data_in);
      
      // Write program address into R0
      addr = `DATA_W'd0;
      data_in = `PROG_MEM_BASE;
      
      write_data_axi(addr,data_in);
      
      start_time = $time;
      
      //
      // Emulate BUS AXI4 signals
      //
      
      // Wait for versat to exit from bootrom
      #(11*clk_period);
      M_AXI_ARREADY = 0;
   
      // Wait for versat to return to bootrom
      while (FINISH == 0 ) begin
	 addr = `DATA_W'd0;
	 read_data_axi(addr,data_in);
	 FINISH = (data_in == 0);
	 
	 if (M_AXI_ARVALID == 1'b1) begin
	    i = (M_AXI_ARADDR/4);
	    NTRANSF = M_AXI_ARLEN + 1;
	    M_AXI_ARREADY = 1;
	    
	    #clk_period;
	    
	    M_AXI_ARREADY = 0;
	    
	    #(6*clk_period);
	    
	    M_AXI_RVALID = 1;
	    
	    for (k = 0; k < NTRANSF; k=k+1) begin
	       M_AXI_RDATA = data[i+k];
	       
	       if (k == (NTRANSF-1))
		 M_AXI_RLAST = 1;
	       
	       #clk_period;
	    end
	    
	    M_AXI_RVALID = 0;
	    M_AXI_RLAST = 0;
	 end

	 if (M_AXI_AWVALID == 1'b1) begin
	    i = (M_AXI_AWADDR/4);
	    NTRANSF = M_AXI_AWLEN + 1;
	    mem_used = mem_used + NTRANSF;
	    M_AXI_AWREADY = 1;
	    
	    #clk_period;
	    
	    M_AXI_AWREADY = 0;
	    
	    #(6*clk_period);
	    
	    M_AXI_WREADY = 1;
	    
	    for (k = 0; k < NTRANSF; k=k+0) begin

	       #(clk_period_div2);
   `ifdef PR
	       #(clk_period_div2);
   `endif 
	       if (M_AXI_WVALID == 1'b1) begin
		  data[i+k] = M_AXI_WDATA;
		  k=k+1;
	       end
   `ifndef PR	       
	       #(clk_period_div2);
   `endif 
	       
	    end
	    
	    M_AXI_BVALID = 1;
	    
	    #clk_period;
	    
	    M_AXI_BVALID = 0;
	    M_AXI_WREADY = 0;
	    
	    #clk_period;
	 end
	 
	 #clk_period;
      end
      
      $display("Total Clocks: %0d",($time-start_time)/clk_period);
      $display("Controller Clocks: %0d", ctr_timer);
      
      // Read registers
      for (k=mem_used-1; (mem_used-k) < 16; k=k-1) begin
	 addr = `DATA_W'd 16-(mem_used-k);
	 read_data_axi(addr,data_in);
	 data[k] = data_in;
      end
      
      fp = $fopen("./mem_out.hex","w");
      
      for (k=0; k < mem_used; k=k+1)
	$fwrite(fp,"%8h\n",data[k]);
      
      $fclose(fp);
      
      // Simulation time
      #clk_period $finish;
   end

`ifndef	SYNTH
   //Measure unhidden controller time
   assign ctr_timer_en = (& uut.data_eng.mem_done) & (uut.pc >= ({1'b0,`ROM_BASE} + 2**`ROM_ADDR_W)) & ~uut.dma.status_int & uut.div_wrapper.ready;

   always @ (posedge clk) begin
      if(rst == 1)
	ctr_timer = 0;
      else if (ctr_timer_en)
	ctr_timer = ctr_timer+1;
   end
`endif

   always 
     #(clk_period_div2) clk = ~clk;

   wire [`DATA_W-1:0] r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15;

`ifndef	SYNTH
   assign r0 = uut.ctrl_regf.ctrl_reg_1[0];
   assign r1 = uut.ctrl_regf.ctrl_reg_1[1];
   assign r2 = uut.ctrl_regf.ctrl_reg_1[2];
   assign r3 = uut.ctrl_regf.ctrl_reg_1[3];
   assign r4 = uut.ctrl_regf.ctrl_reg_1[4];
   assign r5 = uut.ctrl_regf.ctrl_reg_1[5];
   assign r6 = uut.ctrl_regf.ctrl_reg_1[6];
   assign r7 = uut.ctrl_regf.ctrl_reg_1[7];
   assign r8 = uut.ctrl_regf.ctrl_reg_1[8];
   assign r9 = uut.ctrl_regf.ctrl_reg_1[9];
   assign r10 = uut.ctrl_regf.ctrl_reg_1[10];
   assign r11 = uut.ctrl_regf.ctrl_reg_1[11];
   assign r12 = uut.ctrl_regf.ctrl_reg_1[12];
   assign r13 = uut.ctrl_regf.ctrl_reg_1[13];
   assign r14 = uut.ctrl_regf.ctrl_reg_1[14];
   assign r15 = uut.ctrl_regf.ctrl_reg_1[15];
`endif

endmodule
