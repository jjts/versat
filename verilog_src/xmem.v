/* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: 

 Copyright (C) 2014 Authors

 Author(s): Jose T. de Sousa <jose.t.de.sousa@gmail.com>
 Joao Dias Lopes <joao.d.lopes91@gmail.com>
 Andre Lopes <andre.a.lopes@netcabo.pt>

 ***************************************************************************** */

`timescale 1ns / 1ps
`include "xdefs.v"


module xmem(

	    //control 
	    input 			    clk,
	    input 			    rst,
	    input 			    initA,
	    input 			    initB,
	    input 			    runA,
	    input 			    runB,
	    output 			    doneA,
	    output 			    doneB,

	    //controller interface
	    input 			    rw_addrA_req,
	    input 			    rw_addrB_req,

	    input 			    rw_mem_req,
	    input 			    rw_rnw,
	    input [`DADDR_W-1:0] 	    rw_addr,
	    input [`DATA_W-1:0] 	    rw_data_to_wr,

	    //dma interface
 	    input 			    dma_rnw,
	    input [`DADDR_W-1:0] 	    dma_addr,
	    input [`DATA_W-1:0] 	    dma_data_in,
	    input 			    dma_mem_req,

	    //input / output data
	    input [`N*`DATA_W-1:0] 	    data_in_bus,
	    output [`DATA_W-1:0] 	    outA,
	    output [`DATA_W-1:0] 	    outB,

	    //configurations
	    input [2*`MEMP_CONFIG_BITS-1:0] config_bits

	    );

   function [`DADDR_W-1:0] reverseBits;    
      input [`DADDR_W-1:0] 		    word;             
      integer 				    i;
      
      begin
	 for (i=0; i < `DADDR_W; i=i+1)
	   reverseBits[i]=word[`DADDR_W-1 - i];         
      end                           
   endfunction    
   
   wire wrA, wrB;
   
   //memory
`ifndef ASIC
   reg [`DATA_W-1:0] mem [(2**`DADDR_W)-1:0];
`endif
   
   //port addresses and enables 
   wire [`DADDR_W-1:0] addrA, addrA_int, addrA_int2;
   wire [`DADDR_W-1:0] addrB, addrB_int, addrB_int2;
   
`ifdef XILINX
   wire 	       enA, enB;
`endif
   
   //data inputs
   wire [`DATA_W-1:0]  inA;
   wire [`DATA_W-1:0]  inB;

   //configuration
   wire [`PERIOD_W-1:0] perA;
   wire [`PERIOD_W-1:0] dutyA;
   wire [`DADDR_W-1:0] 	iterationsA;
   
   wire [`N_W-1:0] 	selA;
   wire [`DADDR_W-1:0] 	startA;
   wire [`DADDR_W-1:0] 	shiftA;
   wire [`DADDR_W-1:0] 	incrA;
   wire [`PERIOD_W-1:0] delayA;
   wire 		reverseA;
   wire 		extA;	
   
   wire [`PERIOD_W-1:0] perB;
   wire [`PERIOD_W-1:0] dutyB;
   wire [`DADDR_W-1:0] 	iterationsB;
   
   wire [`N_W-1:0] 	selB;
   wire [`DADDR_W-1:0] 	startB;
   wire [`DADDR_W-1:0] 	shiftB;
   wire [`DADDR_W-1:0] 	incrB;
   wire [`PERIOD_W-1:0] delayB;
   wire 		reverseB;	
   wire 		extB;	
   
   wire 		in_enA, in_enB, enA_int, enB_int;
`ifdef ASIC
   wire 		enA, enB;
   wire [`DATA_W-1:0] 	outA_int, outB_int;
`else    
   reg [`DATA_W-1:0] 	outA_int, outB_int;
`endif
   
   wire [`DATA_W-1:0] 	data_to_wrA;
   wire [`DATA_W-1:0] 	data_to_wrB;

   //unpack config data
   assign iterationsA = config_bits[2*`MEMP_CONFIG_BITS-1 -: `DADDR_W];
   assign perA = config_bits[2*`MEMP_CONFIG_BITS-`DADDR_W-1 -: `PERIOD_W];
   assign dutyA = config_bits[2*`MEMP_CONFIG_BITS-`DADDR_W-`PERIOD_W-1 -: `PERIOD_W];   
   assign selA = config_bits[2*`MEMP_CONFIG_BITS-`DADDR_W-2*`PERIOD_W-1 -: `N_W];
   assign startA = config_bits[2*`MEMP_CONFIG_BITS-`DADDR_W-2*`PERIOD_W-`N_W-1 -: `DADDR_W];
   assign shiftA = config_bits[2*`MEMP_CONFIG_BITS-`DADDR_W-2*`PERIOD_W-`N_W-`DADDR_W-1 -: `DADDR_W];
   assign incrA = config_bits[2*`MEMP_CONFIG_BITS-`DADDR_W-2*`PERIOD_W-`N_W-2*`DADDR_W-1 -: `DADDR_W];
   assign delayA = config_bits[2*`MEMP_CONFIG_BITS-`DADDR_W-2*`PERIOD_W-`N_W-3*`DADDR_W-1 -: `PERIOD_W];
   assign reverseA = config_bits[2*`MEMP_CONFIG_BITS-`DADDR_W-2*`PERIOD_W-`N_W-3*`DADDR_W-`PERIOD_W-1 -: 1];
   assign extA = config_bits[2*`MEMP_CONFIG_BITS-`DADDR_W-2*`PERIOD_W-`N_W-3*`DADDR_W-`PERIOD_W-1-1 -: 1];
   
   assign iterationsB = config_bits[`MEMP_CONFIG_BITS-1 -: `DADDR_W];
   assign perB = config_bits[`MEMP_CONFIG_BITS-`DADDR_W-1 -: `PERIOD_W];
   assign dutyB = config_bits[`MEMP_CONFIG_BITS-`DADDR_W-`PERIOD_W-1 -: `PERIOD_W];
   assign selB = config_bits[`MEMP_CONFIG_BITS-`DADDR_W-2*`PERIOD_W-1 -: `N_W];
   assign startB = config_bits[`MEMP_CONFIG_BITS-`DADDR_W-2*`PERIOD_W-`N_W-1 -: `DADDR_W];
   assign shiftB = config_bits[`MEMP_CONFIG_BITS-`DADDR_W-2*`PERIOD_W-`N_W-`DADDR_W-1 -: `DADDR_W];
   assign incrB = config_bits[`MEMP_CONFIG_BITS-`DADDR_W-2*`PERIOD_W-`N_W-2*`DADDR_W-1 -: `DADDR_W];
   assign delayB = config_bits[`MEMP_CONFIG_BITS-`DADDR_W-2*`PERIOD_W-`N_W-3*`DADDR_W-1 -: `PERIOD_W];
   assign reverseB = config_bits[`MEMP_CONFIG_BITS-`DADDR_W-2*`PERIOD_W-`N_W-3*`DADDR_W-`PERIOD_W-1 -: 1];
   assign extB = config_bits[`MEMP_CONFIG_BITS-`DADDR_W-2*`PERIOD_W-`N_W-3*`DADDR_W-`PERIOD_W-1-1 -: 1];


   //compute enables
`ifdef XILINX
   assign enA = 1'b1;
   assign enB = 1'b1;
`endif
   
`ifdef ASIC
   assign enA = enA_int|dma_mem_req;
   assign enB = enB_int|rw_mem_req;
`endif
   
   assign wrA = dma_mem_req? ~dma_rnw : (enA_int & in_enA & ~extA);
   assign wrB = rw_mem_req? ~rw_rnw : (enB_int & in_enB & ~extB);

   assign outA = (rw_addrA_req == 1'b1 || selA == `saddr)? 
		 {{(`DATA_W-`DADDR_W){addrA_int2[`DADDR_W-1]}}, addrA_int2} : outA_int;
   assign outB = (rw_addrB_req == 1'b1 || selB == `saddr)? 
		 {{(`DATA_W-`DADDR_W){addrB_int2[`DADDR_W-1]}}, addrB_int2}: outB_int;
   
   assign data_to_wrA = dma_mem_req? dma_data_in : inA ;
   assign data_to_wrB = rw_mem_req?  rw_data_to_wr : inB;
   
   
   //input selection 
   xinmux muxa (
		.sel(selA),
		.data_in_bus(data_in_bus),
		.data_out(inA),
		.enabled(in_enA)
		);	
   
   xinmux  muxb (
		 .sel(selB),
		 .data_in_bus(data_in_bus),
		 .data_out(inB),
		 .enabled(in_enB)		
		 );

   //address generators
   xaddrgen addrgenA (
		      .clk(clk),
		      .rst(rst),
		      .init(initA),
		      .run(runA),
		      .iterations(iterationsA), 
		      .period(perA), 
		      .duty(dutyA), 
		      .start(startA),
		      .shift(shiftA),
		      .incr(incrA),
		      .delay(delayA), 
		      .addr(addrA_int), 
		      .mem_en(enA_int), 
		      .done(doneA)
		      );

   xaddrgen addrgenB (
		      .clk(clk),
		      .rst(rst),
		      .init(initB),
		      .run(runB),
		      .iterations(iterationsB), 
		      .period(perB), 
		      .duty(dutyB), 
		      .start(startB),
		      .shift(shiftB),
		      .incr(incrB),
		      .delay(delayB), 
		      .addr(addrB_int), 
		      .mem_en(enB_int), 
		      .done(doneB)
		      );

`ifdef XDATA_ENG_IF 
   initial
     $readmemh("xmemdata.hex", mem, 0, 156);
`endif
   
   assign addrA = dma_mem_req? dma_addr[`DADDR_W-1:0] : extB? inB[`DADDR_W-1:0] : addrA_int2;
   assign addrB = rw_mem_req? rw_addr : extA? inA[`DADDR_W-1:0] : addrB_int2;
   
   assign addrA_int2 = reverseA? reverseBits(addrA_int) : addrA_int;
   assign addrB_int2 = reverseB? reverseBits(addrB_int) : addrB_int;
   
   //memory implementations
`ifdef XILINX
   //PORT A
   always @ (posedge clk) begin
      if (enA) begin
	 if (wrA)
           mem[addrA] <= data_to_wrA;
	 outA_int <= mem[addrA];
      end
   end
   
   //PORT B
   always @ (posedge clk) begin
      if (enB) begin
	 if (wrB)
           mem[addrB] <= data_to_wrB;
	 outB_int <= mem[addrB];
      end
   end
`endif //  `ifdef XILINX
   
`ifdef ALTERA
   //PORT A
   always @ (posedge clk) begin 
      if (wrA)
        mem[addrA] <= data_to_wrA;
      outA_int <= mem[addrA];
   end
   
   //PORT B
   always @ (posedge clk) begin			
      if (wrB)
        mem[addrB] <= data_to_wrB;
      outB_int <= mem[addrB];
   end
`endif //  `ifdef ALTERA

`ifdef ASIC
   
   xmem_wrapper2048x32 memory_wrapper (
				       .clk(clk),
				       //PORT A
				       .enA(enA),
				       .wrA(wrA),
				       .addrA(addrA),
				       .data_inA(data_to_wrA),
				       .data_outA(outA_int),

				       //PORT B
				       .enB(enB),
				       .wrB(wrB),
				       .addrB(addrB),
				       .data_inB(data_to_wrB),
				       .data_outB(outB_int)		
				       );
`endif //  `ifdef ASIC

endmodule
