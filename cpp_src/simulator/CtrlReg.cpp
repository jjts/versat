#include <iostream>
#include <stdio.h>
#include <string.h>
#include "DataEngine.hpp"
#include <math.h>


	int ctrlReg::get(std::list<int> componentList, int init) { //create init/run mask

		this->ctrlMask = 0;
		for (std::list<int>::iterator it = componentList.begin(); it != componentList.end(); it++) { //iterator
			this->ctrlMask = this->ctrlMask + (int) pow((double)2, (double)CTRL_BIT_SIZE-*it);   }
		if(init == 1)  this->ctrlMask = this->ctrlMask+1;
		else this->ctrlMask = this->ctrlMask+2;
		return this->ctrlMask;

}


	int ctrlReg::check(std::list<int> componentList) { //check status register

		int statusCheck; //indicates if DataEngine finished operating		

		this->statusMask = 0;
		for (std::list<int>::iterator it = componentList.begin(); it != componentList.end(); it++) { //iterator
			if(*it == smem0A) this->statusMask = this->statusMask + 2;
			if(*it == smem0B) this->statusMask = this->statusMask + 1;
			if(*it == smem1A) this->statusMask = this->statusMask + 8;
			if(*it == smem1B) this->statusMask = this->statusMask + 4;
			if(*it == smem2A) this->statusMask = this->statusMask + 32;
			if(*it == smem2B) this->statusMask = this->statusMask + 16;
			if(*it == smem3A) this->statusMask = this->statusMask + 128;
			if(*it == smem3B) this->statusMask = this->statusMask + 64;	}

		statusCheck = ENG_STATUS_REG & this->statusMask;
	

		return this->statusMask;

}


TEST(CtrlTest, ctrlMask)  { //create control mask test

	ctrlReg dummy;	
	int myints[] = {smem0A,smul0,salu0,smem1A}; //dummy data
  	std::list<int> components (myints, myints + sizeof(myints) / sizeof(int) ); //dummy list

	
	EXPECT_EQ (1314881,dummy.get(components, 1));


}

TEST(CtrlTest, ctrlMask2)  { //create control mask test

	ctrlReg dummy;	
	int myints[] = {smem0A,smul0,salu0,smem1A}; //dummy data
  	std::list<int> components (myints, myints + sizeof(myints) / sizeof(int) ); //dummy list

	
	EXPECT_EQ (1314882,dummy.get(components, 0));


}

TEST(CtrlTest, statusMask)  { //create control mask test

	ctrlReg dummy;	
	int myints[] = {smem0A,smem1A,smul0}; //dummy data
  	std::list<int> components (myints, myints + sizeof(myints) / sizeof(int) ); //dummy list

	
	EXPECT_EQ (10,dummy.check(components));


}








