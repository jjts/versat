#include <iostream>
#include <stdio.h>
#include <string.h>
#include "DataEngine.hpp"


	void Alu::setOper(char oper) { //chose operand

		

		if (oper=='+')   this->oper = "ADD";
		if (oper=='-')   this->oper = "SUB";
		if (oper=='&')   this->oper = "AND";
		if (oper=='|')   this->oper = "OR";
		if (oper=='^')   this->oper = "XOR";
	     
}

	void Alu::setOper(std::string oper) { //chose other operand represented with a char array

		

		if (oper == "~&")   this->oper = "ANDN";
		if (oper == "SEXT8")   this->oper = "SEXT8";
		if (oper == "SEXT16")  this->oper = "SEXT16";
		if (oper == "SRA")   this->oper = "SRA";
		if (oper == "SRL")   this->oper = "SRL";
		if (oper == "CMPU")  this->oper = "CMPU";
		if (oper == "CMPS")  this->oper = "CMPS";
		if (oper == "CLZ")   this->oper = "CLZ";
		if (oper == "MAX")   this->oper = "MAX";
		if (oper == "MIN")   this->oper = "MIN";
		if (oper == "ABS")   this->oper = "ABS";
		
	     
} 

	void Alu::init()  {  std::cout << this->oper;  } //Delete in future


	std::string Alu::ShowOper() { return this->oper;  }


TEST(ALUTest, connectPort) {  //ALu connections test

	Alu dummy(salu0);
	Alu dummy2(salu1);
	AluLite dummy3(salulite0);
	Bs dummy4(sbs0);
	mult dummy5(smul0);
	Mem dummy6(smem0A, smem0B);
	//const char * c = dummy.getOutlabel().c_str();

	dummy.connectPortB(dummy2);
   	EXPECT_EQ (salu0, dummy.getOutlabel());
	
	EXPECT_EQ (salu1, dummy.getOutPortBSel());
	dummy.connectPortA(dummy3);
	EXPECT_EQ (salulite0, dummy.getOutPortASel());
   	dummy.connectPortA(dummy4);
	dummy.connectPortB(dummy5);
	EXPECT_EQ (sbs0, dummy.getOutPortASel());
	EXPECT_EQ (smul0, dummy.getOutPortBSel());
	dummy.connectPortA(dummy6, 'a');
	dummy.connectPortB(dummy6, 'b');
	EXPECT_EQ (smem0A, dummy.getOutPortASel());
	EXPECT_EQ (smem0B, dummy.getOutPortBSel());
} 


TEST(ALUTest, Oper)  {

	Alu dummy(salu0);	

	dummy.setOper("~&");
	EXPECT_STREQ ("ANDN", dummy.ShowOper().c_str());
	dummy.setOper('+');
	EXPECT_STREQ ("ADD", dummy.ShowOper().c_str());


}





