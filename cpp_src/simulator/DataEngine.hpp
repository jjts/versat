

#ifndef __X_H_INCLUDED__   // if x.h hasn't been included yet...
#define __X_H_INCLUDED__   //   #define this so the compiler knows it has been included



#include <string>
#include <string.h>
#include "gtest/gtest.h"
#include "defs_library.h"
#include "mem_ends.h"
#include <list>

#define CTRL_BIT_SIZE 24


class mult;
class Alu;
class AluLite;
class Bs;
class Mem;

class ctrlReg {

	private : int ctrlMask; //control mask
	private : int statusMask; //status mask
	
	public : int get(std::list<int> componentList, int init);
	public : int check(std::list<int> componentList);
	public : int showCtrlMask() { return this->ctrlMask; }
	public : int showStatusMask() { return this->statusMask; }




};

class portMem { //memory's port

	private : int sel; //connection's label
	private : int start;
	private : int shift;
	private : int incr;
	private : int iter;
	private : int duty;
	private : int per;
	private : int memInit;
	private : int label; //label

	public : portMem () {}
	public : portMem (int sel) { this->label = sel; }
	//public : void connect(std::string sinalEntrada);
	public : void connect(Mem aux, char port);
	public : void connect(mult aux);
	public : void connect(Alu aux);
	public : void connect(AluLite aux);
	public : void connect(Bs aux);  
	public : void setStart(int start);
	public : void setDuty(int duty);
	public : void setShift(int shift);
	public : void setPer(int per);
	public : void setIncr(int incr);
	public : void setIter(int iter);
	public : void setlabel(int label) { this->label = label;  }
	public : int getOutlabel();
	public : int getOutSel()  { return this->sel;  }
	public : int printConfigValues(std::string parameter);


};  



class Mem  { //memory

	public 	: portMem portA, portB;

	public : Mem()  {}
	public : Mem(int labelA, int labelB) { 
		portA.setlabel(labelA); portB.setlabel(labelB);   }
	public : void mark();

};


class Port  { //FUs port

	private : int sel; //connection label

	public : void connect(int aux);
	public : int getOutSel();

};

class FU { //FU

	private : int dummy;
	private : Port portA;
	private : Port portB;
	private : int label; //FU's label

	public : int getOutPortASel();
	public : int getOutPortBSel();
	public : void connectPortB(Mem aux, char port);
	public : void connectPortB(mult aux);
	public : void connectPortB(Alu aux);
	public : void connectPortB(AluLite aux);
	public : void connectPortB(Bs aux);  
	public : void connectPortA(Mem aux, char port);
	public : void connectPortA(mult aux);
	public : void connectPortA(Alu aux);
	public : void connectPortA(AluLite aux);
	public : void connectPortA(Bs aux); 
	public : int getOutlabel();
	public : void putLabel(int sel);
	

};


class mult : public FU { //multiplier

	
	private : int lonhi;
	private : int div2;
	
	public : mult (int sel) { putLabel(sel); }
	public : mult () {  }
	public : void setLonhi(int lonhi);
	public : void setDiv2(int div2);
	public : void init();
	
};

class Alu : public FU  { //alu

	private : std::string oper;
	
	public : Alu (int sel) { putLabel(sel); }
	public : Alu () {  }
	public : std::string ShowOper();
	public : void setOper(std::string oper);
	public : void setOper(char oper);
	public : void init();

};

class AluLite : public FU { //alulite

	private : std::string oper;
	
	public : AluLite (int sel) { putLabel(sel); }
	public : AluLite () {  }
	public : std::string ShowOper();
	public : void setOper(std::string oper);
	public : void setOper(char oper);
	public : void init();

};

class Bs : public FU { //barrel shifter

	private : std::string oper;
	
	public : Bs () {  }
	public : Bs (int sel) { putLabel(sel); }
	public : void setOper(std::string oper);


};

class DataEngine {

	private : ctrlReg controlRegister;
	//private : int adjacencyVector[15];

	public : void init();
	public : void run() {}
	public : void wait() {}


};

#endif



