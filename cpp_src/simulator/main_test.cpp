#include <iostream>
#include <stdio.h>
#include "DataEngine.hpp"
#include <string>
#include "gtest/gtest.h"


TEST(MainTest, connectPort) {  //Vec_add without wait and init engine

	Alu alu0(salu0);
	Mem mem2(smem2A, smem2B);
	Mem mem3(smem3A, smem3B);

	mem2.portA.setStart(0);
	mem2.portA.setDuty(1);
	mem2.portA.setIncr(2);
	mem2.portA.setIter(256);
	mem2.portA.setPer(1);
	mem2.portA.setShift(0);

	mem2.portB.setStart(1);
	mem2.portB.setDuty(1);
	mem2.portB.setIncr(2);
	mem2.portB.setIter(256);
	mem2.portB.setPer(1);
	mem2.portB.setShift(0);

	mem3.portA.setStart(0);
	mem3.portA.setDuty(1);
	mem3.portA.setIncr(6);
	mem3.portA.setIter(256);
	mem3.portA.setPer(1);
	mem3.portA.setShift(0);

	alu0.setOper('+');

	alu0.connectPortA(mem2, 'a');

	alu0.connectPortB(mem2, 'b');

	mem3.portA.connect(alu0);


	EXPECT_EQ (salu0, alu0.getOutlabel());
	EXPECT_EQ (smem2A, mem2.portA.getOutlabel());
	EXPECT_EQ (smem2B, mem2.portB.getOutlabel());
	EXPECT_EQ (smem3A, mem3.portA.getOutlabel());
	EXPECT_EQ (smem3B, mem3.portB.getOutlabel());

	EXPECT_EQ (0, mem2.portA.printConfigValues("start"));
	EXPECT_EQ (1, mem2.portA.printConfigValues("duty"));
	EXPECT_EQ (2, mem2.portA.printConfigValues("incr"));
	EXPECT_EQ (256, mem2.portA.printConfigValues("iter"));
	EXPECT_EQ (1, mem2.portA.printConfigValues("per"));
	EXPECT_EQ (0, mem2.portA.printConfigValues("shift"));

	EXPECT_EQ (1, mem2.portB.printConfigValues("start"));
	EXPECT_EQ (1, mem2.portB.printConfigValues("duty"));
	EXPECT_EQ (2, mem2.portB.printConfigValues("incr"));
	EXPECT_EQ (256, mem2.portB.printConfigValues("iter"));
	EXPECT_EQ (1, mem2.portB.printConfigValues("per"));
	EXPECT_EQ (0, mem2.portB.printConfigValues("shift"));

	EXPECT_EQ (0, mem3.portA.printConfigValues("start"));
	EXPECT_EQ (1, mem3.portA.printConfigValues("duty"));
	EXPECT_EQ (6, mem3.portA.printConfigValues("incr"));
	EXPECT_EQ (256, mem3.portA.printConfigValues("iter"));
	EXPECT_EQ (1, mem3.portA.printConfigValues("per"));
	EXPECT_EQ (0, mem3.portA.printConfigValues("shift"));

	EXPECT_STREQ ("ADD", alu0.ShowOper().c_str());

	EXPECT_EQ (smem2A, alu0.getOutPortASel());
	EXPECT_EQ (smem2B, alu0.getOutPortBSel());
	
	EXPECT_EQ (salu0, mem3.portA.getOutSel());  

	
}  



int main(int argc, char **argv)
{

	/*portMem aaa;
	Alu abc;
	std::string testar_strings = "admakdmakmdka";
	
	Mem def;	
	mult ijk("smul0"); 
	mult mno("smul1");

	ijk.setLonhi(0);
	ijk.connectPortB(mno);
	abc.setOper('+'); */
	
	

	//aaa.connect("afafadaw");
	

	::testing::InitGoogleTest(&argc, argv);
  	std::cout << '\n' << RUN_ALL_TESTS() << '\n';

	

	
	
}
