#include <iostream>
#include <stdio.h>
#include <string.h>
#include "DataEngine.hpp"


	void AluLite::setOper(char oper) { //chose operand

		if (oper=='+')   this->oper = "ADD";
		if (oper=='-')   this->oper = "SUB";
		if (oper=='&')   this->oper = "AND";
		if (oper=='|')   this->oper = "OR";
		if (oper=='^')   this->oper = "XOR";
	     
}

	void AluLite::setOper(std::string oper) { //chose other operand represented with a char array

		if (oper == "~&")   this->oper = "ANDN";
		if (oper == "SEXT8")   this->oper = "SEXT8";
	     
} 

	void AluLite::init()  {  std::cout << this->oper;  } //Will be done in future


	std::string AluLite::ShowOper() { return this->oper;  }


TEST(ALULiteTest, Oper)  {

	AluLite dummy(salu0);	

	dummy.setOper("~&");
	EXPECT_STREQ ("ANDN", dummy.ShowOper().c_str());
	dummy.setOper('+');
	EXPECT_STREQ ("ADD", dummy.ShowOper().c_str());


}

