#include <iostream>
#include <stdio.h>
#include "genAssembly.hpp"
#include <sstream>

/*************************************************************************/
//Calculates Alu Oper Assembly
//
//
/*************************************************************************/
void GenFU::genAluOperAsm (int oper, int unit) { 

	string line;
	string asmInstruction;
	string asmOper;
	string configAddr; 

	asmInstruction = _ldi;
	if(oper == ALU_ADD) asmOper = _alu_add;
	if(oper == ALU_SUB) asmOper = _alu_sub;
	if(oper == ALU_AND) asmOper = _alu_and;
	if(oper == ALU_OR) asmOper = _alu_or;
	if(oper == ALU_XOR) asmOper = _alu_xor;
	if(oper == SEXT8) asmOper = _alu_sext8;
	if(oper == SEXT16) asmOper = _alu_sext16;
	if(oper == _SRA) asmOper = _alu_sra;
	if(oper == _SRL) asmOper = _alu_srl;
	if(oper == CMPU) asmOper = _alu_cmp_uns;
	if(oper == CMPS) asmOper = _alu_cmp_sig;
	if(oper == _CLZ) asmOper = _alu_clz;
	if(oper == ALU_MAX) asmOper = _alu_max;
	if(oper == ALU_MIN) asmOper = _alu_min;
	if(oper == ALU_ABS) asmOper = _alu_abs;
	line = asmInstruction+' '+asmOper+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == salu0) configAddr = _alu0_config_addr;
	if(unit == salu1) configAddr = _alu1_config_addr;	
	line = '\t'+asmInstruction+' '+configAddr+','+_alu_conf_fns_offset+_nextLine;
	cout << line;	

}

/*************************************************************************/
//Calculates Alu PortA connection Assembly
//
//
/*************************************************************************/


void GenFU::genAluConnectPortA_Asm (int unit, int selUnit) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == salu0) asmConnection = _salu0;
	if(selUnit == salu1) asmConnection = _salu1;
	if(selUnit == salulite0) asmConnection = _salulite0;
	if(selUnit == salulite1) asmConnection = _salulite1;
	if(selUnit == salulite2) asmConnection = _salulite2;
	if(selUnit == salulite3) asmConnection = _salulite3;
	if(selUnit == smul0) asmConnection = _smul0;
	if(selUnit == smul1) asmConnection = _smul1;
	if(selUnit == smul2) asmConnection = _smul2;
	if(selUnit == smul3) asmConnection = _smul3;
	if(selUnit == sbs0) asmConnection = _sbs0;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == salu0) configAddr = _alu0_config_addr;
	if(unit == salu1) configAddr = _alu1_config_addr;	
	line = '\t'+asmInstruction+' '+configAddr+','+_alu_conf_sela_offset+_nextLine;
	cout << line;	

}

/*************************************************************************/
//Calculates Alu PortB connection Assembly
//
//
/*************************************************************************/

void GenFU::genAluConnectPortB_Asm (int unit, int selUnit) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == salu0) asmConnection = _salu0;
	if(selUnit == salu1) asmConnection = _salu1;
	if(selUnit == salulite0) asmConnection = _salulite0;
	if(selUnit == salulite1) asmConnection = _salulite1;
	if(selUnit == salulite2) asmConnection = _salulite2;
	if(selUnit == salulite3) asmConnection = _salulite3;
	if(selUnit == smul0) asmConnection = _smul0;
	if(selUnit == smul1) asmConnection = _smul1;
	if(selUnit == smul2) asmConnection = _smul2;
	if(selUnit == smul3) asmConnection = _smul3;
	if(selUnit == sbs0) asmConnection = _sbs0;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == salu0) configAddr = _alu0_config_addr;
	if(unit == salu1) configAddr = _alu1_config_addr;	
	line = '\t'+asmInstruction+' '+configAddr+','+_alu_conf_selb_offset+_nextLine;
	cout << line;	

}


/*************************************************************************/
//Calculates Alu PortA connection Assembly to Memories
//
//
/*************************************************************************/


void GenFU::genAluConnectPortA_Asm (int unit, int selUnit, int port) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == MEM0 && port == PORT_A) asmConnection = _smem0A;
	if(selUnit == MEM0 && port == PORT_B) asmConnection = _smem0B;
	if(selUnit == MEM1 && port == PORT_A) asmConnection = _smem1A;
	if(selUnit == MEM1 && port == PORT_B) asmConnection = _smem1B;
	if(selUnit == MEM2 && port == PORT_A) asmConnection = _smem2A;
	if(selUnit == MEM2 && port == PORT_B) asmConnection = _smem2B;
	if(selUnit == MEM3 && port == PORT_A) asmConnection = _smem3A;
	if(selUnit == MEM3 && port == PORT_B) asmConnection = _smem3B;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == salu0) configAddr = _alu0_config_addr;
	if(unit == salu1) configAddr = _alu1_config_addr;	
	line = '\t'+asmInstruction+' '+configAddr+','+_alu_conf_sela_offset+_nextLine;
	cout << line;	

}

/*************************************************************************/
//Calculates Alu PortB connection Assembly to memories
//
//
/*************************************************************************/

void GenFU::genAluConnectPortB_Asm (int unit, int selUnit, int port) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == MEM0 && port == PORT_A) asmConnection = _smem0A;
	if(selUnit == MEM0 && port == PORT_B) asmConnection = _smem0B;
	if(selUnit == MEM1 && port == PORT_A) asmConnection = _smem1A;
	if(selUnit == MEM1 && port == PORT_B) asmConnection = _smem1B;
	if(selUnit == MEM2 && port == PORT_A) asmConnection = _smem2A;
	if(selUnit == MEM2 && port == PORT_B) asmConnection = _smem2B;
	if(selUnit == MEM3 && port == PORT_A) asmConnection = _smem3A;
	if(selUnit == MEM3 && port == PORT_B) asmConnection = _smem3B;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == salu0) configAddr = _alu0_config_addr;
	if(unit == salu1) configAddr = _alu1_config_addr;	
	line = '\t'+asmInstruction+' '+configAddr+','+_alu_conf_selb_offset+_nextLine;
	cout << line;	

}




/*************************************************************************/
//Calculates AluLite Oper Assembly
//
//
/*************************************************************************/
void GenFU::genAluLiteOperAsm (int oper, int unit) { 

	string line;
	string asmInstruction;
	string asmOper;
	string configAddr; 

	asmInstruction = _ldi;
	if(oper == ALU_ADD) asmOper = _alu_add;
	if(oper == ALU_SUB) asmOper = _alu_sub;
	if(oper == ALU_AND) asmOper = _alu_and;
	if(oper == ALU_OR) asmOper = _alu_or;
	if(oper == ALU_XOR) asmOper = _alu_xor;
	if(oper == ALU_NAND) asmOper = _alu_andn;
	line = asmInstruction+' '+asmOper+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == salulite0) configAddr = _aluLite0_config_addr;
	if(unit == salulite1) configAddr = _aluLite1_config_addr;	
	if(unit == salulite2) configAddr = _aluLite2_config_addr;
	if(unit == salulite3) configAddr = _aluLite3_config_addr;		
	line = '\t'+asmInstruction+' '+configAddr+','+_aluLite_conf_fns_offset+_nextLine;
	cout << line;	

}

/*************************************************************************/
//Calculates AluLite PortA connection Assembly
//
//
/*************************************************************************/


void GenFU::genAluLiteConnectPortA_Asm (int unit, int selUnit) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == salu0) asmConnection = _salu0;
	if(selUnit == salu1) asmConnection = _salu1;
	if(selUnit == salulite0) asmConnection = _salulite0;
	if(selUnit == salulite1) asmConnection = _salulite1;
	if(selUnit == salulite2) asmConnection = _salulite2;
	if(selUnit == salulite3) asmConnection = _salulite3;
	if(selUnit == smul0) asmConnection = _smul0;
	if(selUnit == smul1) asmConnection = _smul1;
	if(selUnit == smul2) asmConnection = _smul2;
	if(selUnit == smul3) asmConnection = _smul3;
	if(selUnit == sbs0) asmConnection = _sbs0;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == salulite0) configAddr = _aluLite0_config_addr;
	if(unit == salulite1) configAddr = _aluLite1_config_addr;	
	if(unit == salulite2) configAddr = _aluLite2_config_addr;
	if(unit == salulite3) configAddr = _aluLite3_config_addr;		
	line = '\t'+asmInstruction+' '+configAddr+','+_aluLite_conf_sela_offset+_nextLine;
	cout << line;	

}

/*************************************************************************/
//Calculates Alu PortB connection Assembly
//
//
/*************************************************************************/

void GenFU::genAluLiteConnectPortB_Asm (int unit, int selUnit) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == salu0) asmConnection = _salu0;
	if(selUnit == salu1) asmConnection = _salu1;
	if(selUnit == salulite0) asmConnection = _salulite0;
	if(selUnit == salulite1) asmConnection = _salulite1;
	if(selUnit == salulite2) asmConnection = _salulite2;
	if(selUnit == salulite3) asmConnection = _salulite3;
	if(selUnit == smul0) asmConnection = _smul0;
	if(selUnit == smul1) asmConnection = _smul1;
	if(selUnit == smul2) asmConnection = _smul2;
	if(selUnit == smul3) asmConnection = _smul3;
	if(selUnit == sbs0) asmConnection = _sbs0;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == salulite0) configAddr = _aluLite0_config_addr;
	if(unit == salulite1) configAddr = _aluLite1_config_addr;	
	if(unit == salulite2) configAddr = _aluLite2_config_addr;
	if(unit == salulite3) configAddr = _aluLite3_config_addr;		
	line = '\t'+asmInstruction+' '+configAddr+','+_aluLite_conf_selb_offset+_nextLine;
	cout << line;	

}


/*************************************************************************/
//Calculates AluLite PortA connection Assembly to Memories
//
//
/*************************************************************************/


void GenFU::genAluLiteConnectPortA_Asm (int unit, int selUnit, int port) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == MEM0 && port == PORT_A) asmConnection = _smem0A;
	if(selUnit == MEM0 && port == PORT_B) asmConnection = _smem0B;
	if(selUnit == MEM1 && port == PORT_A) asmConnection = _smem1A;
	if(selUnit == MEM1 && port == PORT_B) asmConnection = _smem1B;
	if(selUnit == MEM2 && port == PORT_A) asmConnection = _smem2A;
	if(selUnit == MEM2 && port == PORT_B) asmConnection = _smem2B;
	if(selUnit == MEM3 && port == PORT_A) asmConnection = _smem3A;
	if(selUnit == MEM3 && port == PORT_B) asmConnection = _smem3B;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == salulite0) configAddr = _aluLite0_config_addr;
	if(unit == salulite1) configAddr = _aluLite1_config_addr;	
	if(unit == salulite2) configAddr = _aluLite2_config_addr;
	if(unit == salulite3) configAddr = _aluLite3_config_addr;		
	line = '\t'+asmInstruction+' '+configAddr+','+_aluLite_conf_sela_offset+_nextLine;
	cout << line;	

}

/*************************************************************************/
//Calculates AluLite PortB connection Assembly to memories
//
//
/*************************************************************************/

void GenFU::genAluLiteConnectPortB_Asm (int unit, int selUnit, int port) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == MEM0 && port == PORT_A) asmConnection = _smem0A;
	if(selUnit == MEM0 && port == PORT_B) asmConnection = _smem0B;
	if(selUnit == MEM1 && port == PORT_A) asmConnection = _smem1A;
	if(selUnit == MEM1 && port == PORT_B) asmConnection = _smem1B;
	if(selUnit == MEM2 && port == PORT_A) asmConnection = _smem2A;
	if(selUnit == MEM2 && port == PORT_B) asmConnection = _smem2B;
	if(selUnit == MEM3 && port == PORT_A) asmConnection = _smem3A;
	if(selUnit == MEM3 && port == PORT_B) asmConnection = _smem3B;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == salulite0) configAddr = _aluLite0_config_addr;
	if(unit == salulite1) configAddr = _aluLite1_config_addr;	
	if(unit == salulite2) configAddr = _aluLite2_config_addr;
	if(unit == salulite3) configAddr = _aluLite3_config_addr;		
	line = '\t'+asmInstruction+' '+configAddr+','+_aluLite_conf_selb_offset+_nextLine;
	cout << line;	

}


/*************************************************************************/
//Calculates Mult Lonhi Assembly
//
//
/*************************************************************************/
void GenFU::genMultLonhiAsm (int lonhi, int unit) { 

	string line;
	string asmInstruction;
	string asmLonhi;
	string configAddr; 
	ostringstream convert;   // stream used for the conversion
	convert << lonhi;
	

	asmInstruction = _ldi;
	if(lonhi >=0 && lonhi<=1) asmLonhi = convert.str();
	line = asmInstruction+' '+asmLonhi+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == smul0) configAddr = _mult0_config_addr;
	if(unit == smul1) configAddr = _mult1_config_addr;	
	if(unit == smul2) configAddr = _mult2_config_addr;
	if(unit == smul3) configAddr = _mult3_config_addr;		
	line = '\t'+asmInstruction+' '+configAddr+','+_mult_conf_lonhi_offset+_nextLine;
	cout << line;	

}


/*************************************************************************/
//Calculates Mult Div2 Assembly
//
//
/*************************************************************************/
void GenFU::genMultDiv2Asm (int div2, int unit) { 

	string line;
	string asmInstruction;
	string asmDiv2;
	string configAddr; 
	ostringstream convert;   // stream used for the conversion
	convert << div2;
	

	asmInstruction = _ldi;
	if(div2 >=0 && div2<=1) asmDiv2 = convert.str();
	line = asmInstruction+' '+asmDiv2+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == smul0) configAddr = _mult0_config_addr;
	if(unit == smul1) configAddr = _mult1_config_addr;	
	if(unit == smul2) configAddr = _mult2_config_addr;
	if(unit == smul3) configAddr = _mult3_config_addr;		
	line = '\t'+asmInstruction+' '+configAddr+','+_mult_conf_div2_offset+_nextLine;
	cout << line;	

}


/*************************************************************************/
//Calculates Mult PortA connection Assembly to Memories
//
//
/*************************************************************************/


void GenFU::genMultConnectPortA_Asm (int unit, int selUnit, int port) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == MEM0 && port == PORT_A) asmConnection = _smem0A;
	if(selUnit == MEM0 && port == PORT_B) asmConnection = _smem0B;
	if(selUnit == MEM1 && port == PORT_A) asmConnection = _smem1A;
	if(selUnit == MEM1 && port == PORT_B) asmConnection = _smem1B;
	if(selUnit == MEM2 && port == PORT_A) asmConnection = _smem2A;
	if(selUnit == MEM2 && port == PORT_B) asmConnection = _smem2B;
	if(selUnit == MEM3 && port == PORT_A) asmConnection = _smem3A;
	if(selUnit == MEM3 && port == PORT_B) asmConnection = _smem3B;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == smul0) configAddr = _mult0_config_addr;
	if(unit == smul1) configAddr = _mult1_config_addr;	
	if(unit == smul2) configAddr = _mult2_config_addr;
	if(unit == smul3) configAddr = _mult3_config_addr;		
	line = '\t'+asmInstruction+' '+configAddr+','+_mult_conf_sela_offset+_nextLine;
	cout << line;	

}

/*************************************************************************/
//Calculates Mult PortB connection Assembly to memories
//
//
/*************************************************************************/

void GenFU::genMultConnectPortB_Asm (int unit, int selUnit, int port) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == MEM0 && port == PORT_A) asmConnection = _smem0A;
	if(selUnit == MEM0 && port == PORT_B) asmConnection = _smem0B;
	if(selUnit == MEM1 && port == PORT_A) asmConnection = _smem1A;
	if(selUnit == MEM1 && port == PORT_B) asmConnection = _smem1B;
	if(selUnit == MEM2 && port == PORT_A) asmConnection = _smem2A;
	if(selUnit == MEM2 && port == PORT_B) asmConnection = _smem2B;
	if(selUnit == MEM3 && port == PORT_A) asmConnection = _smem3A;
	if(selUnit == MEM3 && port == PORT_B) asmConnection = _smem3B;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == smul0) configAddr = _mult0_config_addr;
	if(unit == smul1) configAddr = _mult1_config_addr;	
	if(unit == smul2) configAddr = _mult2_config_addr;
	if(unit == smul3) configAddr = _mult3_config_addr;		
	line = '\t'+asmInstruction+' '+configAddr+','+_mult_conf_selb_offset+_nextLine;
	cout << line;	

}

/*************************************************************************/
//Calculates Mult PortA connection Assembly
//
//
/*************************************************************************/


void GenFU::genMultConnectPortA_Asm (int unit, int selUnit) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == salu0) asmConnection = _salu0;
	if(selUnit == salu1) asmConnection = _salu1;
	if(selUnit == salulite0) asmConnection = _salulite0;
	if(selUnit == salulite1) asmConnection = _salulite1;
	if(selUnit == salulite2) asmConnection = _salulite2;
	if(selUnit == salulite3) asmConnection = _salulite3;
	if(selUnit == smul0) asmConnection = _smul0;
	if(selUnit == smul1) asmConnection = _smul1;
	if(selUnit == smul2) asmConnection = _smul2;
	if(selUnit == smul3) asmConnection = _smul3;
	if(selUnit == sbs0) asmConnection = _sbs0;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == smul0) configAddr = _mult0_config_addr;
	if(unit == smul1) configAddr = _mult1_config_addr;	
	if(unit == smul2) configAddr = _mult2_config_addr;
	if(unit == smul3) configAddr = _mult3_config_addr;		
	line = '\t'+asmInstruction+' '+configAddr+','+_mult_conf_sela_offset+_nextLine;
	cout << line;	

}

/*************************************************************************/
//Calculates Mult PortB connection Assembly
//
//
/*************************************************************************/

void GenFU::genMultConnectPortB_Asm (int unit, int selUnit) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == salu0) asmConnection = _salu0;
	if(selUnit == salu1) asmConnection = _salu1;
	if(selUnit == salulite0) asmConnection = _salulite0;
	if(selUnit == salulite1) asmConnection = _salulite1;
	if(selUnit == salulite2) asmConnection = _salulite2;
	if(selUnit == salulite3) asmConnection = _salulite3;
	if(selUnit == smul0) asmConnection = _smul0;
	if(selUnit == smul1) asmConnection = _smul1;
	if(selUnit == smul2) asmConnection = _smul2;
	if(selUnit == smul3) asmConnection = _smul3;
	if(selUnit == sbs0) asmConnection = _sbs0;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == smul0) configAddr = _mult0_config_addr;
	if(unit == smul1) configAddr = _mult1_config_addr;	
	if(unit == smul2) configAddr = _mult2_config_addr;
	if(unit == smul3) configAddr = _mult3_config_addr;		
	line = '\t'+asmInstruction+' '+configAddr+','+_mult_conf_selb_offset+_nextLine;
	cout << line;	

}


/*************************************************************************/
//Calculates Bs PortA connection Assembly
//
//
/*************************************************************************/

void GenFU::genBsConnectPortA_Asm (int unit, int selUnit) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == salu0) asmConnection = _salu0;
	if(selUnit == salu1) asmConnection = _salu1;
	if(selUnit == salulite0) asmConnection = _salulite0;
	if(selUnit == salulite1) asmConnection = _salulite1;
	if(selUnit == salulite2) asmConnection = _salulite2;
	if(selUnit == salulite3) asmConnection = _salulite3;
	if(selUnit == smul0) asmConnection = _smul0;
	if(selUnit == smul1) asmConnection = _smul1;
	if(selUnit == smul2) asmConnection = _smul2;
	if(selUnit == smul3) asmConnection = _smul3;
	if(selUnit == sbs0) asmConnection = _sbs0;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == sbs0) configAddr = _bs0_config_addr;
	line = '\t'+asmInstruction+' '+configAddr+','+_bs_conf_sela_offset+_nextLine;
	cout << line;	

}


/*************************************************************************/
//Calculates Bs PortB connection Assembly
//
//
/*************************************************************************/

void GenFU::genBsConnectPortB_Asm (int unit, int selUnit) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == salu0) asmConnection = _salu0;
	if(selUnit == salu1) asmConnection = _salu1;
	if(selUnit == salulite0) asmConnection = _salulite0;
	if(selUnit == salulite1) asmConnection = _salulite1;
	if(selUnit == salulite2) asmConnection = _salulite2;
	if(selUnit == salulite3) asmConnection = _salulite3;
	if(selUnit == smul0) asmConnection = _smul0;
	if(selUnit == smul1) asmConnection = _smul1;
	if(selUnit == smul2) asmConnection = _smul2;
	if(selUnit == smul3) asmConnection = _smul3;
	if(selUnit == sbs0) asmConnection = _sbs0;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == sbs0) configAddr = _bs0_config_addr;
	line = '\t'+asmInstruction+' '+configAddr+','+_bs_conf_selb_offset+_nextLine;
	cout << line;	

}


/*************************************************************************/
//Calculates Bs PortA connection Assembly to Memories
//
//
/*************************************************************************/


void GenFU::genBsConnectPortA_Asm (int unit, int selUnit, int port) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == MEM0 && port == PORT_A) asmConnection = _smem0A;
	if(selUnit == MEM0 && port == PORT_B) asmConnection = _smem0B;
	if(selUnit == MEM1 && port == PORT_A) asmConnection = _smem1A;
	if(selUnit == MEM1 && port == PORT_B) asmConnection = _smem1B;
	if(selUnit == MEM2 && port == PORT_A) asmConnection = _smem2A;
	if(selUnit == MEM2 && port == PORT_B) asmConnection = _smem2B;
	if(selUnit == MEM3 && port == PORT_A) asmConnection = _smem3A;
	if(selUnit == MEM3 && port == PORT_B) asmConnection = _smem3B;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == sbs0) configAddr = _bs0_config_addr;
	line = '\t'+asmInstruction+' '+configAddr+','+_mult_conf_sela_offset+_nextLine;
	cout << line;	

}

/*************************************************************************/
//Calculates Bs PortB connection Assembly to memories
//
//
/*************************************************************************/

void GenFU::genBsConnectPortB_Asm (int unit, int selUnit, int port) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	

	asmInstruction = _ldi;
	if(selUnit == MEM0 && port == PORT_A) asmConnection = _smem0A;
	if(selUnit == MEM0 && port == PORT_B) asmConnection = _smem0B;
	if(selUnit == MEM1 && port == PORT_A) asmConnection = _smem1A;
	if(selUnit == MEM1 && port == PORT_B) asmConnection = _smem1B;
	if(selUnit == MEM2 && port == PORT_A) asmConnection = _smem2A;
	if(selUnit == MEM2 && port == PORT_B) asmConnection = _smem2B;
	if(selUnit == MEM3 && port == PORT_A) asmConnection = _smem3A;
	if(selUnit == MEM3 && port == PORT_B) asmConnection = _smem3B;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == sbs0) configAddr = _bs0_config_addr;
	line = '\t'+asmInstruction+' '+configAddr+','+_bs_conf_selb_offset+_nextLine;
	cout << line;	

}

/*************************************************************************/
//Calculates Bs LNR Assembly 
//
//
/*************************************************************************/

void GenFU::genBsLNR_Asm (int unit, int value) {

	string line;
	string asmInstruction;
	string asmValue;
	string configAddr; 
	ostringstream convert;   // stream used for the conversion
	convert << value;
	

	asmInstruction = _ldi;
	if(value >=0 && value<=1) asmValue = convert.str();
	line = asmInstruction+' '+asmValue+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == sbs0) configAddr = _bs0_config_addr;	
	line = '\t'+asmInstruction+' '+configAddr+','+_bs_conf_lnr_offset+_nextLine;
	cout << line;	



}

/*************************************************************************/
//Calculates Bs LNA Assembly 
//
//
/*************************************************************************/

void GenFU::genBsLNA_Asm (int unit, int value) {

	string line;
	string asmInstruction;
	string asmValue;
	string configAddr; 
	ostringstream convert;   // stream used for the conversion
	convert << value;
	

	asmInstruction = _ldi;
	if(value >=0 && value<=1) asmValue = convert.str();
	line = asmInstruction+' '+asmValue+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == sbs0) configAddr = _bs0_config_addr;	
	line = '\t'+asmInstruction+' '+configAddr+','+_bs_conf_lna_offset+_nextLine;
	cout << line;	



}

/*************************************************************************/
//Disables functional unit
//
//
/*************************************************************************/

void GenFU::genDisableFUAsm(int unit) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	string conf_sel_offset;
	

	asmInstruction = _ldi;
	asmConnection = _sdisabled;
	
	
	line = '\t'+asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == salu0) { configAddr = _alu0_config_addr; conf_sel_offset = _alu_conf_sela_offset; }
	if(unit == salu1) { configAddr = _alu1_config_addr; conf_sel_offset = _alu_conf_sela_offset;}
	if(unit == salulite0) { configAddr = _aluLite0_config_addr; conf_sel_offset = _aluLite_conf_sela_offset;}
	if(unit == salulite1) { configAddr = _aluLite1_config_addr; conf_sel_offset = _aluLite_conf_sela_offset;}
	if(unit == salulite2) { configAddr = _aluLite2_config_addr; conf_sel_offset = _aluLite_conf_sela_offset;}
	if(unit == salulite3) { configAddr = _aluLite3_config_addr; conf_sel_offset = _aluLite_conf_sela_offset;}
	if(unit == smul0) { configAddr = _mult0_config_addr; conf_sel_offset = _mult_conf_sela_offset;}
	if(unit == smul1) { configAddr = _mult1_config_addr; conf_sel_offset = _mult_conf_sela_offset;}
	if(unit == smul2) { configAddr = _mult2_config_addr; conf_sel_offset = _mult_conf_sela_offset;}
	if(unit == smul3) { configAddr = _mult3_config_addr; conf_sel_offset = _mult_conf_sela_offset;}
	if(unit == sbs0) { configAddr = _bs0_config_addr; conf_sel_offset = _bs_conf_sela_offset;}

	
	
	line = '\t'+asmInstruction+' '+configAddr+','+conf_sel_offset+_nextLine;
	cout << line;	

}








