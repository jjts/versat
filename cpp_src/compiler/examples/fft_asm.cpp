int main() {


asm {	
#clear conf_reg
	wrw CLEAR_CONFIG_ADDR
	ldi 1024
	wrw R10
	ldi 1
	wrw R9
	wrw R8
	wrw R11
	ldi 0
	wrw R7
	wrw R5
	wrw R4
	wrw R3
	ldi 512
	wrw R6  }


	/*for(i=0;i<1023;i++) {
			mem0A[j(1)+1*i]=mem2A[j(1)+2*i];  
			mem0B[1024+j(1)+1*i]=mem2B[1024+j(1)+1*i]; 
			mem1A[j(1)+1*i]=mem3A[j(1)+2*i]; }

	
	mem2.portA.setReverse(1);
	

	mem3.portA.setReverse(1);  */
asm {
#do reverse bits
#configure mem0 for storing result, real parts
	ldi smem2A
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	ldi smem2B
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	ldi 1024
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	ldi 1
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi 1023
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem1 for storing result, imaginary parts
	ldi smem3A
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	ldi 1
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi 1023
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem2 for reading a and b vectors, real parts
	ldi 1024
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	ldi 1
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_RVRS_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi 2
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi 1023
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem3 for reading a and b vectors, imaginary parts
	ldi 1
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_RVRS_OFFSET
	ldi 2
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi 1023
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET }

	
	de_ctrl_reg.init(mem0, mem1A, mem2, mem3A);
	de_ctrl_reg.run(mem0, mem1A, mem2, mem3A);
	
	clearConfigReg();
	//complex product
	
	for(R9=1;R9<11;R9=R9+1) {
		RB=1&R9;
		if(RB==1) {		


		if(R9==7) goto (comp);
		if(R9==8) goto (comp2);
		if(R9==9) goto (comp2);
		if(R9==10) goto (comp2);

		

begin2		R12=R11;
		R7=0;		
		do {

		RB = R8-R5+R4-1;
		R13 = R8-R5+RB;
		R14 = R6-1;
		R1 = R8+R7;
		R2 = R3-1024+RB;

		for(i=0;i<R14;i++) {
			for(j=0;j<RB;j++) 
				mem0B[R1+j(R13)+1*i] = (mem1A[R1+j(R13)+1*i]*mem2B[1025+j(R2)+R10*i])-(mem0A[R1+j(R13)+1*i]*mem2A[1024+j(R2)+R10*i]); 
				mem1B[R1+j(R13)+1*i] = (mem1A[R1+j(R13)+1*i]*mem2A[1024+j(R2)+R10*i])+(mem0A[R1+j(R13)+1*i]*mem2B[1025+j(R2)+R10*i]);}
				


		
		RB = R8-R5+R4-1;
		mem0.portA.setDuty(RB);
		mem0.portB.setDuty(RB);
		mem1.portB.setDelay(6);

		RB = R8-R5+R4-1;
		mem1.portA.setDuty(RB);
		mem1.portB.setDuty(RB);

		RB=R8-R5+R4-1;
		mem2.portA.setDuty(RB);
		
		mem2.portB.setDuty(RB);

		//de_ctrl_reg.wait(mem0A);
asm {
#wait for results to run next configuration
wait	ldi 0x0002
	and ENG_STATUS_REG
	beqi wait }
			
		de_ctrl_reg.init(mem0, mem1, mem2);
		de_ctrl_reg.run(mem0, mem1, mem2, alu0, alu1, mult0, mult1, mult2, mult3);

		clearConfigReg();
		if(R11==1) {
				
			goto (next);
			 }
		
		R7=R7+R6+R6;
		//R7=R7+R6;
		R12=R12-1;

	}while(R12!=0);

next		R12=R11;
		R7=0;
	do {
	//complex sum

	RB = R8-R5+R4-1;
	R13 = R8-R5+RB;
	R14 = R6-1;
	R1 = R8+R7;
	R2 = R3-1024+RB;


	for(i=0;i<R14;i++) {
			for(j=0;j<RB;j++) 
				mem2A[R7+j(R13)+1*i] = mem0A[R7+j(R13)+1*i]+mem0B[R1+j(R13)+1*i];  
				mem3A[R7+j(R13)+1*i] = mem1A[R7+j(R13)+1*i]+mem1B[R1+j(R13)+1*i];
				mem2B[R1+j(R13)+1*i] = mem0B[R1+j(R13)+1*i]-mem0A[R7+j(R13)+1*i];
				mem3B[R1+j(R13)+1*i] = mem1B[R1+j(R13)+1*i]-mem1A[R7+j(R13)+1*i];  } 




		
		RB = R8-R5+R4-1;
		
		mem0.portA.setDuty(RB);
		mem0.portB.setDuty(RB);
		RB = R8-R5+R4-1;
		mem1.portA.setDuty(RB);
		mem1.portB.setDuty(RB);
		RB=R8-R5+R4-1;
		
		mem2.portA.setDuty(RB);
		mem2.portB.setDuty(RB);
		RB=R8-R5+R4-1;
		mem3.portA.setDuty(RB);
		mem3.portB.setDuty(RB);

		de_ctrl_reg.wait(mem2A);

		de_ctrl_reg.init(mem0, mem1, mem2, mem3);
		de_ctrl_reg.run(mem0, mem1, mem2, mem3, alu0, alu1, aluLite0, aluLite1);

		clearConfigReg();
		
		if(R11==1) goto(next3);
		R7=R7+R6+R6;

		R12=R12-1;
		}while(R12!=0);
}
else {	
		
		if(R9==7) goto (comp);
		if(R9==8) goto (comp2);
		if(R9==9) goto (comp2);
		if(R9==10) goto (comp2);

begin2i		R12=R11;
		R7=0;
		
		do {


		RB = R8-R5+R4-1;
		R13 = R8-R5+RB;
		R14 = R6-1;
		R1 = R8+R7;
		R2 = R3-1024+RB;

		for(i=0;i<R14;i++) {
			for(j=0;j<RB;j++) 
				mem2B[R1+j(R13)+1*i] = (mem3A[R1+j(R13)+1*i]*mem0B[1025+j(R2)+R10*i])-(mem2A[R1+j(R13)+1*i]*mem0A[1024+j(R2)+R10*i]); 
				mem3B[R1+j(R13)+1*i] = (mem2A[R1+j(R13)+1*i]*mem0B[1025+j(R2)+R10*i])+(mem3A[R1+j(R13)+1*i]*mem0A[1024+j(R2)+R10*i]);}


		//configure mem2 for reading b vector, real part
		RB=R8-R5+R4-1;
		
		mem2.portA.setDuty(RB);
		mem2.portB.setDuty(RB);

		//configure mem3 for reading b vector, imaginary part
		RB=R8-R5+R4-1;
		mem3.portA.setDuty(RB);
		mem3.portB.setDuty(RB);

		//configure mem0 for reading coeficients
		RB=R8-R5+R4-1;
		mem0.portA.setDuty(RB);
		mem0.portB.setDuty(RB);
		de_ctrl_reg.wait(mem2A);
		de_ctrl_reg.init(mem0, mem2, mem3);
		de_ctrl_reg.run(mem0, mem2, mem3, alu0, alu1, mult0, mult1, mult2, mult3);		

		clearConfigReg();

		if(R11==1) goto(nexti);
		R7=R7+R6+R6;
		R12=R12-1;
		}while(R12!=0);

nexti		R12=R11;
		R7=0;

		//configure mem2 for reading a and b vectors, real parts
	do {

		RB = R8-R5+R4-1;
		R13 = R8-R5+RB;
		R14 = R6-1;
		R1 = R8+R7;
		R2 = R3-1024+RB;


		for(i=0;i<R14;i++) {
			for(j=0;j<RB;j++) 
				mem0A[R7+j(R13)+1*i] = mem2A[R7+j(R13)+1*i]+mem2B[R1+j(R13)+1*i];  
				mem1A[R7+j(R13)+1*i] = mem3A[R7+j(R13)+1*i]+mem3B[R1+j(R13)+1*i];
				mem0B[R1+j(R13)+1*i] = mem2B[R1+j(R13)+1*i]-mem2A[R7+j(R13)+1*i];
				mem1B[R1+j(R13)+1*i] = mem3B[R1+j(R13)+1*i]-mem3A[R7+j(R13)+1*i];  } 

		RB = R8-R5+R4-1;
		mem2.portA.setDuty(RB);
		mem2.portB.setDuty(RB);
		//configure mem3 for reading a and b vectors, imaginary parts
		RB = R8-R5+R4-1;
		mem3.portA.setDuty(RB);
		mem3.portB.setDuty(RB);

		//configure mem0 for storing result, real parts
		
		RB=R8-R5+R4-1;
		
		mem0.portA.setDuty(RB);
		
		mem0.portB.setDuty(RB);

		//configure mem1 for storing result, imaginary parts
		
		RB=R8-R5+R4-1;
		mem1.portA.setDuty(RB);
		mem1.portB.setDuty(RB);

		de_ctrl_reg.wait(mem0A);
		de_ctrl_reg.init(mem0, mem1, mem2, mem3);
		de_ctrl_reg.run(mem0, mem1, mem2, mem3, alu0, alu1, aluLite0, aluLite1);
		
		clearConfigReg();

	if(R11==1) goto(next3);
	R7=R7+R6+R6;
	//R7=R7+R6;	
	R12=R12-1;
	}while(R12!=0);
}
		//RB=0;
		//if(R9==10) goto (wait4);
		
next3		R8 = R8 << 1;
		R10 = R10 >> 1;
		R6=R6>>1;

		
	
}
//wait4	RB=1;
	de_ctrl_reg.wait(mem1, mem2, mem3, mem0);


return 0;

comp	R11=16;
R4=1;
R3=1024;
R6=R6<<1;
comp2	R11=R11>>1;
R6=R6<<2;
R5=R8;
RB=1&R9;
if(RB!=0) goto (begin2);
goto (begin2i);




}






