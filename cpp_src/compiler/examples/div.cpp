int main() {

  //Load dividend in R10 and divisor in R11.
  //

  R10 = mem0[0]; 
  R11= mem0[1];

  de.clearConfig();

  //set alu0 to do CLZ using aluLite0 as input
  alu0.setOper('CLZ');
  alu0.connectPortA(aluLite0);
  alu0.connectPortB(aluLite0);

  de.run();

  //get clz of divisor
  RaluLite0 = R11;
  R15 = Ralu0;

  //get clz of dividend
  RaluLite0 = R10;
  R13 = Ralu0;
  
  //compute divident/divisor distance
  R12 = R15 - R13;

  R5 = 0; //init quotient

  //exit if divisor greater than dividend
  if(R12 < 0)
    goto terminate;
  
  //shift divisor left by distance
  for(R14=0; R14 < R12; R14++){
    R11 = R11 << 1;
  }



  for(R14 = 0; R14 <= R12; R14++) {
    R2 = R10-R11;
    R4 = 0;
    if(R2 >= 0) {
      R4 = 1;
      R10 = R2; //update dividend
    }
    R5 = (R5<<1) + R4; //update quotient
    R11 = R11 >> 1; //update divisor
  }

  //Store dividend in R10 and divisor in R11.
  //
  //This assembly code works around the fact that the compiler can't
  //do mem0[0] = R10 and mem0[1] = R11. Remove this comment when the
  //compiler issue is solved

 terminate:
  asm {
    ldi ENG_MEM_BASE
    addi 2
    wrw RB
    rdw R5
    wrwb  

    rdw RB
    addi 1
    wrw RB
    rdw R10
    wrwb
  }

}
