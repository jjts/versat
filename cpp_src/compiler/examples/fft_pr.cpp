int main() {
	
  R3=0; //used to compute address increments
  R4=0; //used to compute address increments
  R5=0; //used to compute address increments
  R6=512; //number of blocks
  R8=1; //number of butterflies per block
  R10=1024; //number of fft points
  R11=1; //number of configs per stage

  //mirror address bits and copy coefs
  de.clearConfig();
  mem2A.setReverse(1);
  mem3A.setReverse(1);
  for(j=0;j<1024;j++) {
    //mirror address bits 
    mem0A[j]=mem2A[2*j];  
    mem1A[1*j]=mem3A[2*j];
    //copy coefs     
    mem0B[1024+1*j]=mem2B[1024+1*j]; 
  }	
  de.run();

  //complex product b*w
  de.clearConfig();
  for(j=0;j<R6;j++) {
    for(i=0;i<R14;i++) {
      mem0B[i] = (mem1A[i]*mem2B[1025])-(mem0A[i]*mem2A[1024]); 
      mem1B[i] = (mem1A[i]*mem2A[1024])+(mem0A[i]*mem2B[1025]);
    }
  } 
  de.saveConfig(0);

  
  //complex sum a+b*w and a-b*w
  de.clearConfig();
  for(j=0;j<R6;j++) {
    for(i=0;i<R14;i++) {
      mem2A[i] = mem0A[i]+mem0B[i];  
      mem3A[i] = mem1A[i]+mem1B[i];
      mem2B[i] = mem0B[i]-mem0A[i];
      mem3B[i] = mem1B[i]-mem1A[i];
    }
  }
  de.saveConfig(1);

  //complex product b*w
  de.clearConfig();
  for(j=0;j<R6;j++) {
    for(i=0;i<R14;i++) {
      mem2B[i] = (mem3A[i]*mem0B[1025])-(mem2A[i]*mem0A[1024]); 
      mem3B[i] = (mem2A[i]*mem0B[1025])+(mem3A[i]*mem0A[1024]);
    }
  }
  de.saveConfig(2);

  //complex sum a+b*w and a-b*w
  de.clearConfig();
  for(j=0;j<R6;j++) {
    for(i=0;i<R14;i++) {
      mem0A[i] = mem2A[i]+mem2B[i];  
      mem1A[i] = mem3A[i]+mem3B[i];
      mem0B[i] = mem2B[i]-mem2A[i];
      mem1B[i] = mem3B[i]-mem3A[i];
    }
  }
  de.saveConfig(3);


  //de.clearConfig();


   

  
  //FFT main loop
  for(R9=1;R9<11;R9=R9+1) { //iterate over all stages

    //set registers used in partial reconfigurations
    if(R9==7) {
      R11=16;
      R4=1;
      R3=1024;
      R6=R6<<1;
    }
    
    if(R9>6) {
      R11=R11>>1;
      R6=R6<<2;
      R5=R8;
    }
    
    R14 = R8-R5+R4;
    R13 = R8-R5+R14; //address increment

    R12=R11; //do-while iterator
    R7=0; //start address offset
    R2 = R3-1024+R14; //address increment


    R15=1&R9; //get stage parity
    if(R15==1) { //stage is odd

      //complex product b*w
      de.loadConfig(0);
      //1st expression
      mem0B.setIter(R6);
      mem0B.setPer(R14);
      mem0B.setDuty(R14);
      mem0B.setShift(R13-R14);

      //2nd expression
      mem1B.setIter(R6);
      mem1B.setPer(R14);
      mem1B.setDuty(R14);
      mem1B.setShift(R13-R14);

      //common
      mem1A.setIter(R6);
      mem1A.setPer(R14);
      mem1A.setDuty(R14);
      mem1A.setShift(R13-R14);

      mem0A.setIter(R6);
      mem0A.setPer(R14);
      mem0A.setDuty(R14);
      mem0A.setShift(R13-R14);

      mem2B.setIter(R6);
      mem2B.setPer(R14);
      mem2B.setDuty(R14);
      mem2B.setShift(R2-R14);
      mem2B.setIncr(R10);

      mem2A.setIter(R6);
      mem2A.setPer(R14);
      mem2A.setDuty(R14);
      mem2A.setShift(R2-R14);
      mem2A.setIncr(R10);
      
      
      do { //partial reconfiguration until all done		
	mem0A.setStart(R8+R7);
	mem0B.setStart(R8+R7);
	mem1A.setStart(R8+R7);
	mem1B.setStart(R8+R7);
		
	de.wait(mem0A);		
	//de.run(mem0, mem1, mem2);
	de.run();
		
	R7=R7+R6+R6;		
	R12=R12-1;

      } while(R12!=0);

      //complex sum a+b*w and a-b*w
      R12=R11;
      R7=0;

      de.loadConfig(1);

      mem2A.setIter(R6);
      mem2A.setPer(R14);
      mem2A.setDuty(R14);
      mem2A.setShift(R13-R14);

      mem0A.setIter(R6);
      mem0A.setPer(R14);
      mem0A.setDuty(R14);
      mem0A.setShift(R13-R14);

      mem0B.setIter(R6);
      mem0B.setPer(R14);
      mem0B.setDuty(R14);
      mem0B.setShift(R13-R14);

      mem3A.setIter(R6);
      mem3A.setPer(R14);
      mem3A.setDuty(R14);
      mem3A.setShift(R13-R14);

      mem1A.setIter(R6);
      mem1A.setPer(R14);
      mem1A.setDuty(R14);
      mem1A.setShift(R13-R14);

      mem1B.setIter(R6);
      mem1B.setPer(R14);
      mem1B.setDuty(R14);
      mem1B.setShift(R13-R14);

      mem2B.setIter(R6);
      mem2B.setPer(R14);
      mem2B.setDuty(R14);
      mem2B.setShift(R13-R14);

      mem3B.setIter(R6);
      mem3B.setPer(R14);
      mem3B.setDuty(R14);
      mem3B.setShift(R13-R14);     
      
      do { //partial reconfiguration until all done
	mem0A.setStart(R7);
	mem0B.setStart(R8+R7);
	mem1A.setStart(R7);
	mem1B.setStart(R8+R7);
	mem2A.setStart(R7);
	mem2B.setStart(R8+R7);
	mem3A.setStart(R7);
	mem3B.setStart(R8+R7);

	de.wait(mem2A);
	//de.run(mem0, mem1, mem2, mem3);
	de.run();
		
	R7=R7+R6+R6;
	R12=R12-1;
      } while(R12!=0);
    }
    else { //stage is even

      //complex product
      de.loadConfig(2);
      //1st expression
      mem2B.setIter(R6);
      mem2B.setPer(R14);
      mem2B.setDuty(R14);
      mem2B.setShift(R13-R14);

      //2nd expression
      mem3B.setIter(R6);
      mem3B.setPer(R14);
      mem3B.setDuty(R14);
      mem3B.setShift(R13-R14);

      //common
      mem3A.setIter(R6);
      mem3A.setPer(R14);
      mem3A.setDuty(R14);
      mem3A.setShift(R13-R14);

      mem2A.setIter(R6);
      mem2A.setPer(R14);
      mem2A.setDuty(R14);
      mem2A.setShift(R13-R14);

      mem0B.setIter(R6);
      mem0B.setPer(R14);
      mem0B.setDuty(R14);
      mem0B.setShift(R2-R14);
      mem0B.setIncr(R10);

      mem0A.setIter(R6);
      mem0A.setPer(R14);
      mem0A.setDuty(R14);
      mem0A.setShift(R2-R14);
      mem0A.setIncr(R10);
      
      do { //partial reconfiguration until all done
	mem3A.setStart(R8+R7);
	mem3B.setStart(R8+R7);
	mem2A.setStart(R8+R7);
	mem2B.setStart(R8+R7);

	de.wait(mem2A);
	//de.run(mem0,mem2,mem3);
	de.run();

	R7=R7+R6+R6;
	R12=R12-1;
      } while(R12!=0);

      //complex sum a+b*w and a-b*w
      R12=R11;
      R7=0;

      de.loadConfig(3);

      mem0A.setIter(R6);
      mem0A.setPer(R14);
      mem0A.setDuty(R14);
      mem0A.setShift(R13-R14);

      mem2A.setIter(R6);
      mem2A.setPer(R14);
      mem2A.setDuty(R14);
      mem2A.setShift(R13-R14);

      mem2B.setIter(R6);
      mem2B.setPer(R14);
      mem2B.setDuty(R14);
      mem2B.setShift(R13-R14);

      mem1A.setIter(R6);
      mem1A.setPer(R14);
      mem1A.setDuty(R14);
      mem1A.setShift(R13-R14);

      mem3A.setIter(R6);
      mem3A.setPer(R14);
      mem3A.setDuty(R14);
      mem3A.setShift(R13-R14);

      mem3B.setIter(R6);
      mem3B.setPer(R14);
      mem3B.setDuty(R14);
      mem3B.setShift(R13-R14);

      mem0B.setIter(R6);
      mem0B.setPer(R14);
      mem0B.setDuty(R14);
      mem0B.setShift(R13-R14);

      mem1B.setIter(R6);
      mem1B.setPer(R14);
      mem1B.setDuty(R14);
      mem1B.setShift(R13-R14);
     
      do { //partial reconfiguration untill all done
	mem0A.setStart(R7);
	mem0B.setStart(R8+R7);
	mem1A.setStart(R7);
	mem1B.setStart(R8+R7);
	mem2A.setStart(R7);
	mem2B.setStart(R8+R7);
	mem3A.setStart(R7);
	mem3B.setStart(R8+R7);

	de.wait(mem0A);
	//de.run(mem0,mem1,mem2,mem3);
	de.run();
		
	R7=R7+R6+R6;
	R12=R12-1;
      } while(R12!=0);
    }
				
    R8 = R8 << 1;
    R10 = R10 >> 1;
    R6=R6>>1;
  } //end of main loop

  de.wait(mem0, mem1, mem2, mem3);
}

