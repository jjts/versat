int main() {
	
  R3=0; //used to compute address increments
  R4=0; //used to compute address increments
  R5=0; //used to compute address increments
  R6=512; //number of blocks
  R8=1; //number of butterflies per block
  R10=1024; //number of fft points
  R11=1; //number of configs per stage

  //mirror address bits and copy coefs
  de.clearConfig();
  mem2A.setReverse(1);
  mem3A.setReverse(1);
  for(j=0;j<1024;j++) {
    //mirror address bits 
    mem0A[j]=mem2A[2*j];  
    mem1A[j]=mem3A[2*j];
    //copy coefs     
    mem0B[1024+j]=mem2B[1024+j]; 
  }	
  de.run();

  //FFT main loop
  for(R9=1;R9<11;R9++) { //iterate over all stages

    //set registers used in partial reconfigurations
    if(R9==7) {
      R11=16;
      R4=1;
      R3=1024;
      R6=R6<<1;
    }
    
    if(R9>6) {
      R11=R11>>1;
      R6=R6<<2;
      R5=R8;
    }
    
    R14 = R8-R5+R4;
    R13 = (R8-R5)<<1+R4; //address increment

    R12=R11; //do-while iterator
    R7=0; //start address offset
    R2 = R3-1024+R14; //address increment

    de.clearConfig();

    R15=1&R9; //get stage parity
    if(R15==1) { //stage is odd

      //complex product b*w
      
      
      do { //partial reconfiguration until all done	
	de.clearConfig();

	R1=R8+R7;
	for(j=0;j<R6;j++) {
	 for(i=0;i<R14;i++) {
	  mem0B[R1+j*R13+i] = (mem1A[R1+j*R13+i]*mem2B[1025+j*R2+R10*i])-(mem0A[R1+j*R13+i]*mem2A[1024+j*R2+R10*i]); 
	  mem1B[R1+j*R13+i] = (mem1A[R1+j*R13+i]*mem2A[1024+j*R2+R10*i])+(mem0A[R1+j*R13+i]*mem2B[1025+j*R2+R10*i]);
	}
      }  
		
	de.wait(mem0A);		
	de.run();
		
	R7=R7+R6+R6;		
	R12=R12-1;

      } while(R12!=0);

      //complex sum a+b*w and a-b*w
      R12=R11;
      R7=0;

      de.clearConfig();
      

      do { //partial reconfiguration until all done
	
	R1=R8+R7;
	de.clearConfig();
	for(j=0;j<R6;j++) {
	 for(i=0;i<R14;i++) {
	  mem2A[R7+j*R13+i] = mem0A[R7+j*R13+i]+mem0B[R1+j*R13+i];  
	  mem3A[R7+j*R13+i] = mem1A[R7+j*R13+i]+mem1B[R1+j*R13+i];
	  mem2B[R1+j*R13+i] = mem0B[R1+j*R13+i]-mem0A[R7+j*R13+i];
	  mem3B[R1+j*R13+i] = mem1B[R1+j*R13+i]-mem1A[R7+j*R13+i];
	}
      }
	

	de.wait(mem2A);
	de.run();
		
	R7=R7+R6+R6;
	R12=R12-1;
      } while(R12!=0);
    }
    else { //stage is even

      //complex product b*w
      
      do { //partial reconfiguration until all done

	R1=R8+R7;
	de.clearConfig();
	for(j=0;j<R6;j++) {
	for(i=0;i<R14;i++) {
	  mem2B[R1+j*R13+i] = (mem3A[R1+j*R13+i]*mem0B[1025+j*R2+R10*i])-(mem2A[R1+j*R13+i]*mem0A[1024+j*R2+R10*i]); 
	  mem3B[R1+j*R13+i] = (mem2A[R1+j*R13+i]*mem0B[1025+j*R2+R10*i])+(mem3A[R1+j*R13+i]*mem0A[1024+j*R2+R10*i]);
	}
      }


	de.wait(mem2A);
	de.run();

	R7=R7+R6+R6;
	R12=R12-1;
      } while(R12!=0);

      //complex sum a+b*w and a-b*w
      R12=R11;
      R7=0;

      
      do { //partial reconfiguration untill all done
	de.clearConfig();
	R1=R8+R7;
      for(j=0;j<R6;j++) {
	for(i=0;i<R14;i++) {
	  mem0A[R7+j*R13+i] = mem2A[R7+j*R13+i]+mem2B[R1+j*R13+i];  
	  mem1A[R7+j*R13+i] = mem3A[R7+j*R13+i]+mem3B[R1+j*R13+i];
	  mem0B[R1+j*R13+i] = mem2B[R1+j*R13+i]-mem2A[R7+j*R13+i];
	  mem1B[R1+j*R13+i] = mem3B[R1+j*R13+i]-mem3A[R7+j*R13+i];
	}
      }

	de.wait(mem0A);
	de.run();
		
	R7=R7+R6+R6;
	R12=R12-1;
      } while(R12!=0);
    }
				
    R8 = R8 << 1;
    R10 = R10 >> 1;
    R6=R6>>1;
  } //end of main loop

  de.wait(mem0, mem1, mem2, mem3);

}

