#ifndef __ASM_STRING_INCLUDED__   // if x.h hasn't been included yet...
#define __ASM_STRING_INCLUDED__   //   #define this so the compiler knows it has been included

using namespace std;

//alu operations
/*****************************************************/
const string _ldi = "ldi";
const string _ldih = "ldih";
const string _alu_add = "ALU_ADD";
const string _alu_sub = "ALU_SUB";
const string _alu_and = "ALU_LOGIC_AND";
const string _alu_or = "ALU_LOGIC_OR";
const string _alu_xor = "ALU_LOGIC_XOR";
const string _alu_andn = "ALU_LOGIC_ANDN";
const string _alu_sext8 = "ALU_SEXT8";
const string _alu_sext16 = "ALU_SEXT16";
const string _alu_sra = "ALU_SHIFTR_ARTH";
const string _alu_srl = "ALU_SHIFTR_LOG";
const string _alu_cmp_uns = "ALU_CMP_UNS";
const string _alu_cmp_sig = "ALU_CMP_SIG";
const string _alu_clz = "ALU_CLZ";
const string _alu_max = "ALU_MAX";
const string _alu_min = "ALU_MIN";
const string _alu_abs = "ALU_ABS";
/*****************************************************/


//write alu's operation
/*****************************************************/
const string _nextLine = "\n";
const string _wrc = "wrc";
const string _wrw = "wrw"; 
const string _alu0_config_addr = "ALU0_CONFIG_ADDR";
const string _alu1_config_addr = "ALU1_CONFIG_ADDR";
const string _alu_conf_fns_offset = "ALU_CONF_FNS_OFFSET";
const string _alu_conf_sela_offset = "ALU_CONF_SELA_OFFSET";
const string _alu_conf_selb_offset = "ALU_CONF_SELB_OFFSET";
const char comma = ',';
/*****************************************************/


//connects
/*****************************************************/
const string _salu0 = "salu0";
const string _salu1 = "salu1";
const string _salulite0 = "salulite0";
const string _salulite1 = "salulite1";
const string _salulite2 = "salulite2";
const string _salulite3 = "salulite3";
const string _smul0 = "smul0";
const string _smul1 = "smul1";
const string _smul2 = "smul2";
const string _smul3 = "smul3";
const string _sbs0 = "sbs0";
const string _smem0A = "smem0A";
const string _smem0B = "smem0B";
const string _smem1A = "smem1A";
const string _smem1B = "smem1B";
const string _smem2A = "smem2A";
const string _smem2B = "smem2B";
const string _smem3A = "smem3A";
const string _smem3B = "smem3B";
const string _sdisabled = "sdisabled";


//write alulite's operation
/*****************************************************/
const string _aluLite0_config_addr = "ALU_LITE0_CONFIG_ADDR";
const string _aluLite1_config_addr = "ALU_LITE1_CONFIG_ADDR";
const string _aluLite2_config_addr = "ALU_LITE2_CONFIG_ADDR";
const string _aluLite3_config_addr = "ALU_LITE3_CONFIG_ADDR";
const string _aluLite_conf_fns_offset = "ALU_LITE_CONF_FNS_OFFSET";
const string _aluLite_conf_sela_offset = "ALU_LITE_CONF_SELA_OFFSET";
const string _aluLite_conf_selb_offset = "ALU_LITE_CONF_SELB_OFFSET";
/*****************************************************/

//write mult's operation
/*****************************************************/
const string _mult0_config_addr = "MULT0_CONFIG_ADDR";
const string _mult1_config_addr = "MULT1_CONFIG_ADDR";
const string _mult2_config_addr = "MULT2_CONFIG_ADDR";
const string _mult3_config_addr = "MULT3_CONFIG_ADDR";
const string _mult_conf_lonhi_offset = "MUL_CONF_LONHI_OFFSET";
const string _mult_conf_div2_offset = "MUL_CONF_DIV2_OFFSET";
const string _mult_conf_sela_offset = "MUL_CONF_SELA_OFFSET";
const string _mult_conf_selb_offset = "MUL_CONF_SELB_OFFSET";
/*****************************************************/


//write Bs's operation
/*****************************************************/
const string _bs_conf_sela_offset = "BS_CONF_SELA_OFFSET";
const string _bs_conf_selb_offset = "BS_CONF_SELB_OFFSET";
const string _bs_conf_lna_offset = "BS_CONF_LNA_OFFSET";
const string _bs_conf_lnr_offset = "BS_CONF_LNR_OFFSET";
const string _bs0_config_addr = "BS0_CONFIG_ADDR";
/*****************************************************/


//write Mem's operation
/*****************************************************/
const string _rdwb = "rdwb";
const string _mem_conf_sela_offset = "MEM_CONF_SELA_OFFSET";
const string _mem_conf_selb_offset = "MEM_CONF_SELA_OFFSET";
const string _mem_conf_duty_offset = "MEM_CONF_DUTY_OFFSET";
const string _mem_conf_start_offset = "MEM_CONF_START_OFFSET";
const string _mem_conf_incr_offset = "MEM_CONF_INCR_OFFSET";
const string _mem_conf_iter_offset = "MEM_CONF_ITER_OFFSET";
const string _mem_conf_shift_offset = "MEM_CONF_SHIFT_OFFSET";
const string _mem_conf_per_offset = "MEM_CONF_PER_OFFSET";
const string _mem_conf_delay_offset = "MEM_CONF_DELAY_OFFSET";
const string _mem_conf_reverse_offset = "MEM_CONF_RVRS_OFFSET";
const string _mem0A_config_addr = "MEM0A_CONFIG_ADDR";
const string _mem0B_config_addr = "MEM0B_CONFIG_ADDR";
const string _mem1A_config_addr = "MEM1A_CONFIG_ADDR";
const string _mem1B_config_addr = "MEM1B_CONFIG_ADDR";
const string _mem2A_config_addr = "MEM2A_CONFIG_ADDR";
const string _mem2B_config_addr = "MEM2B_CONFIG_ADDR";
const string _mem3A_config_addr = "MEM3A_CONFIG_ADDR";
const string _mem3B_config_addr = "MEM3B_CONFIG_ADDR";
/*****************************************************/


//Control/Status Reg's strings
/*****************************************************/
const string _ctrl_reg = "wrw ENG_CTRL_REG";
const string _nop = "nop";
const string _status_reg = "ENG_STATUS_REG";
const string _bneqi_waitres = "bneqi waitres";
const string _clear_flags = "wrw CLEAR_CONFIG_ADDR";
const string _conf_mem = "CONF_MEM_BASE";
/*****************************************************/

//Controller operations
/******************************************************/
const string _and = "and";
const string _add = "add";
const string _addi = "addi";
const string _rdw = "rdw";
const string _sub = "sub";
const string _shft = "shft";
const string _if_statment = "ifStatment";
const string _while_statment = "whileStatment";
const string _while_direct_statment = "whileDirectStatment";
const string _for_direct_statment = "forDirectStatment";
const string _for_statment = "forStatment";
const string _else_statment = "elseStatment";
const string _do_statment = "doStatment";
/******************************************************/

//DMA operations
/******************************************************/
const string _extDMA = "DMA_EXT_ADDR_ADDR";
const string _intDMA = "DMA_INT_ADDR_ADDR";
const string _sizeDMA = "DMA_SIZE_ADDR";
const string _directionDMA = "DMA_DIRECTION_ADDR";
const string _statusDMA = "DMA_STATUS_ADDR";
const string _wdma = "wdma";


#endif










