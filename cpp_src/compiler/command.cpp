#include "command.hpp"
#include <stdlib.h>
#include <math.h>

using namespace std;


int RB_full; //necessary to do expressions

int largest_node; //necessary do calculate delay

global_mem_data mem_iter_per; //auxiliary structure

resourcesAlloc *vec_aux = new resourcesAlloc[TOTAL_RSC];

variable var_controller;

std::list<varStruct> varList;

string auxList;

NODE2 *auxNodeVars;

std::list<int> totalResources; //resources used in last mem expressions for
int markResources[TOTAL_RSC];

std::list<string> existingVars;

int auto_duty_disable=0; //autoduty 

std::list<int> memExpResources[CONF_MEM_SIZE]; //memories expressions saved resources

void COMMAND::printComponentsList() {

	std::list<int>::iterator auxIt;
	for(auxIt=totalResources.begin();auxIt!=totalResources.end();auxIt++) {
		cout << *auxIt << '\n';

		}
	cout << "---------------------------------\n";

}
/*************************************************************************/
//Loads a new component list.
//
//Receivs as argument the new component list.
//
/*************************************************************************/
void COMMAND::setNewComponentList(std::list<int> b) {

	totalResources.clear();
	totalResources = b;


}

/*************************************************************************/
//Saves the last memory expressions list in the resources vector
//
//Receivs as argument the memory's delay and a resource list. 
//
/*************************************************************************/
void saveResources(int a, std::list<int> b) {

	memExpResources[a].clear();
	memExpResources[a] = b;


}

/*************************************************************************/
//Loads a saved memory expressions resources
//
//Receivs as argument the memory's delay. 
//
/*************************************************************************/
void loadResources(int a) {

	totalResources = memExpResources[a];
	
	//return memExpResources[a];


}

/*************************************************************************/
//Saves auto-duty command data.
//
//Receivs as argument a bit. If 0 autoDuty is ON, if 1 autoDuty is off
//
/*************************************************************************/
void COMMAND::endisAutoDuty(int a) {

	this->en_dis_disable = a;

	//cout << "entrou no disableAD 2\n\n";

}



/*************************************************************************/
//Controls auto-duty
//
//Receivs as argument a bit. If 0 autoDuty is ON, if 1 autoDuty is off
//
/*************************************************************************/
void disableAutoDuty(int a) {

//	cout << "entrou no disableAD 1\n\n";

	auto_duty_disable = a;

}


/*************************************************************************/
//Frees resources
//
//Receivs as argument a functional unit in string mode and a mode (register or FU) in int. 
//
/*************************************************************************/
void freeResources(string fu, int var) {

	int fu_aux=0, aux=0;
	varStruct struct_aux;   
	
	if(var == 0) {
		if(fu == "alu0") fu_aux = salu0;
		if(fu == "alu1") fu_aux = salu1;
		if(fu == "aluLite0") fu_aux = salulite0;
		if(fu == "aluLite1") fu_aux = salulite1;
		if(fu == "aluLite2") fu_aux = salulite2;
		if(fu == "aluLite3") fu_aux = salulite3;
		if(fu == "mult0") fu_aux = smul0;
		if(fu == "mult1") fu_aux = smul1;
		if(fu == "mult2") fu_aux = smul2;
		if(fu == "mult3") fu_aux = smul3; 
		if(fu_aux>=salu0 && fu_aux<=salulite3) mem_iter_per.freeAlu(fu_aux);
		if(fu_aux>=smul0 && fu_aux<=smul3) mem_iter_per.freeMult(fu_aux);    }

	else {
		struct_aux = returnVar(fu);
		cout << struct_aux.returnFuUnit() << '\n';
		aux = struct_aux.returnFuUnit();
		if(aux>=salu0 && aux<=salulite3) mem_iter_per.freeAlu(aux);
		if(aux>=smul0 && aux<=smul3) mem_iter_per.freeMult(aux);

		}

	


	

	//mem_iter_per

}

/*************************************************************************/
//Frees resources
//
//Receivs as argument a functional unit in string mode and a mode (register or FU) in int. 
//
/*************************************************************************/
void freeAllResources() {

	
	for(int i=salu0;i<salulite3;i++) mem_iter_per.freeAlu(i);
	for(int i=smul0;i<smul3;i++) mem_iter_per.freeMult(i);

	
}



/*************************************************************************/
//Returns a var from declared vars.
//
//Receivs as argument the var's name.
//
/*************************************************************************/
varStruct returnVar(string a) {

	for (std::list<varStruct>::iterator it = varList.begin(); it != varList.end(); it++) { //iterator
		 if(it->returnVarName() == a) return *it; } 



}

/*************************************************************************/
//Verifies if is an alu allocation or multiplier alocation
//
//Receivs as argument the Fu's type, the next alu and the next multiplier
//
/*************************************************************************/
int verifyAllocation(string a) {

	if(a=="alu") return var_controller.getNextAlu();
	if(a=="mult") return var_controller.getNextMult();

	return 0;

}


/*************************************************************************/
//Verifies if delay is different from original delay in memory expressions
//
//Receivs as argument a the new delay
//
/*************************************************************************/
void resourcesAlloc::compareDelay(int a) {

	if(this->use_delay==1) { 
		//cout << "aaabbbccc\n";
		if(this->delay != a) { cout << "Different delay from the same address generator\n"; 
			exit(-1); } }



}

/*************************************************************************/
//Puts in control structure the delay
//
//Receivs as argument the new delay
//
/*************************************************************************/
void resourcesAlloc::putDelay(int a) {

	this->delay = a;
	this->use_delay=1;



}


/*************************************************************************/
//Resets allocated resources of mem expressions.
//
//
//
/*************************************************************************/
void variable::resetVecs() {

	int i=0;
	
	for(i=0;i<MULTS;i++) this->vec_mult[i]=0;
	for(i=0;i<ALUS;i++) this->vec_alu[i]=0;



}


/*************************************************************************/
//Verifies if variable exists. If variable does'nt exist, compiler gives an error and exits.
//
//Receivs as argument the var's name.
//
/*************************************************************************/
void searchVarName(string a) {

	for (std::list<string>::iterator it = existingVars.begin(); it != existingVars.end(); it++) { //iterator
		 if(*it == a) return; } ;


	cout << "Variable " << a << " not declared!\n";
	exit(-1);

}

/*************************************************************************/
//Saves a new var.
//
//Receivs as argument the var's name.
//
/*************************************************************************/
void saveVarName(string a) {

	for (std::list<string>::iterator it = existingVars.begin(); it != existingVars.end(); it++) { //iterator
		 if(*it == a) return;  }

	existingVars.push_back(a);

}

/*************************************************************************/
//Converts a FU number to respective string.
//
//Receivs as argument the var's number.
//
/*************************************************************************/
string unitToString(int a) {

	if(a == salu0) return "alu0";
	if(a == salu1) return "alu1";
	if(a == salulite0) return "aluLite0";
	if(a == salulite1) return "aluLite1";
	if(a == salulite2) return "aluLite2";
	if(a == salulite3) return "aluLite3";
	if(a == smul0) return "mult0";
	if(a == smul1) return "mult1";
	if(a == smul2) return "mult2";
	if(a == smul3) return "mult3";
	if(a == sbs0) return "bs0";


}

/*************************************************************************/
//Returns the unit's name in string from declared var.
//
//Receivs as argument the var's name.
//
/*************************************************************************/
string returnVarUnit(string a) {

	for (std::list<varStruct>::iterator it = varList.begin(); it != varList.end(); it++) { //iterator
		 if(it->returnVarName() == a) return unitToString(it->returnFuUnit());
		 }

	cout << "Variable " << a << " does'nt exist!!!\n";
	exit(-1);


}

/*************************************************************************/
//Returns the unit's int mode from declared var.
//
//Receivs as argument the var's name.
//
/*************************************************************************/
int searchVar(string a) {

	for (std::list<varStruct>::iterator it = varList.begin(); it != varList.end(); it++) { //iterator
		 if(it->returnVarName() == a) return it->returnFuUnit();
		 }

	cout << "Variable " << a << " does'nt exist!!!\n";
	exit(-1);

}

/*************************************************************************/
//Verifies if variable is already declared.
//
//Receivs as argument the var's name.
//
/*************************************************************************/
void verifyExistingVars(string a)  {

	for (std::list<varStruct>::iterator it = varList.begin(); it != varList.end(); it++) { //iterator
		if(it->returnVarName() == a) { cout << "Variable " << a << " exists already!\n"; exit(-1); }
		 }


}

/*************************************************************************/
//Verifies if output mem parameters are compatible with previous declarations.
//This aplies in memory expressions.
//
//Receivs as argument two memories.
//
/*************************************************************************/
void verifyMemTargCtrl(NODE2 *a, NODE2 *b) {

	//cout << "aaaaa" << ' ' << auxList <<  "\n";
	if(b==NULL) return;
	if(a==NULL) {
				
		varStruct *aux = new varStruct();
		
		aux->putVariableName(auxList);
		//$$=aux;
		//aux->fu = auxIt->mem_tree;
		aux->putFuUnit(b->fu_unit);
		aux->putFU(b);
		varList.push_back(*aux);

}
	else return;

} 

/*************************************************************************/
//Prints node's name.
//
//
//
/*************************************************************************/
void varStruct::printNode() {

	if(this->fu!=NULL)
		cout << this->fu->fu << '\n';


}

/*************************************************************************/
//Assigns memory expression base to a variable.
//
//Receivs as argument the memory expression root.
//
/*************************************************************************/
void varStruct::putFU(NODE2 *a) {

	this->fu = a;


}

/*************************************************************************/
//Converts respective FU to equivalent register.
//
//
//
/*************************************************************************/
string varStruct::fuToReg() {

	if(this->fu_unit == smul0) return "Rmult0";
	if(this->fu_unit == smul1) return "Rmult1";
	if(this->fu_unit == smul2) return "Rmult2";
	if(this->fu_unit == smul3)return "Rmult3";
	if(this->fu_unit == salu0) return "Ralu0";  
	if(this->fu_unit == salu1) return "Ralu1";  
	if(this->fu_unit == salulite0) return "RaluLite0";  
	if(this->fu_unit == salulite1) return "RaluLite1";  
	if(this->fu_unit == salulite2) return "RaluLite2";  
	if(this->fu_unit == salulite3) return "RaluLite3";  
	
}

/*************************************************************************/
//Returns variable's FU in int mode.
//
//
//
/*************************************************************************/
int varStruct::returnFuUnit() {

	return this->fu_unit;

}

/*************************************************************************/
//Returns variable's name.
//
//
//
/*************************************************************************/
string varStruct::returnVarName() {

	return this->var_unit;

}

/*************************************************************************/
//Prints existing variables. Used to debug.
//
//
//
/*************************************************************************/
void printVarStruct() {

	for (std::list<varStruct>::iterator it = varList.begin(); it != varList.end(); it++) { //iterator
		cout <<"varStruct = " << it->returnFuUnit() << ' ' << it->returnVarName() << '\n'; 
		it->printNode(); }


}

/*************************************************************************/
//Assigns a name to a variable.
//
//Receivs as argument a name.
//
/*************************************************************************/
void varStruct::putVariableName(string a) { 

	this->var_unit = a; }


/*************************************************************************/
//Assigns a unit number to a variable.
//
//Receivs as argument the unit number.
//
/*************************************************************************/
void varStruct::putFuUnit(int a) { 

	this->fu_unit = a; }


/*************************************************************************/
//Returns the next unit free to use. Manual configurations does'nt alloc resources.
//Only memory expressions and variable allocs can alloc reosurces.
//
//Receivs as argument the unit number.
//
/*************************************************************************/
int variable::getNext(string a) {

	if(a == "alu") return this->getNextAlu(); 
	if(a == "mult") return this->getNextAlu(); 


}


/*************************************************************************/
//Returns the next alu unit free to use. Manual configurations does'nt alloc resources.
//Only memory expressions and variable allocs can alloc reosurces.
//
//
//
/*************************************************************************/
int variable::getNextAlu() {

	int i=0;

	for(i=0;i<ALUS;i++)
		if(vec_alu[i]==0) { vec_alu[i]=1; return salu0+i;  }

	cout << "Not enough Alus!!!\n";
	exit(-1);


}


/*************************************************************************/
//Returns the next mult unit free to use. Manual configurations does'nt alloc resources.
//Only memory expressions and variable allocs can alloc reosurces.
//
//
//
/*************************************************************************/
int variable::getNextMult() {

	int i=0;

	for(i=0;i<MULTS;i++)
		if(vec_mult[i]==0) { vec_mult[i]=1; return smul0+i; }

	cout << "Not enough Multipliers!!!\n";
	exit(-1);


}


/*************************************************************************/
//Copy allocated resources. Necessary to all commands have a map of resources they can use.
//
//
//Receives as argument the alu's aux vector and mult's aux vector.
//
/*************************************************************************/
void variable::copyUsedResources(int a[MULTS], int b[ALUS]) {

	int i=0;
	for(i=0;i<MULTS;i++) 
		this->vec_mult[i] = a[i];

	for(i=0;i<ALUS;i++)
		this->vec_alu[i] = b[i];


}


/*************************************************************************/
//Frees memory expressions checkout vector. 
//
//
//
//
/*************************************************************************/
void COMMAND::cleanAllParameters() {

	for(int i=0;i<8;i++) {
		vec_aux[i].cleanParameters(); }


}

/*************************************************************************/
//Frees memory for expression resources
//
//
/*************************************************************************/
void freeTotalResources() {

	for(int i=0;i<TOTAL_RSC;i++) { markResources[i]=0; }

	totalResources.clear();
}

/*************************************************************************/
//Verifies if label is permited. Is important because loops and conditions use dedicated labels
//
//Receives as argument a string.
/*************************************************************************/
void COMMAND::verifyLabel(string a) {

	if(a.substr(0, 10) == "ifStatment") { cout << "Label not allowed\n"; exit(-1); }
	if(a.substr(0, 13) == "whileStatment") { cout << "Label not allowed\n"; exit(-1); }
	if(a.substr(0, 10) == "whileDirect") { cout << "Label not allowed\n"; exit(-1); }
	if(a.substr(0, 8) == "forDirect") { cout << "Label not allowed\n"; exit(-1); }
	if(a.substr(0, 11) == "forStatment") { cout << "Label not allowed\n"; exit(-1); }
	if(a.substr(0, 12) == "elseStatment") { cout << "Label not allowed\n"; exit(-1); }
	if(a.substr(0, 10) == "waitres") { cout << "Label not allowed\n"; exit(-1); }

}

/*************************************************************************/
//Return cnst value
//
//
/*************************************************************************/
int COMMAND::returnCnst() { return this->cnst; }

/*************************************************************************/
//Puts resources used to init/run/wait/runMemExp
//
//Receivs as argument a auxiliary list and puts that list as list of components
//
/*************************************************************************/
void COMMAND::putResources(std::list<int> aux) {

	this->componentList=aux;


}

/*************************************************************************/
//Puts resources used to init/run/wait/runMemExp
//
//Receivs as argument a auxiliary list and puts that list as list of components
//
/*************************************************************************/
void COMMAND::putResources(std::list<int> aux, std::list<int> aux2) {

	this->componentList.insert(this->componentList.end(), aux.begin(), aux.end());


}


/*************************************************************************/
//Returns componentList
//
//Returns the list of components used in init/run/wait/runMemExp instructions
/*************************************************************************/
std::list<int> COMMAND::returnResources() {

	return this->componentList; }

/*************************************************************************/
//Dummy function. To delete in future.
//
//
/*************************************************************************/
/*void testResource(int iter, int period, int start, int shift, int incr, int iter_type, int period_type, int start_type, int shift_type, int incr_type, int fu_unit, int fu_unit_port) {

	int i=0;
	int j=30;

	
	if(fu_unit==MEM0 && fu_unit_port == PORT_A) j=0;
	if(fu_unit==MEM0 && fu_unit_port == PORT_B) j=1;
	if(fu_unit==MEM1 && fu_unit_port == PORT_A) j=2;
	if(fu_unit==MEM1 && fu_unit_port == PORT_B) j=3;
	if(fu_unit==MEM2 && fu_unit_port == PORT_A) j=4;
	if(fu_unit==MEM2 && fu_unit_port == PORT_B) j=5;	
	if(fu_unit==MEM3 && fu_unit_port == PORT_A) j=6;
	if(fu_unit==MEM3 && fu_unit_port == PORT_B) j=7;		
	
	if(j==30) j = fu_unit-smem0A;
	
	

} */

/*************************************************************************/
//Puts FU's parameters in resource's controller.
//
//Receivs as argument the parameters.
/*************************************************************************/
void resourcesAlloc::putParameters(int iter, int period, int start, int shift, int incr, int iter_type, int period_type, int start_type, int shift_type, int incr_type){

	this->iter = iter;
	this->period = period;
	this->start = start;
	this->shift = shift;
	this->incr = incr;
	this->iter_type = iter_type;
	this->period_type = period_type;
	this->start_type = start_type;
	this->shift_type = shift_type;
	this->incr_type = incr_type;
	this->use = 1;



}

/*************************************************************************/
//Cleans resource's controller old parameters. Used when a memory expression cycle ends.
//
//
/*************************************************************************/
void resourcesAlloc::cleanParameters() {

	this->iter = 0;
	this->period = 0;
	this->start = 0;
	this->shift = 0;
	this->incr = 0;
	this->iter_type = 0;
	this->period_type = 0;
	this->start_type = 0;
	this->shift_type = 0;
	this->incr_type = 0;
	this->use = 0;
	this->delay=0;
	this->use_delay=0;



}

/*************************************************************************/
//Prints data. Used to debug.
//
//
/*************************************************************************/
void resourcesAlloc::printData()  {

	if(this->use==1) {
		cout << "iter = " << this->iter << '\n';
		cout << "period = " << this->period << '\n';
		cout << "start = " << this->start << '\n';
		cout << "shift = " << this->shift << '\n';
		cout << "incr = " << this->incr << '\n';
		cout << "iter_type = " << this->iter_type << '\n';
		cout << "period_type = " << this->period_type << '\n';
		cout << "start_type = " << this->start_type << '\n';
		cout << "shift_type = " << this->shift_type << '\n';
		cout << "incr_type = " << this->incr_type << '\n';
		cout << "*************************************************\n";  }

	if(this->use_delay==1) cout << "delay = " << this->delay << '\n';

}

/*************************************************************************/
//Compares new data. If new data is diferent from old data, compiler gives error.
//Used to control configurations in memory expressions.
//
//Receves as arguments new parameters.
/*************************************************************************/
void resourcesAlloc::compareData(int iter, int period, int start, int shift, int incr, int iter_type, int period_type, int start_type, int shift_type, int incr_type){

	if(this->use == 1) {
	if(this->iter != iter) { cout << "Different iter from the same address generator\n"; exit(-1); }
	if(this->period != period) { cout << "Different period from the same address generator\n"; exit(-1); }
	if(this->start != start) { cout << "Different start from the same address generator\n"; exit(-1); }
	if(this->shift != shift) { cout << "Different shift from the same address generator\n"; exit(-1); }
	if(this->incr != incr) { cout << "Different incr from the same address generator\n"; exit(-1); }
	if(this->iter_type != iter_type) { cout << "Different iter type from the same addresse generator\n"; exit(-1); }
	if(this->period_type != period_type) { cout << "Different period type from the same address generator\n"; exit(-1); }
	if(this->start_type != start_type) { cout << "Different start type from the same address generator\n"; exit(-1); }
	if(this->shift_type != shift_type) { cout << "Different shift type from the same address generator\n"; exit(-1); }
	if(this->incr_type != incr_type) { cout << "Different incr type from the same address generator\n"; exit(-1); }   }



}

/*************************************************************************/
//Prints data. Used to debug.
//
//
/*************************************************************************/
void printAlloc() {

	for(int i=0;i<8;i++) { 

		vec_aux[i].printData();

	}


}

/*************************************************************************/
//Puts resource in full mode. When a resource is allocated, is necessary put it in full mode.
//
//
/*************************************************************************/
void resourcesAlloc::fullResource() {

	this->full=1; 

}

/*************************************************************************/
//Verifies if resource is in full mode.
//
//Returns 0 if is free, returns 1 when is ocupied.
/*************************************************************************/
int resourcesAlloc::returnFullRsc() { return this->full; }

/*************************************************************************/
//Returns resource's vector position equivalent to register.
//
//Receives as argument the register in cause.
/*************************************************************************/
int returnVectorPosition(int a) {

	
		if(a == _Ralu0) return 0;		
		if(a == _Ralu1) return 1;
		if(a == _RaluLite0) return 2;
		if(a == _RaluLite1) return 3;
		if(a == _RaluLite2) return 4;
		if(a == _RaluLite3) return 5;      
		if(a == _Rmult0) return 0;		
		if(a == _Rmult1) return 1;
		if(a == _Rmult2) return 2;
		if(a == _Rmult3) return 3;


}

/*************************************************************************/
//Frees a alu.
//
//Receives as argument the alu number.
/*************************************************************************/
void global_mem_data::freeAlu(int fu)  {

	alloc_alu[fu-salu0] = 0;
	vec_alu[fu-salu0] = 0;

}

/*************************************************************************/
//Frees a multiplier.
//
//Receives as argument the multiplier number.
/*************************************************************************/
void global_mem_data::freeMult(int fu)  {

	alloc_mult[fu-smul0] = 0;
	vec_mult[fu-smul0] = 0;

}
/*************************************************************************/
//Puts iter in auxiliary structure
//
//Receives as argument the value and the value type (register or constant)
/*************************************************************************/
void global_mem_data::setIter(int a, int aType) {

	this->iter = a;   
	this->iterType = aType; }
/*************************************************************************/
//Puts period in auxiliary structure
//
//Receives as argument the value and the value type (register or constant)
/*************************************************************************/
void global_mem_data::setPer(int a, int aType) {

	this->per = a; 
	this->perType = aType; }
/*************************************************************************/
//Reset auxiliary structure
//
//
/*************************************************************************/
void global_mem_data::resetParameters() {
	this->iter = 0;
	this->iterType = 0;
	this->per = 0; 
	this->perType = 0; 
	for(int i=0;i<ALUS;i++) {
		if(alloc_alu[i] == 0) {
			vec_alu[i]=0;   }   }




	for(int i=0;i<MULTS;i++) {
		if(alloc_mult[i] == 0) {		
			vec_mult[i]=0;  } } 

}

/*************************************************************************/
//Alloc a resource.
//
//Receives as argument resource's type
/*************************************************************************/

void global_mem_data::allocRsc(string a, int fu) {

	if(fu>=salu0 && fu <=salulite3) {
		alloc_alu[fu-salu0] = 1;
		vec_alu[fu-salu0] = 1; }

	if(fu>=smul0 && fu <=smul3) {
		alloc_mult[fu-smul0] = 1;
		vec_mult[fu-smul0] = 1; }


}

/*************************************************************************/
//Alloc a resource.
//
//Receives as argument resource's type
/*************************************************************************/

void global_mem_data::allocRsc(int fu) {

	if(fu>=salu0 && fu <=salulite3) {
		alloc_alu[fu-salu0] = 1;
		vec_alu[fu-salu0] = 1; }

	if(fu>=smul0 && fu <=smul3) {
		alloc_mult[fu-smul0] = 1;
		vec_mult[fu-smul0] = 1; }


}


/*************************************************************************/
//Verify if mem expressions for has correct parameters
//
//Receives as argument the extern for's value and the intern for's value
//Returns nothing (corrigir isto)
/*************************************************************************/
int memExpErrorTest(int out_for, int in_for) {

	if(out_for>0 || in_for>0) { cout << "Error! In memory expression the beggining should be equal to 0.\n"; exit(-1); }


}

/*************************************************************************/
//Return functional unit value
//
//Receives as argument the unit value in string mode.
//Returns the unit value in int mode.
/*************************************************************************/
int returnSelUnit(string var){



	if(var == "mem0A") return MEM0;
	if(var == "mem1A") return MEM1;
	if(var == "mem2A") return MEM2;
	if(var == "mem3A") return MEM3;
	if(var == "mem0B") return MEM0;
	if(var == "mem1B") return MEM1;
	if(var == "mem2B") return MEM2;
	if(var == "mem3B") return MEM3;
	if(var == "alu0") return salu0;
	if(var == "alu1") return salu1;
	if(var == "aluLite0") return salulite0;
	if(var == "aluLite1") return salulite1;
	if(var == "aluLite2") return salulite2;
	if(var == "aluLite3") return salulite3;
	if(var == "mult0") return smul0;
	if(var == "mult1") return smul1;
	if(var == "mult2") return smul2;
	if(var == "mult3") return smul3;
	if(var == "bs0") return sbs0;

}


/*************************************************************************/
//Return reg
//
//Receives the register in string mode.
//Returns the register in int mode.
/*************************************************************************/

int returnReg(string a) {

	if(a == "R1"){ return _R1; }
	if(a == "R2"){ return _R2;  }
	if(a == "R3"){ return _R3;  }
	if(a == "R4"){ return _R4;  }
	if(a == "R5"){ return _R5; }
	if(a == "R6"){ return _R6;  }
	if(a == "R7"){ return _R7; }
	if(a == "R8"){ return _R8;  }
	if(a == "R9"){ return _R9;  }
	if(a == "R10"){ return _R10;  }
	if(a == "R11"){ return _R11;  }
	if(a == "R12"){ return _R12;  }
	if(a == "R13"){ return _R13;  }
	if(a == "R14"){ return _R14;  }
	if(a == "R15"){ return _R15;  }
	if(a == "RB" ){ return _RB; }
	if(a == "Ralu0"){ return _Ralu0;  }
	if(a == "Ralu1"){ return _Ralu1;  }
	if(a == "RaluLite0"){ return _RaluLite0;  }
	if(a == "RaluLite1"){ return _RaluLite1;  }
	if(a == "RaluLite2"){ return _RaluLite2;  }
	if(a == "RaluLite3"){ return _RaluLite3;  }
	if(a == "Rmult0"){ return _Rmult0;  }
	if(a == "Rmult1"){ return _Rmult1;  }
	if(a == "Rmult2"){ return _Rmult2;  }
	if(a == "Rmult3"){ return _Rmult3;  }
	if(a == "Rbs0") { return _Rbs0;  }



}

/*************************************************************************/
//Return Data Engine Unit Reg
//
//Receives the register in string mode.
//Returns the unit in int mode.
/*************************************************************************/

int returnUnitReg(string a) {

	
	if(a == "Ralu0"){ return salu0;  }
	if(a == "Ralu1"){ return salu1;  }
	if(a == "RaluLite0"){ return salulite0;  }
	if(a == "RaluLite1"){ return salulite1;  }
	if(a == "RaluLite2"){ return salulite2;  }
	if(a == "RaluLite3"){ return salulite3;  }
	if(a == "Rmult0"){ return smul0;  }
	if(a == "Rmult1"){ return smul1;  }
	if(a == "Rmult2"){ return smul2;  }
	if(a == "Rmult3"){ return smul3;  }
	//if(a == "Rbs0") { return _Rbs0;  }



}




/*************************************************************************/
//Resets alu's resource vector.
//
//
/*************************************************************************/
/*void reorganize_resources::resetAluResources() {

	for(int i=0;i<6;i++) this->aluResources[i]=0;




} */


/*void reorganize_resources::reorganizeAlu(NODE2 *tree_aux) {

	if(tree_aux->fu_unit>=salu0 && tree_aux->fu_unit<=salulite3) this->aluResources[tree_aux->fu_unit-salu0]=this->aluResources[tree_aux->fu_unit-salu0]+1;

	if(tree_aux->left!=NULL) { this->reorganizeAlu(tree_aux->left); }


	if(tree_aux->right!=NULL) { this->reorganizeAlu(tree_aux->right); }

	for(int i=0;i<6;i++) cout << this->aluResources[i] << '\n';


} */

/*************************************************************************/
//Puts assembly commands in command list.
//
//Receives as argument the assembly command.
/*************************************************************************/
void COMMAND::putAsmInstr(string aux) {

	this->assembly_instr = aux;


}

/*************************************************************************/
//Puts memory position in controller expressions
//
//
/*************************************************************************/
void COMMAND::setTargetOffset() { this->targetOffset = this->cnst; }
	
/*************************************************************************/
//Generates assembly wrote in asm instruction
//
//
/*************************************************************************/
void COMMAND::genAsmAssemblyInstr() {

	cout << this -> label;
	cout << this->assembly_instr << '\n';


}

/*************************************************************************/
//Reset auxiliary variable used to calculate delay
//
//
/*************************************************************************/

void COMMAND::resetLargestNode()  {

	largest_node=0;



}
/*************************************************************************/
//Generates Data Engine expressions
//
//Receives as argument the data engine expression's tree.
/*************************************************************************/
void COMMAND::genEndMemExpAsm(NODE2 *node_aux) {

	char *port = new char[4];
	char *port2 = new char[4];
	int aux=0;
	int aux2=0;

	
	if(mem_target==NULL) return;

	if(node_aux->fu.substr(0,3) == "mem") {
		strcpy(port2, node_aux->fu.c_str());
		if(port2[4] == 'A') aux2 = PORT_A;
		if(port2[4] == 'B') aux2 = PORT_B;
		
	}

	if(mem_target->fu.substr(0,3) == "mem") {
		//if(mem_target->period_type == 0) { mem_target->period = 1; mem_target->period_type = CONST; }
		strcpy(port, mem_target->fu.c_str());
		if(port[4] == 'A') aux = PORT_A;
		if(port[4] == 'B') aux = PORT_B;
		if(mem_target->start_type!=0) {
			cout << '\t';
			_genMem.genMemStartAsm (returnSelUnit(mem_target->fu), aux, mem_target->start, mem_target->start_type, 0, 0, 0);  }
		if(mem_target->iter_type!=0) {
			cout << '\t';
			_genMem.genMemIterAsm (returnSelUnit(mem_target->fu), aux, mem_target->iter, mem_target->iter_type, 0, 0, 0); }
		if(mem_target->incr_type!=0) {
			cout << '\t';
			_genMem.genMemIncrAsm (returnSelUnit(mem_target->fu), aux, mem_target->incr, mem_target->incr_type, 0, 0, 0); }
		if(mem_target->shift_type!=0 && mem_target->period_type!=0) {
			cout << '\t';
			_genMem.genMemShiftAsm (returnSelUnit(mem_target->fu), aux, mem_target->shift, mem_target->shift_type, mem_target->period, mem_target->period_type, SUB);  }

		if(mem_target->shift_type!=0 && mem_target->period_type==0) {
			cout << '\t';
			_genMem.genMemShiftAsm (returnSelUnit(mem_target->fu), aux, mem_target->shift, mem_target->shift_type, 1, CONST, SUB);  }


		if(mem_target->period_type!=0) {
			cout << '\t';
			_genMem.genMemPerAsm (returnSelUnit(mem_target->fu), aux, mem_target->period, mem_target->period_type, 0, 0, 0); 
			if(auto_duty_disable == 0) {
				cout << '\t';
				_genMem.genMemDutyAsm (returnSelUnit(mem_target->fu), aux, mem_target->period, mem_target->period_type, 0, 0, 0);}   }
		
		cout << '\t';		
		if(node_aux->fu_unit>=MEM0 && node_aux->fu_unit<=MEM3) {
			_genMem.genMemConnectAsm (mem_target->fu_unit, node_aux->fu_unit, aux, aux2); 
			//cout << "aaaa = " << ((node_aux->fu_unit-1)*2+(node_aux->fu_unit_port-1)) << '\n';
			//cout << "bbbb = " << (mem_target->fu_unit) << '\n';
			vec_aux[(mem_target->fu_unit-1)*2+(mem_target->fu_unit_port-1)].compareDelay(1);
			vec_aux[(mem_target->fu_unit-1)*2+(mem_target->fu_unit_port-1)].putDelay(1);
			cout << '\t';
			_genMem.genMemDelayAsm (returnSelUnit(mem_target->fu), aux, 1, CONST, 0, 0, 0); 
			}
		else {
			_genMem.genMemConnectAsm (mem_target->fu_unit, node_aux->fu_unit, aux); 
			//cout << "aaaa = " << ((node_aux->fu_unit-1)*2+(node_aux->fu_unit_port-1)) << '\n';
			//cout << "bbbb = " << (mem_target->fu_unit) << '\n';
			vec_aux[(mem_target->fu_unit-1)*2+(mem_target->fu_unit_port-1)].compareDelay(largest_node+1);
			vec_aux[(mem_target->fu_unit-1)*2+(mem_target->fu_unit_port-1)].putDelay(largest_node+1);
			cout << '\t';
			_genMem.genMemDelayAsm (returnSelUnit(mem_target->fu), aux, (largest_node+1), CONST, 0, 0, 0); }
		}


}

/*************************************************************************/
//Reset auxiliary structure used to calculate memory expressions
//
//
/*************************************************************************/

void COMMAND::resetTotalTreeAux() {

	//this->aux_tree2.iter=0;
	//this->aux_tree2.iter_type=0;
	this->aux_tree2.fu_unit=0;
	this->aux_tree2.fu_unit_mem=0;
	//this->aux_tree2.period=0;
	//this->aux_tree2.period_type=0;
	this->aux_tree2.start=0;
	this->aux_tree2.start_type=0;
	this->aux_tree2.shift=0;
	this->aux_tree2.shift_type=0;
	this->aux_tree2.period_shift=0;
	this->aux_tree2.period_shift_type=0;
	this->aux_tree2.delay=0;
	this->aux_tree2.incr=0;
	this->aux_tree2.incr_type=0;




}

/*************************************************************************/
//
//
//
/*************************************************************************/

void COMMAND::resetTreeAux() {

	//this->aux_tree2.iter=0;
	this->aux_tree2.fu_unit=0;
	this->aux_tree2.fu_unit_mem=0;
	//this->aux_tree2.period=0;
	this->aux_tree2.start=0;
	this->aux_tree2.shift=0;
	this->aux_tree2.period_shift=0;
	this->aux_tree2.delay=0;
	this->aux_tree2.incr=0;
	this->aux_tree2.start_type=0;
	this->aux_tree2.incr_type=0;
	this->aux_tree2.shift_type=0;
	//this->aux_tree2.iter_type=0;


}

/*************************************************************************/
//Prints data engine expression node
//
//Receives the node's type in string mode and tree's level
/*************************************************************************/

void NODE2::printNode(string node, int level){
	// redo

	cout << "asknasaskadakda = " << this->fu << ' ' << "fu unit port = " << this->fu_unit_port << ' ' << this->start << ' ' << this->iter << ' ' << this->period << ' ' << this->shift << " node = " << node << ' ' << level << '\n';

	if(this->left != NULL) this->left->printNode("left", level+1);
	if(this->right != NULL) this->right->printNode("right", level+1);

}


/*************************************************************************/
//Delete in future
//
//
/*************************************************************************/

void NODE2::testing() {


	cout << "akdmaskdasjdasdnajdnads\n\n";

}

/*************************************************************************/
//Init data engine node
//
//Receives as arguments component's name, iterations value, period value, start value, shift value, delay value, increment value,
//two leafs, iterations type (REGISTER or CONST) period type, start type, shift type, increment type and
//two auxiliary vectors, responsible for save functional units in use
//
//Is a constructor.
/*************************************************************************/

NODE2::NODE2(string fu , int iter , int period, int start, int shift, int delay, int incr, NODE2 *d, NODE2 *e, int iter_type, int period_type, int start_type, int shift_type, int incr_type, int *vec_alu, int *vec_mult){
	
	int i=0;
	std::list<int>::iterator auxIt;
	

	this->left = d;
	this->right = e;

	this->fu = fu;
	
	

	if(fu == "Ralu0") { this->fu_unit = salu0; vec_alu[returnVectorPosition(_Ralu0)]=1; this->disabled=1; }
	if(fu == "Ralu1") { this->fu_unit = salu1; vec_alu[returnVectorPosition(_Ralu1)]=1; this->disabled=1; }
	if(fu == "RaluLite0") { this->fu_unit = salulite0; vec_alu[returnVectorPosition(_RaluLite0)]=1; this->disabled=1; }
	if(fu == "RaluLite1") { this->fu_unit = salulite1; vec_alu[returnVectorPosition(_RaluLite1)]=1; this->disabled=1; }
	if(fu == "RaluLite2") { this->fu_unit = salulite2; vec_alu[returnVectorPosition(_RaluLite2)]=1; this->disabled=1; }
	if(fu == "RaluLite3") { this->fu_unit = salulite3; vec_alu[returnVectorPosition(_RaluLite3)]=1; this->disabled=1; }
	if(fu == "Rmult0") { this->fu_unit = smul0; vec_mult[returnVectorPosition(_Rmult0)]=1; this->disabled=1; }
	if(fu == "Rmult1") { this->fu_unit = smul1; vec_mult[returnVectorPosition(_Rmult1)]=1; this->disabled=1; }
	if(fu == "Rmult2") { this->fu_unit = smul2; vec_mult[returnVectorPosition(_Rmult2)]=1; this->disabled=1; }
	if(fu == "Rmult3") { this->fu_unit = smul3; vec_mult[returnVectorPosition(_Rmult3)]=1; this->disabled=1; }
	if(fu == "Rbs0") { this->fu_unit = sbs0; this->disabled=1; }


	if(fu == "+" || fu == "-" || fu == "&" || fu == "|" || fu == "~&" || fu == "^" ) {
		for(i=0;i<6;i++) {
				if(vec_alu[i]!=1)  { 
					this->fu_unit = salu0+i;
					vec_alu[i]=1;
					if(markResources[salu0+i-4]!=1) {
						totalResources.push_back(salu0+i);
						markResources[salu0+i-4]=1; }
					i=8;		}}
		if(i==6) { cout << "Not enough alus!!!!\n"; exit(-1); }

	}
	if(fu == "*" || fu == "*'") 	{ 
		for( i=0;i<4;i++) {
			if(vec_mult[i]!=1)  { 
				this->fu_unit = smul0+i;
				vec_mult[i]=1;
				if(markResources[smul0+i-4]!=1) {
					totalResources.push_back(smul0+i);
					markResources[smul0+i-4]=1; }
				i=8;		}}  
		if(i==4) { cout << "Not enough multipliers!!!!\n"; exit(-1); } }

			//if(*vec_mult < 4) { this->fu_unit = smul0+*vec_mult; *vec_mult=*vec_mult+1; } 
			//  else cout << "Not enough multipliers\n";      }




	if(fu == "mem0A") { this->fu_unit=MEM0; this->fu_unit_port = PORT_A; 
		if(markResources[smem0A-4]!=1) {totalResources.push_back(smem0A); markResources[smem0A-4]=1; }
		else this->opt = 1;
			vec_aux[0].compareData(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type);  
			vec_aux[0].putParameters(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type); }

			
	if(fu == "mem1B") { this->fu_unit=MEM1;  this->fu_unit_port = PORT_B; 
		if(markResources[smem1B-4]!=1) { totalResources.push_back(smem1B); markResources[smem1B-4]=1;}
		else this->opt=1;
		vec_aux[3].compareData(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type); 
		vec_aux[3].putParameters(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type); }
	if(fu == "mem2A") { this->fu_unit=MEM2; this->fu_unit_port = PORT_A; 
		if(markResources[smem2A-4]!=1) { totalResources.push_back(smem2A); markResources[smem2A-4]=1;}
		else this->opt = 1;
		vec_aux[4].compareData(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type);  
		vec_aux[4].putParameters(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type); }
	if(fu == "mem3B") { this->fu_unit=MEM3;  this->fu_unit_port = PORT_B; 
		if(markResources[smem3B-4]!=1) { totalResources.push_back(smem3B); markResources[smem3B-4]=1;} 
		else this->opt = 1;
		vec_aux[7].compareData(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type); 
		vec_aux[7].putParameters(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type); }
	if(fu == "mem0B") { this->fu_unit=MEM0; this->fu_unit_port = PORT_B; 
		if(markResources[smem0B-4]!=1) { totalResources.push_back(smem0B); markResources[smem0B-4]=1;} 
		else this->opt = 1;
		vec_aux[1].compareData(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type); 
		vec_aux[1].putParameters(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type); }
	if(fu == "mem1A") { this->fu_unit=MEM1; this->fu_unit_port = PORT_A; 
		if(markResources[smem1A-4]!=1) { totalResources.push_back(smem1A); markResources[smem1A-4]=1;} 
		else this->opt = 1;
		vec_aux[2].compareData(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type); 
		vec_aux[2].putParameters(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type); }
	if(fu == "mem2B") { this->fu_unit=MEM2; this->fu_unit_port = PORT_B; 
		if(markResources[smem2B-4]!=1) { totalResources.push_back(smem2B); markResources[smem2B-4]=1;} 
		else this->opt = 1;
		vec_aux[5].compareData(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type); 
		vec_aux[5].putParameters(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type); }
	if(fu == "mem3A") { this->fu_unit=MEM3; this->fu_unit_port = PORT_A; 
		if(markResources[smem3A-4]!=1) { totalResources.push_back(smem3A); markResources[smem3A-4]=1;} 
		else this->opt = 1;
		vec_aux[6].compareData(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type); 
		vec_aux[6].putParameters(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type); }

	//testResource(iter, period, start, shift, incr, iter_type, period_type, start_type, shift_type, incr_type, fu_unit, fu_unit_port);


	this->iter = iter;
	this->period = period;
	this->start = start;
	this->shift = shift;
	this->incr = incr;
	this->iter_type = iter_type;
	this->period_type = period_type;
	this->start_type = start_type;
	this->shift_type = shift_type;
	this->incr_type = incr_type;

	

	

}

/*************************************************************************/
//Calculates Delay in data engine expressions
//
//Receives a data engine expression node and the previous delay calculated.
/*************************************************************************/

void COMMAND::calcDelay(NODE2 *node_aux, int delay) {

	int aux=0;

	

	if(node_aux->fu_unit >= salu0 && node_aux->fu_unit <= salulite3) { aux = delay+2; node_aux->delay = largest_node-aux; }
	if(node_aux->fu_unit >= smul0 && node_aux->fu_unit <= smul3) { aux = delay+3; node_aux->delay = largest_node-aux; }

	if(largest_node < delay) largest_node = delay; 

	if(node_aux->left!=NULL)
		this->calcDelay(node_aux->left, aux);


	if(node_aux->right!=NULL)
		this->calcDelay(node_aux->right, aux);

	if(node_aux->fu_unit >= MEM0 && node_aux->fu_unit <= MEM3) {
		vec_aux[(node_aux->fu_unit-1)*2+(node_aux->fu_unit_port-1)].compareDelay(largest_node-delay);
		vec_aux[(node_aux->fu_unit-1)*2+(node_aux->fu_unit_port-1)].putDelay(largest_node-delay);
		node_aux->delay = largest_node-delay; }

}

/*************************************************************************/
//Generates data engine expressions assembly
//
//Receives a data engine expression node and the tree's level.
/*************************************************************************/

void COMMAND::genAsmExpressionMem(NODE2 *node_aux, int level) {

	char *port = new char[4];
	int aux=0;

	if(node_aux->left!=NULL) 
		this->genAsmExpressionMem(node_aux->left, level+1);
		

	if(node_aux->right!=NULL)
		this->genAsmExpressionMem(node_aux->right, level+1);

	if(node_aux->disabled==1) _genFU.genDisableFUAsm(node_aux->fu_unit);


	if(node_aux->fu.substr(0,3) == "mem" && node_aux->opt!=1) {
		//if(node_aux->period_type == 0) { node_aux->period = 1; node_aux->period_type = CONST; }
		strcpy(port, node_aux->fu.c_str());
		if(port[4] == 'A') aux = PORT_A;
		if(port[4] == 'B') aux = PORT_B;
		if(node_aux->start_type!=0) {
			cout << '\t';
			_genMem.genMemStartAsm (returnSelUnit(node_aux->fu), aux, node_aux->start, node_aux->start_type, 0, 0, 0);  }
		if(node_aux->iter_type!=0) {
			cout << '\t';
			_genMem.genMemIterAsm (returnSelUnit(node_aux->fu), aux, node_aux->iter, node_aux->iter_type, 0, 0, 0); }
		if(node_aux->incr_type!=0) {
			cout << '\t';
			_genMem.genMemIncrAsm (returnSelUnit(node_aux->fu), aux, node_aux->incr, node_aux->incr_type, 0, 0, 0); }
		if(node_aux->shift_type!=0 && node_aux->period_type!=0) {
			cout << '\t';
			_genMem.genMemShiftAsm (returnSelUnit(node_aux->fu), aux, node_aux->shift, node_aux->shift_type, node_aux->period, node_aux->period_type, SUB);  }

		if(node_aux->shift_type!=0 && node_aux->period_type==0) {
			cout << '\t';
			_genMem.genMemShiftAsm (returnSelUnit(node_aux->fu), aux, node_aux->shift, node_aux->shift_type, 1, CONST, SUB);  }


		if(node_aux->period_type!=0) {
			cout << '\t';
			_genMem.genMemPerAsm (returnSelUnit(node_aux->fu), aux, node_aux->period, node_aux->period_type, 0, 0, 0); 
			if(auto_duty_disable == 0) { 
				cout << '\t';
				_genMem.genMemDutyAsm (returnSelUnit(node_aux->fu), aux, node_aux->period, node_aux->period_type, 0, 0, 0);	}  }
		cout << '\t';
		_genMem.genMemDelayAsm (returnSelUnit(node_aux->fu), aux, node_aux->delay, CONST, 0, 0, 0); 
		}



	if(node_aux->fu=="*") {

		//cout << '\t';
		//_genFU.genMultLonhiAsm (1, node_aux->fu_unit);		
		cout << '\t';
		if(node_aux->left->fu_unit_port == 0)  _genFU.genMultConnectPortA_Asm(node_aux->fu_unit, node_aux->left->fu_unit);  
		else _genFU.genMultConnectPortA_Asm(node_aux->fu_unit, node_aux->left->fu_unit, node_aux->left->fu_unit_port);
		cout << '\t';
		if(node_aux->right->fu_unit_port == 0) _genFU.genMultConnectPortB_Asm(node_aux->fu_unit, node_aux->right->fu_unit);     
		else _genFU.genMultConnectPortB_Asm(node_aux->fu_unit, node_aux->right->fu_unit, node_aux->right->fu_unit_port);

	}




	if(node_aux->fu=="*'") {

		cout << '\t';
		_genFU.genMultLonhiAsm (1, node_aux->fu_unit);	
		cout << '\t';
		_genFU.genMultDiv2Asm (1, node_aux->fu_unit);		
		cout << '\t';
		if(node_aux->left->fu_unit_port == 0)  _genFU.genMultConnectPortA_Asm(node_aux->fu_unit, node_aux->left->fu_unit);  
		else _genFU.genMultConnectPortA_Asm(node_aux->fu_unit, node_aux->left->fu_unit, node_aux->left->fu_unit_port);
		cout << '\t';
		if(node_aux->right->fu_unit_port == 0) _genFU.genMultConnectPortB_Asm(node_aux->fu_unit, node_aux->right->fu_unit);     
		else _genFU.genMultConnectPortB_Asm(node_aux->fu_unit, node_aux->right->fu_unit, node_aux->right->fu_unit_port);

	}



	if(node_aux->fu=="+" || node_aux->fu=="-" || node_aux->fu=="&" || node_aux->fu=="|" || node_aux->fu=="&" || node_aux->fu=="~&" || node_aux->fu=="^") {
		
		if(node_aux->fu=="+") aux = ALU_ADD;
		if(node_aux->fu=="-") aux = ALU_SUB;
		if(node_aux->fu=="&") aux = ALU_AND;
		if(node_aux->fu=="~&") aux = ALU_NAND;
		if(node_aux->fu=="^") aux = ALU_XOR;
		if(node_aux->fu=="|") aux = ALU_OR;
		
		if(node_aux->fu_unit == salu0 || node_aux->fu_unit == salu1) {
				cout << '\t';
				_genFU.genAluOperAsm (aux, node_aux->fu_unit); 
				cout << '\t';
				if(node_aux->left->fu_unit_port == 0) {   _genFU.genAluConnectPortA_Asm(node_aux->fu_unit, node_aux->left->fu_unit);  }
				else { _genFU.genAluConnectPortA_Asm(node_aux->fu_unit, node_aux->left->fu_unit, node_aux->left->fu_unit_port); }
				cout << '\t';
				if(node_aux->right->fu_unit_port == 0) _genFU.genAluConnectPortB_Asm(node_aux->fu_unit, node_aux->right->fu_unit);     
				else _genFU.genAluConnectPortB_Asm(node_aux->fu_unit, node_aux->right->fu_unit, node_aux->right->fu_unit_port);     


				}
		else {
				cout << '\t';
				_genFU.genAluLiteOperAsm (aux, node_aux->fu_unit); 
				cout << '\t';
				if(node_aux->left->fu_unit_port == 0)  _genFU.genAluLiteConnectPortA_Asm(node_aux->fu_unit, node_aux->left->fu_unit);  
				else _genFU.genAluLiteConnectPortA_Asm(node_aux->fu_unit, node_aux->left->fu_unit, node_aux->left->fu_unit_port);
				cout << '\t';
				if(node_aux->right->fu_unit_port == 0) _genFU.genAluLiteConnectPortB_Asm(node_aux->fu_unit, node_aux->right->fu_unit);     
				else _genFU.genAluLiteConnectPortB_Asm(node_aux->fu_unit, node_aux->right->fu_unit, node_aux->right->fu_unit_port);        } }

			

}


/*************************************************************************/
//Set register node in controller expressions
//
//Receives as argument the register in string mode.
/*************************************************************************/
void NODE::setNode(string a){

	if(a == "R1"){ value = _R1; cnst = 1; }
	if(a == "R2"){ value = _R2; cnst = 1; }
	if(a == "R3"){ value = _R3; cnst = 1; }
	if(a == "R4"){ value = _R4; cnst = 1; }
	if(a == "R5"){ value = _R5; cnst = 1; }
	if(a == "R6"){ value = _R6; cnst = 1; }
	if(a == "R7"){ value = _R7; cnst = 1; }
	if(a == "R8"){ value = _R8; cnst = 1; }
	if(a == "R9"){ value = _R9; cnst = 1; }
	if(a == "R10"){ value = _R10; cnst = 1; }
	if(a == "R11"){ value = _R11; cnst = 1; }
	if(a == "R12"){ value = _R12; cnst = 1; }
	if(a == "R13"){ value = _R13; cnst = 1; }
	if(a == "R14"){ value = _R14; cnst = 1; }
	if(a == "R15"){ value = _R15; cnst = 1; }
	if(a == "RB" ){ value = _RB; cnst = 1; }
	if(a == "Ralu0"){ value = _Ralu0; cnst = 1; }
	if(a == "Ralu1"){ value = _Ralu1; cnst = 1; }
	if(a == "RaluLite0"){ value = _RaluLite0; cnst = 1; }
	if(a == "RaluLite1"){ value = _RaluLite1; cnst = 1; }
	if(a == "RaluLite2"){ value = _RaluLite2; cnst = 1; }
	if(a == "RaluLite3"){ value = _RaluLite3; cnst = 1; }
}


/*************************************************************************/
//Init a controller expression node with nothing
//
//
/*************************************************************************/
NODE::NODE(){
	oper = "";
	cnst = "";
	value = 0;
	left = NULL;
	right = NULL;
}

/*************************************************************************/
//Init a controller expression node
//
//Receives as arguments operation in string mode, the unit in string mode, the value if type
//is constant, two leafs and the type.
/*************************************************************************/

NODE::NODE(string a , string b , int c, NODE *d, NODE *e, int operType){
	oper = a;
	cnst = b;
	value = c;
	left = d;
	right = e;

	if(oper == "+") 	this->ctrOperator = ADD;
	if(oper == "-") 	this->ctrOperator = SUB;	
	if(oper == "&") 	this->ctrOperator = AND;
	if(oper == ">>" || oper == "<<") this->ctrOperator = SHFT;
	if(oper == "<<") this->operShift = -1;
	if(oper == ">>") this->operShift = 1;
	
	if(operType == CONST || operType == REGISTER || operType==_MEMORY_TYPE) this->operOneType = operType;
	if(operType == CONST) this->value = c;
	if(operType == REGISTER) {
		if(b == "R1") this->value = _R1;
		if(b == "R2") this->value = _R2;
		if(b == "R3") this->value = _R3;
		if(b == "R4") this->value = _R4;
		if(b == "R5") this->value = _R5;
		if(b == "R6") this->value = _R6;
		if(b == "R7") this->value = _R7;
		if(b == "R8") this->value = _R8;
		if(b == "R9") this->value = _R9;
		if(b == "R10") this->value = _R10;
		if(b == "R11") this->value = _R11;
		if(b == "R12") this->value = _R12;
		if(b == "R13") this->value = _R13;
		if(b == "R14") this->value = _R14;
		if(b == "R15") this->value = _R15;
		if(b == "Ralu0") this->value = _Ralu0;
		if(b == "Ralu1") this->value = _Ralu1;
		if(b == "RaluLite0") this->value = _RaluLite0;
		if(b == "RaluLite1") this->value = _RaluLite1;
		if(b == "RaluLite2") this->value = _RaluLite2;
		if(b == "RaluLite3") this->value = _RaluLite3;
		if(b == "Rbs0") this->value = _Rbs0;
		if(b == "RB") this->value = _RB;   }
	if(operType == _MEMORY_TYPE) {
		if(b == "mem0A") this->value = _Mem0A;
		if(b == "mem0B") this->value = _Mem0B;
		if(b == "mem1A") this->value = _Mem1A;
		if(b == "mem1B") this->value = _Mem1B;
		if(b == "mem2A") this->value = _Mem2A;
		if(b == "mem2B") this->value = _Mem2B;
		if(b == "mem3A") this->value = _Mem3A;
		if(b == "mem3B") this->value = _Mem3B;   
		this->memOffset = c; }


	

}

void COMMAND::addNode(NODE *a){
	tree = a;
}

/*************************************************************************/
//Print controller expression's node
//
//Receives as arguments the unit in node and the level
/*************************************************************************/
void NODE::printNode(string node, int level){
	// redo


	cout << "asknasaskadakda = " << this->oper << ' ' << this->value << ' ' << this->cnst << " node = " << node << ' ' << ctrOperator << ' ' << operOneType << '\n';

	if(this->left != NULL) this->left->printNode("left", level+1);
	if(this->right != NULL) this->right->printNode("right", level+1);

}


/*************************************************************************/
//Reset a command
//
//
/*************************************************************************/

void COMMAND::resetCmd(){
	this-> cmdType = 0;
	this-> unit = 0;
	this-> port = 0;
	this-> method = 0;
	this-> cnst = 0;
	this-> selUnit = 0;
	this-> selUnitPort = 0;

	this-> init = 0;
	this-> operOne = 0;
	this-> operTwo = 0;
	this-> operOneType = 0;
	this-> operTwoType = 0;
	this-> relOper = 0;
	this-> ctrlOperator = 0;
	this-> jmpCnst = 0;
	this-> aluOperator = 0;
	this->forCycle.initTarget = 0;
	this->forCycle.initOperOneType=0; 
	this->forCycle.initOperOne=0;
	this->forCycle.initOperTwoType=0; 
	this->forCycle.initOperTwo=0; 
	this->forCycle.condOperOneType=0; 
	this->forCycle.condOperOne=0; 
	this->forCycle.condRelOper=0; 
	this->forCycle.incrTarget=0;	
	this->forCycle.incrOperOneType=0;
	this->forCycle.incrOperOne=0; 
	this->forCycle.incrOperTwoType=0; 
	this->forCycle.incrOperTwo=0; 
	this->forCycle.initCtrlOperator=0;
	this->forCycle.incrCtrlOperator=0;
	this->tree = new NODE();
	//resetComponentListList();
	//resetTargetConditionalPairsList();
}

/*************************************************************************/
//Set auxiliary jump const
//
//
/*************************************************************************/
void COMMAND::setJmpCnst(int var){
	this->jmpCnst = var;
}


/*************************************************************************/
//Ver o que é isto
//
//
/*************************************************************************/

void COMMAND::AddTargetConditionalPair(std::string line_name, int line_num){

	targetConditionalPairsList.first = line_name;
	targetConditionalPairsList.second = line_num;
}

/*************************************************************************/
//Set Alu operator in command
//
//Receives as argument the operation in string.
/*************************************************************************/
void COMMAND::setOperator(string var){

	if(var == "+") this->aluOperator = ALU_ADD;
	if(var == "-") this->aluOperator = ALU_SUB;
	if(var == "&") this->aluOperator = ALU_AND;
	if(var == "|") this->aluOperator = ALU_OR;
	if(var == "^") this->aluOperator = ALU_XOR;
	if(var == "~&") this->aluOperator = ALU_NAND;
	if(var == "SEXT8") this->aluOperator = SEXT8;
	if(var == "SEXT16") this->aluOperator = SEXT16;
	if(var == "SRA") this->aluOperator = _SRA;
	if(var == "SRL") this->aluOperator = _SRL;
	if(var == "CMPU") this->aluOperator = CMPU;
	if(var == "CMPS") this->aluOperator = CMPS;
	if(var == "CLZ") this->aluOperator = _CLZ;
	if(var == "MAX") this->aluOperator = ALU_MAX;
	if(var == "MIN") this->aluOperator = ALU_MIN;
	if(var == "ABS") this->aluOperator = ALU_ABS; 


}

/*************************************************************************/
//Returns targetConditional
//
//
/*************************************************************************/
int COMMAND::returnTargetConditional()  {

	return this->targetConditional;


}

/*************************************************************************/
//Set Rel Oper
//
//Receives as argument the controller operation in string mode.
/*************************************************************************/
void COMMAND::setRelOper(string var){
	

	if(var == "+") 	this->ctrlOperator = ADD;
	if(var == "-") 	this->ctrlOperator = SUB;	
	if(var == "&") 	this->ctrlOperator = AND;
	if(var == ">>" || var == "<<") this->ctrlOperator = SHFT;
	if(var == "<<") this->operTwo = this->operTwo*-1;
	
	
}

/*************************************************************************/
//Set operTwo Type
//
//Receives as argument operTwoType value
/*************************************************************************/

void COMMAND::setOperTwoType(int var){
	this->operTwoType = var;
}

/*************************************************************************/
//Set operOne Type
//
//Receives as argument operOneType value
/*************************************************************************/
void COMMAND::setOperOneType(int var){
	this->operOneType = var;
}

/*************************************************************************/
//Set controller expression's output type
//
//Receives as argument output register value in int mode.
/*************************************************************************/
void COMMAND::setTargetConditionalType(int var){
	this->targetConditionalType = var;
}
/*************************************************************************/
//Set operand two. Useful in data engine configurations, conditions and cycles.
//
//Receives operand two in string mode.
/*************************************************************************/
void COMMAND::setOperTwo(string var){

	if(var == "R1") this->operTwo = _R1;
	if(var == "R2") this->operTwo = _R2;
	if(var == "R3") this->operTwo = _R3;
	if(var == "R4") this->operTwo = _R4;
	if(var == "R5") this->operTwo = _R5;
	if(var == "R6") this->operTwo = _R6;
	if(var == "R7") this->operTwo = _R7;
	if(var == "R8") this->operTwo = _R8;
	if(var == "R9") this->operTwo = _R9;
	if(var == "R10") this->operTwo = _R10;
	if(var == "R11") this->operTwo = _R11;
	if(var == "R12") this->operTwo = _R12;
	if(var == "R13") this->operTwo = _R13;
	if(var == "R14") this->operTwo = _R14;
	if(var == "R15") this->operTwo = _R15;
	if(var == "Ralu0") this->operTwo = _Ralu0;
	if(var == "Ralu1") this->operTwo = _Ralu1;
	if(var == "RaluLite0") this->operTwo = _RaluLite0;
	if(var == "RaluLite1") this->operTwo = _RaluLite1;
	if(var == "RaluLite2") this->operTwo = _RaluLite2;
	if(var == "RaluLite3") this->operTwo = _RaluLite3;
	if(var == "RB") this->operTwo = _RB;

}


/*************************************************************************/
//Set operand one. Useful in data engine configurations, conditions and cycles.
//
//Receives operand one in string mode.
/*************************************************************************/
void COMMAND::setOperOne(string var){

	if(var == "R1") this->operOne = _R1;
	if(var == "R2") this->operOne = _R2;
	if(var == "R3") this->operOne = _R3;
	if(var == "R4") this->operOne = _R4;
	if(var == "R5") this->operOne = _R5;
	if(var == "R6") this->operOne = _R6;
	if(var == "R7") this->operOne = _R7;
	if(var == "R8") this->operOne = _R8;
	if(var == "R9") this->operOne = _R9;
	if(var == "R10") this->operOne = _R10;
	if(var == "R11") this->operOne = _R11;
	if(var == "R12") this->operOne = _R12;
	if(var == "R13") this->operOne = _R13;
	if(var == "R14") this->operOne = _R14;
	if(var == "R15") this->operOne = _R15;
	if(var == "Ralu0") this->operOne = _Ralu0;
	if(var == "Ralu1") this->operOne = _Ralu1;
	if(var == "RaluLite0") this->operOne = _RaluLite0;
	if(var == "RaluLite1") this->operOne = _RaluLite1;
	if(var == "RaluLite2") this->operOne = _RaluLite2;
	if(var == "RaluLite3") this->operOne = _RaluLite3;
	if(var == "RB") this->operOne = _RB;
	if(var == "mem0A") this->operOne = _Mem0A;
	if(var == "mem0B") this->operOne = _Mem0B;
	if(var == "mem1A") this->operOne = _Mem1A;
	if(var == "mem1B") this->operOne = _Mem1B;
	if(var == "mem2A") this->operOne = _Mem2A;
	if(var == "mem2B") this->operOne = _Mem2B;
	if(var == "mem3A") this->operOne = _Mem3A;
	if(var == "mem3B") this->operOne = _Mem3B; 
	if(var == "MEM0" || var == "mem0") this->operOne = ENG_MEM_BASE;
	if(var == "MEM1" || var == "mem1") this->operOne = ENG_MEM_BASE + (int)pow(2,ADDR_W);
	if(var == "MEM2" || var == "mem2") this->operOne = ENG_MEM_BASE + (int)pow(2,ADDR_W)*2;
	if(var == "MEM3" || var == "mem3") this->operOne = ENG_MEM_BASE + (int)pow(2,ADDR_W)*3; 
	if(var == "DMA_EXT_ADDR") this->operOne = _DMA_EXT_ADDR;
	if(var == "DMA_INT_ADDR") this->operOne = _DMA_INT_ADDR;
	
	

}

/*************************************************************************/
//Set init. 
//
//Receives init value.
/*************************************************************************/
void COMMAND::setInit(int var){
	this->init = var;
}

/*************************************************************************/
//Set operand one. Useful in data engine configurations, conditions and cycles.
//
//Receives operand one in int mode. Useful when oper one is constant.
/*************************************************************************/
void COMMAND::setOperOne(int var){
	this->operOne = var;
}

/*************************************************************************/
//Set operand one. Useful in data engine configurations, conditions and cycles.
//
//Receives operand one in int mode. Useful when oper one is constant and negative.
/*************************************************************************/
void COMMAND::setOperOne(char signal, int var) {
	this->operOne = var*-1; }

/*************************************************************************/
//Set operand two. Useful in data engine configurations, conditions and cycles.
//
//Receives operand two in int mode. Useful when oper two is constant.
/*************************************************************************/
void COMMAND::setOperTwo(int var){
	this->operTwo = var;
}

int COMMAND::popComponentListList(){
	return componentList.back();
}

int COMMAND::sizComponentListList(){

	return componentList.size();
}

void COMMAND::setComponentList(int var){

	componentList.push_back(var);

}

/*************************************************************************/
//Set componentsList. Used in init, run and wait instructions.
//
//Receives as argument the functional unit to init/run/wait.
/*************************************************************************/
void COMMAND::setComponentList(string var) { 


	if(var == "mem0") { componentList.push_back(smem0A); componentList.push_back(smem0B); }
	if(var == "mem1") { componentList.push_back(smem1A); componentList.push_back(smem1B); }
	if(var == "mem2") { componentList.push_back(smem2A); componentList.push_back(smem2B); }
	if(var == "mem3") { componentList.push_back(smem3A); componentList.push_back(smem3B); }
	
	if(var == "mem0A") { componentList.push_back(smem0A);  }
	if(var == "mem1B") {  componentList.push_back(smem1B); }
	if(var == "mem2A") { componentList.push_back(smem2A); }
	if(var == "mem3B") {  componentList.push_back(smem3B); }
	if(var == "mem0B") { componentList.push_back(smem0B);  }
	if(var == "mem1A") {  componentList.push_back(smem1A); }
	if(var == "mem2B") { componentList.push_back(smem2B);  }
	if(var == "mem3A") {  componentList.push_back(smem3A); }

	if(var == "alu0") componentList.push_back(salu0);
	if(var == "alu1") componentList.push_back(salu1);
	if(var == "aluLite0") componentList.push_back(salulite0);
	if(var == "aluLite1") componentList.push_back(salulite1);
	if(var == "aluLite2") componentList.push_back(salulite2);
	if(var == "aluLite3") componentList.push_back(salulite3);
	if(var == "mult0") componentList.push_back(smul0);
	if(var == "mult1") componentList.push_back(smul1);
	if(var == "mult2") componentList.push_back(smul2);
	if(var == "mult3") componentList.push_back(smul3);
	if(var == "Bs0") componentList.push_back(sbs0);


}

/*************************************************************************/
//Não é usado!!!!!!!
/*************************************************************************/
void COMMAND::addComponentListList(int var){
	componentList.push_back(var);
}

/*************************************************************************/
//Não é usado!!!!!!!
/*************************************************************************/
void COMMAND::setSelUnitPort(int var){
	this->selUnitPort = var;
}

/*************************************************************************/
//Set memory's port. Used in memory objects.
//
//Receives as argument the port in string mode.
/*************************************************************************/
void COMMAND::setSelUnitPort(string var){
	

	if(var == "A") { this->selUnitPort = PORT_A;  }
	if(var == "B") { this->selUnitPort = PORT_B;  }

}

/*************************************************************************/
//Set functional unit. 
//Acho que não é usado!!!!!!!!
//Receives as argument the functional unit.
/*************************************************************************/
void COMMAND::setSelUnit(int var){
	this->selUnit = var;
}

/*************************************************************************/
//Set sel unit.
//
////Receives as argument the functional unit in string mode.
/*************************************************************************/
void COMMAND::setSelUnit(string var){



	if(var == "mem0") this->selUnit = MEM0;
	if(var == "mem1") this->selUnit = MEM1;
	if(var == "mem2") this->selUnit = MEM2;
	if(var == "mem3") this->selUnit = MEM3;
	if(var == "alu0") this->selUnit = salu0;
	if(var == "alu1") this->selUnit = salu1;
	if(var == "aluLite0") this->selUnit = salulite0;
	if(var == "aluLite1") this->selUnit = salulite1;
	if(var == "aluLite2") this->selUnit = salulite2;
	if(var == "aluLite3") this->selUnit = salulite3;
	if(var == "mult0") this->selUnit = smul0;
	if(var == "mult1") this->selUnit = smul1;
	if(var == "mult2") this->selUnit = smul2;
	if(var == "mult3") this->selUnit = smul3;
	if(var == "bs0") this->selUnit = sbs0;

}

/*************************************************************************/
//Set constant.
//
//Receives as argument the constant.
/*************************************************************************/
void COMMAND::setCnst(int var){
	this->cnst = var;
}

/*************************************************************************/
//Set constant.
//
//Receives as argument the method.
/*************************************************************************/
void COMMAND::setMethod(int var){
	this->method = var;
}

/*************************************************************************/
//Set method to connect to a memory.
//
////Receives as argument the command type and the port.
/*************************************************************************/
void COMMAND::setMethod(int cmdType, string port){
	
	if(port == "connectPortA") this->method = CONNECT_PORT_A;
	if(port == "connectPortB") this->method = CONNECT_PORT_B;
	if(port == "connect") this->method = CONNECT_PORT_MEM;	
	if(port == "disableFU") this->method = DISABLE;
	
}

void COMMAND::setPort(int var){
	this->port = var;
}

/*************************************************************************/
//Set controller expression output.
//
//Receives as argument the output in string.
/*************************************************************************/
void COMMAND::setTargetConditional(string var){

	if(var == "R1") this->targetConditional = _R1;
	if(var == "R2") this->targetConditional = _R2;
	if(var == "R3") this->targetConditional = _R3;
	if(var == "R4") this->targetConditional = _R4;
	if(var == "R5") this->targetConditional = _R5;
	if(var == "R6") this->targetConditional = _R6;
	if(var == "R7") this->targetConditional = _R7;
	if(var == "R8") this->targetConditional = _R8;
	if(var == "R9") this->targetConditional = _R9;
	if(var == "R10") this->targetConditional = _R10;
	if(var == "R11") this->targetConditional = _R11;
	if(var == "R12") this->targetConditional = _R12;
	if(var == "R13") this->targetConditional = _R13;
	if(var == "R14") this->targetConditional = _R14;
	if(var == "R15") this->targetConditional = _R15;
	if(var == "Ralu0") this->targetConditional = _Ralu0;
	if(var == "Ralu1") this->targetConditional = _Ralu1;
	if(var == "RaluLite0") this->targetConditional = _RaluLite0;
	if(var == "RaluLite1") this->targetConditional = _RaluLite1;
	if(var == "RaluLite2") this->targetConditional = _RaluLite2;
	if(var == "RaluLite3") this->targetConditional = _RaluLite3;
	if(var == "Rmult0") this->targetConditional = _Rmult0;
	if(var == "Rmult1") this->targetConditional = _Rmult1;
	if(var == "Rmult2") this->targetConditional = _Rmult2;
	if(var == "Rmult3") this->targetConditional = _Rmult3;
	if(var == "Rbs0") this->targetConditional = _Rbs0;
	if(var == "RB") this->targetConditional = _RB;
	if(var == "mem0A") this->targetConditional = _Mem0A;
	if(var == "mem0B") this->targetConditional = _Mem0B;
	if(var == "mem1A") this->targetConditional = _Mem1A;
	if(var == "mem1B") this->targetConditional = _Mem1B;
	if(var == "mem2A") this->targetConditional = _Mem2A;
	if(var == "mem2B") this->targetConditional = _Mem2B;
	if(var == "mem3A") this->targetConditional = _Mem3A;
	if(var == "mem3B") this->targetConditional = _Mem3B;

}

/*************************************************************************/
//Set Memory's port.
//
//Receives as argument the port in string.
/*************************************************************************/

void COMMAND::setPort(string var){

	
	
	if(var == "portA") this->port = PORT_A;
	if(var == "portB") this->port = PORT_B;

}

/*************************************************************************/
//Set unit used in Data Engine.
//Não é usado!!!!!!
//Receives as argument the functional unit type and the number's type
/*************************************************************************/
void COMMAND::setUnit(int FU_type, int var){

	if(FU_type == MULTIPLIER && var == 0) this->unit = smul0;
	if(FU_type == MULTIPLIER && var == 1) this->unit = smul1;		
	if(FU_type == MULTIPLIER && var == 2) this->unit = smul2;
	if(FU_type == MULTIPLIER && var == 3) this->unit = smul3;
	if(FU_type == ALU_FULL && var == 0) this->unit = salu0;
	if(FU_type == ALU_FULL && var == 1) this->unit = salu1;	
	if(FU_type == ALULITE && var == 0) this->unit = salulite0;
	if(FU_type == ALULITE && var == 1) this->unit = salulite1;		
	if(FU_type == ALULITE && var == 2) this->unit = salulite2;
	if(FU_type == ALULITE && var == 3) this->unit = salulite3;
	if(FU_type == MEMORY && var == 0) this->unit = MEM0;
	if(FU_type == MEMORY && var == 1) this->unit = MEM1;		
	if(FU_type == MEMORY && var == 2) this->unit = MEM2;
	if(FU_type == MEMORY && var == 3) this->unit = MEM3;	
	if(FU_type == BSHIFTER && var == 0) this->unit = sbs0;

}

/*************************************************************************/
//Set unit used in Data Engine.
//Receives as argument the functional unit
/*************************************************************************/
void COMMAND::setUnit(int FU_type){

	this->unit = FU_type;

}

/*************************************************************************/
//Return unit. I don't know if this method is used.
//
//Receives as argument the functional unit type and the unit's number.
//Returns the unit's number.
/*************************************************************************/
int COMMAND::returnUnit(int FU_type, int var){


	if(FU_type == MULTIPLIER && var == 0) return smul0;
	if(FU_type == MULTIPLIER && var == 1) return smul1;		
	if(FU_type == MULTIPLIER && var == 2) return smul2;
	if(FU_type == MULTIPLIER && var == 3) return smul3;
	if(FU_type == ALU_FULL && var == 0) return salu0;
	if(FU_type == ALU_FULL && var == 1) return salu1;	
	if(FU_type == ALULITE && var == 0) return salulite0;
	if(FU_type == ALULITE && var == 1) return salulite1;		
	if(FU_type == ALULITE && var == 2) return salulite2;
	if(FU_type == ALULITE && var == 3) return salulite3;
	if(FU_type == MEMORY && var == 0) return MEM0;
	if(FU_type == MEMORY && var == 1) return MEM1;		
	if(FU_type == MEMORY && var == 2) return MEM2;
	if(FU_type == MEMORY && var == 3) return MEM3;	

}

/*************************************************************************/
//Show command type.
//
//Returns command's type.
/*************************************************************************/
int COMMAND::showCmdType(){
	return cmdType;
}

/*************************************************************************/
//Set command type.
//
//Receives as argument command's type.
/*************************************************************************/
void COMMAND::setCmdType(int var){
	this->cmdType = var;
}

/*************************************************************************/
//Set condition operator.
//
//Receives condition in string. Used in cycles and conditions.
/*************************************************************************/
void COMMAND::setCtrlOperator(string var) {


	if(var == "==") this->relOper = EQUAL;
	if(var == "!=") this->relOper = DIFFERENT;	
	if(var == "<") this->relOper = LOWER;
	if(var == ">") this->relOper = HIGHER;	
	if(var == "<=") this->relOper = LOWER_EQUAL;
	if(var == ">=") this->relOper = HIGHER_EQUAL;	
	

}

/*************************************************************************/
//Print for structure. Used in debug.
//
//
/*************************************************************************/
void COMMAND::printForStruct() {

	cout << "initTarget = " << this->forCycle.initTarget << '\n';
	cout << "initOperOneType = " << this->forCycle.initOperOneType<< '\n'; 
	cout << "initOperIne = " << this->forCycle.initOperOne << '\n';
	cout << "initOperTwoType = " << this->forCycle.initOperTwoType<< '\n'; 
	cout << "initOperTwo = " << this->forCycle.initOperTwo << '\n'; 
	cout << "condOperOneType = " << this->forCycle.condOperOneType<< '\n'; 
	cout << "condOperOne = " << this->forCycle.condOperOne << '\n'; 
	cout << "condOperTwoType = " << this->forCycle.condOperTwoType<< '\n'; 
	cout << "condOperTwo = " << this->forCycle.condOperTwo << '\n'; 
	cout << "condRelOper = " << this->forCycle.condRelOper << '\n'; 
	cout << "incrTarget = " << this->forCycle.incrTarget << '\n';	
	cout << "incrOperOneType = " << this->forCycle.incrOperOneType << '\n';
	cout << "incrOperOne = " << this->forCycle.incrOperOne << '\n'; 
	cout << "incrOperTwoType = " << this->forCycle.incrOperTwoType << '\n'; 
	cout << "incrOperTwo = " << this->forCycle.incrOperTwo << '\n'; 
	cout << "initCtrlOperator = " << this->forCycle.initCtrlOperator << '\n';
	cout << "incrCtrlOperator = " << this->forCycle.incrCtrlOperator << '\n';




}


/*************************************************************************/
//Put hex value to program DMA
//
//
/*************************************************************************/
void COMMAND::setDMAHexValue(string a) {

	//this->dma_hex_string = a;
	_dmaEngine.setHexValue(a);

}

/*************************************************************************/
//Print command. Used in debug.
//
//
/*************************************************************************/
void COMMAND::printData() {

		
	cout << "cmdType = " << this->cmdType << '\n';
	cout << "unit = " << this->unit << '\n';
	cout << "port = " << this->port << '\n';
	cout << "method = " << this->method << '\n';
	cout << "cnst = " << this->cnst << '\n';
	cout << "selUnit = " << this->selUnit << '\n';
	cout << "selUnitPort = " << this->selUnitPort << '\n';

	cout << "init = " << this->init << '\n';
	cout << "operOne = " << this->operOne << '\n';
	cout << "targetConditional = " << this->targetConditional << '\n';
	cout << "targetConditionalType = " << this->targetConditionalType << '\n';
	cout << "operTwo = " << this->operTwo << '\n';
	cout << "operOneType = " << this->operOneType << '\n';
	cout << "operTwoType = " << this->operTwoType << '\n';
	cout << "relOper = " << this->relOper << '\n';
	cout << "ctrlOperator = " << this->ctrlOperator << '\n';
	cout << "jmpCnst = " << this->jmpCnst << '\n';
	cout << "aluOperator = " << this->aluOperator << '\n';
	cout << "----------------------------------------------------------------\n";
	this->printForStruct();
	//for (std::list<int>::iterator it = componentList.begin(); it != componentList.end(); it++) { //iterator
		//cout << *it << '\n'; }
	cout << "\n\n\n";

}


/*************************************************************************/
//Generates controller expression assembly.
//
//Receives as argument a controller expression node and the respective level.
/*************************************************************************/
void COMMAND::generateExpAssemblyTree(int level, NODE *node_aux) {


	if(level==0 && node_aux->ctrOperator ==0)  { 
		_controller.genExpressionAsm (this->targetConditional, node_aux->value, 0, node_aux->operOneType, 0, 0, 0, 1, "", this->targetOffset, this->targetConditionalType, 0);  }



	else {

	if(node_aux->left->ctrOperator!=0 && node_aux->right->ctrOperator!=0) {

	if(node_aux->right->ctrOperator!=0) {
		this->generateExpAssemblyTree(level+1, node_aux->right); }


	if(node_aux->left->ctrOperator!=0) {
		if(node_aux->left->ctrOperator!=0 && node_aux->right->ctrOperator!=0) {
			if(RB_full==1) { cout << "Not enough resources to do the expression\n"; exit(-1); }
			cout << '\t'+_wrw + ' ' + "RB" + _nextLine;
			cout << '\t';
			RB_full=1; }
		this->generateExpAssemblyTree(level+1, node_aux->left); } }


	else { 

	if(node_aux->left->ctrOperator!=0) {
		if(node_aux->left->ctrOperator!=0 && node_aux->right->ctrOperator!=0) {
			if(RB_full==1) { cout << "Not enough resources to do the expression\n"; exit(-1); }
			cout << '\t'+_wrw + ' ' + "RB" + _nextLine;
			cout << '\t';
			RB_full=1; }
		this->generateExpAssemblyTree(level+1, node_aux->left); } 



	if(node_aux->right->ctrOperator!=0) {
		this->generateExpAssemblyTree(level+1, node_aux->right); }  }
	


	if(node_aux->left->ctrOperator==0 && node_aux->right->ctrOperator==0 ) {
		if(node_aux->operShift==-1) node_aux->right->value = node_aux->right->value*-1;
		_controller.genExpressionAsm (0, node_aux->left->value, node_aux->right->value, node_aux->left->operOneType, node_aux->right->operOneType, node_aux->ctrOperator, 0, 1, "", node_aux->right->memOffset, this->targetConditionalType, node_aux->left->memOffset); }
	



	if(node_aux->left->ctrOperator==0 && node_aux->right->ctrOperator!=0 ) {
		_controller.genExpressionAsm (0, node_aux->left->value, node_aux->left->value, node_aux->left->operOneType, node_aux->left->operOneType, node_aux->ctrOperator, 0, 0, "", node_aux->right->memOffset, this->targetConditionalType, node_aux->right->memOffset); }


	if(node_aux->left->ctrOperator!=0 && node_aux->right->ctrOperator==0 ) {
		if(node_aux->operShift==-1) node_aux->right->value = node_aux->right->value*-1;
		_controller.genExpressionAsm (0, node_aux->right->value, node_aux->right->value, node_aux->right->operOneType, node_aux->right->operOneType, node_aux->ctrOperator, 0, 0, "", node_aux->right->memOffset, this->targetConditionalType, 0); }


	/*if(node_aux->left->ctrOperator!=0 && node_aux->right->ctrOperator!=0 ) {
		_controller.genExpressionAsm (0, node_aux->right->value, node_aux->right->value, node_aux->right->operOneType, node_aux->right->operOneType, node_aux->ctrOperator, 0, 0, ""); }  */


	if((level==0 || level==1) && node_aux->right->ctrOperator!=0 && node_aux->left->ctrOperator!=0) {
		_controller.genExpressionAsm (0, _RB, _RB, REGISTER, REGISTER, node_aux->ctrOperator, 0, 0, "", node_aux->right->memOffset, this->targetConditionalType, 0); 
		RB_full=0;  }




}

}


/*************************************************************************/
//Generates command assembly. This is the main function in assembly generation.
//This function calls all functions necessary to generate command's assembly.
//
/*************************************************************************/
void COMMAND::generateAssembly() {



	std::list<int>::iterator it;

	
		
	if(this->cmdType != _DMA || this->method != _SET_DIRECTION_ADDR || this->cmdType !=AUTO_DUTY) {

	if(this->cmdType != WAIT && this->cmdType != END_CONDITIONAL && this->cmdType != _DO && this->cmdType != END_ELSE && this->cmdType != _WHILE && this->cmdType != MEM_EXPRESSION && this->cmdType != ASSEMBLY_CMD && this->cmdType !=MEM_END) cout << this->label << '\t';  }
	
	//config type
	if(this->cmdType == CONFIG) {
		if(this->unit >= smul0 && this->unit <= smul3) {
			switch (this->method) {
				case SET_LONHI:
					_genFU.genMultLonhiAsm (this->cnst, this->unit);
					break;
				case SET_DIV2:
					_genFU.genMultDiv2Asm (this->cnst, this->unit);
					break;
				default:
					break;  } }

		if(this->unit >= salu0 && this->unit <= salu1) {

			switch (this->method) {
				case SET_OPER:
					_genFU.genAluOperAsm (this->aluOperator, this->unit);
					break;
				default:
					break;  } }

		if(this->unit >= salulite0 && this->unit <= salulite3) {
			switch (this->method) {
				case SET_OPER:
					_genFU.genAluLiteOperAsm (this->aluOperator, this->unit);
					break;
				default:
					break;  } }

		if(this->unit == sbs0) {
			switch (this->method) {
				case SET_BS_LNR:
					_genFU.genBsLNR_Asm(this->unit, this->cnst);
					break;
				case SET_BS_LNA:
					_genFU.genBsLNA_Asm(this->unit, this->cnst);
					break;
				default:
					break;  } }

		if(this->unit >= MEM0 && this->unit <= MEM3) {
			switch (this->method) {
				case SET_START:
					_genMem.genMemStartAsm (this->unit, this->port, this->operOne, this->operOneType, this->operTwo, this->operTwoType, this->ctrlOperator);
					break;
				case SET_DUTY:
					_genMem.genMemDutyAsm (this->unit, this->port, this->operOne, this->operOneType, this->operTwo, this->operTwoType, this->ctrlOperator);
					break;
				case SET_ITER:
					_genMem.genMemIterAsm (this->unit, this->port, this->operOne, this->operOneType, this->operTwo, this->operTwoType, this->ctrlOperator);
					break;
				case SET_INCR:
					_genMem.genMemIncrAsm (this->unit, this->port, this->operOne, this->operOneType, this->operTwo, this->operTwoType, this->ctrlOperator);
					break;
				case SET_PER:
					_genMem.genMemPerAsm (this->unit, this->port, this->operOne, this->operOneType, this->operTwo, this->operTwoType, this->ctrlOperator);
					break;
				case SET_DELAY:
					_genMem.genMemDelayAsm (this->unit, this->port, this->operOne, this->operOneType, this->operTwo, this->operTwoType, this->ctrlOperator);
					break;
				case SET_SHIFT:
					_genMem.genMemShiftAsm (this->unit, this->port, this->operOne, this->operOneType, this->operTwo, this->operTwoType, this->ctrlOperator);
					break;
				case SET_REVERSE:
					_genMem.genMemReverseAsm (this->unit, this->port, this->operOne, this->operOneType, this->operTwo, this->operTwoType, this->ctrlOperator);
					break;
				default:
					break;  } } }

	//connect type
	if(this->cmdType == CONNECT) {

		if(this->method == DISABLE) _genFU.genDisableFUAsm(this->unit);
		  

		if(this->unit >= salu0 && this->unit <= salu1) {

			if(this->method == CONNECT_PORT_A) {			
				if(selUnitPort == PORT_A || selUnitPort == PORT_B)  {
					_genFU.genAluConnectPortA_Asm (this->unit, this->selUnit, this->selUnitPort);  }
				else {

					_genFU.genAluConnectPortA_Asm (this->unit, this->selUnit);	}
					 }



			if(this->method == CONNECT_PORT_B) {			
				if(selUnitPort == PORT_A || selUnitPort == PORT_B)  {
					_genFU.genAluConnectPortB_Asm (this->unit, this->selUnit, this->selUnitPort);  }
				else {
					_genFU.genAluConnectPortB_Asm (this->unit, this->selUnit);	}
					 }   }

		if(this->unit >= salulite0 && this->unit <= salulite3) {

			if(this->method == CONNECT_PORT_A) {			
				if(selUnitPort == PORT_A || selUnitPort == PORT_B)  {
					_genFU.genAluLiteConnectPortA_Asm (this->unit, this->selUnit, this->selUnitPort);  }
				else {

					_genFU.genAluLiteConnectPortA_Asm (this->unit, this->selUnit);	}
					 }



			if(this->method == CONNECT_PORT_B) {			
				if(selUnitPort == PORT_A || selUnitPort == PORT_B)  {
					_genFU.genAluLiteConnectPortB_Asm (this->unit, this->selUnit, this->selUnitPort);  }
				else {
					_genFU.genAluLiteConnectPortB_Asm (this->unit, this->selUnit);	}
					 }   }

		if(this->unit >= smul0 && this->unit <= smul3) {

			if(this->method == CONNECT_PORT_A) {			
				if(selUnitPort == PORT_A || selUnitPort == PORT_B)  {
					_genFU.genMultConnectPortA_Asm (this->unit, this->selUnit, this->selUnitPort);  }
				else {

					_genFU.genMultConnectPortA_Asm (this->unit, this->selUnit);	}
					 }



			if(this->method == CONNECT_PORT_B) {			
				if(selUnitPort == PORT_A || selUnitPort == PORT_B)  {
					_genFU.genMultConnectPortB_Asm (this->unit, this->selUnit, this->selUnitPort);  }
				else {
					_genFU.genMultConnectPortB_Asm (this->unit, this->selUnit);	}
					 }   }

		if(this->unit >= sbs0 && this->unit <= sbs0) {

			if(this->method == CONNECT_PORT_A) {			
				if(selUnitPort == PORT_A || selUnitPort == PORT_B)  {
					_genFU.genBsConnectPortA_Asm (this->unit, this->selUnit, this->selUnitPort);  }
				else {

					_genFU.genBsConnectPortA_Asm (this->unit, this->selUnit);	}
					 }



			if(this->method == CONNECT_PORT_B) {			
				if(selUnitPort == PORT_A || selUnitPort == PORT_B)  {
					_genFU.genBsConnectPortB_Asm (this->unit, this->selUnit, this->selUnitPort);  }
				else {
					_genFU.genBsConnectPortB_Asm (this->unit, this->selUnit);	}
					 }   }






		if(this->unit >= MEM0 && this->unit <= MEM3) {
			if(selUnitPort == PORT_A || selUnitPort == PORT_B)  
				_genMem.genMemConnectAsm (this->unit, this->selUnit, this-> port, this->selUnitPort);  
			else
				_genMem.genMemConnectAsm (this->unit, this->selUnit, this-> port);  }  } 
	//init type
	if(this->cmdType == INIT) {
		if(this->method == CTRL_REG_INIT) 
			_ctrlReg.genInitEngineAsm(_ctrlReg.get(this->componentList, 1)); }		
	//run type
	if(this->cmdType == _RUN) {
		if(this->method == CTRL_REG_RUN) {

			 _ctrlReg.genInitEngineAsm(_ctrlReg.get(this->componentList, 2));  }

		if(this->method == CTRL_REG_RUN_EXP) {

			_ctrlReg.genInitEngineAsm(_ctrlReg.get(this->componentList, 1));
			cout << '\t';
			_ctrlReg.genInitEngineAsm(_ctrlReg.get(this->componentList, 2));  }	}
	//wait type
	if(this->cmdType == WAIT) {
		if(this->method == CTRL_REG_WAIT) 
			_ctrlReg.genWaitEngineAsm(_ctrlReg.check(this->componentList)); }

	//wait type
	if(this->cmdType == CLEAR) {
		if(this->method == CLEAR_REG_FLAGS) {
			_ctrlReg.clearFlags(); } }	



	//expression type
	if(this->cmdType == EXPRESSION) { 
		this->generateExpAssemblyTree(0, this->tree); 
		_controller.genExpressionAsm (this->targetConditional, 0, 0, 0, 0, 0, 1, 0, "", this->targetOffset, this->targetConditionalType, 0);  }


	if(this->cmdType == VECTOR_RW) { 
		this->generateExpAssemblyTree(0, this->tree); 
		_controller.genExpressionAsm (this->targetConditional, this->operOne, 0, this->operOneType, 0, 0, 1, 1, "", this->targetOffset, this->targetConditionalType, 0);  
	}


	if(this->cmdType == CONDITIONAL) {

		_controller.genIfAsm (this->operOne, this->operTwo, this->operOneType, this->operTwoType, this->relOper);

}

	if(this->cmdType == END_CONDITIONAL) {
		_controller.genEndifAsm (); }

	if(this->cmdType == JUMP) {
		_controller.genGotoAsm (this->gt_label); }

	if(this->cmdType == _WHILE) {
		_controller.genWhileAsm (this->operOne, this->operTwo, this->operOneType, this->operTwoType, this->relOper); }

	if(this->cmdType == END_WHILE) {
		_controller.genEndWhileAsm (); }


	if(this->cmdType == _FOR) {
		this->forCycleList.push_back(this->forCycle);
		_controller.genForAsm (this->forCycle); }

	if(this->cmdType == END_FOR) {
		_controller.genEndForAsm (this->forCycleList); 
		this->forCycleList.pop_back();}

	if(this->cmdType == _DO) {
		_controller.genDoAsm();  }

	if(this->cmdType == DO_WHILE) {
		_controller.genDoWhileAsm(this->operOne, this->operTwo, this->operOneType, this->operTwoType, this->relOper);   }

	if(this->cmdType == _ELSE) {
		_controller.genElseAsm();
		 }

	if(this->cmdType == END_ELSE) {
		_controller.genEndElseAsm();
		}

	if(this->cmdType == RETURN) {
		_controller.genReturnAsm();
		}


	if(this->cmdType == MEM_EXPRESSION) {
		this->calcDelay(mem_tree, 0);
		this->genAsmExpressionMem(mem_tree, 0);
		this->genEndMemExpAsm(this->mem_tree);
		}


	if(this->cmdType == ASSEMBLY_CMD) {
		this->genAsmAssemblyInstr();
		}

	if(this->cmdType == MEM_INSTR) {
		if(this->method == SAVE_CONFIG) _controller.writeRegMemory(this->cnst);
		if(this->method == LOAD_CONFIG) _controller.readRegMemory(this->cnst);
		
	}
	
	if(this->cmdType == MEM_END) { 
		this->cleanAllParameters();

	}

	if(this->cmdType == AUTO_DUTY) { 


		disableAutoDuty(this->en_dis_disable);

	}


	if(this->cmdType == _DMA) {
		if(this->method == _SET_EXT_ADDR) _dmaEngine.extAddr(this->operOne,this->operOneType,this->operTwo,this->operTwoType,this->ctrlOperator);  
		if(this->method == _SET_INT_ADDR) _dmaEngine.intAddr(this->operOne,this->operOneType,this->operTwo,this->operTwoType,this->ctrlOperator); 
		if(this->method == _SET_SIZE_ADDR) _dmaEngine.sizeAddr(this->cnst);
		if(this->method == _SET_DIRECTION_ADDR) _dmaEngine.directionAddr(this->cnst);   
		if(this->method == DMA_RUN) _dmaEngine.run();
		if(this->method == DMA_WAIT) _dmaEngine.wait();


}



}

/*************************************************************************/
//Send if's list to the command. Necessary to generate condition's labels to the branchs.
//
//Receives as argument a list with existed ifs.
/*************************************************************************/
void COMMAND::sendList(std::list<int> aux) {
	
	this->_controller.ifStatmentList = aux;

}

/*************************************************************************/
//Send while's list to the command. Necessary to generate condition's labels to the branchs.
//
//Receives as argument a list with existed whiles.
/*************************************************************************/
void COMMAND::sendWhileList(std::list<int> aux) {
	
	this->_controller.whileStatmentList = aux;

}

/*************************************************************************/
//Send for's list to the command. Necessary to generate condition's labels to the branchs.
//
//Receives as argument a list with existed fors.
/*************************************************************************/
void COMMAND::sendForList(std::list<int> aux) {
	
	this->_controller.forStatmentList = aux;

}

/*************************************************************************/
//Send do while's list to the command. Necessary to generate condition's labels to the branchs.
//
//Receives as argument a list with existed do whiles.
/*************************************************************************/
void COMMAND::sendDoList(std::list<int> aux) {
	
	this->_controller.doWhileStatmentList = aux;

}

/*************************************************************************/
//Returns existed component list. Necessary to generate condition's labels to the branchs.
//
//Returns a list with ifs for close.
/*************************************************************************/
list<int> COMMAND::returnComponentList() {

	return this->componentList;


}


/*************************************************************************/
//Returns existed ifs list. Necessary to generate condition's labels to the branchs.
//
//Returns a list with ifs for close.
/*************************************************************************/
list<int> COMMAND::returnList() {

	return this->_controller.ifStatmentList;


}

/*************************************************************************/
//Returns existed ifs list. Necessary to generate condition's labels to the branchs.
//
//Returns a list with ifs for close.
/*************************************************************************/
list<int> COMMAND::returnWhileList() {

	return this->_controller.whileStatmentList;


}

/*************************************************************************/
//Returns existed ifs list. Necessary to generate condition's labels to the branchs.
//
//Returns a list with ifs for close.
/*************************************************************************/
list<int> COMMAND::returnForList() {

	return this->_controller.forStatmentList;


}

/*************************************************************************/
//Returns existed ifs list. Necessary to generate condition's labels to the branchs.
//
//Returns a list with ifs for close.
/*************************************************************************/
list<int> COMMAND::returnDoWhileList() {

	return this->_controller.doWhileStatmentList;


}

/*************************************************************************/
//Returns existed ifs list. Necessary to generate condition's labels to the branchs.
//
//Returns a list with ifs for close.
/*************************************************************************/
void COMMAND::sendGlobalIf(int aux) {

	this->_controller.setNumberIfs(aux);


}

/*************************************************************************/
//Set existed ifs list. Necessary to generate condition's labels to the branchs.
//
//Receives as argument the if to put.
/*************************************************************************/
void COMMAND::sendGlobalWhile(int aux) {

	this->_controller.setNumberWhiles(aux);


}

/*************************************************************************/
//Set existed ifs list. Necessary to generate condition's labels to the branchs.
//
//Receives as argument the if to put.
/*************************************************************************/
void COMMAND::sendGlobalDo(int aux) {

	this->_controller.setNumberDos(aux);


}

/*************************************************************************/
//Set existed fors list. Necessary to generate condition's labels to the branchs.
//
//Receives as argument the for to put.
/*************************************************************************/
void COMMAND::sendGlobalFor(int aux) {

	this->_controller.setNumberFors(aux);


}

/*************************************************************************/
//Returns number of ifs.
//
//
/*************************************************************************/
int COMMAND::returnGlobalIf() {

	return this->_controller.returnNumberIfs();


}

/*************************************************************************/
//Returns number of whiles.
//
//
/*************************************************************************/
int COMMAND::returnGlobalWhile() {

	return this->_controller.returnNumberWhiles();


}

/*************************************************************************/
//Returns number of fors.
//
//
/*************************************************************************/
int COMMAND::returnGlobalFor() {

	return this->_controller.returnNumberFors();

}

/*************************************************************************/
//Returns number of ifs.
//
//
/*************************************************************************/
int COMMAND::returnGlobalDo() {

	return this->_controller.returnNumberDos();

}


/*************************************************************************/
//Sets label.
//
//
/*************************************************************************/
void COMMAND::setLabel(string str){
	this -> label = str;
}

/*************************************************************************/
//Sets goto label.
//
//
/*************************************************************************/
void COMMAND::setGtLabel(string str){
	this-> gt_label = str;
}

/*************************************************************************/
//Configs for parameters.
//
//
/*************************************************************************/
void COMMAND::setForParameter(int initOne, int initOneType, int initTwoType, int initTwo, int initTargt, int initCtrlOper, int condOperOneType, int condOperOne, int relOper, int incrTargt, int incrOneType, int incrOne, int incrTwo, int incrTwoType, int incrCtrlOper, int condOperTwo, int condOperTwoType) {


	int x = 1;
	int y= 2;
	if(initOne==1) 	  	this->forCycle.initOperOne = this->operOne;
	if(initOneType==1) 	this->forCycle.initOperOneType = this->operOneType;
	if(initTwoType==1) 	this->forCycle.initOperTwoType = this->operTwoType;
	if(initTwo==1)    	this->forCycle.initOperTwo = this->operTwo;
	if(initTargt==1)  	this->forCycle.initTarget = this->targetConditional;
	if(initCtrlOper==1)	this->forCycle.initCtrlOperator = this->ctrlOperator;
	if(condOperOneType==1)  this->forCycle.condOperOneType = this->operOneType;
	if(condOperOne==1)	this->forCycle.condOperOne = this->operOne;
	if(condOperTwo==1)	this->forCycle.condOperTwo = this->operTwo;
	if(condOperTwoType==1)  this->forCycle.condOperTwoType = this->operTwoType;
	if(relOper==1)     	this->forCycle.condRelOper = this->relOper;
	if(incrTargt==1)   	this->forCycle.incrTarget = this->targetConditional;
	if(incrOneType==1) 	this->forCycle.incrOperOneType = this->operOneType;
	if(incrOne==1)    	this->forCycle.incrOperOne = this->operOne;
	if(incrTwo==1)    	this->forCycle.incrOperTwo = this->operTwo;
	if(incrTwoType==1)      this->forCycle.incrOperTwoType = this->operTwoType; 
	if(incrCtrlOper==1)     this->forCycle.incrCtrlOperator = this->ctrlOperator;

}

/*************************************************************************/
//Sets chained fors list.
//
//
/*************************************************************************/
void COMMAND::setForAuxList(list<for_cycle> aux) {

	this->forCycleList = aux;

}

/*************************************************************************/
//Returns chained for list.
//
//
/*************************************************************************/
list<for_cycle> COMMAND::returnForAuxList() {

	return this->forCycleList;


}


/*************************************************************************/
//Indicates else exists 
//
//
/*************************************************************************/

void COMMAND::elseExists() {

	this->_controller.elseExists();


}

/*************************************************************************/
//Sets existed waits. Used in status reg.
//
//
/*************************************************************************/
void COMMAND::setNumberWaits(int aux) {

	this->_ctrlReg.setNumberWaits(aux);  }

/*************************************************************************/
//Returns existed waits. Used in status reg.
//
//
/*************************************************************************/
int COMMAND::returnNumberWaits() {

	return this->_ctrlReg.returnNumberWaits();  }

/*************************************************************************/
//Sets used multiplier resources. Receives a aux vector.
//
//
/*************************************************************************/
void COMMAND::setMultVec(int *aux) {

	for(int i=0;i<MULTS;i++) this->vec_mult[i] = aux[i];

	}

/*************************************************************************/
//Dummy. Delete in future.
//
//
/*************************************************************************/
void COMMAND::setAluVec(int *aux) {

	for(int i=0;i<ALUS;i++) this->vec_alu[i] = aux[i];

	for(int i=0;i<ALUS;i++) cout << this->vec_alu[i] << "aaaaaa\n";

}

/*************************************************************************/
//Returns mult vec. 
//
//
/*************************************************************************/
int * COMMAND::returnMultVec() {

	return this->vec_mult;

	}

/*************************************************************************/
//Returns alu vec. 
//
//
/*************************************************************************/
int * COMMAND::returnAluVec() {

	return this->vec_alu;

}

