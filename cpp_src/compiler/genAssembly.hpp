#ifndef __GEN_ASM_H_INCLUDED__   // if x.h hasn't been included yet...
#define __GEN_ASM_H_INCLUDED__   //   #define this so the compiler knows it has been included


#include <string>
#include <string.h>
#include <list>
#include "program.h"
#include "mem_ends.h"
#include "defs_library.h"
#include "asmStrings.hpp"

#define CTRL_BIT_SIZE 24

using namespace std;

string RegToString(int );

struct for_cycle {

	int initTarget;	//first parameter
	int initOperOneType; //first parameter
	int initOperOne; //first parameter
	int initOperTwoType; //first parameter
	int initOperTwo; //first parameter
	int initCtrlOperator; //first parameter
	int condOperOneType; //second parameter
	int condOperOne; //second parameter
	int condOperTwoType; //second parameter
	int condOperTwo; //second parameter
	int condRelOper; //second parameter
	int incrTarget;	//third parameter
	int incrOperOneType; //third parameter
	int incrOperOne; //third parameter
	int incrOperTwoType; //third parameter
	int incrOperTwo; //third parameter
	int incrCtrlOperator; //third parameter

};

class controller {


	public : int number_of_ifs;
	private : int number_of_whiles;
	private : int number_of_fors;	
	private : int number_of_dos;
	private : int else_exists;
	public  : std::list<int> ifStatmentList;
	public  : std::list<int> whileStatmentList;
	public  : std::list<int> forStatmentList;
	public  : std::list<int> doWhileStatmentList;	

	public : void genExpressionAsm(int targetConditional, int oper1, int oper2, int oper1Type, int oper2Type, int ctrlOperator, int controllerWrc, int controllerLdi, string label, int memOffset, int targetCondType, int);
	public : void genGotoIfAsm (int oper1, int oper2, int oper1Type, int oper2Type, int relOper, string jump);
	public : void genGotoAsm (string jump);
	public : string genIfLabel ();
	public : string genWhileLabel ();
	public : void genIfAsm (int oper1, int oper2, int oper1Type, int oper2Type, int relOper);
	public : void ifInit ();
	public : void genEndifAsm();
	public : void genWhileAsm (int oper1, int oper2, int oper1Type, int oper2Type, int relOper);	
	public : int returnNumberIfs();
	public : void setNumberIfs(int aux);
	public : void setNumberFors(int aux);
	public : int returnNumberWhiles();
	public : void setNumberWhiles(int aux);
	public : void genEndWhileAsm();
	public : void genEndForAsm(list<for_cycle> aux);
	public : string genWhileDirectLabel();
	public : string genForDirectLabel ();
	public : string genForLabel ();
	public : int returnNumberFors();
	public : int returnNumberDos();
	public : void genForAsm (for_cycle aux);
	public : void genDoWhileAsm(int oper1, int oper2, int oper1Type, int oper2Type, int relOper);
	public : void genDoAsm();
	public : void setNumberDos(int aux);
	public : string genDoLabel ();
	public : void genElseAsm();
	public : void genEndElseAsm();
	public : void elseExists();
	public : string genElseLabel ();
	public : void genReturnAsm();
	public : void writeRegMemory(int offset);
		 void readRegMemory(int offset);

};

class ctrlReg {

	private : int ctrlMask; //control mask
	private : int statusMask; //status mask
	private : int number_of_waits; 
	
	public : int get(std::list<int> componentList, int init);
	public : int check(std::list<int> componentList);
	public : int showCtrlMask() { return this->ctrlMask; }
	public : int showStatusMask() { return this->statusMask; }
	public : void genInitEngineAsm(int initValue);
	public : void genWaitEngineAsm(int waitValue);
	public : void clearFlags();
	public : void setNumberWaits(int);
	public : int returnNumberWaits();



};


class GenMem {

public:		void genMemConnectAsm (int unit, int selUnit, int unitPort);
		void genMemConnectAsm (int unit, int selUnit, int unitPort, int selPort);
		void genMemStartAsm (int unit, int unitPort, int start, int, int operTwo, int operTwoType, int operatorExp);
		void genMemShiftAsm (int unit, int unitPort, int shift, int, int operTwo, int operTwoType, int operatorExp);
		void genMemIterAsm (int unit, int unitPort, int iter, int, int operTwo, int operTwoType, int operatorExp);
		void genMemIncrAsm (int unit, int unitPort, int incr, int, int operTwo, int operTwoType, int operatorExp);
		void genMemDelayAsm (int unit, int unitPort, int delay, int, int operTwo, int operTwoType, int operatorExp); 
		void genMemPerAsm (int unit, int unitPort, int per, int, int operTwo, int operTwoType, int operatorExp);
		void genMemDutyAsm (int unit, int unitPort, int duty, int, int operTwo, int operTwoType, int operatorExp);
		void genMemReverseAsm (int unit, int unitPort, int reverse, int, int operTwo, int operTwoType, int operatorExp);



};

class GenFU {

public:		void genAluOperAsm (int oper, int unit); 
		void genAluConnectPortA_Asm (int unit, int selUnit);
		void genAluConnectPortB_Asm (int unit, int selUnit);
		void genAluLiteOperAsm (int oper, int unit); 
		void genAluLiteConnectPortA_Asm (int unit, int selUnit);
		void genAluLiteConnectPortB_Asm (int unit, int selUnit);
		void genAluConnectPortA_Asm (int unit, int selUnit, int port);
		void genAluConnectPortB_Asm (int unit, int selUnit, int port);
		void genAluLiteConnectPortA_Asm (int unit, int selUnit, int port);
		void genAluLiteConnectPortB_Asm (int unit, int selUnit, int port);
		void genMultLonhiAsm (int lonhi, int unit);
		void genMultDiv2Asm (int div2, int unit);
		void genMultConnectPortA_Asm (int unit, int selUnit, int port);
		void genMultConnectPortB_Asm (int unit, int selUnit, int port);
		void genMultConnectPortA_Asm (int unit, int port);
		void genMultConnectPortB_Asm (int unit, int port);
		void genBsConnectPortA_Asm (int unit, int port);
		void genBsConnectPortB_Asm (int unit, int port);
		void genBsConnectPortA_Asm (int unit, int selUnit, int port);
		void genBsConnectPortB_Asm (int unit, int selUnit, int port);
		void genBsLNR_Asm (int unit, int value);
		void genBsLNA_Asm (int unit, int value);
		void genDisableFUAsm(int unit);





};


class DMA {

	string hex_value;
	
	

public: void extAddr(int operOne, int operOneType, int operTwo, int operTwoType, int);
public: void intAddr(int operOne, int operOneType, int operTwo, int operTwoType, int operatorExp);
public: void sizeAddr(int operOne);
public: void directionAddr(int operOne);
public: void setHexValue(string a);
public: void verifyHexNum();
public: void configs(string a, string b, int c, int d);
	void run();
	void wait();

};

int RegToFu(int unit);

extern int DMAdirection;
extern int label_number;


#endif
