#include <iostream>
#include <stdio.h>
#include "genAssembly.hpp"
#include <sstream>
#include <stdlib.h>
#include <math.h> //retreat if gives problems in prof's PC





/*************************************************************************/
//Return reg string
//
//
/*************************************************************************/
	string RegToString(int targetConditionalRegister) {

		string targetRegister;

		switch (targetConditionalRegister) {
			case _R1: 
				targetRegister = "R1";
				break;
			case _R2: 
				targetRegister = "R2";
				break;
			case _R3: 
				targetRegister = "R3";
				break;
			case _R4: 
				targetRegister = "R4";
				break;
			case _R5: 
				targetRegister = "R5";
				break;
			case _R6: 
				targetRegister = "R6";
				break;
			case _R7: 
				targetRegister = "R7";
				break;
			case _R8: 
				targetRegister = "R8";
				break;
			case _R9: 
				targetRegister = "R9";
				break;
			case _R10: 
				targetRegister = "R10";
				break;
			case _R11: 
				targetRegister = "R11";
				break;
			case _R12: 
				targetRegister = "R12";
				break;
			case _R13: 
				targetRegister = "R13";
				break;
			case _R14: 
				targetRegister = "R14";
				break;
			case _RB: 
				targetRegister = "RB";
				break;
			case _Ralu0: 
				targetRegister = "ALU0";
				break;
			case _Ralu1: 
				targetRegister = "ALU1";
				break;
			case _RaluLite0: 
				targetRegister = "ALU_LITE0";
				break;
			case _RaluLite1: 
				targetRegister = "ALU_LITE1";
				break;
			case _RaluLite2: 
				targetRegister = "ALU_LITE2";
				break;
			case _RaluLite3: 
				targetRegister = "ALU_LITE3";
				break;
			case _Rmult0: 
				targetRegister = "MULT0";
				break;
			case _Rmult1: 
				targetRegister = "MULT1";
				break;
			case _Rmult2: 
				targetRegister = "MULT2";
				break;
			case _Rmult3: 
				targetRegister = "MULT3";
				break;
			case _Rbs0: 
				targetRegister = "BS0";
				break;
			case _R15: 
				targetRegister = "R15";
				break;  
			case _Mem0A: 
				targetRegister = "MEM0A";
				break;
			case _Mem0B: 
				targetRegister = "MEM0B";
				break;
			case _Mem1A: 
				targetRegister = "MEM1A";
				break;
			case _Mem1B: 
				targetRegister = "MEM1B";
				break;
			case _Mem2A: 
				targetRegister = "MEM2A";
				break;
			case _Mem2B: 
				targetRegister = "MEM2B";
				break;
			case _Mem3A: 
				targetRegister = "MEM3A";
				break;
			case _Mem3B: 
				targetRegister = "MEM3B";
				break;
			case _DMA_EXT_ADDR: 
				targetRegister = "DMA_EXT_ADDR_ADDR";
				break;
			case _DMA_INT_ADDR: 
				targetRegister = "DMA_INT_ADDR_ADDR";
				break;
			

		}

		return targetRegister;

}

/*************************************************************************/
//Converts ints in strings
//
//
/*************************************************************************/
	string intToString (int input) {

		ostringstream convert;   // stream used for the conversion
		convert << input;

		return convert.str();

}

/*************************************************************************/
//Converts register unit to functional unit
//
//
/*************************************************************************/		
	int RegToFu(int unit)  {

		switch (unit) {
			
			case _Ralu0: 
				return salu0;
				break;
			case _Ralu1: 
				return salu1;
				break;
			case _RaluLite0: 
				return salulite0;
				break;
			case _RaluLite1: 
				return salulite1;
				break;
			case _RaluLite2: 
				return salulite2;
				break;
			case _RaluLite3: 
				return salulite3;
				break;
			case _Rmult0: 
				return smul0;
				break;
			case _Rmult1: 
				return smul1;
				break;
			case _Rmult2: 
				return smul2;
				break;
			case _Rmult3: 
				return smul3;
				break;
			case _Rbs0: 
				return sbs0;
				break;
			
		}

		return 0;



}

/*************************************************************************/
//Generates expression assembly
//
//
/*************************************************************************/
	void controller::genExpressionAsm (int targetConditional, int oper1, int oper2, int oper1Type, int oper2Type, int ctrlOperator, int controllerWrc, int controllerLdi, string label, int memOffset, int targetCondType, int memLdiOffset) {

		string line;
		string targetRegister;
		string asmInstruction;
		string asmConnection;
		string configAddr; 
		string value1;
		string value2;	
		int aux=0;	
		int aux2=0;
		int oper1Low=0;
		GenFU aux0;
		
		if(controllerLdi!=0) {
			if(oper1Type==CONST && oper1>=pow(2,16)) {
				aux2 = (oper1 & ((int)(pow(2,31)-1)-(int)(pow(2,16)-1)))>>16;
				oper1Low = (oper1 & (int)(pow(2,16)-1));   }
			if(oper1Type==CONST && oper1<=(-pow(2,16))) {
				aux2 = (((oper1*-1) & ((int)(pow(2,31)-1)-(int)(pow(2,16)-1)))*-1)>>16;
				oper1Low = -1*(oper1 & (int)(pow(2,16)-1));     }
			targetRegister = RegToString(targetConditional);
			if(oper1Type==CONST || oper1Type==_MEMORY_RW) value1 = intToString(oper1);
			else value1 = RegToString(oper1);
			if(label!="") cout << label + '\t';
			if(oper1Type == _MEMORY_RW) {
				cout << '\t'+_addi + ' ' + value1 << _nextLine; 
				cout << '\t'+_wrw + " RB " << _nextLine; 
				cout << '\t'+_rdwb << _nextLine; 
				cout << '\t'+_rdwb << _nextLine; 
				cout << '\t'+_rdwb << _nextLine; 
				
			}
			else {
			if(oper1Type == _DMA_REG) {
				cout << '\t'+_rdw + ' ' + value1 << _nextLine; 
				cout << '\t'+_rdw + ' ' + value1 << _nextLine;  }
			else {
			if(oper1Type == CONST) {
				if(oper1<pow(2,16) && oper1>(-pow(2,16))) cout << _ldi + ' ' + value1+_nextLine;  
				else {
					cout << '\t' +_ldi + ' ' + intToString(oper1Low)+_nextLine; 
					cout << '\t'+_ldih + ' ' + intToString(aux2)+_nextLine; } }
			else { if(oper1Type == REGISTER) cout << _rdw + ' ' + value1+_nextLine;  
				//else { if(oper1Type == _MEMORY_ADDR) cout << _ldi + ' ' + dmaToString(oper1) << _nextLine;



				else { cout << _rdw + ' ' + value1 + ',' << memLdiOffset << _nextLine; 
					cout << '\t'+_rdw + ' ' + value1 + ',' << memLdiOffset << _nextLine; 
					cout << '\t'+_rdw + ' ' + value1 + ',' << memLdiOffset << _nextLine; 
					} }  }   }

			if(oper1Type == REGISTER && oper1 >= _Ralu0 && oper1 <= _Rmult3) {
				cout << '\t'+_rdw + ' ' + value1+_nextLine;
				
				} 

			if(oper1Type == REGISTER && oper1 == _Rbs0) {
				cout << '\t'+_rdw + ' ' + value1+_nextLine;
				
				} 


			}

		else {
			targetRegister = RegToString(targetConditional);  }


		 
		if(ctrlOperator == ADD) { 
			if(oper2Type == CONST) asmInstruction = _addi;
			else  asmInstruction = _add; }
	
		if(ctrlOperator == SHFT) { asmInstruction = _shft;  }
		
		if(ctrlOperator == ADDI) asmInstruction = _addi;


		if(ctrlOperator == SUB) { 
			if(oper2Type == CONST) { asmInstruction = _addi; oper2 = oper2*-1; }
			else	asmInstruction = _sub; }


		if(ctrlOperator == AND) { asmInstruction = _and;
			if(oper2Type==CONST) { cout << "Second and operatior should be a register \n"; exit(-1); }  }

		if(oper2Type==CONST) value2 = intToString(oper2);
		else value2 = RegToString(oper2);


		if(ctrlOperator!= SHFT && ctrlOperator!=0) { 
			if(oper2Type!=_MEMORY_TYPE) cout << '\t'+asmInstruction + ' ' +  value2 + _nextLine; 
			else { cout << '\t'+asmInstruction + ' ' +  value2 + ',' << memOffset << _nextLine;
				cout << '\t'+asmInstruction + ' ' +  value2 + ',' << memOffset << _nextLine;
				cout << '\t'+asmInstruction + ' ' +  value2 + ',' << memOffset << _nextLine;   } }

		if(ctrlOperator == SHFT) {
			if(oper2>=1) aux=oper2;
			else aux = oper2*-1;
			if(oper2Type==CONST)  {
				value2 = intToString(oper2/aux);
				for(int i=0;i<aux;i++)
					cout << '\t' << asmInstruction + ' ' +  value2 + _nextLine; 	}   
			else {
				cout << "Second shft operatior should be a constant \n"; exit(-1);
			}

		}
		if(controllerWrc!=0) {
			if(targetCondType==_MEMORY_TYPE) { cout << '\t'+_wrw + ' ' + targetRegister+',' << memOffset << _nextLine;
							   cout << '\t'+_wrw + ' ' + targetRegister+',' << memOffset << _nextLine;
							   cout << '\t'+_wrw + ' ' + targetRegister+',' << memOffset << _nextLine;  }
			else  cout << '\t'+_wrw + ' ' + targetRegister + _nextLine;   
			
			if(targetConditional >= _Ralu0 && targetConditional <= _Rmult3 && targetConditional != _RB) aux0.genDisableFUAsm(RegToFu(targetConditional));

					}

		


}   

/*************************************************************************/
//Generates if assembly label
//
//
/*************************************************************************/
	string controller::genIfLabel () {

		string writing = _if_statment + intToString(this->number_of_ifs);
		return writing;

}  


/*************************************************************************/
//Generates while assembly
//
//
/*************************************************************************/
	string controller::genWhileLabel () {

		string writing = _while_statment + intToString(this->number_of_whiles);
		return writing;

}  

/*************************************************************************/
//Generates another while assembly
//
//
/*************************************************************************/
	string controller::genWhileDirectLabel () {

		std::list<int>::iterator it = this->whileStatmentList.end();
		it--;

		string writing = _while_direct_statment + intToString(*it);
		return writing;

}  

/*************************************************************************/
//Generates for assembly direct label
//
//
/*************************************************************************/
	string controller::genForDirectLabel () {

		std::list<int>::iterator it = this->forStatmentList.end();
		it--;

		string writing = _for_direct_statment + intToString(*it);
		return writing;

}

/*************************************************************************/
//Generates else assembly direct label
//
//
/*************************************************************************/
	string controller::genElseLabel () {

		std::list<int>::iterator it = this->ifStatmentList.end();
		it--;
		string writing = _else_statment + intToString(*it);
		return writing;

}


/*************************************************************************/
//Generates for assembly label
//
//
/*************************************************************************/
	string controller::genForLabel () {

		string writing = _for_statment + intToString(this->number_of_fors);
		return writing;

}  

/*************************************************************************/
//Generates do assembly label
//
//
/*************************************************************************/
	string controller::genDoLabel () {

		string writing = _do_statment + intToString(this->number_of_dos);
		return writing;

}  
  


/*************************************************************************/
//Inits number of ifs
//
//
/*************************************************************************/
	void controller::ifInit () {  this->number_of_ifs=0; }

/*************************************************************************/
//Generates endwhile assembly
//
//
/*************************************************************************/
	void controller::genEndWhileAsm() {

		std::list<int>::iterator it = this->whileStatmentList.end();
		it--;

		string write_end = _while_statment + intToString(*it);

		cout << _ldi + ' ';
		cout << '0';
		cout << _nextLine; 
		cout << '\t' <<"beqi " + this->genWhileDirectLabel() + _nextLine; 
		cout << '\t' <<_nop << ' ' << _nextLine;
	
		cout << write_end + ' ' + _nop + _nextLine;
		this->whileStatmentList.pop_back();

}

/*************************************************************************/
//Generates endfor assembly
//
//
/*************************************************************************/
	void controller::genEndForAsm(list<for_cycle> aux) {

		std::list<int>::iterator it = this->forStatmentList.end();
		it--;
		
		std::list<for_cycle>::iterator it2 = aux.end();
		it2--;
		
		if(it2->incrOperOneType !=0 || it2->incrOperTwoType !=0)
			this->genExpressionAsm(it2->incrTarget, it2->incrOperOne, it2->incrOperTwo, it2->incrOperOneType, it2->incrOperTwoType, it2->incrCtrlOperator, 1, 1,"", 0, 0, 0);

		string write_end = _for_statment + intToString(*it);

		cout << '\t' << _ldi + ' ';
		cout << '0';
		cout << _nextLine; 
		cout << '\t' <<"beqi " + this->genForDirectLabel() + _nextLine; 
		cout << '\t' <<_nop << ' ' << _nextLine;
		cout << write_end + ' ' + _nop + _nextLine;
		
		this->forStatmentList.pop_back();

}


/*************************************************************************/
//Generates endif assembly
//
//
/*************************************************************************/
	void controller::genEndifAsm() {

		std::list<int>::iterator it = this->ifStatmentList.end();
		it--;
		
		string write_end = _if_statment + intToString(*it);

		if(this->else_exists!=1) {
			cout << write_end + ' ' + _nop + _nextLine;
			this->ifStatmentList.pop_back(); }



}

/*************************************************************************/
//Generates while assembly
//
//
/*************************************************************************/
	void controller::genWhileAsm (int oper1, int oper2, int oper1Type, int oper2Type, int relOper) {	

		this->whileStatmentList.push_back(this->number_of_whiles);

		if(relOper == HIGHER_EQUAL) {	
			this->genExpressionAsm(_RB, oper1, oper2, oper1Type, oper2Type, SUB, 1, 1, this->genWhileDirectLabel(), 0, 0, 0);
			//this->genExpressionAsm(_RB, _RB, 1, REGISTER, CONST, ADD, 1, 0, "", 0, 0, 0);
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"bneqi " + this->genWhileLabel() +_nextLine; }
		
		if(relOper == EQUAL) {
			this->genExpressionAsm(oper1, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1, this->genWhileDirectLabel(), 0, 0, 0);
			cout << '\t' <<"bneqi " + this->genWhileLabel() +_nextLine; }

		if(relOper == DIFFERENT) {
			this->genExpressionAsm(oper1, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1, this->genWhileDirectLabel(), 0, 0, 0);
			cout << '\t' <<"beqi " + this->genWhileLabel() +_nextLine; }

		if(relOper == LOWER_EQUAL) {	
			this->genExpressionAsm(_RB, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1, this->genWhileDirectLabel(), 0, 0, 0);
			this->genExpressionAsm(_RB, _RB, 1, REGISTER, CONST, SUB, 1, 0, "", 0, 0, 0);
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"beqi " + this->genWhileLabel() + _nextLine; }

		if(relOper == HIGHER) {	
			this->genExpressionAsm(_RB, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1, this->genWhileDirectLabel(), 0, 0, 0);
			this->genExpressionAsm(_RB, _RB, -1, REGISTER, CONST, ADD, 1, 0, "", 0, 0, 0);
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"bneqi " + this->genWhileLabel() +_nextLine; }

		if(relOper == LOWER) {	
			this->genExpressionAsm(_RB, oper1, oper2, oper1Type, oper2Type, SUB, 1, 1, this->genWhileDirectLabel(), 0, 0, 0);
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"beqi " + this->genWhileLabel() + _nextLine; }

		cout << '\t' <<_nop << ' ' << _nextLine;
		cout << '\t' <<_nop << ' ' << _nextLine;

		this->number_of_whiles++;

}

/*************************************************************************/
//Generates for assembly
//
//
/*************************************************************************/
	void controller::genForAsm (for_cycle aux) {	

		this->forStatmentList.push_back(this->number_of_fors);



	if(aux.initOperOneType !=0 || aux.initOperTwoType !=0 ) 
		this->genExpressionAsm(aux.initTarget, aux.initOperOne, aux.initOperTwo, aux.initOperOneType, aux.initOperTwoType, aux.initCtrlOperator, 1, 1,"", 0, 0, 0);



	if(aux.condRelOper !=0) {
		if(aux.condRelOper == HIGHER_EQUAL) {	
			this->genExpressionAsm(_RB, aux.condOperOne, aux.condOperTwo, aux.condOperOneType, aux.condOperTwoType, SUB, 1, 1, this->genForDirectLabel(), 0, 0, 0);
			
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"bneqi " + this->genForLabel() +_nextLine; }
		
		if(aux.condRelOper == EQUAL) {
			this->genExpressionAsm(aux.condOperOne, aux.condOperOne, aux.condOperTwo, aux.condOperOneType, aux.condOperTwoType, SUB, 0, 1, this->genForDirectLabel(), 0, 0, 0);
			cout << '\t' <<"bneqi " + this->genForLabel() +_nextLine; }

		if(aux.condRelOper == DIFFERENT) {
			this->genExpressionAsm(aux.condOperOne, aux.condOperOne, aux.condOperTwo, aux.condOperOneType, aux.condOperTwoType, SUB, 0, 1, this->genForDirectLabel(), 0, 0, 0);
			cout << '\t' <<"beqi " + this->genForLabel() +_nextLine; }

		if(aux.condRelOper == LOWER_EQUAL) {	
			this->genExpressionAsm(_RB, aux.condOperOne, aux.condOperTwo, aux.condOperOneType, aux.condOperTwoType, SUB, 0, 1, this->genForDirectLabel(), 0, 0, 0);
			this->genExpressionAsm(_RB, _RB, 1, REGISTER, CONST, SUB, 1, 0, "", 0, 0, 0);
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"beqi " + this->genForLabel() + _nextLine; }

		if(aux.condRelOper == HIGHER) {	
			this->genExpressionAsm(_RB, aux.condOperOne, aux.condOperTwo, aux.condOperOneType, aux.condOperTwoType, SUB, 0, 1, this->genForDirectLabel(), 0, 0, 0);
			this->genExpressionAsm(_RB, _RB, -1, REGISTER, CONST, ADD, 1, 0, "", 0, 0, 0);
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"bneqi " + this->genForLabel() +_nextLine; }

		if(aux.condRelOper == LOWER) {	
			this->genExpressionAsm(_RB, aux.condOperOne, aux.condOperTwo, aux.condOperOneType, aux.condOperTwoType, SUB, 1, 1, this->genForDirectLabel(), 0, 0, 0);
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"beqi " + this->genForLabel() + _nextLine; }  }

	else {		cout << this->genForDirectLabel() << ' ' <<  _nop << _nextLine;  }

		cout << '\t'+_nop << ' ' << _nextLine;
		cout << '\t'+_nop << ' ' << _nextLine;

		this->number_of_fors++;
		



}

/*************************************************************************/
//Generates if assembly
//
//
/*************************************************************************/
	void controller::genIfAsm (int oper1, int oper2, int oper1Type, int oper2Type, int relOper) {




		this->ifStatmentList.push_back(this->number_of_ifs);
		
		

		if(relOper == HIGHER_EQUAL) {	
			this->genExpressionAsm(_RB, oper1, oper2, oper1Type, oper2Type, SUB, 1, 1, "", 0, 0, 0);
			//this->genExpressionAsm(_RB, _RB, 1, REGISTER, CONST, ADD, 1, 0, "", 0, 0, 0);
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"bneqi " + this->genIfLabel() +_nextLine;  }
		
		if(relOper == EQUAL) {
			this->genExpressionAsm(oper1, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1, "", 0, 0, 0);
			cout << '\t' <<"bneqi " + this->genIfLabel() +_nextLine; }

		if(relOper == DIFFERENT) {
			this->genExpressionAsm(oper1, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1, "", 0, 0, 0);
			cout << '\t' <<"beqi " + this->genIfLabel() +_nextLine; }

		if(relOper == LOWER_EQUAL) {	
			this->genExpressionAsm(_RB, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1,  "", 0, 0, 0);
			this->genExpressionAsm(_RB, _RB, 1, REGISTER, CONST, SUB, 1, 0, "", 0, 0, 0);
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"beqi " + this->genIfLabel() + _nextLine; }

		if(relOper == HIGHER) {	
			this->genExpressionAsm(_RB, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1,  "", 0, 0, 0);
			this->genExpressionAsm(_RB, _RB, -1, REGISTER, CONST, ADD, 1, 0, "", 0, 0, 0);
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"bneqi " + this->genIfLabel() +_nextLine; }

		if(relOper == LOWER) {	
			this->genExpressionAsm(_RB, oper1, oper2, oper1Type, oper2Type, SUB, 1, 1,  "", 0, 0, 0);
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"beqi " + this->genIfLabel() + _nextLine; }

		cout << '\t' <<_nop << ' ' << _nextLine;
		cout << '\t' <<_nop << ' ' << _nextLine;

		this->number_of_ifs++;
		

}  

/*************************************************************************/
//Generates gotoif assembly
//
//
/*************************************************************************/
	void controller::genGotoIfAsm (int oper1, int oper2, int oper1Type, int oper2Type, int relOper, string jump) {

		if(relOper == HIGHER_EQUAL) {	
			this->genExpressionAsm(oper1, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1,"", 0, 0, 0);
			this->genExpressionAsm(oper1, oper1, 32768, oper1Type, CONST, AND, 0, 0, "", 0, 0, 0);			
			cout << '\t' <<"bneqi " + jump +_nextLine; }
		
		if(relOper == EQUAL) {
			this->genExpressionAsm(oper1, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1, "", 0, 0, 0);
			cout << '\t' <<"bneqi " + jump +_nextLine; }

		if(relOper == DIFFERENT) {
			this->genExpressionAsm(oper1, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1, "", 0, 0, 0);
			cout << '\t' <<"beqi " + jump +_nextLine; }

		if(relOper == LOWER_EQUAL) {	
			this->genExpressionAsm(oper1, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1, "", 0, 0, 0);
			this->genExpressionAsm(oper1, oper1, 32768, oper1Type, CONST, AND, 0, 0, "", 0, 0, 0);			
			cout << '\t' <<"beqi " + jump + _nextLine; }

		if(relOper == HIGHER) {	
			this->genExpressionAsm(oper1, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1,"", 0, 0, 0);
			this->genExpressionAsm(oper1, oper1, 1, oper1Type, CONST, ADDI, 0, 1,"", 0, 0, 0);
			this->genExpressionAsm(oper1, oper1, 32768, oper1Type, CONST, AND, 0, 0, "", 0, 0, 0);			
			cout << '\t' <<"bneqi " + jump +_nextLine; }

		if(relOper == LOWER) {	
			this->genExpressionAsm(oper1, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1, "", 0, 0, 0);
			this->genExpressionAsm(oper1, oper1, 1, oper1Type, CONST, ADDI, 0, 1, "", 0, 0, 0);
			this->genExpressionAsm(oper1, oper1, 32768, oper1Type, CONST, AND, 0, 0, "", 0, 0, 0);			
			cout << '\t' <<"beqi " + jump + _nextLine; }
		

}

/*************************************************************************/
//Generates goto assembly
//
//
/*************************************************************************/
	void controller::genGotoAsm (string jump) {

			cout << _ldi + ' ';
			cout << '0';
			cout << _nextLine; 
			cout << '\t' << "beqi " + jump + _nextLine; 
			cout << '\t'+_nop << ' ' << _nextLine;
			cout << '\t'+_nop << ' ' << _nextLine;
		

}

/*************************************************************************/
//Inserts actual number of fors
//
//
/*************************************************************************/

	void controller::setNumberFors(int aux) {

		this->number_of_fors = aux; }



/*************************************************************************/
//Inserts actual number of ifs
//
//
/*************************************************************************/

	void controller::setNumberIfs(int aux) {

		this->number_of_ifs = aux; }

/*************************************************************************/
//Inserts actual number of dos
//
//
/*************************************************************************/

	void controller::setNumberDos(int aux) {

		this->number_of_dos = aux; }


/*************************************************************************/
//Inserts actual number of whiles
//
//
/*************************************************************************/

	void controller::setNumberWhiles(int aux) {

		this->number_of_whiles = aux; }



/*************************************************************************/
//Returns actual number of ifs
//
//
/*************************************************************************/

	int controller::returnNumberIfs() {

		return this->number_of_ifs; }

/*************************************************************************/
//Returns actual number of whiles
//
//
/*************************************************************************/

	int controller::returnNumberWhiles() {

		return this->number_of_whiles; }

/*************************************************************************/
//Returns actual number of fors
//
//
/*************************************************************************/

	int controller::returnNumberFors() {

		return this->number_of_fors; }

/*************************************************************************/
//Returns actual number of dos
//
//
/*************************************************************************/

	int controller::returnNumberDos() {

		return this->number_of_dos; }

/*************************************************************************/
//Generates do assembly
//
//
/*************************************************************************/

	void controller::genDoAsm() {
		
		this->number_of_dos++;

		this->doWhileStatmentList.push_back(this->number_of_dos);
		
		cout << this->genDoLabel() << ' ' << _nop << _nextLine;
}

/*************************************************************************/
//Generates doWhile assembly
//
//
/*************************************************************************/

	void controller::genDoWhileAsm(int oper1, int oper2, int oper1Type, int oper2Type, int relOper) {

		std::list<int>::iterator it = this->doWhileStatmentList.end();
		it--;

		
		string write_end = _do_statment + intToString(*it);		


		if(relOper == HIGHER_EQUAL) {	
			this->genExpressionAsm(_RB, oper1, oper2, oper1Type, oper2Type, SUB, 1, 1, "", 0, 0, 0);
			//this->genExpressionAsm(_RB, _RB, 1, REGISTER, CONST, ADD, 1, 0, "", 0, 0, 0);
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"beqi " + write_end +_nextLine; }

		
		if(relOper == EQUAL) {
			this->genExpressionAsm(oper1, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1, "", 0, 0, 0);
			cout << '\t' <<"beqi " + write_end +_nextLine; }

		if(relOper == DIFFERENT) {
			this->genExpressionAsm(oper1, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1, "", 0, 0, 0);
			cout << '\t' <<"bneqi " + write_end +_nextLine; }

		if(relOper == LOWER_EQUAL) {	
			this->genExpressionAsm(_RB, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1,  "", 0, 0, 0);
			this->genExpressionAsm(_RB, _RB, 1, REGISTER, CONST, SUB, 1, 0, "", 0, 0, 0);
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"bneqi " + write_end + _nextLine; }

		if(relOper == HIGHER) {	
			this->genExpressionAsm(_RB, oper1, oper2, oper1Type, oper2Type, SUB, 0, 1,  "", 0, 0, 0);
			this->genExpressionAsm(_RB, _RB, -1, REGISTER, CONST, ADD, 1, 0, "", 0, 0, 0);
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"beqi " + write_end +_nextLine; }

		if(relOper == LOWER) {	
			this->genExpressionAsm(_RB, oper1, oper2, oper1Type, oper2Type, SUB, 1, 1,  "", 0, 0, 0);
			cout << '\t' << _ldi + ' ' <<  0 << _nextLine;
			cout << '\t' << _ldih + ' ' <<  "0x8000" << _nextLine;
			cout << '\t' << _and + ' ' << "RB" << _nextLine;
			cout << '\t' <<"bneqi " + write_end + _nextLine;  }

		cout << '\t'+_nop << ' ' << _nextLine;
		cout << '\t'+_nop << ' ' << _nextLine;
		
		this->doWhileStatmentList.pop_back();


}

/*************************************************************************/
//Generates else assembly
//
//
/*************************************************************************/

void controller::genElseAsm() {

	std::list<int>::iterator it = this->ifStatmentList.end();
	it--;
		
	string write_end = _if_statment + intToString(*it);

	this->genGotoAsm(genElseLabel());
	cout << write_end << ' ' << _nop << _nextLine;
	


}

/*************************************************************************/
//Generates endElse assembly
//
//
/*************************************************************************/

void controller::genEndElseAsm() {


	cout << genElseLabel() << ' ' << _nop << _nextLine;
	this->ifStatmentList.pop_back();


}

/*************************************************************************/
//Indicates else exists 
//
//
/*************************************************************************/

void controller::elseExists() {

	this->else_exists=1;

}

/*************************************************************************/
//Generates return assembly 
//
//
/*************************************************************************/

void controller::genReturnAsm() {

	cout << _ldi << ' ' << 0 << _nextLine;
	cout << '\t' << "beqi" << ' ' << 0 << _nextLine;
	cout << '\t'+_nop+_nextLine;

}

/*************************************************************************/
//Writes in configuration memory
//
//
/*************************************************************************/

void  controller::writeRegMemory(int offset) {

	cout << _wrw << ' ' << _conf_mem << ',' << offset << _nextLine;

	}


/*************************************************************************/
//Reads of configuration memory
//
//
/*************************************************************************/

void  controller::readRegMemory(int offset) {

	cout << _rdw << ' ' << _conf_mem << ',' << offset << _nextLine;

	}



