#ifndef __COMMAND_HPP_INCLUDED__   // if x.h hasn't been included yet...
#define __COMMAND_HPP_INCLUDED__   //   #define this so the compiler knows it has been included

//methods
/******************************************************/
//ctrlReg
#define CTRL_REG_INIT 1
#define CTRL_REG_RUN 2
#define CTRL_REG_WAIT 3
#define CTRL_REG_RUN_EXP 4

//portMem
#define SET_START 1
#define SET_DUTY 2
#define SET_SHIFT 3
#define SET_PER 4
#define SET_INCR 5
#define SET_ITER 6
#define SET_DELAY 7
#define SET_LABEL 8
#define SET_REVERSE 10
#define CONNECT_PORT_MEM 9

//port
#define CONNECT_PORT_A 1
#define CONNECT_PORT_B 2
#define DISABLE 3

//FU
#define GET_OUT_PORTA_SEL 1
#define GET_OUT_PORTB_SEL 2
#define GET_OUT_LABEL 3
#define PUT_LABEL 4

//mult
#define SET_LONHI 1
#define SET_DIV2 2

//alu
#define SET_OPER 1
#define SHOW_OPER 2

//alulite
#define SET_OPER_LITE 1
#define SHOW_OPER_LITE 2

//Bs
#define SET_BS_LNR 1
#define SET_BS_LNA 2

//Clear Flag
#define CLEAR_REG_FLAGS 1

//Mem instr
#define SAVE_CONFIG 1
#define LOAD_CONFIG 2

//DMA
#define _SET_EXT_ADDR 1
#define _SET_INT_ADDR 2
#define _SET_SIZE_ADDR 3
#define _SET_DIRECTION_ADDR 4
#define DMA_RUN 5
#define DMA_WAIT 6


/******************************************************/

//command Type
/******************************************************/
#define CONFIG 1
#define CONNECT 2
#define INIT 3
#define _RUN 4
#define WAIT 5
#define CONDITIONAL 6
#define EXPRESSION 7
#define CONDITIONAL_JUMP 8
#define CLEAR 9
#define END_CONDITIONAL 10
#define JUMP 11
#define _WHILE 12 
#define END_WHILE 13
#define _FOR 14
#define END_FOR 15
#define _DO 16
#define DO_WHILE 17
#define _ELSE 18
#define END_ELSE 19
#define RETURN 20
#define MEM_EXPRESSION 21
#define MEM_FOR 22
#define MEM_END 23
#define ASSEMBLY_CMD 24
#define MEM_INSTR 25
#define _DMA 26
#define AUTO_DUTY 27
#define VECTOR_RW 28
/******************************************************/

//alu/aluLite/expression oper
/******************************************************/
#define ALU_ADD 1
#define ALU_SUB 2
#define ALU_AND 3
#define ALU_OR 4
#define ALU_XOR 5
#define ALU_NAND 6
#define SEXT8 7
#define SEXT16 8
#define _SRA 9
#define _SRL 10
#define CMPU 11
#define CMPS 12
#define _CLZ 13
#define ALU_MAX 14
#define ALU_MIN 15
#define ALU_ABS 16
#define LNA 17
#define LNR 18 
#define MULTIPLICATION 19
/******************************************************/

//mem connect 
/******************************************************/
//mem
#define MEM0 1
#define MEM1 2
#define MEM2 3
#define MEM3 4 

//port
#define PORT_A 1
#define PORT_B 2 

//FU
#define MULTIPLIER 1
#define ALU_FULL 2
#define ALULITE 3
#define BSHIFTER 4 
#define MEMORY 5 
/******************************************************/

//oper type (controller)
/******************************************************/
#define CONST 1
#define REGISTER 2
#define _MEMORY_TYPE 3
#define _MEMORY_ADDR 4
#define _DMA_HEX 5
#define _DMA_REG 6
#define _MEMORY_RW 7
/******************************************************/

//registers
/******************************************************/
#define _R1 1
#define _R2 2
#define _R3 3
#define _R4 4
#define _R5 5
#define _R6 6
#define _R7 7
#define _R8 8
#define _R9 9
#define _R10 10
#define _R11 11
#define _R12 12
#define _R13 13
#define _R14 14
#define _R15 15
#define _Ralu0 16
#define _Ralu1 17
#define _RaluLite0 18
#define _RaluLite1 19
#define _RaluLite2 20
#define _RaluLite3 21
#define _RB 22
#define _Rmult0 23
#define _Rmult1 24
#define _Rmult2 25
#define _Rmult3 26
#define _Rbs0 35
#define _Mem0A 27
#define _Mem0B 28
#define _Mem1A 29
#define _Mem1B 30
#define _Mem2A 31
#define _Mem2B 32
#define _Mem3A 33
#define _Mem3B 34
#define _DMA_EXT_ADDR 36
#define _DMA_INT_ADDR 37
/******************************************************/

//if statements
/******************************************************/
#define LOWER 1
#define HIGHER 2
#define EQUAL 3
#define LOWER_EQUAL 4
#define HIGHER_EQUAL 5
#define DIFFERENT 6
/******************************************************/

//resources
/******************************************************/
#define ALUS 6
#define MULTS 4
#define TOTAL_RSC 18

//config mem size
/******************************************************/
#define CONF_MEM_SIZE 64

#endif


