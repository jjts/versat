#include <iostream>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include "genAssembly.hpp"
#include <math.h>


	int ctrlReg::get(std::list<int> componentList, int init) { //create init/run mask

		this->ctrlMask = 0;
		for (std::list<int>::iterator it = componentList.begin(); it != componentList.end(); it++) { //iterator
			this->ctrlMask = this->ctrlMask + (int) pow((double)2, (double)CTRL_BIT_SIZE-*it);     }
		if(init == 1)  this->ctrlMask = this->ctrlMask+1;
		else this->ctrlMask = this->ctrlMask+2;
		return this->ctrlMask;

}


	int ctrlReg::check(std::list<int> componentList) { //check status register

		int statusCheck; //indicates if DataEngine finished operating		

		this->statusMask = 0;
		for (std::list<int>::iterator it = componentList.begin(); it != componentList.end(); it++) { //iterator
			if(*it == smem0A) this->statusMask = this->statusMask + 2;
			if(*it == smem0B) this->statusMask = this->statusMask + 1;
			if(*it == smem1A) this->statusMask = this->statusMask + 8;
			if(*it == smem1B) this->statusMask = this->statusMask + 4;
			if(*it == smem2A) this->statusMask = this->statusMask + 32;
			if(*it == smem2B) this->statusMask = this->statusMask + 16;
			if(*it == smem3A) this->statusMask = this->statusMask + 128;
			if(*it == smem3B) this->statusMask = this->statusMask + 64;	}

		
	

		return this->statusMask;

}


	void ctrlReg::genInitEngineAsm(int initValue) {

		int lowInit=initValue & 65535;
		int highInit=initValue >> 16;
		string line;
		string asmInstruction;
		string Value;
		string configAddr; 
		ostringstream convert;   // stream used for the conversion
		convert << lowInit;
	

		asmInstruction = _ldi;
		Value = convert.str();
		line = asmInstruction+' '+Value+_nextLine;
		cout << line;
		asmInstruction = _ldih;
		convert.str("");
		convert << highInit;	
		Value = convert.str();	
		line = '\t'+asmInstruction+' '+Value+_nextLine;	
		cout << line;
		cout << '\t'+_ctrl_reg+_nextLine;
		if((initValue&1)==1) cout << '\t'+_ctrl_reg+_nextLine;

}


	void ctrlReg::genWaitEngineAsm(int waitValue) {

		string line;
		string asmInstruction;
		string Value;
		string Value2;
		string intValue;
		string configAddr; 
		ostringstream convert;   // stream used for the conversion
		ostringstream convert2;   // stream used for the conversion
		ostringstream convert3;   // stream used for the conversion
		convert << (waitValue);
		convert2 << (waitValue*-1);
		convert3 << this->number_of_waits;
		intValue = convert3.str();
	

		asmInstruction = _ldi;
		Value = convert.str();
		Value2 = convert2.str();
		
		line = "waitres"+intValue+' '+asmInstruction+' '+Value+_nextLine;
		cout << line;
		line = '\t'+_and+' '+_status_reg+_nextLine;
		cout << line;
		line = _addi;	
		cout << '\t'+line << ' ' << Value2 << _nextLine;
		line = '\t'+_bneqi_waitres+intValue+_nextLine;
		cout << line;
		line = '\t'+_nop+_nextLine;
		cout << line;
		this->number_of_waits++;
}


	void ctrlReg::clearFlags() {

		string line;		
		line = _clear_flags+_nextLine;
		cout << line;
}

	void ctrlReg::setNumberWaits(int aux) {

		this->number_of_waits=aux;	}

	int ctrlReg::returnNumberWaits() {
	
		return this->number_of_waits; }






