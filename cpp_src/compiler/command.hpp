#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>
#include <list>
#include <utility>
#include "defs_library.h"
#include "mem_ends.h"
#include "program.h"
#include "genAssembly.hpp"



class global_mem_data{

	int alloc_alu[ALUS];
	int alloc_mult[MULTS];

public:	
	int iter;
	int per;
	int iterType;
	int perType;
	int vec_alu[ALUS];
	int vec_mult[MULTS];


public:
	void setIter(int, int);
	void setPer(int, int);
	void resetParameters();
	void allocRsc(string, int);
	void allocRsc(int);
	void freeAlu(int fu);
	void freeMult(int fu);

};

class variable{

	int fu_alu;
	int fu_mult;
	int vec_mult[MULTS];
	int vec_alu[ALUS];

public:
	int getNextAlu();
	int getNextMult();
	void copyUsedResources(int [MULTS], int [ALUS]);
	void resetVecs();
	int getNext(string a);


};



struct tree_aux{

	int iter;
	int iter_type;
	int fu_unit;
	int fu_unit_mem;
	int period;
	int period_type;
	int start;
	int start_type;
	int shift;
	int shift_type;
	int period_shift;
	int period_shift_type;
	int delay;
	int incr;
	int incr_type;


};

class resourcesAlloc{

	int full;
	int iter;
	int iter_type;
	int period;
	int period_type;
	int start;
	int start_type;
	int shift;
	int shift_type;
	int period_shift;
	int period_shift_type;
	int delay;
	int incr;
	int incr_type;
	int use;
	int use_delay;

public: 
	void cleanParameters();
	void printData();
	void fullResource();
	int returnFullRsc();
	void putParameters(int, int, int, int, int, int, int, int, int, int);
	void putDelay(int);
	void compareDelay(int);
	void compareData(int iter, int period, int start, int shift, int incr, int iter_type, int period_type, int start_type, int shift_type, int incr_type);




};

class NODE2{

public:	
	string fu;
	int iter;
	int iter_type;
	int fu_unit;
	int fu_unit_port;
	int period;
	int period_type;
	int start;
	int start_type;	
	int shift;
	int shift_type;
	int delay;
	int incr;
	int incr_type;
	int disabled;
	int opt;

	NODE2 *left;
	NODE2 *right;

	NODE2(string ,int , int, int, int, int, int, NODE2 *d, NODE2 *e, int, int, int, int, int, int *, int * );
	void printNode(string node, int level);
	void testing();

	
};





class NODE{

public:	
	string oper;
	string cnst;
	int value;

	NODE *left;
	NODE *right;

	int level;
	int operOne;
	int operOneType;
	int ctrOperator;
	int operShift;
	int memOffset;

	NODE();
	NODE(string ,string , int, NODE*, NODE*, int );
	void setNode(string  );
	void printNode(string, int );
};

/*class reorganize_resources{

	int aluResources[6];

	void reorganizeAlu(NODE2 *);
	void resetAluResources();



}; */

class COMMAND {

		int cmdType;
		int unit;
		int port;
		int method;
		int cnst;
		int selUnit;
		string assembly_instr;
public:		int selUnitPort;
		NODE *tree;
		NODE2 *mem_tree;
		NODE2 *mem_target;
		//reorganize_resources reorResources;
		tree_aux aux_tree2;
		int vec_alu[6];
		int vec_mult[4];

private:	int init;
		int operOne;
		int targetConditional;
		int targetConditionalType;
		int targetOffset;
		int operTwo;
		int operOneType;
		int operTwoType;
		int relOper;
		int ctrlOperator;
		int jmpCnst;
		int aluOperator;
		int en_dis_disable;

		string label;
		string gt_label;

		string dma_hex_string;
		
		std::list<int> componentList;
		std::list<int> componentWhileList;
		std::list<int> componentForList;
		std::list<int> componentDoWhileList;
		std::pair<std::string, int> targetConditionalPairsList;
		std::list<COMMAND> subprogram;
		std::list<for_cycle> forCycleList; 


		for_cycle forCycle;
		GenFU _genFU;
		GenMem _genMem;
		ctrlReg _ctrlReg;
		DMA _dmaEngine;
		

public:		controller _controller;

public:		void resetCmd();
		//void resetComponentListList();
		//void resetTargetConditionalPairsList();
		
		void addSubCmd();
		void addNode(NODE*);
		void setLabel(string );
		void setGtLabel(string );	

		void setCmdType(int );
		int showCmdType(); 
		void setUnit(int, int );
		void setUnit(int );
		void setPort(int );
		void setPort(string var);
		void setMethod(int );
		void setMethod(int , string);
		void setCnst(int );
		int returnCnst();
		void setSelUnit(int );
		void setSelUnit(string );
		void setSelUnitPort(int );
		void setSelUnitPort(string var);

		void setComponentList(int );
		void setNewComponentList(std::list<int> );
		void setComponentList(string var);
		int sizComponentListList();
		int popComponentListList();
		void addComponentListList(int );
		void sendList(std::list<int> );

		void setInit(int );
		void setOperOne(string );
		void setOperOne(int );
		void setOperOne(char , int );
		void sendGlobalIf(int );
		int  returnGlobalIf();
		void setOperTwo(string );
		void setOperTwo(int );
		list<int> returnList();
		list<int> returnWhileList();
		list<int> returnForList();
		list<int> returnDoWhileList();
		list<int> returnComponentList();
		void putResources(std::list<int> aux, std::list<int> aux2);
		void sendGlobalWhile(int aux);
		void sendGlobalFor(int aux);
		void sendWhileList(std::list<int> aux);
		void sendForList(std::list<int> aux);
		void sendDoList(std::list<int> aux);
		void sendGlobalDo(int aux);
		void setOperOneType(int );
		int returnGlobalWhile();
		void setOperTwoType(int );
		void setRelOper(string );
		void setCtrlOperator(string );
		void setTargetConditional(string );
		void generateAssembly();
		void printData();
		void setForAuxList(list<for_cycle> aux);
		list<for_cycle> returnForAuxList();
		void setForParameter(int initOne, int initOneType, int initTwoType, int initTwo, int initTargt, int condOperOneType, int condOperOne, int relOper, int incrTargt, int incrOneType, int incrOne, int incrTwo, int incrTwoType, int, int, int, int);
		int returnUnit(int FU_type, int var);
		void setOperator(string var);
		void AddTargetConditionalPair(std::string, int);
		void setJmpCnst(int );
		void printForStruct();
		int  returnGlobalDo();
		int  returnGlobalFor();
		void elseExists();
		void setNumberWaits(int aux);
		int returnNumberWaits();
		void generateExpAssemblyTree(int, NODE *);
		void resetTreeAux();
		void resetTotalTreeAux(); 
		void genAsmExpressionMem(NODE2 *node_aux, int level);
		void genEndMemExpAsm(NODE2 *);
		void calcDelay(NODE2 *node_aux, int delay);
		void setMultVec(int *);
		void setAluVec(int *);
		int * returnMultVec();
		int * returnAluVec();
		void resetLargestNode();
		void putAsmInstr(string);
		void genAsmAssemblyInstr();
		void putResources(std::list<int> aux);
		std::list<int> returnResources();
		void setTargetConditionalType(int var);
		void setTargetOffset();
		void cleanAllParameters();
		void setDMAHexValue(string a);
		void verifyLabel(string a);
		int returnTargetConditional();
		void printComponentsList();
		void endisAutoDuty(int a);

		
	
};

class varStruct{

public:		NODE2 *fu;
private:	int fu_unit;
		string var_unit;


public:
	void putVariableName(string a);
	void putFuUnit(int a);
	int returnFuUnit();
	string returnVarName();
	void putFU(NODE2 *a);
	void printNode();
	string fuToReg();

};


string RegToString(int targetConditionalRegister);
int returnReg(string a);
int returnSelUnit(string var);
int memExpErrorTest(int out_for, int in_for);
//void testResource(int , int , int , int , int , int , int , int , int , int, int, int );
void freeTotalResources();
void printAlloc();
void printVarStruct();
void verifyMemTargCtrl(NODE2 *, NODE2 *);
void verifyExistingVars(string a);
string unitToString(int a);
string returnVarUnit(string a);
void saveVarName(string a);
void searchVarName(string a);
int verifyAllocation(string a);
varStruct returnVar(string a);
void freeResources(string fu, int);
void freeAllResources();
void disableAutoDuty(int a);
int returnUnitReg(string a);
void saveResources(int a, std::list<int> b);
void loadResources(int a);


extern global_mem_data mem_iter_per;
extern std::list<int> totalResources;
extern variable var_controller;
extern std::list<varStruct> varList;
extern string auxList;
extern NODE2 *auxNodeVars;
extern int searchVar(string a);
extern std::list<string> existingVars;
extern std::list<int> memExpResources[CONF_MEM_SIZE];



