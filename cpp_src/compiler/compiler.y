%{

#include "command.hpp"

using namespace std;


extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
extern int line_num;


void yyerror(std::list<COMMAND> *program , const char *s);

%}

%union {
	char *ablc;
	char *abuc;

	int bit;
	int num;

	char *inr;
	char *man;
	char *vod;
	char *chr;
	char *arc;
	char *arv;
	char *rtn;

	char *ast;
	char *ast_pli;
	char *expend;
	char *cmm;
	char *dot;
	char *pho;
	char *phc;
	char *cro;
	char *crc;
	char *aph;

	char *lequ;
	char *ladd;
	char *lsub;
	char *ldiv;
	char *lor;
	char *land;
	char *lnand;
	char *lxor;
	char *ls8;
	char *ls16;
	char *sra;
	char *srl;
	char *spu;
	char *sps;
	char *clz;
	char *lmx;
	char *lmn;
	char *abs;

	char *sif;
	char *four;
	char *gto;
	char *gti;
	char *whl;
	char *clear_flag;
	char *comm;
	char *exc;
	char *sml;
	char *smleq;
	char *big;
	char *bigeq;

	char *ini;
	char *run;
	char *run_exp;
	char *wit;
	char *sop;
	char *slo;
	char *sdi;
	char *sst;
	char *sdu;
	char *sin;
	char *sit;
	char *spr;
	char *ssh;
	char *sde;
	char *srv;
	char *sbslnr;
	char *sbslna;
	char *cnn;
	char *dcr;

	char *por;
	char *cpo;

	char *mem;
	char *mul;
	char *alu;
	char *alul;
	char *bs;
	char *ralu;
	char *ralul;
	char *reg;
	char *rbs;

	char *memt2;
	char *hex;
	char *str;
	char *shiftr;
	char *shiftl;
	char *testing;
	int testing2;
	char *do_while;
	char *lelse;
	char *memp;
	char *neww;
	char *unit;

	char *rmult;
	NODE *nodeptr;
	NODE2 *mem_aaa;
	NODE2 *mem_aaa2;
	NODE2 *mem_tgt;
	NODE2 *mem_tgt2;
	int const_aux;
	int save_method;
	char *str_var;

	char *ch;
	char *mem_exp;
	char *mem_exp2;
	int nm;
	char *squ_brao;
	char *squ_brac;
	char *disable;
	char *assembly;
	char *assembly2;

	char *stc;
	char *ldc;
	char *lvar;

	char *dma_eng;
	char *dma_ext;
	char *dma_int;
	char *dma_bus;
	char *dma_dir;
	char *lfree;

	char *galu;
	char *gmult;
	char *ead;
	char *twoPoints;
	char *exp;
	char *dma_config;
	char *mem_cte;
	char *reg_dma_ext;
	char *reg_dma_int;

}

%token <ablc> ABLC
%token <abuc> ABUC

%token <bit> BIT
%token <num> NUM

%token <inr> INR
%token <man> MAN
%token <vod> VOD
%token <chr> CHR
%token <arc> ARC
%token <arv> ARV
%token <rtn> RTN

%token <ast> AST
%token <ast_pli> AST_PLI
%token <expend> EXPEND
%token <cmm> CMM
%token <dot> DOT
%token <pho> PHO
%token <phc> PHC
%token <cro> CRO
%token <crc> CRC
%token <aph> APH

%token <lequ> LEQU
%token <ladd> LADD
%token <lsub> LSUB
%token <ldiv> LDIV
%token <lor> LOR
%token <land> LAND
%token <lnand> LNAND
%token <lxor> LXOR
%token <ls8> LS8
%token <ls16> LS16
%token <sra> SRA
%token <srl> SRL
%token <spu> SPU
%token <sps> SPS
%token <clz> CLZ
%token <lmx> LMX
%token <lmn> LMN
%token <abs> ABS
%token <twoPoints> TWO_POINTS

%token <sif> SIF
%token <four> FOUR
%token <gto> GTO
%token <gti> GTI
%token <whl> WHL
%token <clear_flag> CLEAR_FLAG
%token <exc> EXC
%token <sml> SML
%token <smleq> SMLEQ
%token <big> BIG
%token <bigeq> BIGEQ

%token <ini> INI
%token <run> RUN
%token <run_exp> RUN_EXP
%token <wit> WIT
%token <sop> SOP
%token <slo> SLO
%token <sli> SDI
%token <sst> SST
%token <sdu> SDU
%token <sin> SIN
%token <sit> SIT
%token <spr> SPR
%token <ssh> SSH
%token <sde> SDE
%token <srv> SRV
%token <sbslnr> SBSLNR
%token <sbslna> SBSLNA
%token <cnn> CNN
%token <dcr> DCR

%token <por> POR
%token <cpo> CPO

%token <mem> MEM
%token <memp> MEMP
%token <mul> MUL
%token <alu> ALU
%token <alul> ALUL
%token <bs> BS
%token <ralu> RALU
%token <ralul> RALUL
%token <rbs> RBS
%token <reg> REG

%token <memt2> MEMT2
%token <hex> HEX
%token <str> STR
%token <shiftr> LSHIFTR
%token <shiftl> LSHIFTL

%token <do_while> DOO

%token <lelse> ELSE

%token <rmult> RMULT

%token <mem_exp> MEM_EXP
%token <mem_exp2> MEM_EXP2

%token <squ_brao> SQU_BRAO
%token <squ_brac> SQU_BRAC

%token <disable> DIS

%token <assembly> ASM

%token <stc> STC
%token <ldc> LDC

%token <dma_eng> DMA
%token <dma_ext> DMA_EXT
%token <dma_int> DMA_INT
%token <dma_bus> DMA_BUS
%token <dma_dir> DMA_DIR

%token <lvar> VAR
%token <galu> GET_ALU
%token <gmult> GET_MULT

%token <neww> NEW
%token <unit> UNIT
%token <lfree> FREE

%token <ead> ENB_AD
%token <exp> EXP
%token <dma_config> DMAA_CONFIG
%token <mem_cte> MEM_CTE
%token <reg_dma_ext> REG_DMA_EXT
%token <reg_dma_int> REG_DMA_INT

%error-verbose

%parse-param {std::list<COMMAND> *program}

%type <nodeptr> expressions
%type <nm> operators
%type <mem_aaa> mem_expressions 
%type <mem_aaa2> mem_expressions2 
%type <mem_tgt> mem_target
%type <mem_tgt2> mem_target2
%type <const_aux> config_offset
//%type <save_method> var_methods
%type <str_var> vars




%left '+' '-' LADD LSUB
%left '&' LAND
%left  "<<" ">>" LSHIFTL LSHIFTR

%left '*' '|' '^' "~&" "*'" AST LOR LXOR LNAND AST_PLI

%%





program: 	
		INR MAN PHO PHC CRO commandlist CRC branch_boot_rom end_condition
		| INR MAN PHO VOD PHC CRO commandlist CRC branch_boot_rom end_condition	
		;

commandlist:	commandlist command
		| commandlist label TWO_POINTS command
		| label TWO_POINTS command
		| command  
		;

label:
	STR{
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->verifyLabel($1);
		auxIt->setLabel($1);
	}
	;

command: 	/* DCR DOT de_methods PHO de_parameters PHC ends
		| DCR DOT de_methods PHO PHC ends { 
			std::list<COMMAND>::iterator auxIt;
			auxIt = --program->end();
			auxIt--;
			auxIt->putResources(totalResources);
			freeTotalResources(); } */

		 DCR DOT de_exp_methods PHO PHC ends { 
			std::list<COMMAND>::iterator auxIt;
			auxIt = --program->end();
			auxIt--;
			auxIt->putResources(totalResources);
			freeTotalResources(); }

		| DCR DOT de_exp_methods PHO de_parameters PHC ends { 
			std::list<COMMAND>::iterator auxIt;
			auxIt = --program->end();
			auxIt--;
			auxIt->putResources(totalResources, auxIt->returnComponentList());
			freeTotalResources(); }


		| DCR DOT de_wait_methods PHO de_mem_parameters PHC ends
		| DCR DOT clear_label PHO PHC ends {   mem_iter_per.resetParameters();  //mem expression vec

			var_controller.resetVecs();  //mem expression vec

			freeTotalResources();   }


		//| DCR DOT clear_label PHO free_units PHC ends
		/*| DCR DOT clear_label PHO EXP PHC ends {

			mem_iter_per.resetParameters();  //mem expression vec
			var_controller.resetVecs();  //mem expression vec

		} */
		| DCR DOT config_mem_stc PHO config_offset PHC ends {

					
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			
			saveResources($5, totalResources);
			//auxIt->printComponentsList();

		}

		| DCR DOT config_mem_ldc PHO config_offset PHC ends {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			//auxIt->setNewComponentList(loadResources($5));
			loadResources($5);
			//cout << "cricas\n";
			//auxIt->printComponentsList();

		}
		
		| alu_units DOT alu_methods PHO APH alu_operators APH PHC ends
		| alul_units DOT alul_methods PHO APH alul_operators APH PHC ends

		| mult_units DOT mult_methods PHO lonhi_div2_args PHC ends

		| mem_units DOT mem_methods PHO expressions2 PHC ends

		| bs_units DOT bs_methods PHO lonhi_div2_args PHC ends
	
		| mem_units DOT mem_cn_methods PHO cnt_alu_units PHC ends
		| mem_units DOT mem_cn_methods PHO cnt_alul_units PHC ends
		| mem_units DOT mem_cn_methods PHO cnt_mult_units PHC ends
		| mem_units DOT mem_cn_methods PHO cnt_bs_units PHC ends
		| mem_units DOT mem_cn_methods PHO cnt_mem_units PHC ends

		| alu_units DOT cnt_methods PHO cnt_alu_units PHC ends
		| alu_units DOT cnt_methods PHO cnt_alul_units PHC ends
		| alu_units DOT cnt_methods PHO cnt_mult_units PHC ends
		| alu_units DOT cnt_methods PHO cnt_bs_units PHC ends
		| alul_units DOT cnt_methods PHO cnt_alu_units PHC ends
		| alul_units DOT cnt_methods PHO cnt_alul_units PHC ends
		| alul_units DOT cnt_methods PHO cnt_mult_units PHC ends
		| alul_units DOT cnt_methods PHO cnt_bs_units PHC ends
		| mult_units DOT cnt_methods PHO cnt_alu_units PHC ends
		| mult_units DOT cnt_methods PHO cnt_alul_units PHC ends
		| mult_units DOT cnt_methods PHO cnt_mult_units PHC ends
		| mult_units DOT cnt_methods PHO cnt_bs_units PHC ends
		| bs_units DOT cnt_methods PHO cnt_alu_units PHC ends
		| bs_units DOT cnt_methods PHO cnt_alul_units PHC ends
		| bs_units DOT cnt_methods PHO cnt_mult_units PHC ends
		| bs_units DOT cnt_methods PHO cnt_bs_units PHC ends

		| alu_units DOT cnt_methods PHO cnt_mem_units PHC ends 
		| alul_units DOT cnt_methods PHO cnt_mem_units PHC ends 
		| mult_units DOT cnt_methods PHO cnt_mem_units PHC ends 
		| bs_units DOT cnt_methods PHO cnt_mem_units PHC ends 

		/*| alu_units DOT disable_methods PHO PHC ends
		| alul_units DOT disable_methods PHO PHC ends
		| mult_units DOT disable_methods PHO PHC ends
		| bs_units DOT disable_methods PHO PHC ends  */


		| reg_target offset_set LEQU expressions ends {
			
			std::list<COMMAND>::iterator auxIt;
			auxIt = --program->end();
			auxIt--;
			auxIt->tree = $4;
			
		}

		| reg_target offset_set LEQU MEM SQU_BRAO expressions SQU_BRAC ends  {
			
			std::list<COMMAND>::iterator auxIt;
			auxIt = --program->end();
			auxIt--;
			auxIt->setOperOne($4);
			auxIt->setOperOneType(_MEMORY_RW);
			auxIt->setCmdType(VECTOR_RW);
			auxIt->tree = $6;
			
		}  

		| sif_units PHO exp PHC end_condition end_if_statment end_condition ends
		| sif_units PHO exp PHC end_condition command end_if_statment end_condition
		| else_units end_condition command end_else_statment end_condition
		
		
		| sif_units PHO exp PHC end_condition CRO CRC end_if_statment end_condition
		| else_units end_condition CRO CRC end_else_statment end_condition		
		| sif_units PHO exp PHC end_condition CRO commandlist CRC end_if_statment end_condition 
		| else_units end_condition CRO commandlist CRC end_else_statment end_condition 
		
		| whl_units PHO exp PHC end_condition CRO CRC end_whl_statment end_condition
		| whl_units PHO exp PHC end_condition CRO commandlist CRC end_whl_statment end_condition

		| fr_units PHO mem_exp_end_condition mem_exp_cond EXPEND MEM_EXP2 LADD LADD PHC CRO fr_units PHO mem_exp_end_condition2 mem_exp_cond2 EXPEND MEM_EXP LADD LADD PHC CRO mem_expressions_main CRC CRC mem_end_for mem_clean_vec end_condition 

		| fr_units PHO mem_exp_end_condition mem_exp_cond EXPEND MEM_EXP2 LADD LADD PHC CRO mem_expressions_main2 CRC mem_end_for mem_clean_vec end_condition 



 
		| fr_units PHO exp2 EXPEND exp2cond EXPEND exp3 PHC set_for_type end_condition CRO CRC end_fr_statment end_condition
		| fr_units PHO exp2 EXPEND exp2cond EXPEND exp3 PHC set_for_type end_condition CRO commandlist CRC end_fr_statment end_condition

		| fr_units PHO EXPEND exp2cond EXPEND exp3 PHC set_for_type end_condition CRO commandlist CRC end_fr_statment end_condition
		| fr_units PHO exp2 EXPEND EXPEND exp3 PHC set_for_type end_condition CRO commandlist CRC end_fr_statment end_condition
		| fr_units PHO exp2 EXPEND exp2cond EXPEND PHC set_for_type end_condition CRO commandlist CRC end_fr_statment end_condition
		| fr_units PHO EXPEND exp2cond EXPEND PHC set_for_type end_condition CRO commandlist CRC end_fr_statment end_condition
		| fr_units PHO exp2 EXPEND EXPEND PHC set_for_type end_condition CRO commandlist CRC end_fr_statment end_condition
		| fr_units PHO EXPEND EXPEND exp3 PHC set_for_type end_condition CRO commandlist CRC end_fr_statment end_condition
		| fr_units PHO EXPEND EXPEND PHC set_for_type end_condition CRO commandlist CRC end_fr_statment end_condition
		

		| fr_units PHO exp2 EXPEND exp2cond EXPEND exp3 PHC set_for_type end_condition command end_fr_statment end_condition
		| fr_units PHO exp2 EXPEND exp2cond EXPEND exp3 PHC set_for_type end_condition EXPEND end_fr_statment end_condition

		| fr_units PHO EXPEND exp2cond EXPEND exp3 PHC set_for_type end_condition command end_fr_statment end_condition
		| fr_units PHO exp2 EXPEND EXPEND exp3 PHC set_for_type end_condition EXPEND end_fr_statment end_condition
		| fr_units PHO exp2 EXPEND exp2cond EXPEND PHC set_for_type end_condition command end_fr_statment end_condition
		| fr_units PHO exp2 EXPEND EXPEND PHC set_for_type end_condition EXPEND end_fr_statment end_condition
		| fr_units PHO  EXPEND exp2cond EXPEND PHC set_for_type end_condition command end_fr_statment end_condition
		| fr_units PHO  EXPEND EXPEND exp3 PHC set_for_type end_condition EXPEND end_fr_statment end_condition
		| fr_units PHO  EXPEND EXPEND PHC set_for_type end_condition EXPEND end_fr_statment end_condition

		| go_units gi_label ends

		| do_units CRO end_condition commandlist CRC whl_units PHO exp PHC EXPEND end_do_statment end_condition
		| returnList ends 
		| assembly end_condition

		| dma_engine DOT dma_methods ends

		/*| vars LEQU var_methods EXPEND {
 			searchVarName($1);
			varStruct *aux = new varStruct();
			aux->putVariableName($1);
			aux->putFuUnit($3);
			verifyExistingVars($1); //atencao a isto
			varList.push_back(*aux);
		}  */

		| vars LEQU expressions ends {

			searchVarName($1);

			std::list<COMMAND>::iterator auxIt;
			auxIt = --program->end();
			auxIt--;

			varStruct aux = returnVar($1);
 			
			auxIt->tree = $3;

			auxIt->setCmdType(EXPRESSION);
			auxIt->setTargetConditional(aux.fuToReg());
			auxIt->setTargetConditionalType(REGISTER);	


			

	
		}


		| var_units DOT alu_methods PHO APH alu_operators APH PHC ends
		| var_units DOT mult_methods PHO lonhi_div2_args PHC ends
		| var_units DOT bs_methods PHO lonhi_div2_args PHC ends
		//| var_units DOT disable_methods PHO PHC ends

		| var_units DOT cnt_methods PHO cnt_alu_units PHC ends
		| var_units DOT cnt_methods PHO cnt_alul_units PHC ends
		| var_units DOT cnt_methods PHO cnt_mult_units PHC ends
		| var_units DOT cnt_methods PHO cnt_bs_units PHC ends
		

		| var_units DOT cnt_methods PHO cnt_mem_units PHC ends 

		| VAR STR EXPEND {

			saveVarName($2);

		}

		/*| VAR STR LEQU NEW UNIT EXPEND {

			saveVarName($2);
			varStruct *aux = new varStruct();
			aux->putVariableName($2);
			aux->putFuUnit(verifyAllocation($5));
			verifyExistingVars($1);
			varList.push_back(*aux);
			mem_iter_per.allocRsc($5, aux->returnFuUnit()); //used to allocate resources
			

		} */

		//| FREE PHO free_units PHC EXPEND 

		| DCR DOT ENB_AD enableADuty ends 


		;

/*ablc_unit:
	ABLC {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setSelUnitPort($1);	
		}
	;   */


alu_units:
	ALU {  		string aux2;
			string aux3;
			
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			aux2 = $1;
			
			aux3 = aux2[aux2.length()-1];

			auxIt->setUnit(ALU_FULL, atoi(aux3.c_str()));	

			mem_iter_per.allocRsc(returnSelUnit($1)); //used to allocate resources

		}
	


	;

alu_methods:
	SOP {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setCmdType(CONFIG);
			auxIt->setMethod(SET_OPER);	
		}
	;

alu_operators:
	alul_operators
	| LS8 {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	| LS16 {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	| SRA {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	| SRL {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	| SPU {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	| SPS {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	| CLZ {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	| LMX {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	| LMN {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	| ABS {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	;

alul_units:
	ALUL {  	string aux2;
			string aux3;
			
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			aux2 = $1;
			
			aux3 = aux2[aux2.length()-1];

			
			auxIt->setUnit(ALULITE, atoi(aux3.c_str()));	

			mem_iter_per.allocRsc(returnSelUnit($1)); //used to allocate resources

		}


	;

alul_methods:
	SOP {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(CONFIG);
			auxIt->setMethod(SET_OPER);	
		}
	;

alul_operators:
	LADD {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	| LSUB {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	
	| LOR {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	| LAND {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	| LNAND {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	| LXOR {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperator($1);
		}
	
	;

assembly:

	ASM { std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setCmdType(ASSEMBLY_CMD); 
			auxIt->putAsmInstr(yylval.assembly); }	



	;

branch_boot_rom:

		{		std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(RETURN);
			
		}


	;

bs_units:
	BS {  		string aux2;
			string aux3;
			
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			aux2 = $1;
			
			aux3 = aux2[aux2.length()-1];

			auxIt->setUnit(BSHIFTER, atoi(aux3.c_str()));	
		}
	;

bs_methods:
	SBSLNR {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			
			auxIt->setCmdType(CONFIG);
			auxIt->setMethod(SET_BS_LNR);		
		}	

	| SBSLNA {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			
			auxIt->setCmdType(CONFIG);
			auxIt->setMethod(SET_BS_LNA);		
		}	
	;

calc_vars:
	{ 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt; 
		--auxIt;
		verifyMemTargCtrl(auxNodeVars, auxIt->mem_tree); }



	;



clear_label:
	
	CLEAR_FLAG mem_clean_vec {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(CLEAR);
			auxIt->setMethod(CLEAR_REG_FLAGS);	

			

		}
	;

cnt_alu_units:
	ALU {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setSelUnit($1);	
		}

	| STR {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setSelUnit(returnVarUnit($1));	

		}

	;

cnt_alul_units:
	ALUL {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setSelUnit($1);	
		}

	;

cnt_mult_units:
	MUL {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setSelUnit($1);	
		}

	;

cnt_bs_units:
	BS {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setSelUnit($1);	
		}


	;

cnt_mem_units:
	MEMP {
			std::list<COMMAND>::iterator auxIt;
			string aux2;
			string aux3;
			string aux4;

			auxIt = program->end();
			--auxIt;
			aux2 = $1;
			aux3 = aux2.substr(0,4);

			auxIt->setSelUnit(aux3);	
			aux4 = aux2[aux2.length()-1];

			auxIt->setSelUnitPort(aux4);	

		}
	;

cnt_methods:
	CPO {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setCmdType(CONNECT);
			auxIt->setMethod(CONNECT, $1);
		}

	;

config_mem_stc:

	STC {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setCmdType(MEM_INSTR);
			auxIt->setMethod(SAVE_CONFIG);
		}

	/*| LDC {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setCmdType(MEM_INSTR);
			auxIt->setMethod(LOAD_CONFIG);
		}  */
	;

config_mem_ldc:

	LDC {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setCmdType(MEM_INSTR);
			auxIt->setMethod(LOAD_CONFIG);
		}
	;

config_offset:

	BIT {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			
			auxIt->setCnst($1);
			$$ = $1;	
		}



	| NUM {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			$$ = $1;
			auxIt->setCnst($1);	
		}




	;

const_start:
	BIT { 
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->aux_tree2.start_type = CONST;
			auxIt->aux_tree2.start = $1;  }
	| NUM 	{ 
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->aux_tree2.start_type = CONST;
			auxIt->aux_tree2.start = $1; }


	| REG {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.start = returnReg($1);
		auxIt->aux_tree2.start_type = REGISTER;
		//auxIt->setCmdType(MEM_EXPRESSION);	
	}


	| RALU {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.start = returnReg($1);
		auxIt->aux_tree2.start_type = REGISTER;
		//auxIt->setCmdType(MEM_EXPRESSION);	
	}

	| RALUL {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.start = returnReg($1);
		auxIt->aux_tree2.start_type = REGISTER;
		//auxIt->setCmdType(MEM_EXPRESSION);	
	}



	; 


const_period_shift:
	BIT { 
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->aux_tree2.shift_type = CONST;
			auxIt->aux_tree2.period_shift = $1; }
	| NUM 	{ 
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->aux_tree2.shift_type = CONST;
			auxIt->aux_tree2.period_shift = $1; }


	| REG {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.period_shift = returnReg($1);
		auxIt->aux_tree2.shift_type = REGISTER;
		//auxIt->setCmdType(MEM_EXPRESSION);	
	}


	| RALU {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.period_shift = returnReg($1);
		auxIt->aux_tree2.shift_type = REGISTER;
		//auxIt->setCmdType(MEM_EXPRESSION);	
	}

	| RALUL {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.period_shift = returnReg($1);
		auxIt->aux_tree2.shift_type = REGISTER;
		//auxIt->setCmdType(MEM_EXPRESSION);	
	}


	

	; 

const_incr:
	BIT { 
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->aux_tree2.incr_type = CONST;
			auxIt->aux_tree2.incr = $1; }
	| NUM 	{ 
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->aux_tree2.incr_type = CONST;
			auxIt->aux_tree2.incr = $1; }

	| REG {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.incr = returnReg($1);
		auxIt->aux_tree2.incr_type = REGISTER;
		//auxIt->setCmdType(MEM_EXPRESSION);	
	}


	| RALU {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.incr = returnReg($1);
		auxIt->aux_tree2.incr_type = REGISTER;
		//auxIt->setCmdType(MEM_EXPRESSION);	
	}

	| RALUL {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.incr = returnReg($1);
		auxIt->aux_tree2.incr_type = REGISTER;
		//auxIt->setCmdType(MEM_EXPRESSION);	
	}



	; 



de_exp_methods:
	RUN_EXP {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setMethod(CTRL_REG_RUN_EXP);
			auxIt->setCmdType(_RUN);
			var_controller.resetVecs();
		}

	;

/* de_methods:	
	INI {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setMethod(CTRL_REG_INIT);
			auxIt->setCmdType(INIT);	
			var_controller.resetVecs();
		}
	| RUN {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setMethod(CTRL_REG_RUN);
			auxIt->setCmdType(_RUN);
			var_controller.resetVecs();
		}  
	;  */

/*debug:

	{ cout << "cricas\n"; }

	;  */

de_parameters:	
	list_de_paremeters
	;

de_mem_parameters: 
	de_mem_parameter CMM de_mem_parameters
	| de_mem_parameter
	;

de_parameter:
	
	de_mem_parameter

	| ALU {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setComponentList($1);	
		}
	| ALUL {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setComponentList($1);	
		}
	| MUL {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setComponentList($1);	
		}
	| BS {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setComponentList($1);	
		}

	| STR {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setComponentList(returnVarUnit($1));	

		}


	;

de_mem_parameter:

	de_mem_without_ports
	
	| MEMP {
			string aux = $1;
			char aux2 = aux[aux.length()];
			std::list<COMMAND>::iterator auxIt;

			auxIt = program->end();
			--auxIt;
			
			auxIt->setComponentList($1); //is necessary config each port individually	
		}

	;

de_mem_without_ports:

	MEM {
			string aux = $1;
			char aux2 = aux[aux.length()];
			std::list<COMMAND>::iterator auxIt;

			auxIt = program->end();
			--auxIt;
			
			auxIt->setComponentList($1); //is necessary config each port individually	
		}
	;


de_wait_methods:
	
	WIT {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setMethod(CTRL_REG_WAIT);
			auxIt->setCmdType(WAIT);
		}
	;

/*disable_methods:

	DIS {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setCmdType(CONNECT);
			auxIt->setMethod(CONNECT, $1);
		}



	;  */

dma_engine:

	DMA {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setCmdType(_DMA);
			//auxIt->setMethod(SET_EXT_ADDR);
		}

	;

dma_expressions:

	dma_expression1 operators dma_expression2
	| dma_expression1

	| HEX {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			//auxIt->setOperOne(strtol($1,NULL,16));	
			auxIt->setDMAHexValue($1);
			auxIt->setOperOneType(_DMA_HEX);	
	}

	;

dma_expression1:

	REG {
			
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperOne($1);				
			auxIt->setOperOneType(REGISTER);
		}
	| BIT {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);	
			auxIt->setOperOneType(CONST);	
		}
	| NUM {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);	
			auxIt->setOperOneType(CONST);	
		}

	
	| LSUB BIT {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne('-', $2);	
			auxIt->setOperOneType(CONST);	
		}
	| LSUB NUM {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne('-', $2);	
			auxIt->setOperOneType(CONST);	
		
		}
	| RALU {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);	
			auxIt->setOperOneType(REGISTER);	
		}
	| RALUL {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);	
			auxIt->setOperOneType(REGISTER);	
		}

	| RMULT {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);	
			auxIt->setOperOneType(REGISTER);	
		}

	| RBS {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);	
			auxIt->setOperOneType(REGISTER);	
		}

	
	
	| MEM_CTE {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);	
			//auxIt->setOperOneType(_MEMORY_ADDR);	
			auxIt->setOperOneType(CONST);
	}

	| REG_DMA_EXT {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperOne($1);	
			//auxIt->setOperOneType(_MEMORY_ADDR);	
			auxIt->setOperOneType(_DMA_REG);
	}

	| REG_DMA_INT {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperOne($1);	
			//auxIt->setOperOneType(_MEMORY_ADDR);	
			auxIt->setOperOneType(_DMA_REG);
	}



	;

dma_expression2:

	REG {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);	
			auxIt->setOperTwoType(REGISTER);	
		}
	| BIT {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);	
			auxIt->setOperTwoType(CONST);	
		}
	| NUM {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);	
			auxIt->setOperTwoType(CONST);	
		}
	| RALU {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);	
			auxIt->setOperTwoType(REGISTER);	
		}
	| RALUL {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);	
			auxIt->setOperTwoType(REGISTER);	
		}


	| RMULT {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);	
			auxIt->setOperTwoType(REGISTER);	
		}


	| RBS {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);	
			auxIt->setOperTwoType(REGISTER);	
		}




	| HEX {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo(strtol($1,NULL,16));	
			auxIt->setOperTwoType(CONST);	
	}

	

	
	;




dma_methods:

	DMA_EXT PHO dma_expressions PHC {
			
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setMethod(_SET_EXT_ADDR);
		}

	| DMA_INT PHO dma_expressions PHC {
			
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setMethod(_SET_INT_ADDR);
		}
	| DMA_BUS PHO config_offset PHC {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setMethod(_SET_SIZE_ADDR);
		}
	| DMA_DIR PHO config_offset PHC {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setMethod(_SET_DIRECTION_ADDR);
		}

	| DMAA_CONFIG PHO dma_expressions dma_setExt_methods end_condition CMM dma_expressions dma_setInt_methods end_condition CMM config_offset dma_setSize_methods end_condition CMM config_offset dma_setDir_methods PHC  


	| RUN_EXP PHO PHC {
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->setMethod(DMA_RUN);
	
	}

	| WIT PHO PHC {
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->setMethod(DMA_WAIT);
	
	}  

	;

dma_setExt_methods:
		{
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setMethod(_SET_EXT_ADDR);
			auxIt->setCmdType(_DMA);
		}
	;

dma_setInt_methods:
		{
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setMethod(_SET_INT_ADDR);
			auxIt->setCmdType(_DMA);
		}
	;

dma_setSize_methods:

	{
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setMethod(_SET_SIZE_ADDR);  
			auxIt->setCmdType(_DMA); }

	;

dma_setDir_methods:

	{
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setMethod(_SET_DIRECTION_ADDR);  
			auxIt->setCmdType(_DMA);  }


	;

do_units:

	DOO {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setCmdType(_DO);
	}
	;

else_units:

	ELSE {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(_ELSE);		
	}

	;

enableADuty:  

	PHO BIT PHC {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(AUTO_DUTY);
			auxIt->endisAutoDuty($2);	


	}


	;


end_do_statment:
	{
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setCmdType(DO_WHILE);
	}
	;

end_else_statment:
	{
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(END_ELSE);		
	}
	;

exp:
	reg_units sif_operators reg_units2
	| reg_units sif_operators reg_const
	;

exp2:	
	reg_units operators reg_units2 {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			
			auxIt->setForParameter(1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			
		}
	| reg_units operators reg_const {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			
			auxIt->setForParameter(1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			
		}
	| reg_target LEQU expressions2 {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			
			auxIt->setForParameter(1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			
		}
	;

exp2cond:
	reg_units sif_operators reg_units2 {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setForParameter(0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1);
			
		}
	| reg_units sif_operators reg_const {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setForParameter(0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1);
			
		}	
	;

exp3:	
	/*reg_units operators reg_units2 {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			
			auxIt->setForParameter(0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0);
			
		}
	| reg_units operators reg_const {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setForParameter(0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0);
			
		}  */

	reg_target LSUB LSUB {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;

		auxIt->setOperOne(auxIt->returnTargetConditional());
		auxIt->setOperOneType(REGISTER);
		auxIt->setOperTwo(1);
		auxIt->setOperTwoType(CONST);
		auxIt->setRelOper("-");

		auxIt->setForParameter(0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0);

		

	}



	| reg_target LADD LADD {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;

		auxIt->setOperOne(auxIt->returnTargetConditional());
		auxIt->setOperOneType(REGISTER);
		auxIt->setOperTwo(1);
		auxIt->setOperTwoType(CONST);
		auxIt->setRelOper("+");

		auxIt->setForParameter(0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0);

		

	}
	

	| reg_target LEQU expressions2 {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setForParameter(0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0);
			
		}
	;

expressions:
	expressions LADD expressions{  $$ = new NODE("+"," ",0,$1,$3,0);  }

	| expressions LSUB expressions{  $$ = new NODE("-"," ",0,$1,$3,0); }

	| expressions LAND expressions{  $$ = new NODE("&"," ",0,$1,$3,0); }

	| expressions LSHIFTR expressions{  $$ = new NODE(">>"," ",0,$1,$3,0); }

	| expressions LSHIFTL expressions{  $$ = new NODE("<<"," ",0,$1,$3,0); }

	|  PHO expressions PHC { $$ = $2; } 

	| REG {		
			NODE *aux;
			aux = new NODE(" ",$1,0,NULL,NULL, REGISTER);
			$$ = aux;
		}

	| NUM {	
			
			$$ = new NODE(" "," ",$1,NULL,NULL, CONST);
			
		}

	| BIT {	
			
			$$ = new NODE(" "," ",$1,NULL,NULL, CONST);
				
		}
	
	| HEX {		
			$$ = new NODE(" "," ",strtol($1,NULL,16),NULL,NULL, CONST);	
	}

	
	| MEMP SQU_BRAO config_offset SQU_BRAC {

			

			$$ = new NODE(" ",$1,$3,NULL,NULL, _MEMORY_TYPE);

		}

	//| MEMP SQU_BRAO config_offset SQU_BRAC

	//| MEM SQU_BRAO expressions SQU_BRAC


	| LSUB BIT {	

			$$ = new NODE(" "," ",($2*-1),NULL,NULL, CONST);
		}
	| LSUB NUM {	

			$$ = new NODE(" "," ",($2*-1),NULL,NULL, CONST);
		
		}

	| RALU {	
			$$ = new NODE(" ",$1,0,NULL,NULL, REGISTER);		}





	| RALUL {
			
			$$ = new NODE(" ",$1,0,NULL,NULL, REGISTER);		}

	| RMULT {
			
			$$ = new NODE(" ",$1,0,NULL,NULL, REGISTER);		}

	| RBS {
			
			$$ = new NODE(" ",$1,0,NULL,NULL, REGISTER);		}


	| STR {

			searchVarName($1);
			//cout << "cricas\n";
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			auxIt--;

			varStruct aux = returnVar($1);
 			
			$$ = new NODE(" ",aux.fuToReg(),0,NULL,NULL, REGISTER);

			//cout << "cricas 2 = " << aux.fuToReg() << '\n';
			
			//auxIt->setOperOne(aux.fuToReg());
			//auxIt->setOperOneType(REGISTER);	
		}


	;


expressions2:
	expression operators expression2
	| expression
	;

expression:
	REG {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setOperOne($1);				
			auxIt->setOperOneType(REGISTER);
		}
	| BIT {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);	
			auxIt->setOperOneType(CONST);	
		}
	| NUM {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);	
			auxIt->setOperOneType(CONST);	
		}

	
	| LSUB BIT {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne('-', $2);	
			auxIt->setOperOneType(CONST);	
		}
	| LSUB NUM {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne('-', $2);	
			auxIt->setOperOneType(CONST);	
		
		}
	| RALU {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);	
			auxIt->setOperOneType(REGISTER);	
		}
	| RALUL {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);	
			auxIt->setOperOneType(REGISTER);	
		}

	| RMULT {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);	
			auxIt->setOperOneType(REGISTER);	
		}

	| RBS {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);	
			auxIt->setOperOneType(REGISTER);	
		}


	| HEX {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne(strtol($1,NULL,16));	
			auxIt->setOperOneType(CONST);	
	}
	;

expression2:
	REG {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);	
			auxIt->setOperTwoType(REGISTER);	
		}
	| BIT {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);	
			auxIt->setOperTwoType(CONST);	
		}
	| NUM {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);	
			auxIt->setOperTwoType(CONST);	
		}
	| RALU {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);	
			auxIt->setOperTwoType(REGISTER);	
		}
	| RALUL {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);	
			auxIt->setOperTwoType(REGISTER);	
		}

	| RMULT {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);	
			auxIt->setOperTwoType(REGISTER);	
		}

	| RBS {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);	
			auxIt->setOperTwoType(REGISTER);	
		}
	


	| HEX {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo(strtol($1,NULL,16));	
			auxIt->setOperTwoType(CONST);	
	}
	;  

/*free_units:

	ALU { 
		//searchVarName($1);		
		freeResources($1, 0); }

	| ALUL { 
		//searchVarName($1);
		freeResources($1, 0); }

	| MUL { 
		//searchVarName($1);
		freeResources($1, 0); }

	| STR { 
		//searchVarName($1);
		freeResources($1, 1); }

	;  */

gi_label:
	STR {
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;

		auxIt->setGtLabel($1); //JUMP is defined in program.h
	}
	;

go_units:
	GTO{

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;

		auxIt->setCmdType(JUMP); //JUMP is defined in program.h
	}
	;

increment:

	const_incr AST MEM_EXP

	|  MEM_EXP AST const_incr

	| MEM_EXP { 
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->aux_tree2.incr_type = CONST;
			auxIt->aux_tree2.incr = 1; }


	;

increment2:

	const_incr AST MEM_EXP2

	|  MEM_EXP2 AST const_incr

	| MEM_EXP2 { 
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->aux_tree2.incr_type = CONST;
			auxIt->aux_tree2.incr = 1; }


	;


list_de_paremeters: 
	de_parameter CMM list_de_paremeters
	| de_parameter
	;

lonhi_div2_args:
	BIT {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCnst($1);	
		}
	;

mem_clean_vec:
	{
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->cleanAllParameters();
		
	}
	;

mem_end_for:

	{
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->setCmdType(MEM_END);
		var_controller.copyUsedResources(mem_iter_per.vec_mult, mem_iter_per.vec_alu);	
		//mem_iter_per.resetParameters();

	}



	;

mem_exp_cond:

	MEM_EXP2 SML NUM {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.iter = $3;
		auxIt->aux_tree2.iter_type = CONST;
		auxIt->setCmdType(MEM_EXPRESSION);	
		mem_iter_per.setIter($3, CONST);
	}


	| MEM_EXP2 SML BIT {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.iter = $3;
		auxIt->aux_tree2.iter_type = CONST;
		auxIt->setCmdType(MEM_EXPRESSION);	
		mem_iter_per.setIter($3, CONST);
	}

	| MEM_EXP2 SML REG {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.iter = returnReg($3);	
		auxIt->aux_tree2.iter_type = REGISTER;	
		auxIt->setCmdType(MEM_EXPRESSION);	
		mem_iter_per.setIter(returnReg($3), REGISTER);
	
	}


	| MEM_EXP2 SML RALU {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.iter = returnReg($3);
		auxIt->aux_tree2.iter_type = REGISTER;
		auxIt->setCmdType(MEM_EXPRESSION);
		mem_iter_per.setIter(returnReg($3), REGISTER);
	
	}

	| MEM_EXP2 SML RALUL {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.iter = returnReg($3);
		auxIt->aux_tree2.iter_type = REGISTER;
		auxIt->setCmdType(MEM_EXPRESSION);
		mem_iter_per.setIter(returnReg($3), REGISTER);	

	}


	;

mem_exp_end_condition:

	MEM_EXP2 LEQU BIT EXPEND { memExpErrorTest($3, 0);   
		}

	; 

mem_exp_end_condition2:

	MEM_EXP LEQU BIT EXPEND { memExpErrorTest(0, $3); }
	; 

mem_expressions:

	

	//mem_expressions alul_operators mem_expressions
	
	mem_expressions  LADD mem_expressions { 


		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 			
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux();
		}

	| mem_expressions LSUB mem_expressions { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;	
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 	
		auxIt->mem_tree = $$;  
		auxIt->resetTreeAux(); }

	| mem_expressions LOR mem_expressions { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 	
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux(); } 

	| mem_expressions LAND mem_expressions {  
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 	
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux(); }

	| mem_expressions LNAND mem_expressions {  
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 	
		auxIt->mem_tree = $$;  
		auxIt->resetTreeAux(); }

	| mem_expressions AST mem_expressions {  
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 	
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux();  }



	| mem_expressions AST_PLI mem_expressions {  
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 	
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux();  }  

	

	| mem_expressions LXOR mem_expressions {  
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 	
		auxIt->mem_tree = $$;  
		auxIt->resetTreeAux(); } 

	|  PHO mem_expressions PHC { $$ = $2; }  

	| MEMP mem_configs { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($1,mem_iter_per.iter,mem_iter_per.per,auxIt->aux_tree2.start,auxIt->aux_tree2.period_shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,NULL,NULL,mem_iter_per.iterType,mem_iter_per.perType,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, auxIt->vec_alu, auxIt->vec_mult); 
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux(); } 


	| RALU { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($1,mem_iter_per.iter,mem_iter_per.per,auxIt->aux_tree2.start,auxIt->aux_tree2.period_shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,NULL,NULL,mem_iter_per.iterType,mem_iter_per.perType,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, auxIt->vec_alu, auxIt->vec_mult); 
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux(); } 


	| RALUL { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($1,mem_iter_per.iter,mem_iter_per.per,auxIt->aux_tree2.start,auxIt->aux_tree2.period_shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,NULL,NULL,mem_iter_per.iterType,mem_iter_per.perType,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, auxIt->vec_alu, auxIt->vec_mult); 
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux(); } 


	| RMULT { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($1,mem_iter_per.iter,mem_iter_per.per,auxIt->aux_tree2.start,auxIt->aux_tree2.period_shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,NULL,NULL,mem_iter_per.iterType,mem_iter_per.perType,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, auxIt->vec_alu, auxIt->vec_mult); 
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux(); } 


	| RBS { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($1,mem_iter_per.iter,mem_iter_per.per,auxIt->aux_tree2.start,auxIt->aux_tree2.period_shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,NULL,NULL,mem_iter_per.iterType,mem_iter_per.perType,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, auxIt->vec_alu, auxIt->vec_mult); 
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux(); } 



	| STR  {
		searchVarName($1);


		varStruct aux = returnVar($1);
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2(aux.fuToReg(),mem_iter_per.iter,mem_iter_per.per,auxIt->aux_tree2.start,auxIt->aux_tree2.period_shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,NULL,NULL,mem_iter_per.iterType,mem_iter_per.perType,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, auxIt->vec_alu, auxIt->vec_mult); 
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux();
		
		}	

	;


mem_expressions2:

	

	//mem_expressions alul_operators mem_expressions
	
	mem_expressions2  LADD mem_expressions2 { 


		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 			
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux();
		}

	| mem_expressions2 LSUB mem_expressions2 { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;	
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 	
		auxIt->mem_tree = $$;  
		auxIt->resetTreeAux(); }

	| mem_expressions2 LOR mem_expressions2 { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 	
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux(); } 

	| mem_expressions2 LAND mem_expressions2 {  
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 	
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux(); }

	| mem_expressions2 LNAND mem_expressions2 {  
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 	
		auxIt->mem_tree = $$;  
		auxIt->resetTreeAux(); }

	| mem_expressions2 AST mem_expressions2 {  
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 	
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux();  }



	| mem_expressions2 AST_PLI mem_expressions2 {  
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 	
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux();  }  

	

	| mem_expressions2 LXOR mem_expressions2 {  
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($2,auxIt->aux_tree2.iter,auxIt->aux_tree2.period,auxIt->aux_tree2.start,auxIt->aux_tree2.shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,$1,$3,auxIt->aux_tree2.iter_type,auxIt->aux_tree2.period_type,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, mem_iter_per.vec_alu, mem_iter_per.vec_mult ); 	
		auxIt->mem_tree = $$;  
		auxIt->resetTreeAux(); } 

	|  PHO mem_expressions2 PHC { $$ = $2; }  

	| MEMP mem_configs2 { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($1,mem_iter_per.iter,mem_iter_per.per,auxIt->aux_tree2.start,auxIt->aux_tree2.period_shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,NULL,NULL,mem_iter_per.iterType,mem_iter_per.perType,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, auxIt->vec_alu, auxIt->vec_mult); 
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux(); } 


	| RALU { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($1,mem_iter_per.iter,mem_iter_per.per,auxIt->aux_tree2.start,auxIt->aux_tree2.period_shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,NULL,NULL,mem_iter_per.iterType,mem_iter_per.perType,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, auxIt->vec_alu, auxIt->vec_mult); 
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux(); } 


	| RALUL { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($1,mem_iter_per.iter,mem_iter_per.per,auxIt->aux_tree2.start,auxIt->aux_tree2.period_shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,NULL,NULL,mem_iter_per.iterType,mem_iter_per.perType,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, auxIt->vec_alu, auxIt->vec_mult); 
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux(); } 


	| RMULT { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($1,mem_iter_per.iter,mem_iter_per.per,auxIt->aux_tree2.start,auxIt->aux_tree2.period_shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,NULL,NULL,mem_iter_per.iterType,mem_iter_per.perType,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, auxIt->vec_alu, auxIt->vec_mult); 
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux(); } 


	| RBS { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($1,mem_iter_per.iter,mem_iter_per.per,auxIt->aux_tree2.start,auxIt->aux_tree2.period_shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,NULL,NULL,mem_iter_per.iterType,mem_iter_per.perType,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, auxIt->vec_alu, auxIt->vec_mult); 
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux(); } 



	| STR  {
		searchVarName($1);


		varStruct aux = returnVar($1);
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2(aux.fuToReg(),mem_iter_per.iter,mem_iter_per.per,auxIt->aux_tree2.start,auxIt->aux_tree2.period_shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,NULL,NULL,mem_iter_per.iterType,mem_iter_per.perType,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, auxIt->vec_alu, auxIt->vec_mult); 
		auxIt->mem_tree = $$; 
		auxIt->resetTreeAux();
		
		}	

	;


mem_save_data:

	{

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->setCmdType(MEM_EXPRESSION);	 
		COMMAND *aux = new COMMAND();

		aux->resetCmd();
		program->push_back(*aux); }


	;

mem_expressions_main:

	mem_target LEQU mem_expressions EXPEND mem_save_data calc_vars mem_expressions_main 


	| mem_target LEQU mem_expressions EXPEND mem_save_data { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt; 
		--auxIt;
		verifyMemTargCtrl($1, auxIt->mem_tree); }

	;

mem_expressions_main2:

	mem_target2 LEQU mem_expressions2 EXPEND mem_save_data calc_vars mem_expressions_main2 


	| mem_target2 LEQU mem_expressions2 EXPEND mem_save_data { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt; 
		--auxIt;
		verifyMemTargCtrl($1, auxIt->mem_tree); }

	;


mem_configs:

	SQU_BRAO const_start LADD per_shift LADD increment SQU_BRAC 
	//---------------------------------------------------------------------------------

	| SQU_BRAO per_shift LADD increment SQU_BRAC

	| SQU_BRAO const_start LADD increment SQU_BRAC /*{ 
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->aux_tree2.shift_type = 0;
			auxIt->aux_tree2.period_shift = 0; } */
	//---------------------------------------------------------------------------------
	| SQU_BRAO const_start LADD per_shift SQU_BRAC /*{ 
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->aux_tree2.incr_type = 0;
			auxIt->aux_tree2.incr = 0; } */


	| SQU_BRAO per_shift SQU_BRAC

	| SQU_BRAO const_start SQU_BRAC 
	//---------------------------------------------------------------------------------

	| SQU_BRAO increment SQU_BRAC

	//---------------------------------------------------------------------------------

	//| SQU_BRAO MEM_EXP2 PHO const_period_shift PHC LADD const_incr AST MEM_EXP SQU_BRAC

	//---------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------

	;


mem_configs2:

	//SQU_BRAO const_start LADD per_shift LADD increment SQU_BRAC 
	//---------------------------------------------------------------------------------

	//| SQU_BRAO per_shift LADD increment SQU_BRAC

	| SQU_BRAO const_start LADD increment2 SQU_BRAC 
	//---------------------------------------------------------------------------------
	//| SQU_BRAO const_start LADD per_shift SQU_BRAC 
			/*{ 
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->aux_tree2.incr_type = 0;
			auxIt->aux_tree2.incr = 0; } */


	//| SQU_BRAO per_shift SQU_BRAC

	| SQU_BRAO const_start SQU_BRAC 
	//---------------------------------------------------------------------------------

	| SQU_BRAO increment2 SQU_BRAC

	//---------------------------------------------------------------------------------

	//| SQU_BRAO MEM_EXP2 PHO const_period_shift PHC LADD const_incr AST MEM_EXP SQU_BRAC

	//---------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------

	;

mem_target:

	MEMP mem_configs { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($1,mem_iter_per.iter,mem_iter_per.per,auxIt->aux_tree2.start,auxIt->aux_tree2.period_shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,NULL,NULL,mem_iter_per.iterType,mem_iter_per.perType,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, auxIt->vec_alu, auxIt->vec_mult); 
		auxIt->mem_target = $$; 
		auxIt->resetTotalTreeAux(); 
		auxNodeVars = $$; } 

	
	| STR {
 			
			searchVarName($1);
			//varStruct *aux = new varStruct();
			auxList=$1;
			$$=NULL;
			auxNodeVars = NULL;
			//aux->putFuUnit($3);
			//$$=aux;
			//aux->fu = auxIt->mem_tree;
			//aux->putFU(auxIt->mem_tree);
			//varList.push_back(*aux);
		}


	;

mem_target2:

	MEMP mem_configs2 { 
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		$$ = new NODE2($1,mem_iter_per.iter,mem_iter_per.per,auxIt->aux_tree2.start,auxIt->aux_tree2.period_shift,auxIt->aux_tree2.delay,auxIt->aux_tree2.incr,NULL,NULL,mem_iter_per.iterType,mem_iter_per.perType,auxIt->aux_tree2.start_type,auxIt->aux_tree2.shift_type,auxIt->aux_tree2.incr_type, auxIt->vec_alu, auxIt->vec_mult); 
		auxIt->mem_target = $$; 
		auxIt->resetTotalTreeAux(); 
		auxNodeVars = $$; } 

	
	| STR {
 			
			searchVarName($1);
			//varStruct *aux = new varStruct();
			auxList=$1;
			$$=NULL;
			auxNodeVars = NULL;
			//aux->putFuUnit($3);
			//$$=aux;
			//aux->fu = auxIt->mem_tree;
			//aux->putFU(auxIt->mem_tree);
			//varList.push_back(*aux);
		}


	;


mem_methods:
	SST {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(CONFIG);
			auxIt->setMethod(SET_START);		
		}
	| SDU {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(CONFIG);
			auxIt->setMethod(SET_DUTY);		
		}
	| SIN {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->setCmdType(CONFIG);
			auxIt->setMethod(SET_INCR);		
		}
	| SIT {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(CONFIG);
			auxIt->setMethod(SET_ITER);		
		}
	| SPR {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(CONFIG);
			auxIt->setMethod(SET_PER);		
		}
	| SSH {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(CONFIG);
			auxIt->setMethod(SET_SHIFT);		
		}
	| SDE {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(CONFIG);
			auxIt->setMethod(SET_DELAY);		
		}
	| SRV {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(CONFIG);
			auxIt->setMethod(SET_REVERSE);		
		}
	;

mem_cn_methods:
	CNN {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(CONNECT);
			auxIt->setMethod(CONNECT, $1);
		}
	;


//mem_exp3:


//	 MEM_EXP LEQU expressions2 





	//;

mem_exp_cond2:

	MEM_EXP sif_operators NUM {
		
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.period = $3;
		auxIt->aux_tree2.period_type = CONST;
		auxIt->setCmdType(MEM_EXPRESSION);	
		mem_iter_per.setPer($3, CONST);
	}


	| MEM_EXP sif_operators BIT {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.period = $3;
		auxIt->aux_tree2.period_type = CONST;
		auxIt->setCmdType(MEM_EXPRESSION);	
		mem_iter_per.setPer($3, CONST);
	}

	| MEM_EXP sif_operators REG {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.period = returnReg($3);
		auxIt->aux_tree2.period_type = REGISTER;
		auxIt->setCmdType(MEM_EXPRESSION);	
		mem_iter_per.setPer(returnReg($3), REGISTER);
	}


	| MEM_EXP sif_operators RALU {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.period = returnReg($3);
		auxIt->aux_tree2.period_type = REGISTER;
		auxIt->setCmdType(MEM_EXPRESSION);
		mem_iter_per.setPer(returnReg($3), REGISTER);	
	}

	| MEM_EXP sif_operators RALUL {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.period = returnReg($3);
		auxIt->aux_tree2.period_type = REGISTER;
		auxIt->setCmdType(MEM_EXPRESSION);	
		mem_iter_per.setPer(returnReg($3), REGISTER);
	}

	| MEM_EXP sif_operators RMULT {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.period = returnReg($3);
		auxIt->aux_tree2.period_type = REGISTER;
		auxIt->setCmdType(MEM_EXPRESSION);	
		mem_iter_per.setPer(returnReg($3), REGISTER);
	}

	| MEM_EXP sif_operators RBS {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		auxIt->aux_tree2.period = returnReg($3);
		auxIt->aux_tree2.period_type = REGISTER;
		auxIt->setCmdType(MEM_EXPRESSION);	
		mem_iter_per.setPer(returnReg($3), REGISTER);
	}


	| MEM_EXP sif_operators STR {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;
		searchVarName($3);
		varStruct aux = returnVar($3);	
		auxIt->aux_tree2.period = returnReg(aux.fuToReg());
		auxIt->aux_tree2.period_type = REGISTER;
		auxIt->setCmdType(MEM_EXPRESSION);	
		mem_iter_per.setPer(returnReg(aux.fuToReg()), REGISTER);

	}


	;

/*mem_rw:

	REG {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(VECTOR_RW);
			auxIt->setTargetConditional($1);
			auxIt->setTargetConditionalType(REGISTER);
			
		}

	; */

mem_units:
	MEMP {  	string aux2;
			string aux3;
			string aux4;
			
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			aux2 = $1;
			
			aux3 = aux2[aux2.length()-2];

			aux4 = aux2[aux2.length()-1];

			auxIt->setUnit(MEMORY, atoi(aux3.c_str()));	
			auxIt->setPort("port"+aux4);
			//cout << "port"+aux4;

		}
	;

mult_units:
	MUL {  		string aux2;
			string aux3;
			
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			aux2 = $1;
			
			aux3 = aux2[aux2.length()-1];

			
			auxIt->setUnit(MULTIPLIER, atoi(aux3.c_str()));	

			mem_iter_per.allocRsc(returnSelUnit($1)); //used to allocate resources

		}
	;

mult_methods:
	SLO {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(CONFIG);
			auxIt->setMethod(SET_LONHI);		
		}
	| SDI {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(CONFIG);
			auxIt->setMethod(SET_DIV2);		
		}
	;

offset_set:

	{ 		std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;  
			
			auxIt->setTargetOffset();

	}

	;

operators:
	LADD {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setRelOper($1);	
		}
	| LSUB {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setRelOper($1);	
		}
	| LAND {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setRelOper($1);	
		}
	;

per_shift:

	//const_period_shift AST MEM_EXP2

	  MEM_EXP2 AST const_period_shift

	//| const_period_shift AST MEM_EXP2

	| MEM_EXP2 { 
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			auxIt->aux_tree2.shift_type = CONST;
			auxIt->aux_tree2.period_shift = 1; } 


	;

/*ports:
	POR {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setPort($1);	
		}
	;  */

reg_const:
	BIT {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);				
			auxIt->setOperTwoType(CONST);	
		}
	| NUM {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);				
			auxIt->setOperTwoType(CONST);	
		}
	| HEX {

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;

		auxIt->setOperTwo(strtol($1,NULL,16));				
		auxIt->setOperTwoType(CONST);
	}
	;

reg_units:
	REG {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);				
			auxIt->setOperOneType(REGISTER);	
		}
	/*| RALU {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);				
			auxIt->setOperOneType(REGISTER);	
		}
	| RALUL {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);				
			auxIt->setOperOneType(REGISTER);	
		}
	| RMULT {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperOne($1);				
			auxIt->setOperOneType(REGISTER);	
		}

	| STR {

			searchVarName($1);
			
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			auxIt--;

			varStruct aux = returnVar($1);
 			
			
			
			auxIt->setOperOne(aux.fuToReg());
			auxIt->setOperOneType(REGISTER);	
		} */

	;
reg_units2:
	REG {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);				
			auxIt->setOperTwoType(REGISTER);	
		}
	/*| RALU {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);				
			auxIt->setOperTwoType(REGISTER);	
		}
	| RALUL {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);				
			auxIt->setOperTwoType(REGISTER);	
		}
	| RMULT {

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setOperTwo($1);				
			auxIt->setOperTwoType(REGISTER);	
		}

	| STR {

			searchVarName($1);
			//cout << "cricas\n";
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			auxIt--;

			varStruct aux = returnVar($1);
 			
			//cout << "cricas 2 = " << aux.fuToReg() << '\n';
			
			auxIt->setOperOne(aux.fuToReg());
			auxIt->setOperOneType(REGISTER);	
		} */


	;




reg_target:
	REG {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(EXPRESSION);
			auxIt->setTargetConditional($1);
			auxIt->setTargetConditionalType(REGISTER);
			
		}
	| RALU {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(EXPRESSION);
			auxIt->setTargetConditional($1);
			auxIt->setTargetConditionalType(REGISTER);
			mem_iter_per.allocRsc(returnUnitReg($1)); //used to allocate resources
	
		}
	| RALUL {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(EXPRESSION);
			auxIt->setTargetConditional($1);
			auxIt->setTargetConditionalType(REGISTER);
			mem_iter_per.allocRsc(returnUnitReg($1)); //used to allocate resources	
		}
	| RMULT {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(EXPRESSION);
			auxIt->setTargetConditional($1);
			auxIt->setTargetConditionalType(REGISTER);
			mem_iter_per.allocRsc(returnUnitReg($1)); //used to allocate resources	
		}

	| RBS {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(EXPRESSION);
			auxIt->setTargetConditional($1);
			auxIt->setTargetConditionalType(REGISTER);	
		}

	| MEMP SQU_BRAO config_offset SQU_BRAC {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(EXPRESSION);
			auxIt->setTargetConditional($1);
			auxIt->setTargetConditionalType(_MEMORY_TYPE);	
		}
	/*| STR { 
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			int aux;
			searchVarName($1);
			aux = searchVar($1);
			auxIt->setCmdType(EXPRESSION);
			auxIt->setTargetConditional($1);
			auxIt->setTargetConditionalType(REGISTER);  }  */
   
	;

returnList:
	RTN BIT {	std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(RETURN);
		}
	| RTN NUM {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(RETURN);
		}


	;

set_for_type:
	{
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;

		auxIt->setCmdType(_FOR);	
	}

;

/*set_rel_oper:
		{
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			
			auxIt->setRelOper(">>");	
		}
	;

set_rel_oper2:
		{

			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setRelOper("<<");	
		}
	; */

sif_operators:
	LEQU LEQU {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			string aux = (string)$1+(string)$2;

			auxIt->setCtrlOperator(aux);
		}
	| EXC LEQU {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			string aux = (string)$1+(string)$2;

			auxIt->setCtrlOperator(aux);
		}
	| SML{
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			string aux = (string)$1;

			auxIt->setCtrlOperator(aux);
		}
	| BIG {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			string aux = (string)$1;

			auxIt->setCtrlOperator(aux);
		}
	| BIGEQ {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			string aux = (string)$1;

			auxIt->setCtrlOperator(aux);
		}
	| SMLEQ{
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			string aux = (string)$1;

			auxIt->setCtrlOperator(aux);
		}
	;

vars:

	STR { $$ = $1; }


	;

/*var_methods:

	GET_ALU PHO UNIT PHC { $$ = var_controller.getNext($3); }



	;  */


var_units:

	STR {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			int aux;
			searchVarName($1);
			aux = searchVar($1);
			auxIt->setUnit(aux);
			
	}

	;

fr_units:
	FOUR{

		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;

		auxIt->setCmdType(_FOR);	
	}
	;

sif_units:
	SIF {
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(CONDITIONAL);	
		}
	;


end_condition:
	{	
		COMMAND *aux = new COMMAND();

		aux->resetCmd();
		program->push_back(*aux);	
	}
	;
end_fr_statment:
	{
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;
			
			auxIt->setCmdType(END_FOR);		
	}
	;

end_if_statment:
	{
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(END_CONDITIONAL);		
	}
	;

end_whl_statment:
	{	
		std::list<COMMAND>::iterator auxIt;
		auxIt = program->end();
		--auxIt;	

		auxIt->setCmdType(END_WHILE);
	}
	;


ends:	
	EXPEND {	
		COMMAND *aux = new COMMAND();
		
		aux->resetCmd();
		program->push_back(*aux);
		
	}
	;

whl_units:
	WHL{
			std::list<COMMAND>::iterator auxIt;
			auxIt = program->end();
			--auxIt;

			auxIt->setCmdType(_WHILE);	
	}
	;
%%
int main(int, char**argv) {

	COMMAND *auxCmd;
	std::list<COMMAND> program;
	std::list<COMMAND>::iterator prgIt;
	std::list<COMMAND>::iterator prgIt2;
	std::list<COMMAND>::iterator prgIt3;
	std::list<COMMAND>::iterator prgIt4;
	std::list<int> globalAuxList;
	std::list<int> globalAuxWhileList;
	std::list<int> globalAuxForList;
	std::list<int> globalDoWhileList;
	std::list<for_cycle> globalForStack;
	int global_ifs=0;
	int global_whiles=0;
	int global_fors=0;
	int global_dos=0;
	int global_waits=0;
	//int *aux_alu;
	//int *aux_mult;



	//FILE *myfile = fopen("in.compiler", "r");
	FILE *myfile = fopen(argv[1], "r");

	if (!myfile) {
		cout << "I can't open " << argv[1] << endl;
		return -1;
	}

	yyin = myfile;

	// Initialization of the program
	auxCmd = new COMMAND();
	auxCmd->resetCmd();


	program.push_back(*auxCmd);
	yyparse(&program);
	program.pop_back();

	

	for(prgIt = program.begin();prgIt != program.end();prgIt++) {
		prgIt->sendGlobalWhile(global_whiles);		
		prgIt->sendGlobalIf(global_ifs);
		prgIt->sendGlobalFor(global_fors);
		prgIt->sendList(globalAuxList);
		prgIt->sendWhileList(globalAuxWhileList);
		prgIt->sendDoList(globalDoWhileList);
		prgIt->sendForList(globalAuxForList);
		prgIt->setForAuxList(globalForStack);
		prgIt->sendGlobalDo(global_dos);
		prgIt->setNumberWaits(global_waits);
		//prgIt ->printData();
		prgIt2 = prgIt;//+1;
		prgIt3 = prgIt;
		prgIt3--;
		prgIt2++;
		//printAlloc();
		
		if(prgIt->showCmdType()==_ELSE) {
			if(prgIt3->showCmdType()!=END_CONDITIONAL) {
				cout << "Else statement without an if associated\n";
				exit(-1); }     		}
		if(prgIt->showCmdType()==END_CONDITIONAL && prgIt2!= program.end()) {
			if(prgIt2->showCmdType() == _ELSE) {
				prgIt->elseExists();        }           }
		prgIt->generateAssembly();  
		//printAlloc();
		prgIt->resetLargestNode();
		globalAuxList = prgIt->returnList();
		global_ifs = prgIt->returnGlobalIf();
		global_whiles = prgIt->returnGlobalWhile();
		global_fors = prgIt->returnGlobalFor();
		global_dos = prgIt->returnGlobalDo();
		globalAuxWhileList = prgIt->returnWhileList();	
		globalAuxForList = prgIt->returnForList();	
		globalForStack = prgIt->returnForAuxList();
		globalDoWhileList = prgIt->returnDoWhileList();	
		global_waits = prgIt->returnNumberWaits();	
		//if(prgIt->mem_tree!= NULL)	prgIt->mem_tree->printNode("", 0);	
	} 

		//printVarStruct();
	

}

string returnString(string s) {

	stringstream ss(s);
	stringstream convert;
	string a, b, c, d, e, f = "aaaaa";
	string test2;	
	
	ss >> a >> b >> c >> d >> e >> f;
	if(d!="NUM" && d!="BIT" && d!="NUM," && d!="BIT,") {
		if(f=="aaaaa") d = (string)yylval.testing+".";				
		else d = (string)yylval.testing+","; }
	
	else {
		convert << yylval.testing2;
		if(f=="aaaaa") d = convert.str()+".";	
		else d = convert.str()+",";
		    }


	if(f=="aaaaa") f = "\0";				
		
	if(f == "EXPEND") f = ";";
	if(f=="LEQU") f = "=";
	if(f=="REG") f = "register";
	if(f=="PHO") f = "(";
	if(f=="PHC") f = ")";
	if(f=="APH") f = "'";
	if(f=="LADD") f = "+";
	if(f=="LSUB") f = "-";
	if(f=="LAND") f = "&";
	if(f=="LNAND") f = "~&";
	if(f=="SOP") f = "a method";
	if(f=="LS8") f = "sign extend";
	if(f=="LS16") f = "SEXT16";						
	if(f=="SPU") f = "SMPU";			
	if(f=="SPS") f = "SMPS";		
	if(f=="LMX") f = "MAX";			
	if(f=="LMN") f = "MIN";
	if(f=="POR") f = "port";
	if(f=="CPO") f = "FU connect";
	if(f=="MEM") f = "mem";
	if(f=="MUL") f = "mult";
	if(f=="ALUL") f = "aluLite";
	if(f=="BS") f = "barrel shifter";	
	if(f=="RALUL") f = "aluLite register";
	if(f=="RALU") f = "alu register";
	if(f=="SIF") f = "if statment";
	if(f=="WHL") f = "while statment";
	if(f=="FOUR") f = "for statment";
	if(f=="GTO") f = "goto";
	if(f=="CLEAR_FLAG") f = "clearflag";
	
	
	

	test2 = a+' '+b+' '+c+' '+d+' '+e+' '+f;
	return test2;

}

void yyerror(std::list<COMMAND> *program, const char *s) {
	string aux = (string)s;
	aux = returnString(aux);
	cout << "Parse error on line " << line_num << "!  Message: " << aux << endl;
	exit(-1);
}










