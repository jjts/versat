#include <iostream>
#include <stdio.h>
#include "genAssembly.hpp"
#include <sstream>
#include <stdlib.h>
#include <math.h> //retreat if gives problems in prof's PC


int DMAdirection = 0;
int label_number = 0;

void DMA::verifyHexNum() {

	if(this->hex_value.length()!=10) {
		 cout << "Hex number " << this->hex_value << " not allowed! Versat has 32 bits\n";
		exit(0); }

}

void DMA::setHexValue(string a) {

	this->hex_value = a; }


void DMA::extAddr(int operOne, int operOneType, int operTwo, int operTwoType, int operatorExp) {

	string line;
	string asmInstruction;
	string asmIncr;
	string configAddr; 
	controller expGenerator;
	ostringstream convert;   // stream used for the conversion
	convert << operOne;

		if(operOneType == _DMA_HEX) { this->verifyHexNum();
					      cout << _ldih + ' ' + this->hex_value.substr(0,6) +_nextLine; 
					      cout << '\t'+_ldi + ' ' + "0x" + this->hex_value.substr(6,4) +_nextLine; }

		else {
			expGenerator.genExpressionAsm(0, operOne, operTwo, operOneType, operTwoType, operatorExp, 0, 1,"", 0, 0, 0); }
	

	line = asmInstruction+' '+asmIncr+_nextLine;
	cout << line;
	cout << '\t'+_wrw + ' ' + _extDMA+_nextLine;
	//cout << operOne << ' ' << operTwo <<' ' <<  operOneType  <<' ' <<  operTwoType << '\n';
}

void DMA::intAddr(int operOne, int operOneType, int operTwo, int operTwoType, int operatorExp) {

	string line;
	string asmInstruction;
	string asmIncr;
	string configAddr; 
	controller expGenerator;
	ostringstream convert;   // stream used for the conversion
	convert << operOne;

	if(operOneType == _DMA_HEX) { this->verifyHexNum();
					      cout << _ldih + ' ' + this->hex_value.substr(0,6) +_nextLine; 
					      cout << '\t'+_ldi + ' ' + "0x" + this->hex_value.substr(6,4) +_nextLine; }

		else {
			expGenerator.genExpressionAsm(0, operOne, operTwo, operOneType, operTwoType, operatorExp, 0, 1,"", 0, 0, 0); }
	

	line = asmInstruction+' '+asmIncr+_nextLine;
	cout << line;
	cout << '\t'+_wrw + ' ' + _intDMA+_nextLine;
	//cout << operOne << ' ' << operTwo <<' ' <<  operOneType  <<' ' <<  operTwoType << '\n';
}


void DMA::sizeAddr(int operOne) {

	cout << _ldi << ' ' << operOne << _nextLine;

	
	cout << '\t'+_wrw + ' ' + _sizeDMA+_nextLine;
	//cout << operOne << ' ' << operTwo <<' ' <<  operOneType  <<' ' <<  operTwoType << '\n';
}

void DMA::directionAddr(int operOne) {

	if(operOne<1 || operOne >2) {
		cout << "Direction should be 1 or 2.\n"; exit(-1); }

	//cout << _ldi << ' ' << operOne << _nextLine;

	DMAdirection = operOne;

	cout << "\n";
	//cout << '\t'+_wrw + ' ' + _directionDMA+_nextLine;
	//cout << operOne << ' ' << operTwo <<' ' <<  operOneType  <<' ' <<  operTwoType << '\n';
}

void DMA::run() {

	if(DMAdirection == 0) {
		cout << "Is necessary a direction.\n"; exit(-1); }	

	cout << "\n\t" << _ldi << ' ' << DMAdirection << _nextLine;

	
	cout << '\t'+_wrw + ' ' + _directionDMA+_nextLine;
	DMAdirection = 0;
	
	//cout << operOne << ' ' << operTwo <<' ' <<  operOneType  <<' ' <<  operTwoType << '\n';
}


void DMA::wait() {

	cout << '\n';
	cout << _wdma << label_number << ' ';
	cout << _ldi << ' ' << "0x0001" << _nextLine;
	cout << '\t' << _and << ' ' << _statusDMA << _nextLine;
	cout << '\t' << "bneqi" << ' ' << _wdma << label_number << _nextLine;
	cout << '\t' << _nop << _nextLine;
	label_number++;
	
	
}


void DMA::configs(string a, string b, int c, int d)  {





}  















