%{
#include <iostream>
#include <list>
#include "command.hpp"
#include "compiler.tab.h"
#include "program.h"
using namespace std;

#define YY_DECL extern "C" int yylex()

int line_num = 1;


%}

%x comment
%x comment2
%x comment3

%%	

"asm {"    { BEGIN(comment3);  }
	 <comment3>[^}\n]*     { yylval.assembly2 = strdup(yytext); return ASM; }
         <comment3>\n   line_num++;        
         <comment3>"}"  {    BEGIN(INITIAL); }



"/*"        { BEGIN(comment);  }
     
         <comment>[^*\n]*    
         <comment>"*"+[^*/\n]*  
         <comment>\n   line_num++;        
         <comment>"*"+"/"  {    BEGIN(INITIAL); }

"//"        { BEGIN(comment2);  }
     
         <comment2>[^\n]*      
        
         <comment2>\n    { line_num++; BEGIN(INITIAL);       }




[ \t\r\f]		;
[a-b]			{ yylval.testing = strdup(yytext);yylval.ablc = strdup(yytext); return ABLC; }
[A-B]			{ yylval.testing = strdup(yytext);yylval.abuc = strdup(yytext); return ABUC; } 

[0-1]			{ yylval.testing2 = atoi(yytext);yylval.bit = atoi(yytext); return BIT; }			
[0-9]*			{ yylval.testing2 = atoi(yytext);yylval.num = atoi(yytext); return NUM; }	
	

"*"			{ yylval.testing = strdup(yytext);yylval.ast = strdup(yytext); return AST; }
"*'"			{ yylval.testing = strdup(yytext);yylval.ast_pli = strdup(yytext); return AST_PLI; }
int			{ yylval.testing = strdup(yytext);yylval.inr = strdup(yytext); return INR; }
main			{ yylval.testing = strdup(yytext);yylval.man = strdup(yytext); return MAN; }
void			{ yylval.testing = strdup(yytext);yylval.vod = strdup(yytext); return VOD; }
char			{ yylval.testing = strdup(yytext);yylval.chr = strdup(yytext); return CHR; }
argc			{ yylval.testing = strdup(yytext);yylval.arc = strdup(yytext); return ARC; }
argv			{ yylval.testing = strdup(yytext);yylval.arv = strdup(yytext); return ARV; }
return			{ yylval.testing = strdup(yytext);yylval.rtn = strdup(yytext); return RTN; }


new			{ yylval.testing = strdup(yytext);yylval.neww = strdup(yytext); return NEW; }

";"		  	{ yylval.testing = strdup(yytext);yylval.expend = strdup(yytext); return EXPEND; }
","			{ yylval.testing = strdup(yytext);yylval.cmm = strdup(yytext); return CMM; }
"."			{ yylval.testing = strdup(yytext);yylval.dot = strdup(yytext); return DOT; }
"("			{ yylval.testing = strdup(yytext);yylval.pho = strdup(yytext); return PHO; }
")"			{ yylval.testing = strdup(yytext);yylval.phc = strdup(yytext); return PHC; }
"{"			{ yylval.testing = strdup(yytext);yylval.cro = strdup(yytext); return CRO; }
"}"			{ yylval.testing = strdup(yytext);yylval.crc = strdup(yytext); return CRC; }
"'"			{ yylval.testing = strdup(yytext);yylval.aph = strdup(yytext); return APH; }

"="			{ yylval.testing = strdup(yytext);yylval.lequ = strdup(yytext); return LEQU; }
"+"			{ yylval.testing = strdup(yytext);yylval.ladd = strdup(yytext); return LADD; }
"-"			{ yylval.testing = strdup(yytext);yylval.lsub = strdup(yytext); return LSUB; }
"/"			{ yylval.testing = strdup(yytext);yylval.ldiv = strdup(yytext); return LDIV; }
"|"			{ yylval.testing = strdup(yytext);yylval.lor = strdup(yytext); return LOR; }
"&"			{ yylval.testing = strdup(yytext);yylval.land = strdup(yytext); return LAND; }
"~&"			{ yylval.testing = strdup(yytext);yylval.lnand = strdup(yytext); return LNAND; }
"^"			{ yylval.testing = strdup(yytext);yylval.lxor = strdup(yytext); return LXOR; }
":"			{ yylval.testing = strdup(yytext);yylval.twoPoints = strdup(yytext); return TWO_POINTS; }

SEXT8			{ yylval.testing = strdup(yytext);yylval.ls8 = strdup(yytext); return LS8; }
SEXT16			{ yylval.testing = strdup(yytext);yylval.ls16 = strdup(yytext); return LS16; }
SRA			{ yylval.testing = strdup(yytext);yylval.sra = strdup(yytext); return SRA; }
SRL			{ yylval.testing = strdup(yytext);yylval.srl = strdup(yytext); return SRL; }
SMPU			{ yylval.testing = strdup(yytext);yylval.spu = strdup(yytext); return SPU; }
SMPS			{ yylval.testing = strdup(yytext);yylval.sps = strdup(yytext); return SPS; }
CLZ			{ yylval.testing = strdup(yytext);yylval.clz = strdup(yytext); return CLZ; }
MAX			{ yylval.testing = strdup(yytext);yylval.lmx = strdup(yytext); return LMX; }
MIN			{ yylval.testing = strdup(yytext);yylval.lmn = strdup(yytext); return LMN; }
ABS			{ yylval.testing = strdup(yytext);yylval.abs = strdup(yytext); return ABS; }

if			{ yylval.testing = strdup(yytext);yylval.sif = strdup(yytext); return SIF; }
while			{ yylval.testing = strdup(yytext);yylval.whl = strdup(yytext); return WHL; }
for			{ yylval.testing = strdup(yytext);yylval.four = strdup(yytext); return FOUR; }
goto			{ yylval.testing = strdup(yytext);yylval.gto = strdup(yytext); return GTO; }
gotoif			{ yylval.testing = strdup(yytext);yylval.gti = strdup(yytext); return GTI; }
do			{ yylval.testing = strdup(yytext);yylval.gti = strdup(yytext); return DOO; }
clearConfig		{ yylval.testing = strdup(yytext);yylval.clear_flag = strdup(yytext); return CLEAR_FLAG; }
"!"			{ yylval.testing = strdup(yytext);yylval.exc = strdup(yytext); return EXC; }
"<"			{ yylval.testing = strdup(yytext);yylval.sml = strdup(yytext); return SML; }
"<="			{ yylval.testing = strdup(yytext);yylval.smleq = strdup(yytext); return SMLEQ; }
">"			{ yylval.testing = strdup(yytext);yylval.big = strdup(yytext); return BIG; }
">="			{ yylval.testing = strdup(yytext);yylval.bigeq = strdup(yytext); return BIGEQ; }
">>"			{ yylval.testing = strdup(yytext);yylval.shiftr = strdup(yytext); return LSHIFTR; }
"<<"			{ yylval.testing = strdup(yytext);yylval.shiftl = strdup(yytext); return LSHIFTL; }
else			{ yylval.testing = strdup(yytext);yylval.lelse = strdup(yytext); return ELSE; }
node			{ yylval.testing = strdup(yytext);yylval.lvar = strdup(yytext); return VAR; }
getNext			{ yylval.testing = strdup(yytext);yylval.galu = strdup(yytext); return GET_ALU; }
getNextMult		{ yylval.testing = strdup(yytext);yylval.gmult = strdup(yytext); return GET_MULT; }

init			{ yylval.testing = strdup(yytext);yylval.ini = strdup(yytext); return INI; }
run			{ yylval.testing = strdup(yytext);yylval.run_exp = strdup(yytext); return RUN_EXP; }
EXP			{ yylval.testing = strdup(yytext);yylval.exp = strdup(yytext); return EXP; }
wait			{ yylval.testing = strdup(yytext);yylval.wit = strdup(yytext); return WIT; }
setOper			{ yylval.testing = strdup(yytext);yylval.sop = strdup(yytext); return SOP; }
setLonhi		{ yylval.testing = strdup(yytext);yylval.slo = strdup(yytext); return SLO; }
setDiv2			{ yylval.testing = strdup(yytext);yylval.sdi = strdup(yytext); return SDI; }
setStart 		{ yylval.testing = strdup(yytext);yylval.sst = strdup(yytext); return SST; }
setDuty			{ yylval.testing = strdup(yytext);yylval.sdu = strdup(yytext); return SDU; }
setIncr			{ yylval.testing = strdup(yytext);yylval.sin = strdup(yytext); return SIN; }	
setIter			{ yylval.testing = strdup(yytext);yylval.sit = strdup(yytext); return SIT; }
setPer			{ yylval.testing = strdup(yytext);yylval.spr = strdup(yytext); return SPR; }
setShift		{ yylval.testing = strdup(yytext);yylval.ssh = strdup(yytext); return SSH; }
setDelay 		{ yylval.testing = strdup(yytext);yylval.sde = strdup(yytext); return SDE; }
setReverse 		{ yylval.testing = strdup(yytext);yylval.srv = strdup(yytext); return SRV; }
setLNR			{ yylval.testing = strdup(yytext);yylval.sbslnr = strdup(yytext); return SBSLNR; }
setLNA			{ yylval.testing = strdup(yytext);yylval.sbslna = strdup(yytext); return SBSLNA; }
connect			{ yylval.testing = strdup(yytext);yylval.cnn = strdup(yytext); return CNN; }
de			{ yylval.testing = strdup(yytext);yylval.dcr = strdup(yytext); return DCR; }
saveConfig	        { yylval.testing = strdup(yytext);yylval.stc = strdup(yytext); return STC; }
loadConfig              { yylval.testing = strdup(yytext);yylval.ldc = strdup(yytext); return LDC; }

port[A-B]		{ yylval.testing = strdup(yytext);yylval.por = strdup(yytext); return POR; }
connectPort[A-B]	{ yylval.testing = strdup(yytext);yylval.cpo = strdup(yytext); return CPO; }
%disableFU		{ yylval.testing = strdup(yytext);yylval.cpo = strdup(yytext); return DIS; }
disableAutoDuty		{ yylval.testing = strdup(yytext);yylval.ead = strdup(yytext); return ENB_AD; }

MEM[0-3]		{ yylval.testing = strdup(yytext);yylval.mem_cte = strdup(yytext); return MEM_CTE; }
mem[0-3]		{ yylval.testing = strdup(yytext);yylval.mem = strdup(yytext); return MEM; }
mem[0-3][AB]		{ yylval.testing = strdup(yytext);yylval.memp = strdup(yytext); return MEMP; }
mult[0-3]		{ yylval.testing = strdup(yytext);yylval.mul = strdup(yytext); return MUL; } 
alu[0-1]		{ yylval.testing = strdup(yytext);yylval.alu = strdup(yytext); return ALU; } 
aluLite[0-3]		{ yylval.testing = strdup(yytext);yylval.alul = strdup(yytext); return ALUL; }
bs[0]			{ yylval.testing = strdup(yytext);yylval.bs = strdup(yytext); return BS; }
Ralu[0-1]		{ yylval.testing = strdup(yytext);yylval.ralu = strdup(yytext); return RALU; }
RaluLite[0-3]		{ yylval.testing = strdup(yytext);yylval.ralul = strdup(yytext); return RALUL; }
Rmult[0-3]		{ yylval.testing = strdup(yytext);yylval.rmult = strdup(yytext); return RMULT; }
Rbs0			{ yylval.testing = strdup(yytext);yylval.rbs = strdup(yytext); return RBS; }
R[0-9]			{ yylval.testing = strdup(yytext);yylval.reg = strdup(yytext); return REG; }
R[1][0-5]		{ yylval.testing = strdup(yytext);yylval.reg = strdup(yytext); return REG; }
DMA_EXT_ADDR	 	{ yylval.testing = strdup(yytext);yylval.reg_dma_ext = strdup(yytext); return REG_DMA_EXT; }
DMA_INT_ADDR	 	{ yylval.testing = strdup(yytext);yylval.reg_dma_int = strdup(yytext); return REG_DMA_INT; }

alu			{ yylval.testing = strdup(yytext);yylval.unit = strdup(yytext); return UNIT; }
mult			{ yylval.testing = strdup(yytext);yylval.unit = strdup(yytext); return UNIT; }
delete 			{ yylval.testing = strdup(yytext);yylval.lfree = strdup(yytext); return FREE; }


dma			{ yylval.testing = strdup(yytext);yylval.dma_eng = strdup(yytext); return DMA; }
setExtAddr		{ yylval.testing = strdup(yytext);yylval.dma_ext = strdup(yytext); return DMA_EXT; }
setIntAddr		{ yylval.testing = strdup(yytext);yylval.dma_int = strdup(yytext); return DMA_INT; }
setSize			{ yylval.testing = strdup(yytext);yylval.dma_bus = strdup(yytext); return DMA_BUS; }
setDirection		{ yylval.testing = strdup(yytext);yylval.dma_dir = strdup(yytext); return DMA_DIR; }
config			{ yylval.testing = strdup(yytext);yylval.dma_config = strdup(yytext); return DMAA_CONFIG; }
"i"			{ yylval.testing = strdup(yytext);yylval.mem_exp = strdup(yytext); return MEM_EXP; }
"j"			{ yylval.testing = strdup(yytext);yylval.mem_exp2 = strdup(yytext); return MEM_EXP2; }
"["			{ yylval.testing = strdup(yytext);yylval.squ_brao = strdup(yytext); return SQU_BRAO; }
"]"			{ yylval.testing = strdup(yytext);yylval.squ_brac = strdup(yytext); return SQU_BRAC; }

%mem[0-3][a-bA-B]"["[a-bA-b0-9]+"]"	{ yylval.testing = strdup(yytext);yylval.memt2 = strdup(yytext); return MEMT2; }
0x[0-9a-fA-f]+		{ yylval.testing = strdup(yytext);yylval.hex = strdup(yytext); return HEX; }
[a-zA-Z0-9ç_]*		{ yylval.testing = strdup(yytext);yylval.str = strdup(yytext); return STR; }
\n			{ ++line_num; }
.			;
%%
