#include <iostream>
#include <stdio.h>
#include <sstream>
#include "genAssembly.hpp"

/*************************************************************************/
//Calculates Bs PortB connection Assembly
//
//
/*************************************************************************/

void GenMem::genMemConnectAsm (int unit, int selUnit, int unitPort) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	string portUnit;
	

	asmInstruction = _ldi;
	if(selUnit == salu0) asmConnection = _salu0;
	if(selUnit == salu1) asmConnection = _salu1;
	if(selUnit == salulite0) asmConnection = _salulite0;
	if(selUnit == salulite1) asmConnection = _salulite1;
	if(selUnit == salulite2) asmConnection = _salulite2;
	if(selUnit == salulite3) asmConnection = _salulite3;
	if(selUnit == smul0) asmConnection = _smul0;
	if(selUnit == smul1) asmConnection = _smul1;
	if(selUnit == smul2) asmConnection = _smul2;
	if(selUnit == smul3) asmConnection = _smul3;
	if(selUnit == sbs0) asmConnection = _sbs0;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == MEM0 && unitPort == PORT_A) configAddr = _mem0A_config_addr;
	if(unit == MEM0 && unitPort == PORT_B) configAddr = _mem0B_config_addr;
	if(unit == MEM1 && unitPort == PORT_A) configAddr = _mem1A_config_addr;
	if(unit == MEM1 && unitPort == PORT_B) configAddr = _mem1B_config_addr;
	if(unit == MEM2 && unitPort == PORT_A) configAddr = _mem2A_config_addr;
	if(unit == MEM2 && unitPort == PORT_B) configAddr = _mem2B_config_addr;
	if(unit == MEM3 && unitPort == PORT_A) configAddr = _mem3A_config_addr;
	if(unit == MEM3 && unitPort == PORT_B) configAddr = _mem3B_config_addr;
	
	if(unitPort == PORT_A) portUnit = _mem_conf_sela_offset;
	if(unitPort == PORT_B) portUnit = _mem_conf_sela_offset;
	line = '\t'+asmInstruction+' '+configAddr+','+portUnit+_nextLine;
	cout << line;	

}

/*************************************************************************/
//Calculates Bs PortB connection to memories Assembly
//
//
/*************************************************************************/


void GenMem::genMemConnectAsm (int unit, int selUnit, int unitPort, int selPort) {

	string line;
	string asmInstruction;
	string asmConnection;
	string configAddr; 
	string portUnit;
	

	asmInstruction = _ldi;
	if(selUnit == MEM0 && selPort == PORT_A) asmConnection = _smem0A;
	if(selUnit == MEM0 && selPort == PORT_B) asmConnection = _smem0B;
	if(selUnit == MEM1 && selPort == PORT_A) asmConnection = _smem1A;
	if(selUnit == MEM1 && selPort == PORT_B) asmConnection = _smem1B;
	if(selUnit == MEM2 && selPort == PORT_A) asmConnection = _smem2A;
	if(selUnit == MEM2 && selPort == PORT_B) asmConnection = _smem2B;
	if(selUnit == MEM3 && selPort == PORT_A) asmConnection = _smem3A;
	if(selUnit == MEM3 && selPort == PORT_B) asmConnection = _smem3B;
	line = asmInstruction+' '+asmConnection+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == MEM0 && unitPort == PORT_A) configAddr = _mem0A_config_addr;
	if(unit == MEM0 && unitPort == PORT_B) configAddr = _mem0B_config_addr;
	if(unit == MEM1 && unitPort == PORT_A) configAddr = _mem1A_config_addr;
	if(unit == MEM1 && unitPort == PORT_B) configAddr = _mem1B_config_addr;
	if(unit == MEM2 && unitPort == PORT_A) configAddr = _mem2A_config_addr;
	if(unit == MEM2 && unitPort == PORT_B) configAddr = _mem2B_config_addr;
	if(unit == MEM3 && unitPort == PORT_A) configAddr = _mem3A_config_addr;
	if(unit == MEM3 && unitPort == PORT_B) configAddr = _mem3B_config_addr;
	
	if(unitPort == PORT_A) portUnit = _mem_conf_sela_offset;
	if(unitPort == PORT_B) portUnit = _mem_conf_selb_offset;
	line = '\t'+asmInstruction+' '+configAddr+','+portUnit+_nextLine;
	cout << line;	

}

/*************************************************************************/
//Set address generator start
//
//
/*************************************************************************/

void GenMem::genMemStartAsm (int unit, int unitPort, int start, int operType, int operTwo, int operTwoType, int operatorExp) {

	string line;
	string asmInstruction;
	string asmStart;
	string configAddr; 
	controller expGenerator;
	ostringstream convert;   // stream used for the conversion
	convert << start;
	
	if(operTwoType==0) {
		if(operType==CONST) asmInstruction = _ldi;
		if(operType==REGISTER) asmInstruction = _rdw;
		if(operType == CONST && start >=0) asmStart = convert.str();
		if(operType==REGISTER) asmStart = RegToString(start); }
	else {
		expGenerator.genExpressionAsm(0, start, operTwo, operType, operTwoType, operatorExp, 0, 1,"", 0, 0, 0);
	}
	line = asmInstruction+' '+asmStart+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == MEM0 && unitPort == PORT_A) configAddr = _mem0A_config_addr;
	if(unit == MEM0 && unitPort == PORT_B) configAddr = _mem0B_config_addr;
	if(unit == MEM1 && unitPort == PORT_A) configAddr = _mem1A_config_addr;
	if(unit == MEM1 && unitPort == PORT_B) configAddr = _mem1B_config_addr;
	if(unit == MEM2 && unitPort == PORT_A) configAddr = _mem2A_config_addr;
	if(unit == MEM2 && unitPort == PORT_B) configAddr = _mem2B_config_addr;
	if(unit == MEM3 && unitPort == PORT_A) configAddr = _mem3A_config_addr;
	if(unit == MEM3 && unitPort == PORT_B) configAddr = _mem3B_config_addr;	
	
	line = '\t'+asmInstruction+' '+configAddr+','+_mem_conf_start_offset+_nextLine;
	cout << line;	


}

/*************************************************************************/
//Set address generator duty
//
//
/*************************************************************************/

void GenMem::genMemDutyAsm (int unit, int unitPort, int duty, int operType, int operTwo, int operTwoType, int operatorExp) {

	string line;
	string asmInstruction;
	int addi_decr=0;
	string asmDuty;
	controller expGenerator;
	string configAddr; 
	ostringstream convert;   // stream used for the conversion

	if(operType==CONST) duty = duty-1;
	else {
		if(operTwoType==CONST) operTwo = operTwo-1;   
		else addi_decr = 1; }

	convert << duty;
	
	if(operTwoType==0) {
		if(operType==CONST) asmInstruction = _ldi;
		if(operType==REGISTER) asmInstruction = _rdw;
		if(operType == CONST && duty >=0) asmDuty = convert.str();
		if(operType==REGISTER) asmDuty = RegToString(duty); }
	else {
		expGenerator.genExpressionAsm(0, duty, operTwo, operType, operTwoType, operatorExp, 0, 1,"", 0, 0, 0);
	}
	line = asmInstruction+' '+asmDuty+_nextLine;
	cout << line;
	
	if(addi_decr==1) cout << '\t' + _addi + ' ' << -1 << _nextLine; 

	asmInstruction = _wrc;
	if(unit == MEM0 && unitPort == PORT_A) configAddr = _mem0A_config_addr;
	if(unit == MEM0 && unitPort == PORT_B) configAddr = _mem0B_config_addr;
	if(unit == MEM1 && unitPort == PORT_A) configAddr = _mem1A_config_addr;
	if(unit == MEM1 && unitPort == PORT_B) configAddr = _mem1B_config_addr;
	if(unit == MEM2 && unitPort == PORT_A) configAddr = _mem2A_config_addr;
	if(unit == MEM2 && unitPort == PORT_B) configAddr = _mem2B_config_addr;
	if(unit == MEM3 && unitPort == PORT_A) configAddr = _mem3A_config_addr;
	if(unit == MEM3 && unitPort == PORT_B) configAddr = _mem3B_config_addr;	
	
	line = '\t'+asmInstruction+' '+configAddr+','+_mem_conf_duty_offset+_nextLine;
	cout << line;	


}


/*************************************************************************/
//Set address generator per
//
//
/*************************************************************************/

void GenMem::genMemPerAsm (int unit, int unitPort, int per, int operType, int operTwo, int operTwoType, int operatorExp) {

	string line;
	int addi_decr=0;
	string asmInstruction;
	string asmPer;
	string configAddr; 
	controller expGenerator;
	ostringstream convert;   // stream used for the conversion

	if(operType==CONST) per = per-1;
	else {
		if(operTwoType==CONST) operTwo = operTwo-1;   
		else addi_decr = 1; }

	convert << per;
	
	if(operTwoType==0) {
		if(operType==CONST) asmInstruction = _ldi;
		if(operType==REGISTER) asmInstruction = _rdw;
		if(operType == CONST && per >=0) asmPer = convert.str();
		if(operType==REGISTER) asmPer = RegToString(per); }
	else {
		expGenerator.genExpressionAsm(0, per, operTwo, operType, operTwoType, operatorExp, 0, 1,"", 0, 0, 0);
	}


	



	line = asmInstruction+' '+asmPer+_nextLine;
	cout << line;
	if(operType == REGISTER && per >= _Ralu0 && per <= _RaluLite3) {
				cout << '\t';
				cout << line; 
				}
	if(addi_decr==1) cout << '\t' + _addi + ' ' << -1 << _nextLine; 
	asmInstruction = _wrc;
	if(unit == MEM0 && unitPort == PORT_A) configAddr = _mem0A_config_addr;
	if(unit == MEM0 && unitPort == PORT_B) configAddr = _mem0B_config_addr;
	if(unit == MEM1 && unitPort == PORT_A) configAddr = _mem1A_config_addr;
	if(unit == MEM1 && unitPort == PORT_B) configAddr = _mem1B_config_addr;
	if(unit == MEM2 && unitPort == PORT_A) configAddr = _mem2A_config_addr;
	if(unit == MEM2 && unitPort == PORT_B) configAddr = _mem2B_config_addr;
	if(unit == MEM3 && unitPort == PORT_A) configAddr = _mem3A_config_addr;
	if(unit == MEM3 && unitPort == PORT_B) configAddr = _mem3B_config_addr;	
	
	line = '\t'+asmInstruction+' '+configAddr+','+_mem_conf_per_offset+_nextLine;
	cout << line;	


}


/*************************************************************************/
//Set address generator delay
//
//
/*************************************************************************/

void GenMem::genMemDelayAsm (int unit, int unitPort, int delay, int operType, int operTwo, int operTwoType, int operatorExp) {

	string line;
	string asmInstruction;
	string asmDelay;
	string configAddr; 
	controller expGenerator;
	ostringstream convert;   // stream used for the conversion
	convert << delay;
	
	if(operTwoType==0) {
		if(operType==CONST) asmInstruction = _ldi;
		if(operType==REGISTER) asmInstruction = _rdw;
		if(operType == CONST && delay >=0) asmDelay = convert.str();
		if(operType==REGISTER) asmDelay = RegToString(delay); }
	else {
		expGenerator.genExpressionAsm(0, delay, operTwo, operType, operTwoType, operatorExp, 0, 1,"", 0, 0, 0);
	}
	line = asmInstruction+' '+asmDelay+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == MEM0 && unitPort == PORT_A) configAddr = _mem0A_config_addr;
	if(unit == MEM0 && unitPort == PORT_B) configAddr = _mem0B_config_addr;
	if(unit == MEM1 && unitPort == PORT_A) configAddr = _mem1A_config_addr;
	if(unit == MEM1 && unitPort == PORT_B) configAddr = _mem1B_config_addr;
	if(unit == MEM2 && unitPort == PORT_A) configAddr = _mem2A_config_addr;
	if(unit == MEM2 && unitPort == PORT_B) configAddr = _mem2B_config_addr;
	if(unit == MEM3 && unitPort == PORT_A) configAddr = _mem3A_config_addr;
	if(unit == MEM3 && unitPort == PORT_B) configAddr = _mem3B_config_addr;	
	
	line = '\t'+asmInstruction+' '+configAddr+','+_mem_conf_delay_offset+_nextLine;
	cout << line;	


}

/*************************************************************************/
//Set address generator reverse
//
//
/*************************************************************************/

void GenMem::genMemReverseAsm (int unit, int unitPort, int reverse, int operType, int operTwo, int operTwoType, int operatorExp) {

	string line;
	string asmInstruction;
	controller expGenerator;
	string asmDelay;
	string configAddr; 
	ostringstream convert;   // stream used for the conversion
	convert << reverse;
	
	if(operTwoType==0) {
		if(operType==CONST) asmInstruction = _ldi;
		if(operType==REGISTER) asmInstruction = _rdw;
		if(operType == CONST && reverse >=0 && reverse <=1) asmDelay = convert.str();
		if(operType==REGISTER) asmDelay = RegToString(reverse); }
	else {
		expGenerator.genExpressionAsm(0, reverse, operTwo, operType, operTwoType, operatorExp, 0, 1,"", 0, 0, 0);
	}
	line = asmInstruction+' '+asmDelay+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == MEM0 && unitPort == PORT_A) configAddr = _mem0A_config_addr;
	if(unit == MEM0 && unitPort == PORT_B) configAddr = _mem0B_config_addr;
	if(unit == MEM1 && unitPort == PORT_A) configAddr = _mem1A_config_addr;
	if(unit == MEM1 && unitPort == PORT_B) configAddr = _mem1B_config_addr;
	if(unit == MEM2 && unitPort == PORT_A) configAddr = _mem2A_config_addr;
	if(unit == MEM2 && unitPort == PORT_B) configAddr = _mem2B_config_addr;
	if(unit == MEM3 && unitPort == PORT_A) configAddr = _mem3A_config_addr;
	if(unit == MEM3 && unitPort == PORT_B) configAddr = _mem3B_config_addr;	
	
	line = '\t'+asmInstruction+' '+configAddr+','+_mem_conf_reverse_offset+_nextLine;
	cout << line;	


}


/*************************************************************************/
//Set address generator incr
//
//
/*************************************************************************/

void GenMem::genMemIncrAsm (int unit, int unitPort, int incr, int operType, int operTwo, int operTwoType, int operatorExp) {

	string line;
	string asmInstruction;
	string asmIncr;
	string configAddr; 
	controller expGenerator;
	ostringstream convert;   // stream used for the conversion
	convert << incr;
	
	if(operTwoType==0) {
		if(operType==CONST) asmInstruction = _ldi;
		if(operType==REGISTER) asmInstruction = _rdw;
		if(operType == CONST && incr >=0) asmIncr = convert.str();
		if(operType==REGISTER) asmIncr = RegToString(incr); }
	else {
		expGenerator.genExpressionAsm(0, incr, operTwo, operType, operTwoType, operatorExp, 0, 1,"", 0, 0, 0);
	}
	line = asmInstruction+' '+asmIncr+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == MEM0 && unitPort == PORT_A) configAddr = _mem0A_config_addr;
	if(unit == MEM0 && unitPort == PORT_B) configAddr = _mem0B_config_addr;
	if(unit == MEM1 && unitPort == PORT_A) configAddr = _mem1A_config_addr;
	if(unit == MEM1 && unitPort == PORT_B) configAddr = _mem1B_config_addr;
	if(unit == MEM2 && unitPort == PORT_A) configAddr = _mem2A_config_addr;
	if(unit == MEM2 && unitPort == PORT_B) configAddr = _mem2B_config_addr;
	if(unit == MEM3 && unitPort == PORT_A) configAddr = _mem3A_config_addr;
	if(unit == MEM3 && unitPort == PORT_B) configAddr = _mem3B_config_addr;	
	
	line = '\t'+asmInstruction+' '+configAddr+','+_mem_conf_incr_offset+_nextLine;
	cout << line;	


}


/*************************************************************************/
//Set address generator iter
//
//
/*************************************************************************/

void GenMem::genMemIterAsm (int unit, int unitPort, int iter, int operType, int operTwo, int operTwoType, int operatorExp) {

	string line;
	string asmInstruction;
	int addi_decr=0;
	string asmIter;
	string configAddr; 
	controller expGenerator;
	ostringstream convert;   // stream used for the conversion

	if(operType==CONST) iter = iter-1;
	else {
		if(operTwoType==CONST) operTwo = operTwo-1;   
		else addi_decr = 1; }

	convert << iter;
	
	if(operTwoType==0) {
		if(operType==CONST) asmInstruction = _ldi;
		if(operType==REGISTER) asmInstruction = _rdw;
		if(operType == CONST && iter >=0) asmIter = convert.str();
		if(operType==REGISTER) asmIter = RegToString(iter); }
	else {
		expGenerator.genExpressionAsm(0, iter, operTwo, operType, operTwoType, operatorExp, 0, 1,"", 0, 0, 0);
	}
	line = asmInstruction+' '+asmIter+_nextLine;
	cout << line;
	if(addi_decr==1) cout << '\t' + _addi + ' ' << -1 << _nextLine; 
	asmInstruction = _wrc;
	if(unit == MEM0 && unitPort == PORT_A) configAddr = _mem0A_config_addr;
	if(unit == MEM0 && unitPort == PORT_B) configAddr = _mem0B_config_addr;
	if(unit == MEM1 && unitPort == PORT_A) configAddr = _mem1A_config_addr;
	if(unit == MEM1 && unitPort == PORT_B) configAddr = _mem1B_config_addr;
	if(unit == MEM2 && unitPort == PORT_A) configAddr = _mem2A_config_addr;
	if(unit == MEM2 && unitPort == PORT_B) configAddr = _mem2B_config_addr;
	if(unit == MEM3 && unitPort == PORT_A) configAddr = _mem3A_config_addr;
	if(unit == MEM3 && unitPort == PORT_B) configAddr = _mem3B_config_addr;	
	
	line = '\t'+asmInstruction+' '+configAddr+','+_mem_conf_iter_offset+_nextLine;
	cout << line;	


}


/*************************************************************************/
//Set address generator shift
//
//
/*************************************************************************/

void GenMem::genMemShiftAsm (int unit, int unitPort, int shift, int operType, int operTwo, int operTwoType, int operatorExp) {

	string line;
	string asmInstruction;
	string asmShift;
	string configAddr; 
	controller expGenerator;
	ostringstream convert;   // stream used for the conversion
	convert << shift;
	
	if(operTwoType==0) {
		if(operType==CONST) asmInstruction = _ldi;
		if(operType==REGISTER) asmInstruction = _rdw;
		if(operType == CONST && shift >=0) asmShift = convert.str();
		if(operType==REGISTER) asmShift = RegToString(shift); }
	else {
		expGenerator.genExpressionAsm(0, shift, operTwo, operType, operTwoType, operatorExp, 0, 1,"", 0, 0, 0);
	}
	line = asmInstruction+' '+asmShift+_nextLine;
	cout << line;
	asmInstruction = _wrc;
	if(unit == MEM0 && unitPort == PORT_A) configAddr = _mem0A_config_addr;
	if(unit == MEM0 && unitPort == PORT_B) configAddr = _mem0B_config_addr;
	if(unit == MEM1 && unitPort == PORT_A) configAddr = _mem1A_config_addr;
	if(unit == MEM1 && unitPort == PORT_B) configAddr = _mem1B_config_addr;
	if(unit == MEM2 && unitPort == PORT_A) configAddr = _mem2A_config_addr;
	if(unit == MEM2 && unitPort == PORT_B) configAddr = _mem2B_config_addr;
	if(unit == MEM3 && unitPort == PORT_A) configAddr = _mem3A_config_addr;
	if(unit == MEM3 && unitPort == PORT_B) configAddr = _mem3B_config_addr;	
	
	line = '\t'+asmInstruction+' '+configAddr+','+_mem_conf_shift_offset+_nextLine;
	cout << line;	


}











