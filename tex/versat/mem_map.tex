\section{Memory Map}
\label{sec:mem_map}

In this section a detailed memory map of the Versat system is
presented as seen by its internal and external interfaces.

The internal interface, aka RW bus, allows Versat programmers to
access the different parts of the system by using the addresses given
in this section.

The external interfaces are the control interface, aka PARallel
interface, and the data interface, aka DMA interface. The PAR
interface is used by an external host to control the execution of
Versat. The DMA interface is used by an external host to supply or get
massive amounts of data from Versat.

The base addresses are given in Table~\ref{tab:memmap}. Other adresses
that use these base addresses are given in the following subsections.

\begin{table}[!htbp]
  \centering
    \begin{tabular}{|p{4cm}|p{2cm}|p{2cm}|p{5cm}|}
    \hline 
    {\bf Mnemonic} & {\bf Address} & {\bf Seen by} & {\bf Description} \\
    \hline \hline 
      CTRL\_REGF\_BASE & 0 & RW/PAR & Control register file base\\
    \hline
     RB & 63 & RW & Register B \\
    \hline
     PROG\_MEM\_BASE &  2048 & RW/DMA & Program memory  base \\
    \hline
     CONF\_MEM\_BASE &  4096 & RW/DMA &  Configuration memory base\\
    \hline
     ENG\_BASE & 6144 & RW/DMA & Data engine base \\
    \hline
 
    \end{tabular}
  \caption{Memory map base addresses}
  \label{tab:memmap}
\end{table}


\subsection{Control Register File Addresses}

The address of the control register file register Ri, where i=0, ..,
15, as seen by the RW and PARallel buses, is given by

ADDR(Ri) = CTRL\_REGF\_BASE + i

This address range is Read/Write.

\subsection{Program Memory Addresses}

The program memory address range, as seen by the RW and DMA buses, is given by

PROG\_MEM\_BASE + i, for i = 0, ..., 2047

This address range is Write Only.


\subsection{Configuration Addresses}

\subsubsection{Configuration Memory Addresses}

The configuration memory address range, as seen by the RW and DMA buses, is given by

CONF\_MEM\_BASE + i, for i = 0, ..., 63

Using the RW bus, this address range is Read/Write where the read destination and the
write source is the Configuration Register.

Using the DMA bus, this address range is Read/Write.


\subsubsection{Configuration Register Addresses}
\label{sec:conf_reg_addrs}

The addresses of the configuration register spaces are given in
Table~\ref{tab:confspaces}. Recall that there is one configuration
space for each FU. The first address in Table~\ref{tab:confspaces},
CLEAR\_CONFIG\_ADDR, is used to clear all configuration spaces of all
FUs.

\begin{table}[!htbp]
  \centering
    \begin{tabular}{|p{5cm}|p{2cm}|}
    \hline 
    {\bf Mnemonic} & {\bf Address}  \\
    \hline \hline 
      CLEAR\_CONFIG\_ADDR & 5119 \\
    \hline \hline 
      MEM0A\_CONFIG\_ADDR & 5120 \\
    \hline
      MEM0B\_CONFIG\_ADDR & 5129 \\
    \hline
      MEM1A\_CONFIG\_ADDR & 5138 \\
    \hline
      MEM1B\_CONFIG\_ADDR & 5147 \\
    \hline
      MEM2A\_CONFIG\_ADDR & 5156 \\
    \hline
      MEM2B\_CONFIG\_ADDR & 5165 \\
    \hline
      MEM3A\_CONFIG\_ADDR & 5174 \\
    \hline
      MEM3B\_CONFIG\_ADDR & 5183 \\
    \hline
      ALU0\_CONFIG\_ADDR & 5192 \\
    \hline
      ALU1\_CONFIG\_ADDR & 5195 \\
    \hline
      ALU\_LITE0\_CONFIG\_ADDR & 5198 \\
    \hline
      ALU\_LITE1\_CONFIG\_ADDR & 5201 \\
    \hline
      ALU\_LITE2\_CONFIG\_ADDR & 5204 \\
    \hline
      ALU\_LITE3\_CONFIG\_ADDR & 5207 \\
    \hline
      MULT0\_CONFIG\_ADDR & 5210 \\
    \hline
      MULT1\_CONFIG\_ADDR & 5214 \\
    \hline
      MULT2\_CONFIG\_ADDR & 5218 \\
    \hline
      MULT3\_CONFIG\_ADDR & 5222 \\
    \hline
      BS0\_CONFIG\_ADDR & 5226 \\ 
    \hline

    \end{tabular}
  \caption{Configuration register addresses}
  \label{tab:confspaces}
\end{table}

To access a certain configuration field in a configuration space it is
necessary to add the field offset to the configuration space
address. The configuration field offsets are given from
Table~\ref{tab:memoffsets} through Table~\ref{tab:bsoffsets}.


\begin{table}[!htbp]
  \centering
    \begin{tabular}{|p{6cm}|p{2cm}|}
    \hline 
    {\bf Mnemonic} & {\bf Offset} \\
    \hline \hline 
      MEM\_CONF\_ITER\_OFFSET & 0 \\
    \hline
      MEM\_CONF\_PER\_OFFSET & 1 \\
    \hline
      MEM\_CONF\_DUTY\_OFFSET & 2 \\
    \hline
      MEM\_CONF\_SELA\_OFFSET & 3 \\
    \hline
      MEM\_CONF\_START\_OFFSET & 4 \\
    \hline
     MEM\_CONF\_SHIFT\_OFFSET & 5 \\
    \hline
      MEM\_CONF\_INCR\_OFFSET & 6 \\
    \hline
      MEM\_CONF\_DELAY\_OFFSET & 7 \\
    \hline
      MEM\_CONF\_RVRS\_OFFSET & 8 \\
    \hline
    \end{tabular}
  \caption{Memory configuration offsets}
  \label{tab:memoffsets}
\end{table}

\begin{table}[!htbp]
  \centering
    \begin{tabular}{|p{6cm}|p{2cm}|}
    \hline 
    {\bf Mnemonic} & {\bf Offset}  \\
    \hline \hline 
      ALU\_CONF\_SELA\_OFFSET & 0 \\
    \hline
      ALU\_CONF\_SELB\_OFFSET & 1 \\
    \hline
      ALU\_CONF\_FNS\_OFFSET & 2 \\
    \hline
    \end{tabular}
  \caption{ALU and ALU Lite configuration offsets}
  \label{tab:aluoffsets}
\end{table}

\begin{table}[!htbp]
  \centering
    \begin{tabular}{|p{6cm}|p{2cm}|}
    \hline 
    {\bf Mnemonic} & {\bf Offset}  \\
    \hline \hline 
     MUL\_CONF\_SELA\_OFFSET & 0 \\
    \hline
     MUL\_CONF\_SELB\_OFFSET & 1  \\
    \hline 
     MUL\_CONF\_LONHI\_OFFSET & 2  \\
    \hline
     MUL\_CONF\_DIV2\_OFFSET & 3  \\
    \hline
    \end{tabular}
  \caption{Multiplier configuration offsets}
  \label{tab:multoffsets}
\end{table}

\begin{table}[!htbp]
  \centering
    \begin{tabular}{|p{6cm}|p{2cm}|}
    \hline 
    {\bf Mnemonic} & {\bf Offset}  \\
    \hline \hline 
     BS\_CONF\_SELA\_OFFSET & 0  \\
    \hline
     BS\_CONF\_SELB\_OFFSET & 1  \\
    \hline 
     BS\_CONF\_LNA\_OFFSET & 2  \\
    \hline
     BS\_CONF\_LNR\_OFFSET & 3  \\
    \hline
    \end{tabular}
  \caption{Barrel shifter configuration offsets}
  \label{tab:bsoffsets}
\end{table}

For example to access the SELA field of port B of MEM1,
one should use the address

MEM1B\_CONFIG\_ADDR + MEM\_CONF\_SELA\_OFFSET

This address range is Write Only.

\subsection{Data Engine Addresses}

The Data Engine address offset are given in
Table~\ref{tab:deoffsets}. It is possible to access the Data Engine
control and status register (ENG\_CTRL\_REG and ENG\_STATUS\_REG), the
outputs of the FUs from position ENG\_FU\_BASE, and the contents of
the Data Engine memories from position ENG\_MEM\_BASE.

\begin{table}[!htbp]
  \centering
    \begin{tabular}{|p{4cm}|p{2cm}|p{2cm}|p{2.8cm}|}
    \hline 
    {\bf Mnemonic} & {\bf Offset} & {\bf Mode} & {\bf Read Latency}\\
    \hline \hline 
     ENG\_CTRL\_REG & 0  & Write & NA\\
    \hline
     ENG\_STATUS\_REG & 1 & Read/Write & 2\\
    \hline 
     ENG\_FU\_BASE & 32 & Read/Write & 3 \\
    \hline
     ENG\_MEM\_BASE & 2048 & Read/Write & 3 \\
    \hline
    \end{tabular}
  \caption{Data Engine address offsets}
  \label{tab:deoffsets}
\end{table}


To access the output of the FUs one should use the address given by

ENG\_BASE + ENG\_FU\_BASE + FU\_offset

where the FU\_offset is given in Table~\ref{tab:fuoffset}.


\begin{table}[htbp]
  \centering
    \begin{tabular}{|p{1.8cm}|p{1.8cm}|}
    \hline 
    {\bf FU} & {\bf Offset} \\
    \hline \hline 
     mem0A & 0 \\
    \hline
     mem0B & 1 \\
    \hline
     mem1A & 2 \\
    \hline
     mem1B & 3 \\
    \hline
     mem2A & 4 \\
    \hline
     mem2B & 5 \\
    \hline
     mem3A & 6 \\
    \hline
     mem3B & 7 \\
    \hline
     alu0 & 8 \\
    \hline
     alu1 & 9 \\
    \hline
     alulite0 & 10 \\
    \hline
     alulite1 & 11 \\
    \hline
     alulite2 & 12 \\
    \hline
     alulite3 & 13 \\
    \hline
     mul0 & 14 \\
    \hline
     mul1 & 15 \\
    \hline
     mul2 & 16 \\
    \hline
     mul3 & 17 \\
    \hline
     bs0 & 18 \\
    \hline

    \end{tabular}
  \caption{FU offsets}
  \label{tab:fuoffset}
\end{table}

To access a certain memory position one should use the address given by 

ENG\_BASE + ENG\_MEM\_BASE + MEM\_offset + (address in selected memory)

where MEM\_offset is given in Table~\ref{tab:memoffset}.

\begin{table}[htbp]
  \centering
    \begin{tabular}{|p{1.8cm}|p{1.8cm}|}
    \hline 
    {\bf FU} & {\bf Offset} \\
    \hline \hline 
     mem0 & 0 \\
    \hline
     mem1 & 2048 \\
    \hline
     mem2 & 4096 \\
    \hline
     mem3 & 6144 \\
    \hline

    \end{tabular}
  \caption{Memory offsets}
  \label{tab:memoffset}
\end{table}
