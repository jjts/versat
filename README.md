Versat is a computing engine whose architecture can be described as a
Coarse Grain Array (CGA) or Soft Vector Processor (SVP). Versat can be
included as an Intellectual Property (IP) core in more complex designs
that require a co-processor for intense data processing.

Versat is composed by a controller and a configurable data engine. The
main function of the controller is to configure and run the data
engine multiple times before the result is produced. The controller
can be programmed using Versat's machine instructions. The controller
executes pre-processing, post-processing and glue-processing in
between data engine runs.

Versat has a control interface, normally connected to a host
processor, and a data interface to load or return data, normally
connected to a DMA engine.  The control interface reads and writes a
Control Register File (CRF), which can also be read and written by the
Versat controller.

To run a procedure on Versat the host processor writes the input data
using the data interface and issues a command using the control
interface. The command is the address of the procedure in the Versat
program memory and a few arguments that are written in the CRF. The
procedure arguments are normally constants or pointers to data
arrays. Versat notices this command, executes it, and notifies the
host via the control interface. The host then prooceeds to save the
output data using the data interface. For the host, the advantage of
running a procedure on Versat is to achieve higher performance or
lower energy consumption.

The Data Engine is where high performance and low energy computation
takes place. Computation is carried out on data arrays (vectors)
stored in multiple internal vector RAMs. The conventional computer
equivalent of the vector RAM array is the register file used to hold
scalar quantities. The vector RAMs are dual-port and each port is
equipped with an Address Generation Unit (AGU). The AGU is used to
read or write vector elements in a programmed address sequence. The
Data Engine has a number of runtime configurable Functional Units
(FUs) to process the data in a pipeline fashion. In essence, the data
is streamed out of the vector RAMs, go through a processing pipeline,
and the result vectors are streamed back into the vector RAMs. The
configuration of each FU, including vector memories, specifies the
mode of operation of the FU and its input data. Each unit input may
select the output of any other unit as its drive.

The instruction set of the controller is minimal since most of the
computation happens in the Data Engine. The instructions are chiefly
used for control flow control and reconfiguration of the data engine.
