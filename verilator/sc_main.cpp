// -*- SystemC -*-
// DESCRIPTION: Verilator Example: Top level main for invoking SystemC model
//
// Copyright 2003-2015 by Wilson Snyder. This program is free software; you can
// redistribute it and/or modify it under the terms of either the GNU
// Lesser General Public License Version 3 or the Perl Artistic License
// Version 2.0.
//====================================================================

#include <cstdio>
#include <iostream>
#include <sys/times.h>
#include <sys/stat.h>
#include <assert.h>
#include <fstream>

#include "systemc.h"		// SystemC global header
#include "verilated_vcd_sc.h"	// Tracing

#include "Vxtop.h"		// Top level header, generated from verilog

#define CLK_PER 10
#define CLK_OFFSET 3
#define TP 2

void readfile (const char *filename, unsigned int *code_v, unsigned int size);
void writefile (unsigned int *code0_v, 
		unsigned int *code1_v, 
		unsigned int *code2_v, 
		unsigned int *code3_v, 
		unsigned int size);


using namespace std;

//Versat definitions
#include "versat.h"
#define MEMSZ (1<<ADDR_W)
#define MEM0ADDR (ENG_MEM_BASE)
#define MEM1ADDR (MEM0ADDR+MEMSZ)
#define MEM2ADDR (MEM1ADDR+MEMSZ)
#define MEM3ADDR (MEM2ADDR+MEMSZ)


Vxtop *top;

int sc_main(int argc, char* argv[]) {
  using namespace std;

  Verilated::commandArgs(argc, argv);
  Verilated::randReset(2);
  Verilated::debug(0);	// We compiled with it on for testing, turn it back off

  // General logfile
  ios::sync_with_stdio();

  // Defaults
#if (SYSTEMC_VERSION>20011000)
#else
  sc_time dut(1.0, sc_ns);
  sc_set_default_time_unit(dut);
#endif

  //==========
  // Define the Clocks

  cout << "Defining Clocks\n";
#if (SYSTEMC_VERSION>=20070314)
  sc_clock clk     ("clk",    CLK_PER,SC_NS, 0.5, CLK_OFFSET,SC_NS, true);
#else
  sc_clock clk     ("clk",    10, 0.5, 3, true);
#endif

  cout << "Defining Interconnect\n";
  sc_signal<bool> rst;
  sc_signal<bool> par_we;
  sc_signal<vluint32_t> par_out;
  sc_signal<vluint32_t> par_in;
  sc_signal<vluint32_t> par_addr;
  sc_signal<bool>	  dma_req;
  sc_signal<bool>	  dma_rnw;
  sc_signal<vluint32_t> dma_addr;
  sc_signal<vluint32_t> dma_data_in;
  sc_signal<vluint32_t> dma_data_out;


  //==========
  // Part under test

  Vxtop* top = new Vxtop("top");
  top->clk		(clk);
  top->rst	        (rst);
  top->par_in	        (par_in);
  top->par_out	        (par_out);
  top->par_addr         (par_addr);
  top->par_we	        (par_we);
  top->dma_req          (dma_req);
  top->dma_rnw          (dma_rnw);
  top->dma_addr         (dma_addr);
  top->dma_data_in      (dma_data_in);
  top->dma_data_out     (dma_data_out);

    
  //==========
  //  Waves

#if VM_TRACE
  // Before any evaluation, need to know to calculate those signals only used for tracing
  Verilated::traceEverOn(true);
#endif

  // You must do one evaluation before enabling waves, in order to allow
  // SystemC to interconnect everything for testing.
  cout <<("Test initialization...\n");
  rst = 1;
#if (SYSTEMC_VERSION>=20070314)
  sc_start(1,SC_NS);
#else
  sc_start(1);
#endif

  //==========
  //  Waves

#if VM_TRACE
  cout << "Enabling waves...\n";
  VerilatedVcdSc* tfp = new VerilatedVcdSc;
  top->trace (tfp, 99);
  tfp->open ("vlt_dump.vcd");
#endif

  //=========
  // Read files
  unsigned int opcode_v[MEMSZ];           
  unsigned int mem0_v[MEMSZ];           
  unsigned int mem1_v[MEMSZ];           
  unsigned int mem2_v[MEMSZ];           
  unsigned int mem3_v[MEMSZ];
  
  readfile("opcode.hex", opcode_v, (unsigned int)MEMSZ);
  readfile("xmem0data.hex", mem0_v, (unsigned int)MEMSZ);
  readfile("xmem1data.hex", mem1_v, (unsigned int)MEMSZ);
  readfile("xmem2data.hex", mem2_v, (unsigned int)MEMSZ);
  readfile("xmem3data.hex", mem3_v, (unsigned int)MEMSZ);

  //==========
  // Start of Test
  int stime = CLK_OFFSET+TP;
  int pcnt = 0;
  int progdone = -1;

  cout <<("Test beginning...\n");

  rst = 0;
  dma_addr = PROG_MEM_BASE+PROG_START-1;
    
  while (!Verilated::gotFinish()) {

    if (VL_TIME_Q() > stime) {
    
      if( pcnt == 0 ){// Assert reset
	rst = 1;	
	pcnt++;
      }

      else if( pcnt == 1 ){// Deassert reset
	rst = 0;
	pcnt++;
      }

      else if ( pcnt >= 2 && pcnt < (2+MEMSZ) ) { //Write prog mem
	dma_req = 1;
	dma_rnw = 0;
	dma_data_in = opcode_v[pcnt-2];
	dma_addr = dma_addr+1;
	pcnt ++;
      }

      else if ( pcnt == (2+MEMSZ) ) {//Restart dma address
	dma_addr = MEM0ADDR-1;
	dma_req = 0;
	pcnt++;
      }

      else if ( pcnt >= (3+MEMSZ) && pcnt < (3+2*MEMSZ) ) { //Write mem0
	dma_req = 1;
	dma_rnw = 0;
	dma_data_in = mem0_v[pcnt - (3+MEMSZ)];
	dma_addr = dma_addr+1;
	pcnt ++;
      }

      else if ( pcnt == (3+2*MEMSZ) ) {//Restart dma address
	dma_addr = MEM1ADDR-1;
	dma_req = 0;
	pcnt++;
      }

      else if ( pcnt >= (4+2*MEMSZ) && pcnt < (4+3*MEMSZ) ) { //Write mem1
	dma_req = 1;
	dma_rnw = 0;
	dma_data_in = mem1_v[pcnt - (4+2*MEMSZ)];
	dma_addr = dma_addr+1;
	pcnt ++;
      }

      else if ( pcnt == (4+3*MEMSZ) ) {//Restart dma address
	dma_addr = MEM2ADDR-1;
	dma_req = 0;
	pcnt++;
      }

      else if ( pcnt >= (5+3*MEMSZ) && pcnt <  (5+4*MEMSZ)) { //Write mem2
	dma_req = 1;
	dma_rnw = 0;
	dma_data_in = mem2_v[pcnt - (5+3*MEMSZ)];
	dma_addr = dma_addr+1;
	pcnt ++;
      }

      else if ( pcnt ==   (5+4*MEMSZ) ) {//Restart dma address
	dma_addr = MEM3ADDR-1;
	dma_req = 0;
	pcnt++;
      }

       else if ( pcnt >= (6+4*MEMSZ) && pcnt <  (6+5*MEMSZ) ) { //Write mem3
	dma_req = 1;
	dma_rnw = 0;
	dma_data_in = mem3_v[pcnt - (6+4*MEMSZ)];
	dma_addr = dma_addr+1;
	pcnt ++;
      }

      else if ( pcnt == (6+5*MEMSZ) ) {//Write RUN command to R14
	dma_req = 0;
	dma_addr = MEM0ADDR-1;
	par_addr = 14;
	par_we = 1;
	par_in = BR_RUN;
	pcnt++;
      }
     
      else if ( pcnt == (7+5*MEMSZ) ) {//Write prog start addr to R0
	par_addr = 0;
	par_in = PROG_START;
	pcnt++;
      }
     
      else if ( pcnt > (7+5*MEMSZ) && progdone == -1) {//Wait for R0=0 (prog done)
	par_we = 0;
	if(par_out == 0)
	  progdone = pcnt+1;
	pcnt++;
      }
     
      else if ( pcnt >= progdone && pcnt < (progdone+MEMSZ) && progdone != -1 ) { //Read mem0
	dma_req = 1;
	dma_rnw = 1;
	dma_addr = dma_addr+1;
	if( ( pcnt - progdone ) >= 2 )
	  mem0_v[pcnt - progdone - 2] = dma_data_out;

	pcnt++;
      }

      else if ( pcnt >= (progdone+MEMSZ) && pcnt < (progdone+2+MEMSZ) && progdone != -1 ) {//read last two words due to latency
	dma_req = 0;
	dma_addr = MEM1ADDR-1;
	mem0_v[pcnt - progdone - 2] = dma_data_out;
	pcnt++;
      }
	
      else if ( pcnt >= (progdone+2+MEMSZ) && pcnt < (progdone+2+2*MEMSZ) && progdone != -1 ) { //Read mem1
	dma_req = 1;
	dma_rnw = 1;
	dma_addr = dma_addr+1;
	if( ( pcnt - (progdone+2+MEMSZ) ) >= 2 )
	  mem1_v[pcnt - (progdone+2+MEMSZ) - 2] = dma_data_out;

	pcnt++;
      }

      else if ( pcnt >=  (progdone+2+2*MEMSZ) && pcnt < (progdone+4+2*MEMSZ) && progdone != -1 ) {//read last two words due to latency
	dma_req = 0;
	dma_addr = MEM2ADDR-1;
	mem1_v[pcnt - (progdone+2+MEMSZ) - 2] = dma_data_out;
	pcnt++;
      }
	
      else if ( pcnt >= (progdone+4+2*MEMSZ) && pcnt < (progdone+4+3*MEMSZ) && progdone != -1 ) { //Read mem2
	dma_req = 1;
	dma_rnw = 1;
	dma_addr = dma_addr+1;
	if( ( pcnt - (progdone+4+2*MEMSZ)) >= 2 )
	  mem2_v[pcnt - (progdone+4+2*MEMSZ) - 2] = dma_data_out;

	pcnt++;
      }

      else if ( pcnt >= (progdone+4+3*MEMSZ) && pcnt < (progdone+6+3*MEMSZ) && progdone != -1 ) {//read last two words due to latency
	dma_req = 0;
	dma_addr = MEM3ADDR-1;
	mem2_v[pcnt - (progdone+4+2*MEMSZ)- 2] = dma_data_out;
	pcnt++;
      }
	
      else if ( pcnt >= (progdone+6+3*MEMSZ) && pcnt < (progdone+6+4*MEMSZ) && progdone != -1 ) { //Read mem3
	dma_req = 1;
	dma_rnw = 1;
	dma_addr = dma_addr+1;
	if( ( pcnt - (progdone+6+3*MEMSZ) ) >= 2 )
	  mem3_v[pcnt - (progdone+6+3*MEMSZ) - 2] = dma_data_out;

	pcnt++;
      }

      else if ( pcnt >=  (progdone+6+4*MEMSZ) && pcnt < (progdone+8+4*MEMSZ) && progdone != -1 ) {//read last two words due to latency
	dma_req = 0;
	mem3_v[pcnt - (progdone+6+3*MEMSZ) - 2] = dma_data_out;
	pcnt++;
      }
	
      else if ( pcnt ==  (progdone+8+4*MEMSZ) && progdone != -1) {     
	break;
      }
	
      stime += CLK_PER; //increment sample time by clock period 

    }
#if (SYSTEMC_VERSION>=20070314)
    sc_start(1,SC_NS);
#else
    sc_start(1);
#endif

  }//end of while loop


  // Write results file
  //

  //assert(mem3_v[MEMSZ-1]==0);
  writefile(mem0_v, mem1_v, mem2_v, mem3_v, (unsigned int)MEMSZ);
     



  top->final();

  //==========
  //  Close Waves

#if VM_TRACE
  if (tfp) tfp->flush();
  if (tfp) tfp->close();
#endif

  /*    if (!passed) {
	VL_PRINTF ("A Test failed!!\n");
	abort();
	}*/

  //==========
  //  Coverage analysis (since test passed)
  mkdir("logs", 0777);
#if VM_COVERAGE
  SpCoverage::write();  // Writes logs/coverage.pl
#endif

  //==========
  //  Close LogFiles

  cout << "*-* All Finished *-*\n";  // Magic if using perl's Log::Detect

  return(0);
}
