#include <cstdio>
#include <iostream>
#include <sys/times.h>
#include <sys/stat.h>
#include <sstream>
#include  <iomanip>
#include <fstream>

using namespace std;

void readfile(const char *filename, unsigned int *code_v, unsigned int size){
     int str_cntr = 0;
     string code_s[2048];        
     ifstream fin;     

     fin.open(filename,ios::in);  

     if(fin.fail()){
       cout << "File not found: " << filename << endl;
       exit(1);
     }
     
     while ( str_cntr < size ) 
     {
       fin >> code_s[str_cntr]; 
       stringstream(code_s[str_cntr]) >> std::hex >> code_v[str_cntr];

       /* uncomment for debug
       if(str_cntr < 20){
	 cout << code_s[str_cntr]<<" ";
	 cout << str_cntr<<" ";
	 cout << std::hex << code_v[str_cntr]<<endl; 
       }
       */
       str_cntr++;
     }
     fin.close( );  
}


void writefile(unsigned int *code0_v, 
	       unsigned int *code1_v, 
	       unsigned int *code2_v, 
	       unsigned int *code3_v, 
	       unsigned int size){

     int str_cntr = 0;
     ofstream fout;     // declare stream variable name

     fout.open("results.out",ios::out);    // open file

     if(fout.fail()){ //check failure
       cout << "File not open: results.out" << endl;
       exit(1);
     }
     

     //write code0
     while ( str_cntr < size ) 
     {
       fout << setfill('0') << setw(8) << std::hex << code0_v[str_cntr] << endl;
       str_cntr++;
     }

     //write code1
     fout << endl;
     str_cntr = 0;
     while ( str_cntr < size ) 
     {
       fout << setfill('0') << setw(8) << std::hex << code1_v[str_cntr] << endl;
       str_cntr++;
     }

     //write code2
     fout << endl;
     str_cntr = 0;
     while ( str_cntr < size ) 
     {
       fout << setfill('0') << setw(8) << std::hex << code2_v[str_cntr] << endl;
       str_cntr++;
     }

     //write code3
     fout << endl;
     str_cntr = 0;
     while ( str_cntr < size ) 
     {
       fout << setfill('0') << setw(8) << std::hex << code3_v[str_cntr] << endl;
       str_cntr++;
     }
     fout << endl;

     fout.close( );  
}
