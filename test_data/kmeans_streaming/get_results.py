#! /usr/bin/python
#
#    g e t _ r e s u l t s . p y
#
# Note: It's necessary run source command before run this script
#

import sys
import string
import os
import subprocess

# Add python_src directory to make possible import other python scripts/functions
sys.path.append("../../python_src/")

from datagen import genData

vsrc_path = "../../verilog_src/"
csrc_path = "../../xilinx/sdk/sdk_2016.2/ARM/k-means/"
sim_path = "../../asic_umc130nm/sim/"

centers = range(2, 70, 8)
dims = range(15, 35, 5)
points = range(4)

# Build results
def buildResults():
	fp_res = open("./results.txt","w")
	
	# Run results for centers
	fp_res.write("Centers graph\n")
	
	NDATAPTS = 10064
	DIM = 15
	for NCENTRS in centers:
		fp_temp = open("./temp.txt","w")
		
		# Run kmeans kernel on PC to get mem_in.hex and mem_out.hex files
		os_ret = os.system("./kmeans -NDATAPTS " + str(NDATAPTS) + " -NCENTRS " + str(NCENTRS) + " -DIM " + str(DIM))
		if (os_ret != 0):
			sys.exit("results: error trying to run k-means")
		
		# Run kmeans kernel on Versat
		os_ret = subprocess.call(["make","-C",sim_path,"xtop_axi","test=kmeans_streaming"],stdout=fp_temp)
		
		# Check if final results are correct
		os.system("head -n -15 ../../asic_umc130nm/sim/mem_out.hex > temp.hex")
		os_ret = os.system("diff ./temp.hex ./mem_out.hex")
		if(os_ret != 0):
			message = "There are errors!"
		else:
			message = ""
		
		# Get time result
		os.system("grep \'Total Clocks\' ./temp.txt > time.txt")
		fp_time = open("./time.txt","r")
		time = fp_time.readline().split()[2]
		
		# Write result
		fp_res.write("%d %d %d %s %s\n" % (NDATAPTS, NCENTRS, DIM, time, message))
		
		fp_temp.close()
		fp_time.close()
		
	
	# Run results for dimensions
	fp_res.write("\nDimensions graph\n")
	
	NCENTRS = 34
	for DIM in dims:
		fp_temp = open("./temp.txt","w")
		
		NPTSPERCHUNCK = 2048/DIM
		NDATAPTS = NPTSPERCHUNCK * (10000/NPTSPERCHUNCK + 1)
		
		# Run kmeans kernel on PC to get mem_in.hex and mem_out.hex files
		os_ret = os.system("./kmeans -NDATAPTS " + str(NDATAPTS) + " -NCENTRS " + str(NCENTRS) + " -DIM " + str(DIM))
		if (os_ret != 0):
			sys.exit("results: error trying to run k-means")
		
		# Run kmeans kernel on Versat
		os_ret = subprocess.call(["make","-C",sim_path,"xtop_axi","test=kmeans_streaming"],stdout=fp_temp)
		
		# Check if final results are correct
		os.system("head -n -15 ../../asic_umc130nm/sim/mem_out.hex > temp.hex")
		os_ret = os.system("diff ./temp.hex ./mem_out.hex")
		if(os_ret != 0):
			message = "There are errors!"
		else:
			message = ""
		
		# Get time result
		os.system("grep \'Total Clocks\' ./temp.txt > time.txt")
		fp_time = open("./time.txt","r")
		time = fp_time.readline().split()[2]
		
		# Write result
		fp_res.write("%d %d %d %s %s\n" % (NDATAPTS, NCENTRS, DIM, time, message))
		
		fp_temp.close()
		fp_time.close()
		
	
	# Run results for points
	fp_res.write("\nPoints graph\n")
	
	NCENTRS = 34
	DIM = 30
	for i in points:
		fp_temp = open("./temp.txt","w")
		
		NDATAPTS = 1360 * 10**i
		
		# Run kmeans kernel on PC to get mem_in.hex and mem_out.hex files
		os_ret = os.system("./kmeans -NDATAPTS " + str(NDATAPTS) + " -NCENTRS " + str(NCENTRS) + " -DIM " + str(DIM))
		if (os_ret != 0):
			sys.exit("results: error trying to run k-means")
		
		# Run kmeans kernel on Versat
		os_ret = subprocess.call(["make","-C",sim_path,"xtop_axi","test=kmeans_streaming"],stdout=fp_temp)
		
		# Check if final results are correct
		os.system("head -n -15 ../../asic_umc130nm/sim/mem_out.hex > temp.hex")
		os_ret = os.system("diff ./temp.hex ./mem_out.hex")
		if(os_ret != 0):
			message = "There are errors!"
		else:
			message = ""
		
		# Get time result
		os.system("grep \'Total Clocks\' ./temp.txt > time.txt")
		fp_time = open("./time.txt","r")
		time = fp_time.readline().split()[2]
		
		# Write result
		fp_res.write("%d %d %d %s %s\n" % (NDATAPTS, NCENTRS, DIM, time, message))
		
		fp_temp.close()
		fp_time.close()
		
	
	fp_res.close()
	

def main() :
	if((len(sys.argv) != 1) and (sys.argv[1] != "clean" or len(sys.argv) > 2)):
		sys.exit("Usage: ./get_results.py \tto build results\n       or\n       ./get_results.py clean\tto clean directories\n")
	
	# Clear "sim" directory
	subprocess.call(["make","-C",sim_path,"clean"])
	
	# Clean temporary files
	os.system("rm -f ./kmeans")
	os.system("rm -f ./data.hex")
	os.system("rm -f ./temp.hex")
	os.system("rm -f ./*.txt")
	
	# Clear "verilog_src" directory
	subprocess.call(["make","-C",vsrc_path,"clean"])
	
	# Checks if there is to build results or just clean directories
	if (len(sys.argv) == 1):
		# Build k-means
		os_ret = os.system("gcc -Wall -O3 -fopenmp -DUSE_PC " + csrc_path + "kmeans.c -o kmeans")
		if (os_ret != 0):
			sys.exit("results: error trying to build k-means")
		
		# Generate data.hex
		genData(((1000000+34)*30))
		
		# Run kernel and build results
		buildResults()
		
	

if __name__ == "__main__" : main()
