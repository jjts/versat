#clear conf_reg
	wrw CLEAR_CONFIG_ADDR
	ldi 1024
	wrw R10
	shft 1
	wrw R6
	ldi 1
	wrw R9
	wrw R8
	wrw R11
	ldi 0
	wrw R7
	wrw R5
	wrw R4
	wrw R3
	
#do reverse bits
#configure mem0 for storing result, real parts
	ldi smem2A
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	ldi smem2B
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	ldi 1024
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	ldi 1
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi 1023
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem1 for storing result, imaginary parts
	ldi smem3A
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	ldi 1
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi 1023
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem2 for reading a and b vectors, real parts
	ldi 1024
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	ldi 1
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_RVRS_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi 2
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi 1023
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem3 for reading a and b vectors, imaginary parts
	ldi 1
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_RVRS_OFFSET
	ldi 2
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi 1023
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#init and run engine (0x001DC003)
	ldi 0xC003
	ldih 0x1D
	wrw ENG_CTRL_REG
	
#clear conf_reg
	wrw CLEAR_CONFIG_ADDR
	
#complex product
#configure mem0 for reading b vector, real part
	ldi 6
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	ldi 1
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi salu0
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	
#configure mem1 for reading b vector, imaginary part
	ldi 6
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	ldi 1
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi salu1
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	
#configure mem2 for reading coeficients
	ldi 1024
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_START_OFFSET
	addi 1
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	
#setup mult0 for multiplying Re(b) by Re(w)
	ldi smem0A
	wrc MULT0_CONFIG_ADDR,MUL_CONF_SELA_OFFSET
	ldi smem2A
	wrc MULT0_CONFIG_ADDR,MUL_CONF_SELB_OFFSET
	
#setup mult1 for multiplying Im(b) by Im(w)
	ldi smem1A
	wrc MULT1_CONFIG_ADDR,MUL_CONF_SELA_OFFSET
	ldi smem2B
	wrc MULT1_CONFIG_ADDR,MUL_CONF_SELB_OFFSET
	
#setup mult2 for multiplying Re(b) by Im(w)
	ldi smem0A
	wrc MULT2_CONFIG_ADDR,MUL_CONF_SELA_OFFSET
	ldi smem2B
	wrc MULT2_CONFIG_ADDR,MUL_CONF_SELB_OFFSET
	
#setup mult3 for multiplying Im(b) by Re(w)
	ldi smem1A
	wrc MULT3_CONFIG_ADDR,MUL_CONF_SELA_OFFSET
	ldi smem2A
	wrc MULT3_CONFIG_ADDR,MUL_CONF_SELB_OFFSET
	
#configure alu0 for adding mult0 and mult1
	ldi ALU_SUB
	wrc ALU0_CONFIG_ADDR,ALU_CONF_FNS_OFFSET
	ldi smul1
	wrc ALU0_CONFIG_ADDR,ALU_CONF_SELA_OFFSET
	ldi smul0
	wrc ALU0_CONFIG_ADDR,ALU_CONF_SELB_OFFSET
	
#configure alu1 for adding mult2 and mult3
	ldi ALU_ADD
	wrc ALU1_CONFIG_ADDR,ALU_CONF_FNS_OFFSET
	ldi smul2
	wrc ALU1_CONFIG_ADDR,ALU_CONF_SELA_OFFSET
	ldi smul3
	wrc ALU1_CONFIG_ADDR,ALU_CONF_SELB_OFFSET
	
#write configuration
	wrw CONF_MEM_BASE,0
	
#clear conf_reg
	wrw CLEAR_CONFIG_ADDR
	
#complex sum
#configure mem0 for reading a and b vectors, real parts
	ldi 1
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	
#configure mem1 for reading a and b vectors, imaginary parts
	ldi 1
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	
#configure mem2 for storing result, real parts
	ldi salu0
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	ldi salulite0
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	ldi 3
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	ldi 1
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	
#configure mem3 for storing result, imaginary parts
	ldi salu1
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	ldi salulite1
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	ldi 3
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	ldi 1
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	
#configure alu0 for adding mem0A and mem0B
	ldi ALU_ADD
	wrc ALU0_CONFIG_ADDR,ALU_CONF_FNS_OFFSET
	ldi smem0A
	wrc ALU0_CONFIG_ADDR,ALU_CONF_SELA_OFFSET
	ldi smem0B
	wrc ALU0_CONFIG_ADDR,ALU_CONF_SELB_OFFSET
	
#configure ALU_LITE0 for adding mem0A and -mem0B
	ldi ALU_SUB
	wrc ALU_LITE0_CONFIG_ADDR,ALU_LITE_CONF_FNS_OFFSET
	ldi smem0B
	wrc ALU_LITE0_CONFIG_ADDR,ALU_LITE_CONF_SELA_OFFSET
	ldi smem0A
	wrc ALU_LITE0_CONFIG_ADDR,ALU_LITE_CONF_SELB_OFFSET
	
#configure alu1 for adding mem1A and mem1B
	ldi ALU_ADD
	wrc ALU1_CONFIG_ADDR,ALU_CONF_FNS_OFFSET
	ldi smem1A
	wrc ALU1_CONFIG_ADDR,ALU_CONF_SELA_OFFSET
	ldi smem1B
	wrc ALU1_CONFIG_ADDR,ALU_CONF_SELB_OFFSET
	
#configure ALU_LITE1 for adding mem1A and -mem1B
	ldi ALU_SUB
	wrc ALU_LITE1_CONFIG_ADDR,ALU_LITE_CONF_FNS_OFFSET
	ldi smem1B
	wrc ALU_LITE1_CONFIG_ADDR,ALU_LITE_CONF_SELA_OFFSET
	ldi smem1A
	wrc ALU_LITE1_CONFIG_ADDR,ALU_LITE_CONF_SELB_OFFSET
	
#write configuration
	wrw CONF_MEM_BASE,1
	
#clear conf_reg
	wrw CLEAR_CONFIG_ADDR
	
#complex product
#configure mem2 for reading b vector, real part
	ldi 6
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	ldi 1
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi salu0
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	
#configure mem3 for reading b vector, imaginary part
	ldi 6
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	ldi 1
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi salu1
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	
#configure mem0 for reading coeficients
	ldi 1024
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_START_OFFSET
	addi 1
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	
#setup mult0 for multiplying Re(b) by Re(w)
	ldi smem2A
	wrc MULT0_CONFIG_ADDR,MUL_CONF_SELA_OFFSET
	ldi smem0A
	wrc MULT0_CONFIG_ADDR,MUL_CONF_SELB_OFFSET
	
#setup mult1 for multiplying Im(b) by Im(w)
	ldi smem3A
	wrc MULT1_CONFIG_ADDR,MUL_CONF_SELA_OFFSET
	ldi smem0B
	wrc MULT1_CONFIG_ADDR,MUL_CONF_SELB_OFFSET
	
#setup mult2 for multiplying Re(b) by Im(w)
	ldi smem2A
	wrc MULT2_CONFIG_ADDR,MUL_CONF_SELA_OFFSET
	ldi smem0B
	wrc MULT2_CONFIG_ADDR,MUL_CONF_SELB_OFFSET
	
#setup mult3 for multiplying Im(b) by Re(w)
	ldi smem3A
	wrc MULT3_CONFIG_ADDR,MUL_CONF_SELA_OFFSET
	ldi smem0A
	wrc MULT3_CONFIG_ADDR,MUL_CONF_SELB_OFFSET
	
#configure alu0 for adding mult0 and mult1
	ldi ALU_SUB
	wrc ALU0_CONFIG_ADDR,ALU_CONF_FNS_OFFSET
	ldi smul1
	wrc ALU0_CONFIG_ADDR,ALU_CONF_SELA_OFFSET
	ldi smul0
	wrc ALU0_CONFIG_ADDR,ALU_CONF_SELB_OFFSET
	
#configure alu1 for adding mult2 and mult3
	ldi ALU_ADD
	wrc ALU1_CONFIG_ADDR,ALU_CONF_FNS_OFFSET
	ldi smul2
	wrc ALU1_CONFIG_ADDR,ALU_CONF_SELA_OFFSET
	ldi smul3
	wrc ALU1_CONFIG_ADDR,ALU_CONF_SELB_OFFSET
	
#write configuration
	wrw CONF_MEM_BASE,62
	
#clear conf_reg
	wrw CLEAR_CONFIG_ADDR
	
#complex sum
#configure mem2 for reading a and b vectors, real parts
	ldi 1
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	
#configure mem3 for reading a and b vectors, imaginary parts
	ldi 1
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	
#configure mem0 for storing result, real parts
	ldi salu0
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	ldi salulite0
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	ldi 3
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	ldi 1
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	
#configure mem1 for storing result, imaginary parts
	ldi salu1
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	ldi salulite1
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_SELA_OFFSET
	ldi 3
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_DELAY_OFFSET
	ldi 1
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	
#configure alu0 for adding mem2A and mem2B
	ldi ALU_ADD
	wrc ALU0_CONFIG_ADDR,ALU_CONF_FNS_OFFSET
	ldi smem2A
	wrc ALU0_CONFIG_ADDR,ALU_CONF_SELA_OFFSET
	ldi smem2B
	wrc ALU0_CONFIG_ADDR,ALU_CONF_SELB_OFFSET
	
#configure ALU_LITE0 for adding mem2A and -mem2B
	ldi ALU_SUB
	wrc ALU_LITE0_CONFIG_ADDR,ALU_LITE_CONF_FNS_OFFSET
	ldi smem2B
	wrc ALU_LITE0_CONFIG_ADDR,ALU_LITE_CONF_SELA_OFFSET
	ldi smem2A
	wrc ALU_LITE0_CONFIG_ADDR,ALU_LITE_CONF_SELB_OFFSET
	
#configure alu1 for adding mem3A and mem3B
	ldi ALU_ADD
	wrc ALU1_CONFIG_ADDR,ALU_CONF_FNS_OFFSET
	ldi smem3A
	wrc ALU1_CONFIG_ADDR,ALU_CONF_SELA_OFFSET
	ldi smem3B
	wrc ALU1_CONFIG_ADDR,ALU_CONF_SELB_OFFSET
	
#configure ALU_LITE1 for adding mem3A and -mem3B
	ldi ALU_SUB
	wrc ALU_LITE1_CONFIG_ADDR,ALU_LITE_CONF_FNS_OFFSET
	ldi smem3B
	wrc ALU_LITE1_CONFIG_ADDR,ALU_LITE_CONF_SELA_OFFSET
	ldi smem3A
	wrc ALU_LITE1_CONFIG_ADDR,ALU_LITE_CONF_SELB_OFFSET
	
#write configuration
	wrw CONF_MEM_BASE,63
	
#complex product
# test if it's a simple stage or a complex one
begin	ldi 7
	sub R9
	beqi comp
	ldi 8
	sub R9
	beqi comp_2
	ldi 9
	sub R9
	beqi comp_2
	ldi 10
	sub R9
	beqi comp_2
	
#reload the number of reconfigurations to R12
begin_2	rdw R11
	addi -1
	wrw R12
	ldi 0
	wrw R7
	
#read configuration
	rdw CONF_MEM_BASE,0
	
#configure mem0 for reading b vector, real part
c_prod	rdw R8
	add R7
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_START_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R8
	sub R5
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	add R4
	addi -1
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	rdw R6
	addi -1
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem1 for reading b vector, imaginary part
	rdw R8
	add R7
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_START_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R8
	sub R5
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	add R4
	addi -1
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	rdw R6
	addi -1
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem2 for reading coeficients
	rdw R8
	sub R5
	add R4
	addi -1
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	rdw R10
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi -1024
	add R3
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	rdw R6
	addi -1
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#wait for results to run next configuration
wait	ldi 0x0002
	and ENG_STATUS_REG
	beqi wait
	
#init and run engine (0x0019F87B)
	ldi 0x987B
	ldih 0x1F
	wrw ENG_CTRL_REG
	
#compute R7
	rdw R7
	add R6
	add R6
	wrw R7
	
#test if it's the last reconfiguration
	rdw R12
	bneqi c_prod
	wrw R12
	
#reload the number of reconfigurations to R12
	rdw R11
	addi -1
	wrw R12
	ldi 0
	wrw R7
	
#read configuration
	rdw CONF_MEM_BASE,1
	
#complex sum
#configure mem0 for reading a and b vectors, real parts
c_sum	rdw R7
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R8
	sub R5
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	add R4
	addi -1
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	rdw R8
	add R7
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R6
	addi -1
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem1 for reading a and b vectors, imaginary parts
	rdw R7
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R8
	sub R5
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	add R4
	addi -1
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	rdw R8
	add R7
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R6
	addi -1
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem2 for storing result, real parts
	rdw R7
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R8
	sub R5
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	add R4
	addi -1
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	rdw R8
	add R7
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R6
	addi -1
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem3 for storing result, imaginary parts
	rdw R7
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R8
	sub R5
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	add R4
	addi -1
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	rdw R8
	add R7
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R6
	addi -1
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#wait for results to run next configuration
wait2	ldi 0x0020
	and ENG_STATUS_REG
	beqi wait2
	
#init and run engine (0x001FFE03)
	ldi 0xFE03
	ldih 0x1F
	wrw ENG_CTRL_REG
	
#compute R7
	rdw R7
	add R6
	add R6
	wrw R7
	
#test if it's the last reconfiguration
	rdw R12
	bneqi c_sum
	wrw R12
	
#compute next R6, R8, R9 and R10 values
next3	rdw R9
	addi -10
	beqi wait4
	rdw R8
	shft -1
	wrw R8
	rdw R10
	shft 1
	wrw R10
	rdw R9
	addi 1
	wrw R9
	rdw R6
	shft 1
	wrw R6
	
#test if it's a even or odd stage
	ldi 0x0001
	and R9
	bneqi begin
	
#complex product
# test if it's a simple stage or a complex one
begini	ldi 7
	sub R9
	beqi comp
	ldi 8
	sub R9
	beqi comp_2
	ldi 9
	sub R9
	beqi comp_2
	ldi 10
	sub R9
	beqi comp_2
	nop
	
#reload the number of reconfigurations to R12
begin2i	rdw R11
	addi -1
	wrw R12
	ldi 0
	wrw R7
	
#read configuration
	rdw CONF_MEM_BASE,62
	
#configure mem2 for reading b vector, real part
c_prodi	rdw R8
	add R7
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_START_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R8
	sub R5
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	add R4
	addi -1
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	rdw R6
	addi -1
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem3 for reading b vector, imaginary part
	rdw R8
	add R7
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_START_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R8
	sub R5
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	add R4
	addi -1
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	rdw R6
	addi -1
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem0 for reading coeficients
	rdw R8
	sub R5
	add R4
	addi -1
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	rdw R10
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_INCR_OFFSET
	ldi -1024
	add R3
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	rdw R6
	addi -1
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#wait for results to run next configuration
waiti	ldi 0x0020
	and ENG_STATUS_REG
	beqi waiti
	
#init and run engine (0x0019F87B)
	ldi 0xF87B
	ldih 0x19
	wrw ENG_CTRL_REG
	
#compute R7
	rdw R7
	add R6
	add R6
	wrw R7
	
#test if it's the last reconfiguration
	rdw R12
	bneqi c_prodi
	wrw R12
	
#reload the number of reconfigurations to R12
	rdw R11
	addi -1
	wrw R12
	ldi 0
	wrw R7
	
#read configuration
	rdw CONF_MEM_BASE,63
	
#complex sum
#configure mem2 for reading a and b vectors, real parts
c_sumi	rdw R7
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R8
	sub R5
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	add R4
	addi -1
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	rdw R8
	add R7
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R6
	addi -1
	wrc MEM2A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM2B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem3 for reading a and b vectors, imaginary parts
	rdw R7
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R8
	sub R5
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	add R4
	addi -1
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	rdw R8
	add R7
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R6
	addi -1
	wrc MEM3A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM3B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem0 for storing result, real parts
	rdw R7
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R8
	sub R5
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	add R4
	addi -1
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	rdw R8
	add R7
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R6
	addi -1
	wrc MEM0A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM0B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#configure mem1 for storing result, imaginary parts
	rdw R7
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R8
	sub R5
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_SHIFT_OFFSET
	add R4
	addi -1
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_DUTY_OFFSET
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_PER_OFFSET
	rdw R8
	add R7
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_START_OFFSET
	rdw R6
	addi -1
	wrc MEM1A_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	wrc MEM1B_CONFIG_ADDR,MEM_CONF_ITER_OFFSET
	
#wait for results to run next configuration
wait2i	ldi 0x0002
	and ENG_STATUS_REG
	beqi wait2i
	
#init and run engine (0x001FFE03)
	ldi 0xFE03
	ldih 0x1F
	wrw ENG_CTRL_REG
	
#compute R7
	rdw R7
	add R6
	add R6
	wrw R7
	
#test if it's the last reconfiguration
	rdw R12
	bneqi c_sumi
	wrw R12
	
	ldi 0
	beqi next3
	nop
	
#compute a complex stage
comp	ldi 16
	wrw R11
	ldi 1
	wrw R4
	ldi 1024
	wrw R3
	rdw R6
	shft -1
	wrw R6
comp_2	rdw R11
	shft 1
	wrw R11
	rdw R6
	shft -1
	shft -1
	wrw R6
	rdw R8
	wrw R5
	ldi 0x0001
	and R9
	bneqi begin_2
	nop
	ldi 0
	beqi begin2i
	nop
	
#wait for final results to exit
wait4	ldi 0x00FF
	and ENG_STATUS_REG
	addi -255
	bneqi wait4
	nop
	
#branch to boot ROM (0x000)
exit	ldi 0
	beqi ROM_BASE
	nop
	nop
