#!/usr/bin/python
#
#    
#

import sys, string
import os
from subprocess import call

cpp_to_assembly_path = "../../assembly_src/"
cpp_to_verilog_path = "../../verilog_src/"
verilog_to_test_path = "../test_data/"
fp = open(os.devnull, "w")

#############################################################		
#runs diff command	

#############################################################
def diff(name):
	
	
	numb = call(["diff","xtop.out", verilog_to_test_path+name+"/results.gold"],stdout=fp)
	
	return numb



#############################################################		
#runs the compiler and generates assembly	

#############################################################
def runCompiler(file_name):

	os.chdir("/home/rui/sandbox3/versat/cpp_src/compiler")
	new_assembly = cpp_to_assembly_path+file_name+"_compiled.va"
	#new_assembly = "aaa.va"
	arg = "./vcc ./examples/"+file_name+".cpp >"+new_assembly
	os.system('%s' %arg)


#############################################################		
#runs the assembly test	

#############################################################
def runTest(file_name):

	
	os.chdir("/home/rui/sandbox3/versat/verilog_src")
	#arg = "make xtop_dma test="+file_name+"_compiled" 
	#os.system('%s' %arg)


	call(["make","xtop_dma", "test="+file_name+"_compiled"],stdout=fp)


	a = diff(file_name+"_compiled")
	if(a == 0):
		print "There are no differences in file "+file_name+".cpp"
	else:
		print "There are differences in file "+file_name+".cpp"
	
	






#os.chdir("/home/rui/sandbox3/versat/cpp_src/compiler")
runCompiler("lpf")
runTest("lpf")
runCompiler("vec_add_exp")
runTest("vec_add_exp")
runCompiler("complex_dot_product_exp")
runTest("complex_dot_product_exp")
runCompiler("fft_pr")
runTest("fft_pr")
runCompiler("fft")
runTest("fft")
runCompiler("lpf2_expression")
runTest("lpf2_expression")











