#!/usr/bin/python
#
#    h e x 2 b i n . p y
#

import sys
from array import array

# Read hexadecimal file
def readFile(file):
    data = []
    
    fr = open(file, "r")
    
    line = fr.readline()
    while(not(line == "")):
        data.append(int(line,16))
        line = fr.readline()

    fr.close()
    return data

# Convert hexadecimal data into binary data
def conv(data):
    byte_array = []
    
    # Process input data line by line
    for x in data:
        # Add to an array value x (32 bits) byte by byte (little endian)
        byte = x&0x000000ff
        byte_array.append(byte)
        
        byte = (x&0x0000ff00)>>8
        byte_array.append(byte)
        
        byte = (x&0x00ff0000)>>16
        byte_array.append(byte)
        
        byte = (x&0xff000000)>>24
        byte_array.append(byte)
        
    # Write array to a file
    fw = open('./data.bin', 'wb')
    fw.write(bytearray(byte_array))
    fw.close()
    

def main():
    if(len(sys.argv) == 2):
        if(sys.argv[1][-4:] != ".hex"):
            sys.exit("Usage: ./hex2bin.py <file>.hex")
    else:
        sys.exit("Usage: ./hex2bin.py <file>.hex")
        
    data = readFile(sys.argv[1])
    conv(data)
    

if __name__ == "__main__" : main()
