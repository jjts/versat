#!/usr/bin/python
#
#    d a t a g e n . p y
#
# Note: This script generate one file with hexadecimal values 
#       and another one with the same values in decimal format.
#       All values are random numbers between 0 and 2**31/N-1
#

import sys
from random import randint

# Create two files, one with hexadecimal values and the other with the same values in decimal format
def genData(N):
    fw = open('./data.hex', 'w')
    
    # Generate N random numbers
    for i in xrange(1,N+1):
        x = randint(0,2**31/N-1)
        
        if(i < N):
            fw.write(format(x,'08x')+'\n')
        else:
            fw.write(format(x,'08x'))
        
    fw.close()
    

def main():
    if(len(sys.argv) != 2):
        sys.exit("Usage: ./datagen.py <int>")
        
    genData(int(sys.argv[1]))
    

if __name__ == "__main__" : main()
