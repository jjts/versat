#! /usr/bin/python
#
#    f i l l _ z e r o s . p y
#

import sys
import string

def count_lines(name):
	num_lines = 0
	
	fp = open(name, "r")
	
	line = fp.readline()
	
	while(not(line == "")):
		num_lines += 1
		line = fp.readline()
	
	fp.close()
	
	return num_lines

def fill_rest(name,num_lines):
	fp = open(name, "a")
		
	while(num_lines < 2048):
		num_lines += 1
		fp.write(str("{:08x}".format(0))+"\n")
	
	fp.close()

def main () :
	
	if (len(sys.argv) > 2 or sys.argv[1].find(".hex") == -1):
		print "Usage: python fill_zeros.py <name>.hex"
		print "Wrong argument: " + sys.argv[1]
		sys.exit(1)
	
	num_lines = count_lines (sys.argv[1])
	fill_rest (sys.argv[1], num_lines)

if __name__ == "__main__" : main ()
