#!/usr/bin/python
#
#    
#

import sys, string
import os

list_files = ["./../verilog_src/xdefs.v","./../cpp_src/compiler/defs_library.h","./../cpp_src/compiler/mem_ends.h","./../verilog_src/xmem_map.v"]

 
#############################################################		
#convert hex numbers in decimal	

#############################################################

def hex2dec(char):
	
	if(char == 'A'): 
		return "10"
	if(char == 'B'): 
		return "11"
	if(char == 'C'): 
		return "12"
	if(char == 'D'): 
		return "13"
	if(char == 'E'): 
		return "14"
	if(char == 'F'): 
		return "15"
	return char
#############################################################		
#replace data in C++ format	

#############################################################
def replace(str):  
    str = str.replace('`INT_ADDR_W', '') #delete INT_ADDR of Verilog
    str = str.replace('`', '') #delete single quotes of Verilog
    
    
    for x in range(0, len(str)-3):
	if (str[x:x+3] == "4'h"):
		character = hex2dec(str[x+3]);
    		str = str.replace(str[x:x+4], character) #delete non compatible data 

	if (str[x:x+2] == "'h") : # verify if exists hexadecimal values 
		str_aux = str.split(" ", 3); 
		a = int(str[x+2:x+6], 16);
		s_aux = "%2d" % a
		str = str_aux[0] + ' ' + str_aux[1] + s_aux;

	
	if (str[x:x+2] == "'b"): # verify if exists binary values
		str_aux = str.split(" ", 3); 
		a = int(str[x+2], 2);
		s_aux = "%2d" % a
		str = str_aux[0] + ' ' + str_aux[1] + s_aux + '\n';

	if (str[x:x+2] == "'d"): # verify if exists decimal values
		str = str.replace("'d", '') #delete single quotes of Verilog
		
		
    return str

#############################################################		
#calculates file length	

#############################################################


def file_len(fo):
    fo.seek(0, 2);
    position = fo.tell();
    fo.seek(0, 0);
    return position
    
#############################################################		
#read entry files, process data and write in output file	

#############################################################

def file_reading(fname, fname_out):
    a = file_len(fname);
    i = 0
   
    while fname.tell() < a : 
	lin = fname.readline(); #read one line of entry file 
	 
	if (lin[:7] == "`define") : #put defines in C++ format
		lin = '#' + lin[1:]
	
	lin = replace(lin);
	fname_out.write(lin); #write one line in output file 





# Open files
f_input = open(list_files[0], "r");
f_output = open(list_files[1], "w");
f_input2 = open(list_files[3], "r");
f_output2 = open(list_files[2], "w");
	




	
file_reading(f_input, f_output);
file_reading(f_input2, f_output2);

# Close opend files
f_input.close();
f_output.close();






