/* ****************************************************************************
  This Source Code Form is subject to the terms of the
  Open Hardware Description License, v. 1.0. If a copy
  of the OHDL was not distributed with this file, You
  can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

  Description: 

   Copyright (C) 2014 Authors

  Author(s): Joao Dias Lopes <joao.d.lopes91@gmail.com>

***************************************************************************** */

`timescale 1ns / 1ps

module xtop_xilinx(
	input clk,
	input rst,
	 
	//control slave interface to master CPU 
	 input sclk, 
	 input ss, 
	 input mosi,
	 output miso,
	 output rst_led
	);
	
	wire CLKDV;
	wire clk0_buf;
	wire clk1;
	
   // Instantiate the Unit Under Test (UUT)
   xtop uut (
	     .clk(clk0_buf), 
	     .rst(rst),
	     //.en(en),
	     // control slave interface
	     .sclk(sclk), 
	     .ss(ss), 
	     .mosi(mosi), 
	     .miso(miso)
	     //external memory interface
	     /*		.mreq(mreq),
	      .mack(ack),
	      .mrnw(mrnw),	
	      .maddr(maddr),
	      .mdata_in(mdata_in),
	      .mdata_out(mdata_out),
	      .msel(msel),
	      .mbte(mbte),
	      .mcti(mcti),
	      .mcyc(mcyc)*/
	     //data direct IO
	     /*		.data_in_req(data_in_req),
	      .data_in_rdy(data_in_rdy),
	      .data_i(data_i),
	      .data_out_req(data_out_req),
	      .data_out_rdy(data_out_rdy),	 
	      .data_out(data_out)*/
	     );
		  
		  assign rst_led = rst;
		  
		  DCM_BASE #(
      .CLKDV_DIVIDE(4.0), // Divide by: 1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5
                          //   7.0,7.5,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0 or 16.0
      .CLKFX_DIVIDE(1), // Can be any integer from 1 to 32
      .CLKFX_MULTIPLY(4), // Can be any integer from 2 to 32
      .CLKIN_DIVIDE_BY_2("FALSE"), // TRUE/FALSE to enable CLKIN divide by two feature
      .CLKIN_PERIOD(10.0), // Specify period of input clock in ns from 1.25 to 1000.00
      .CLKOUT_PHASE_SHIFT("NONE"), // Specify phase shift mode of NONE or FIXED
      .CLK_FEEDBACK("1X"), // Specify clock feedback of NONE or 1X
      .DCM_PERFORMANCE_MODE("MAX_SPEED"), // Can be MAX_SPEED or MAX_RANGE
      .DESKEW_ADJUST("SYSTEM_SYNCHRONOUS"), // SOURCE_SYNCHRONOUS, SYSTEM_SYNCHRONOUS or
                                            //   an integer from 0 to 15
      .DFS_FREQUENCY_MODE("LOW"), // LOW or HIGH frequency mode for frequency synthesis
      .DLL_FREQUENCY_MODE("LOW"), // LOW, HIGH, or HIGH_SER frequency mode for DLL
      .DUTY_CYCLE_CORRECTION("TRUE"), // Duty cycle correction, TRUE or FALSE
      .FACTORY_JF(16'hf0f0), // FACTORY JF value suggested to be set to 16'hf0f0
      .PHASE_SHIFT(0), // Amount of fixed phase shift from -255 to 1023
      .STARTUP_WAIT("FALSE") // Delay configuration DONE until DCM LOCK, TRUE/FALSE
   ) DCM_BASE_inst (
      .CLK0(CLK0),         // 0 degree DCM CLK output
      .CLK180(CLK180),     // 180 degree DCM CLK output
      .CLK270(CLK270),     // 270 degree DCM CLK output
      .CLK2X(CLK2X),       // 2X DCM CLK output
      .CLK2X180(CLK2X180), // 2X, 180 degree DCM CLK out
      .CLK90(CLK90),       // 90 degree DCM CLK output
      .CLKDV(CLKDV),       // Divided DCM CLK out (CLKDV_DIVIDE)
      .CLKFX(CLKFX),       // DCM CLK synthesis out (M/D)
      .CLKFX180(CLKFX180), // 180 degree CLK synthesis out
      .LOCKED(LOCKED),     // DCM LOCK status output
      .CLKFB(clk0_buf),       // DCM clock feedback
      .CLKIN(clk),       // Clock input (from IBUFG, BUFG or DCM)
      .RST(rst)            // DCM asynchronous reset input
   );
	
	BUFG BUFG_clk0 (
      .O(clk0_buf),     // Clock buffer output
      .I(CLK0)      // Clock buffer input
   );
	
	BUFG BUFG_clkdv (
      .O(clk1),     // Clock buffer output
      .I(CLKDV)      // Clock buffer input
   );
      	 
endmodule
