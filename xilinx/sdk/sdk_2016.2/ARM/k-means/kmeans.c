/*
 * kmeans.c
 */
#define number_of_datapoints 10064 //max = 1000000
#define number_of_centroids 34 //max = 1024/number_of_coordinates
#define number_of_coordinates 30 //min = 15, max = 30

#define DEBUG

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#ifdef USE_PC
#define TRUE 1
#define FALSE 0
#include <omp.h>
#endif

#ifdef USE_ARM
#define DATA_PTR 0x02000000
#define CENTER_PTR (DATA_PTR+NDATAPTS*DIM*4)
#define LABEL_PTR (CENTER_PTR+NCENTRS*DIM*4)
#define TEMP_CENTER_PTR (LABEL_PTR+NDATAPTS*4)
#define CLUSTER_SZ_PTR (TEMP_CENTER_PTR+NCENTRS*DIM*4)
#include "xtime_l.h"
#include "xil_cache.h"
#endif

#define NREGS 16


/*
 * K-means function
 */
int kmeans(
	   unsigned int NDATAPTS,
	   unsigned int NCENTRS,
	   unsigned int DIM,
	   int * data,
	   unsigned int * label,
	   int * center,
	   int * tempCenter,
	   unsigned int * cluster_size
	   ) {

  int i, j, k; //points, centroids and coordinate iterators
  int done; //algorithm done flag

  int loops; //number of iterations of the algorithm
  unsigned int minDist, tempDist;//distances from point to centroid
  unsigned int updated_center_coordinate;

  // init number of iterations
  loops = 0;
  do{// new iteration
    // init cluster_size array
    for(j = 0; j < NCENTRS; j++)
      cluster_size[j] = 0;
    // init temporary centers
    for(j = 0; j < NCENTRS; j++)
      for(k=0; k < DIM; k++)
	tempCenter[j*DIM+k]=0;
  
    //for(i = 0; i < (NDATAPTS & ~3); i++){
    for(i = 0; i < NDATAPTS ; i++){
      //init min distance
      minDist = 0xffffffff;
      /* compute the distance between the point and each centroid*/
      //for(j = 0; j < (NCENTRS & ~3); j++){
      for(j = 0; j < NCENTRS; j++){
	for(tempDist=0, k=0; k < DIM; k++)
	  tempDist += abs(data[i*DIM+k] - center[j*DIM+k]);
	//dist = abs(data[i].x - center[j].x) + abs(data[i].y - center[j].y);
	if(tempDist < minDist){
	  minDist = tempDist;
	  label[i] = j;
	}
      }

      // accumulate label cluster_size
      ++cluster_size[label[i]];

      // accumulate point coordinates in respective temp center coordinate
      //for(k=0; k < (DIM & ~3); k++)
      for(k=0; k < DIM; k++)
	tempCenter[label[i]*DIM+k] += data[i*DIM+k];
    }
    // done computing labels, label cluster_size and accumulated centroid coordinates


    /* update the centroids */
    done = TRUE;
    //for(j = 0; j < (NCENTRS & ~3); j++){
    for(j = 0; j < NCENTRS; j++)
      //for(j=0; j < (DIM & ~3); j++){
      for(k=0; k < DIM; k++){
	updated_center_coordinate = cluster_size[j]? tempCenter[j*DIM+k] / cluster_size[j] : 0;
	if(updated_center_coordinate != center[j*DIM+k]) {
	  center[j*DIM+k] = updated_center_coordinate;
	  done = FALSE; //algorithm not finished, another iteration needed
	}
      }
    ++loops;
#ifdef DEBUG
  } while(FALSE);
#else
  } while(FALSE);
#endif

return loops;
}


/*
 * Main function
 */
int main(int argc, char **argv){

  unsigned int NDATAPTS = number_of_datapoints;
  unsigned int NCENTRS = number_of_centroids;
  unsigned int DIM = number_of_coordinates;

  int * data;
  unsigned int * label;
  int * center;

  int * tempCenter;
  unsigned int * cluster_size;

#ifdef USE_ARM
  int i, j, k;//points and centroids iterators
  //point to data
  data = (int *) DATA_PTR;

#endif
  
#ifdef USE_PC
  int i, j, k;// iterators for points, centroids and dimensions 
  int * initial_center;
  //handle command line args
  if(argc > 1)
    for(i=1; i < argc; i+=2){
      if(strcmp(argv[i], "-NDATAPTS") == 0)
	sscanf(argv[i+1],"%d", &NDATAPTS);
      else if(strcmp(argv[i], "-NCENTRS") == 0)
	sscanf(argv[i+1],"%d", &NCENTRS);
      else if(strcmp(argv[i], "-DIM") == 0)
	sscanf(argv[i+1],"%d", &DIM);
      else {
	printf("%s: unknown command line option\n", argv[i]);
	exit(1);
      }
    }

  //allocate space for arrays
  data = (int *) malloc(((NDATAPTS+NCENTRS)*DIM)*sizeof(int));
  if(data == NULL) {
    printf("could not allocate data\n");
    exit(1);
  }
  label = (unsigned int *) malloc((NDATAPTS)*sizeof(unsigned int));
  if(label == NULL) {
    printf("could not allocate data\n");
    exit(1);
  }
  initial_center = (int *) malloc((NCENTRS*DIM)*sizeof(int));
  if(initial_center == NULL) {
    printf("could not allocate initial_center\n");
    exit(1);
  }
  tempCenter = (int *) malloc((NCENTRS*DIM)*sizeof(int));
  if(tempCenter == NULL) {
    printf("could not allocate tempCenter\n");
    exit(1);
  }
  cluster_size = (unsigned int *) malloc(NCENTRS*sizeof(unsigned int));
  if(cluster_size == NULL) {
    printf("could not allocate cluster_size\n");
    exit(1);
  }

  //load data from file
  FILE * fp_data = NULL;
  fp_data = fopen("data.hex","r");
  if(fp_data == NULL) {
    printf("data.hex: no such file\n");
    exit(1);
  }
  //read datapoints
  for(i=0; i<((NDATAPTS+NCENTRS)*DIM); i++)
    fscanf(fp_data, "%x", &data[i]);
  center = data + NDATAPTS*DIM;
  
  //create hardware input memory file
  FILE * fp_mem_in = NULL;
  fp_mem_in = fopen("mem_in.hex","w");
  if(fp_mem_in == NULL) {
    printf("mem_in.hex: could not create file\n");
    exit(1);
  }

  //print datapoints to file mem_in
  for(i = 0; i<NDATAPTS; i++)
    for(j = 0; j<DIM; j++)
      fprintf(fp_mem_in,"%08x\n", data[i*DIM+j]);

  //print centroids to file mem_in
  for(i = 0; i<NCENTRS; i++)
    for(j = 0; j<DIM; j++)
      fprintf(fp_mem_in,"%08x\n", center[i*DIM+j]);

  //print registers to file mem_in
  for(i=1; i<NREGS; i++)
    if (i==1) //in data start address
      fprintf(fp_mem_in,"%08x\n", 0);
    else if(i==2) //out data start address
      fprintf(fp_mem_in,"%08x\n", 4*((NDATAPTS+NCENTRS)*DIM));
    else if(i==3) //in centers start address
      fprintf(fp_mem_in,"%08x\n", 4*(NDATAPTS*DIM));
    else if(i==4)
      fprintf(fp_mem_in,"%08x\n", NDATAPTS);
    else if(i==5)
      fprintf(fp_mem_in,"%08x\n", NCENTRS);
    else if(i==6)
      fprintf(fp_mem_in,"%08x\n", DIM);
    else if(i==15)
      fprintf(fp_mem_in,"%08x", 0);
    else
      fprintf(fp_mem_in,"%08x\n", 0);

  //declare time vars 
  double start_time, end_time;
#endif

  //uncomment one of the following
  for(i=0 ; i<1; i++) { //single run
  //for(NCENTRS=2; NCENTRS < 68; NCENTRS += 8) { //center sweep
    //for(DIM=15; DIM <= 30; DIM += 5) { //dimension sweep
    //for(NDATAPTS=1360; NDATAPTS < 2000000; NDATAPTS *= 10) {//datapoit sweep

#ifdef USE_ARM
    //get pointer for centroids
    center = (int *) CENTER_PTR;
    label = (unsigned int *) LABEL_PTR;
    tempCenter = (int *) TEMP_CENTER_PTR;
    cluster_size = (unsigned int *) CLUSTER_SZ_PTR;
    //get initial time
    XTime tstart, tend;
    XTime_GetTime(&tstart);
#endif
    
#ifdef USE_PC
    center = data + NDATAPTS*DIM;
    bcopy (center, initial_center, NCENTRS*DIM*sizeof(int));
    start_time = omp_get_wtime();
#endif

#ifdef DEBUG
    printf("NDATAPTS = %u\n", NDATAPTS);
    printf("NCENTRS = %u\n", NCENTRS);
    printf("DIM = %u\n", DIM);
    printf("print some data values\n");
    printf("data 0 = %d\n", data[0]);
    printf("data 1 = %d\n", data[1]);
    printf("data 2 = %d\n", data[2]);
    printf("print initial centroids\n");
    printf("center 0 = %d\n", center[0]);
    printf("center 1 = %d\n", center[1]);
    printf("center 2 = %d\n", center[2]);
#endif

    //
    // Call K-Means
    //
    int loops;
    loops = kmeans(NDATAPTS, NCENTRS, DIM, data, label, center, tempCenter, cluster_size);

    //print runtime
#ifdef USE_ARM
    XTime_GetTime(&tend);
    printf("[NDATAPTS,NCENTRS,DIM,Cycles,Time] = [%d, %d, %d, %llu, %.2f]\n",
	   NDATAPTS,
	   NCENTRS,
	   DIM,
	   2*(tend - tstart),
	   1.0*(tend - tstart)/(COUNTS_PER_SECOND/1000000) );
    Xil_DCacheFlush();
    Xil_DCacheInvalidate();
  }
#endif
  
#ifdef USE_PC
    //print runtime
    end_time = omp_get_wtime();
    printf("Iterations= %d, Time= %f\n", loops, end_time - start_time);
  }
  //create hardware output memory file
  FILE * fp_mem_out = NULL;
  fp_mem_out = fopen("mem_out.hex","w");
  if(fp_mem_out == NULL) {
    printf("mem_out.hex: could not create file\n");
    exit(1);
  }
  //print datapoints to file mem_out
  for(i=0; i<(NDATAPTS*DIM); i++)
    fprintf(fp_mem_out, "%08x\n", data[i]);
  //print initial centroids to file mem_out
  for(i=0; i<(NCENTRS*DIM); i++)
    fprintf(fp_mem_out, "%08x\n", initial_center[i]);
  //print centroids to file mem_out
  for(j = 0; j<NCENTRS; j++)
    for(k = 0; k<DIM; k++)
      fprintf(fp_mem_out,"%08x\n", center[j*DIM+k]);
#endif

  //print final centroids
#ifdef DEBUG
  printf("======final centroids======\n");
  for(j = 0; j<3; j++)
    for(k = 0; k<DIM; k++)
      printf("%d\n", center[j*DIM+k]);
  printf("======final labels ======\n");
  for(i = 0; i<3; i++)
    printf("%d\n", label[i]);
#endif
  return 0;  
}
