/*
 * benchmarking.h
 *
 *  Created on: 2013-9-6
 *      Author: hqin
 */

#ifndef BENCHMARKING_H_
#define BENCHMARKING_H_

#include "xparameters.h"
#include "xscutimer.h"
#include "xil_printf.h"
#include "xil_cache.h"
#include <xtime_l.h>

/* definition and variable for time measurement */
#define CPU_FREQ_HZ			XPAR_CPU_CORTEXA9_CORE_CLOCK_FREQ_HZ
#define TIMER_DEVICE_ID		XPAR_XSCUTIMER_0_DEVICE_ID
#define TIMER_LOAD_VALUE	0xFFFFFFFF
#define TIMER_PRE_SCALE	    0
#define TIME_PER_TICK		((float)2.0*(TIMER_PRE_SCALE+1)/CPU_FREQ_HZ)

typedef struct {
	unsigned int uiCount;
	unsigned int uiSuccess;
	u64          ullMax;
	u64          ullMin;
	u64          ullTotal;

}BENCHMARK_STATISTICS;

typedef struct {
	/* input */
	char         *pName;
	unsigned int uiTestRounds;
	unsigned int (*initializor)(unsigned int uiParam0, unsigned int uiParam1, unsigned int uiParam2, unsigned int uiParam3 );
	float        (*benchmarker)(float * __restrict, float * __restrict, unsigned int uiParam2, unsigned int uiParam3 );
	unsigned int uiParam[4];
	unsigned int uiRetCode;
	unsigned int (*validator)(unsigned int uiParam0, unsigned int uiParam1, unsigned int uiParam2, unsigned int uiParam3 );
	BENCHMARK_STATISTICS stat;
}BENCHMARK_CASE;

extern void statistics_print(BENCHMARK_STATISTICS *p);
extern int run_benchmark_single(BENCHMARK_CASE *pBenchmarkcase);

#endif /* BENCHMARKING_H_ */
