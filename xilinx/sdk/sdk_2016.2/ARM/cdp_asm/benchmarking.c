/*
 * benchmarking.c
 *
 *  Created on: 2013-9-6
 *      Author: hqin
 */
#include <stdlib.h>
#include <stdio.h>

#include "benchmarking.h"

/**********************************************************
 * benchmark statistics related functions
 **********************************************************/
void statistics_init(BENCHMARK_STATISTICS *p)
{
	p->uiCount=0;
	p->ullMax=0;
	p->ullMin=0xFFFFFFFFFFFFFFFF;
	p->ullTotal=0;
}
void statistics_add(BENCHMARK_STATISTICS *p, u64 ullTickUsed)
{
	if(ullTickUsed > p->ullMax)    p->ullMax = ullTickUsed;
	if(ullTickUsed < p->ullMin)    p->ullMin = ullTickUsed;
	p->ullTotal += ullTickUsed;
	p->uiCount++;
}

u64 statistics_avg(BENCHMARK_STATISTICS *p)
{
	return (p->ullTotal)/(p->uiCount);  /* unit: tick */
}

u64 statistics_filtered_avg(BENCHMARK_STATISTICS *p)
{
	return (p->ullTotal-p->ullMax-p->ullMin)/(p->uiCount-2);  /* unit: tick */
}

void statistics_print(BENCHMARK_STATISTICS *p)
{
    printf("Nr,         Max,         Min,     Average,    Fltr Avg,  Fltr_Avg(us)\r\n");
    printf("%2u,%12llu,%12llu,%12llu,%12llu,%14.3f\r\n", p->uiCount, p->ullMax, p->ullMin, statistics_avg(p), statistics_filtered_avg(p), (double)statistics_filtered_avg(p)*TIME_PER_TICK*1000000.0 );
}

extern void XTime_SetTime(XTime Xtime);
extern void XTime_GetTime(XTime *Xtime);
/**********************************************************
 * benchmark related functions
 **********************************************************/
int run_benchmark_single(BENCHMARK_CASE *pBenchmarkcase)
{
	u64 ullCntValue1, ullCntValue2;
	BENCHMARK_STATISTICS *pStat=&(pBenchmarkcase->stat);

	int i;
	//unsigned int uiResult;
	unsigned int uiSuccess=0;


    statistics_init(pStat);

    for(i=0;i<pBenchmarkcase->uiTestRounds;i++)
	{
    	pBenchmarkcase->initializor(pBenchmarkcase->uiParam[0], pBenchmarkcase->uiParam[1], pBenchmarkcase->uiParam[2], pBenchmarkcase->uiParam[3]);
		Xil_DCacheFlush();

		ullCntValue1 = 0;
    	XTime_SetTime(0L);

		pBenchmarkcase->uiRetCode = pBenchmarkcase->benchmarker((float *)pBenchmarkcase->uiParam[0], (float *)pBenchmarkcase->uiParam[1], pBenchmarkcase->uiParam[2], pBenchmarkcase->uiParam[3] );

    	XTime_GetTime(&ullCntValue2);

		statistics_add(pStat, ullCntValue2 - ullCntValue1);

		uiSuccess += pBenchmarkcase->validator(pBenchmarkcase->uiParam[0], pBenchmarkcase->uiParam[1], pBenchmarkcase->uiParam[2], pBenchmarkcase->uiParam[3] );
	}
    pStat->uiSuccess = uiSuccess;

    return 0;
}
