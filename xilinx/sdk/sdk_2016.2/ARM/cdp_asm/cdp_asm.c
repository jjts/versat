#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xil_printf.h"
#include "xtime_l.h"

#include "benchmarking.h"

/* N should be power of 2, and larger than 8 */
#define N            1024
#define TEST_ROUNDS    10

#define CACHE_LINE_SIZE    32

#define GOLDEN_RESULT 357389824
float a[N];
float b[N];

volatile int * x_ext;
volatile int * y_ext;

unsigned int initializor_dummy(unsigned int uiParam0, unsigned int uiParam1, unsigned int uiParam2, unsigned int uiParam3 )
{
	return 1;
}

unsigned int validator_dummy(unsigned int uiParam0, unsigned int uiParam1, unsigned int uiParam2, unsigned int uiParam3 )
{
	return 1;
}


extern float neon_dot_product_vec16_no_pld(float * __restrict pa, float *  __restrict pb, unsigned int n, unsigned int x );
extern float neon_dot_product_vec16_pld   (float * __restrict pa, float *  __restrict pb, unsigned int n, unsigned int x );

extern void neon_vec_add_pld              (int* __restrict x, int *  __restrict y);
//extern void neon_vec_add                  (int* __restrict x, int *  __restrict y);


#define NR_BENCHMARK_CASE 2
BENCHMARK_CASE BenchmarkCases[NR_BENCHMARK_CASE] = {
		{"Dot product without pld", TEST_ROUNDS, initializor_dummy, neon_dot_product_vec16_no_pld, {(unsigned int)a,(unsigned int)b,N,0}, 0, validator_dummy},
		{"Dot product with pld",    TEST_ROUNDS, initializor_dummy, neon_dot_product_vec16_pld,    {(unsigned int)a,(unsigned int)b,N,0}, 0, validator_dummy}
};

int main()
{
	unsigned int i;
	float fResult1, fResult2;
	XTime tStart, tEnd;

	x_ext = (int *)(0x01000000);
	y_ext = (int *)(0x01000000+0x00001000);

	BENCHMARK_CASE *pBenchmarkCase;
	BENCHMARK_STATISTICS *pStat;

	printf("----Benchmarking starting----\r\n");
	printf("CPU_FREQ_HZ=%d, TIMER_FREQ_HZ=%d\r\n", CPU_FREQ_HZ, CPU_FREQ_HZ/2/(TIMER_PRE_SCALE+1));

	// We need to validate the algorithm's correctness
	for(i=0;i<N;i++)
	{
        a[i]=i;
	    b[i]=i;
	}

	fResult1 = neon_dot_product_vec16_no_pld(a,b,N,0);
	fResult2 = neon_dot_product_vec16_pld(a,b,N,0);

	xil_printf("*****************************************\r\n"
			   "* vec_add kernel w/o DMA transfers test *\r\n"
			   "*****************************************\r\n\r\n");

	neon_vec_add_pld(x_ext,y_ext);

	XTime_GetTime(&tStart); // start measuring time

	neon_vec_add_pld(x_ext,y_ext);

	XTime_GetTime(&tEnd); // end measuring time

	printf("\nOutput took %llu clock cycles.\n", 2*(tEnd - tStart));
	printf("Output took %.2f us.\n\n", 1.0 * (tEnd - tStart) / (COUNTS_PER_SECOND/1000000));

    printf("Golden Result=%d\r\n", GOLDEN_RESULT);
    printf("Result1=%f\r\n", fResult1);
    printf("Result2=%f\r\n", fResult2);

	// Now we can collect the execution time statistics
    for(i=0;i<NR_BENCHMARK_CASE;i++)
    {
    	pBenchmarkCase = &BenchmarkCases[i];
    	pStat = &(pBenchmarkCase->stat);
    	printf("Case %d: %s\r\n", i, pBenchmarkCase->pName);
    	run_benchmark_single(pBenchmarkCase);
    	statistics_print(pStat);
    }
    printf("----Benchmarking Complete----\r\n");

	return 0;
}

