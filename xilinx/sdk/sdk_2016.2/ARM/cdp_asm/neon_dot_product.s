/*
 * neon_dot_product.s
 *
 *  Created on: 2013-9-15
 *      Author: hqin
 */


        .text
        .syntax   unified

        .align  4
        .global neon_dot_product_vec16_no_pld
        .arm

neon_dot_product_vec16_no_pld:
        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        @
        @ int neon_dot_product_vec16_no_pld(int * src0,
        @                 int * src1,
        @                 unsigned int num
        @                 unsigned int x)
        @
        @  r0: vec0's base pointer
        @  r1: vec1's base pointer
        @  r2: vector length
        @  r3: unused
        @
        @  Note: Here we assume r2%16==0
        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        @pld [r1, #0]
        @pld [r2, #0]
        @pld [r1, #32]
        @pld [r2, #32]

        vmov.i32          q10, #0
        vmov.i32          q11, #0
        vmov.i32          q12, #0
        vmov.i32          q13, #0

.L_mainloop_vec_16_no_pld:
        @ load current set of values
        vldm  r0!, {d0, d1, d2, d3, d4, d5, d6, d7}
        vldm  r1!, {d10, d11, d12, d13, d14, d15, d16, d17}

        @pld [r0]
        @pld [r1]
        @pld [r0, #32]
        @pld [r1, #32]

        @ calculate values for current set
        vmla.f32        q10, q0, q5
        vmla.f32        q11, q1, q6
        vmla.f32        q12, q2, q7
        vmla.f32        q13, q3, q8
        @ loop control
        subs            r2, r2, #16
        bgt             .L_mainloop_vec_16_no_pld             @ loop if r3 > 0, if we have more elements to process

.L_return_vec_16_no_pld:
        @ calculate the final result
        vadd.f32        q15, q10, q11
        vadd.f32        q15, q15, q12
        vadd.f32        q15, q15, q13

        vpadd.f32       d30, d30, d31
        vpadd.f32       d30, d30, d30
        vmov.32	        r0, d30[0]
        @ return
        bx                lr


        .align  4
        .global neon_dot_product_vec16_pld
        .arm

neon_dot_product_vec16_pld:
        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        @
        @ int neon_dot_product_vec16_pld(int * vec0,
        @                 int * vec1,
        @                 unsigned int num
        @                 unsigned int x)
        @
        @  r0: vec0's base pointer
        @  r1: vec1's base pointer
        @  r2: vector length
        @  r3: unused
        @
        @  Note: Here we assume r2%16==0
        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        pld [r0, #0]
        pld [r1, #0]
        pld [r0, #32]
        pld [r1, #32]

        vmov.i32          q10, #0
        vmov.i32          q11, #0
        vmov.i32          q12, #0
        vmov.i32          q13, #0

.L_mainloop_vec_16_pld:
        @ load current set of values
        vldm  r0!, {d0, d1, d2, d3, d4, d5, d6, d7}
        vldm  r1!, {d10, d11, d12, d13, d14, d15, d16, d17}

        pld [r0]
        pld [r1]
        pld [r0, #32]
        pld [r1, #32]

        @ calculate values for current set
        vmla.f32        q10, q0, q5
        vmla.f32        q11, q1, q6
        vmla.f32        q12, q2, q7
        vmla.f32        q13, q3, q8
        @ loop control
        subs            r2, r2, #16
        bgt             .L_mainloop_vec_16_pld             @ loop if r3 > 0, if we have more elements to process

.L_return_vec_16_pld:
        @ calculate the final result
        vadd.f32        q15, q10, q11
        vadd.f32        q15, q15, q12
        vadd.f32        q15, q15, q13

        vpadd.f32       d30, d30, d31
        vpadd.f32       d30, d30, d30
        vmov.32	        r0, d30[0]
        @ return
        bx                lr

        .align  4
        .global neon_vec_add_pld
        .arm

neon_vec_add_pld:
        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        @
        @ int neon_vec_add_pld(int * x,
        @                      int * y)
        @
        @  r0: vector x base pointer
        @  r1: vector y base pointer
        @
        @
        @
        @
        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        @ pld [r0]
        @ pld [r0, #32]
        @ pld [r0, #64]
        @ pld [r0, #96]

        mov  r2, #1024

.L_mainloop_vec_add_pld:
        @ load current set of values
        vldm  r0!, {q0, q1, q2, q3}
        vldm  r0!, {q4, q5, q6, q7}

        @ pld [r0]
        @ pld [r0, #32]
        @ pld [r0, #64]
        @ pld [r0, #96]

        @ calculate values for current set
        vadd.i32  q8, q0, q1
        vadd.i32  q9, q2, q3
        vadd.i32  q10, q4, q5
        vadd.i32  q11, q6, q7
        @ store current set of values
        vstm  r1!, {q8, q9, q10, q11}
        @ loop control
        subs            r2, r2, #16
        bgt             .L_mainloop_vec_add_pld             @ loop if r3 > 0, if we have more elements to process

.L_return_vec_add_pld:
        @ return
        bx                lr

