#include <mb_interface.h>

void init_cache ()
{
  /* Initialize ICache */
  //microblaze_invalidate_icache();
  //microblaze_enable_icache ();
  /* Initialize DCache */
  microblaze_invalidate_dcache();
  microblaze_enable_dcache ();
}

void deinit_cache()
{
  /* Clean up DCache. For writeback caches, the disable_dcache routine
     internally does the flush and invalidate. For write through caches,
     an explicit invalidation must be performed on the entire cache. */
#if XPAR_MICROBLAZE_DCACHE_USE_WRITEBACK == 0
  microblaze_invalidate_dcache();
#endif
  microblaze_disable_dcache();
  /* Clean up ICache */
  microblaze_invalidate_icache();
  microblaze_disable_icache();
}
