/* Adapted from Xilinx mailbox example in file prodcon.c */

#include "xparameters.h"
#include "xtmrctr.h"

#define SYSTEM_TIMER_BASEADDR   XPAR_AXI_TIMER_0_BASEADDR

void init_timer ()
{
  /* Set load value = 0, Load timer, clear interrupt bit, start the timer */
  XTmrCtr_SetLoadReg(SYSTEM_TIMER_BASEADDR, 0, 0);
  XTmrCtr_SetControlStatusReg(SYSTEM_TIMER_BASEADDR, 0,
			      XTC_CSR_INT_OCCURED_MASK 
			      | XTC_CSR_LOAD_MASK);
  XTmrCtr_SetControlStatusReg(SYSTEM_TIMER_BASEADDR, 0,
			      XTC_CSR_ENABLE_TMR_MASK 
			      | XTC_CSR_AUTO_RELOAD_MASK);
}

u32 get_timer () 
{
  return XTmrCtr_GetTimerCounterReg(SYSTEM_TIMER_BASEADDR, 0);
}

void set_timer (u32 time)
{
  /* Set load value = 0, Load timer, clear interrupt bit, start the timer */
  XTmrCtr_SetLoadReg(SYSTEM_TIMER_BASEADDR, 0, time);
  XTmrCtr_SetControlStatusReg(SYSTEM_TIMER_BASEADDR, 0,
			      XTC_CSR_INT_OCCURED_MASK 
			      | XTC_CSR_LOAD_MASK);
  XTmrCtr_SetControlStatusReg(SYSTEM_TIMER_BASEADDR, 0,
			      XTC_CSR_ENABLE_TMR_MASK 
			      | XTC_CSR_AUTO_RELOAD_MASK);
}

float conv_cycles_to_secs (u32 cycles)
{
  return ((float)cycles / XPAR_CPU_CORE_CLOCK_FREQ_HZ);
}
