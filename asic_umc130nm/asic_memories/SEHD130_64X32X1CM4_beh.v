`timescale 1ns / 1ps
`include "./xdefs.v"

module SEHD130_64X32X1CM4 (A0,A1,A2,A3,A4,A5,DO0,DO1,DO2,DO3,DO4,DO5,DO6,
                           DO7,DO8,DO9,DO10,DO11,DO12,DO13,DO14,DO15,
                           DO16,DO17,DO18,DO19,DO20,DO21,DO22,DO23,
                           DO24,DO25,DO26,DO27,DO28,DO29,DO30,DO31,
                           DI0,DI1,DI2,DI3,DI4,DI5,DI6,DI7,DI8,DI9,
                           DI10,DI11,DI12,DI13,DI14,DI15,DI16,DI17,
                           DI18,DI19,DI20,DI21,DI22,DI23,DI24,DI25,
                           DI26,DI27,DI28,DI29,DI30,DI31,CK,WEB,CSB,OE);

   output     DO0,DO1,DO2,DO3,DO4,DO5,DO6,DO7,DO8,
              DO9,DO10,DO11,DO12,DO13,DO14,DO15,DO16,DO17,DO18,
              DO19,DO20,DO21,DO22,DO23,DO24,DO25,DO26,DO27,DO28,
              DO29,DO30,DO31;
   input      DI0,DI1,DI2,DI3,DI4,DI5,DI6,DI7,DI8,
              DI9,DI10,DI11,DI12,DI13,DI14,DI15,DI16,DI17,DI18,
              DI19,DI20,DI21,DI22,DI23,DI24,DI25,DI26,DI27,DI28,
              DI29,DI30,DI31;
   input      A0,A1,A2,A3,A4,A5;
   
   input      WEB;                                     
   input      CK;                                      
   input      CSB;                                     
   input      OE;                                     


   function [`CONF_ADDR_W-1:0] reverseAddrBits;    
      input [`CONF_ADDR_W-1:0] word;             
      integer 		       i;
      
      begin
	 for (i=0; i < `CONF_ADDR_W; i=i+1)
	   reverseAddrBits[i]=word[`CONF_ADDR_W-1 - i];         
      end                           
   endfunction    

   function [`DATA_W-1:0] reverseDataBits;    
      input [`DATA_W-1:0] 		    word;             
      integer 				    i;
      
      begin
	 for (i=0; i < `DATA_W; i=i+1)
	   reverseDataBits[i]=word[`DATA_W-1 - i];         
      end                           
   endfunction    



   
   wire [`DATA_W-1:0] din;
   reg [`DATA_W-1:0]  dout;
   wire [`CONF_ADDR_W-1:0] addr;
   reg [`DATA_W-1:0] 	   mem [2**`CONF_ADDR_W-1:0];
   
   assign addr =  reverseAddrBits({A0,A1,A2,A3,A4,A5});
   
   assign din = reverseDataBits({DI0,DI1,DI2,DI3,DI4,DI5,DI6,DI7,DI8,DI9,
				 DI10,DI11,DI12,DI13,DI14,DI15,DI16,DI17,
				 DI18,DI19,DI20,DI21,DI22,DI23,DI24,DI25,
				 DI26,DI27,DI28,DI29,DI30,DI31});
   
   assign {DO0,DO1,DO2,DO3,DO4,DO5,DO6,
           DO7,DO8,DO9,DO10,DO11,DO12,DO13,DO14,DO15,
           DO16,DO17,DO18,DO19,DO20,DO21,DO22,DO23,
           DO24,DO25,DO26,DO27,DO28,DO29,DO30,DO31} = 	 reverseDataBits(dout);
   
   always @ (posedge CK) begin 
      if (~CSB) begin
	 if (~WEB)
	   mem[addr] <= din;
	 if (OE)
	   dout <= mem[addr];
      end
   end
endmodule
