///////////////////////////////////////////////////////////////////////////////////
// 
//              Synchronous 2 Port Register File Compiler 
// 
//                    UMC 0.13um High Speed Logic Process 
//    __________________________________________________________________________
// 
// 
//        (C) Copyright 2016 Faraday Technology Corp. All Rights Reserved.
// 
//      This source code is an unpublished work belongs to Faraday Technology
//      Corp.  It is considered a trade secret and is not to be divulged or
//      used by parties who have not received written authorization from
//      Faraday Technology Corp.
// 
//      Faraday's home page can be found at:
//      http://www.faraday-tech.com/
//     
// ________________________________________________________________________________
// 
//       Module Name       :  SZHD130_16X32X1CM2  
//       Word              :  16                  
//       Bit               :  32                  
//       Byte              :  1                   
//       Mux               :  2                   
//       Power Ring Type   :  port                
//       Power Ring Width  :  5 (um)              
//       Output Loading    :  0.01 (pf)           
//       Input Data Slew   :  0.016 (ns)          
//       Input Clock Slew  :  0.016 (ns)          
// 
// ________________________________________________________________________________
// 
//       Library          : FSC0H_D
//       Memaker          : 201001.1.1
//       Date             : 2016/02/18 08:03:33
// 
// ________________________________________________________________________________
// 
//       Library          : FSC0H_D
//       Memaker          : 201001.1.1
//       Date             : 2016/02/18 08:03:36
//
///////////////////////////////////////////////////////////////////////////////////

model SZHD130_16X32X1CM2 (
    A0, A1, A2, A3,
    B0, B1, B2, B3,
    DI0, DI1, DI2, DI3, DI4, DI5, DI6, DI7, DI8, DI9, DI10, DI11, DI12, DI13, DI14, DI15, DI16, DI17, DI18, DI19, DI20, DI21, DI22, DI23, DI24, DI25, DI26, DI27, DI28, DI29, DI30, DI31,
    DO0, DO1, DO2, DO3, DO4, DO5, DO6, DO7, DO8, DO9, DO10, DO11, DO12, DO13, DO14, DO15, DO16, DO17, DO18, DO19, DO20, DO21, DO22, DO23, DO24, DO25, DO26, DO27, DO28, DO29, DO30, DO31,
    WEB,
    CKA, CKB, CSAN, CSBN) (

    input (WEB, CKA, CKB, CSAN, CSBN) ()
    input (A0, A1, A2, A3) ()
    input (B0, B1, B2, B3) ()
    input (DI0, DI1, DI2, DI3, DI4, DI5, DI6, DI7, DI8, DI9, DI10, DI11, DI12, DI13, DI14, DI15, DI16, DI17, DI18, DI19, DI20, DI21, DI22, DI23, DI24, DI25, DI26, DI27, DI28, DI29, DI30, DI31) ()

    intern(WE)(primitive = _inv (WEB, WE);)
    intern(WECS)(primitive = _and (WE,CSB,WECS);)
    intern(CSA)(primitive = _inv (CSAN, CSA);)
    intern(CSB)(primitive = _inv (CSBN, CSB);)
    intern(WEN)(primitive = _and (WE,CSB,WEN);)
    intern(REN)(primitive = _and (WEB,CSA,REN);)
    intern(OE)(primitive = _tie1 (OE);)

    output (DO0, DO1, DO2, DO3, DO4, DO5, DO6, DO7, DO8, DO9, DO10, DO11, DO12, DO13, DO14, DO15, DO16, DO17, DO18, DO19, DO20, DO21, DO22, DO23, DO24, DO25, DO26, DO27, DO28, DO29, DO30, DO31) (
        data_size = 32;
        address_size = 4;
        min_address = 0;
        max_address = 15;
        edge_trigger = WR;
        read_write_conflict = XX;
        primitive = _cram(, ,
            _write{H,H,H} (CKB, WECS, B0, B1, B2, B3, DI0, DI1, DI2, DI3, DI4, DI5, DI6, DI7, DI8, DI9, DI10, DI11, DI12, DI13, DI14, DI15, DI16, DI17, DI18, DI19, DI20, DI21, DI22, DI23, DI24, DI25, DI26, DI27, DI28, DI29, DI30, DI31),
            _read{1,H,H,H} (OE, CKA, REN, A0, A1, A2, A3, DO0, DO1, DO2, DO3, DO4, DO5, DO6, DO7, DO8, DO9, DO10, DO11, DO12, DO13, DO14, DO15, DO16, DO17, DO18, DO19, DO20, DO21, DO22, DO23, DO24, DO25, DO26, DO27, DO28, DO29, DO30, DO31)
        );
    )
)
