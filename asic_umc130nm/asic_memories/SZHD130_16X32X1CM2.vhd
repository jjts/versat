-- |-----------------------------------------------------------------------|
-- 
--              Synchronous 2 Port Register File Compiler 
-- 
--                    UMC 0.13um High Speed Logic Process 
--    __________________________________________________________________________
-- 
-- 
--        (C) Copyright 2016 Faraday Technology Corp. All Rights Reserved.
-- 
--      This source code is an unpublished work belongs to Faraday Technology
--      Corp.  It is considered a trade secret and is not to be divulged or
--      used by parties who have not received written authorization from
--      Faraday Technology Corp.
-- 
--      Faraday's home page can be found at:
--      http://www.faraday-tech.com/
--     
-- ________________________________________________________________________________
-- 
--       Module Name       :  SZHD130_16X32X1CM2  
--       Word              :  16                  
--       Bit               :  32                  
--       Byte              :  1                   
--       Mux               :  2                   
--       Power Ring Type   :  port                
--       Power Ring Width  :  5 (um)              
--       Output Loading    :  0.01 (pf)           
--       Input Data Slew   :  0.016 (ns)          
--       Input Clock Slew  :  0.016 (ns)          
-- 
-- ________________________________________________________________________________
-- 
--       Library          : FSC0H_D
--       Memaker          : 201001.1.1
--       Date             : 2016/02/18 08:03:33
-- 
-- ________________________________________________________________________________
-- 
--
-- Notice on usage: Fixed delay or timing data are given in this model.
--                  It supports SDF back-annotation, please generate SDF file
--                  by EDA tools to get the accurate timing.
--
-- |-----------------------------------------------------------------------|
--
-- Warning : 
--   If customer's design viloate the set-up time or hold time criteria of 
--   synchronous SRAM, it's possible to hit the meta-stable point of 
--   latch circuit in the decoder and cause the data loss in the memory 
--   bitcell. So please follow the memory IP's spec to design your 
--   product.
--
-- |-----------------------------------------------------------------------|
--
--       Library          : FSC0H_D
--       Memaker          : 201001.1.1
--       Date             : 2016/02/18 08:03:33
--
-- |-----------------------------------------------------------------------|

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.VITAL_Primitives.all;
use IEEE.VITAL_Timing.all;
use std.textio.all;
use IEEE.std_logic_textio.all;

-- entity declaration --
entity SZHD130_16X32X1CM2 is
   generic(
      SYN_CS:      integer  := 1;
      NO_SER_TOH:  integer  := 1;
      AddressSize: integer  := 4;
      Bits:        integer  := 32;
      Words:       integer  := 16;
      Bytes:       integer  := 1;
      AspectRatio: integer  := 2;
      Tr2w:        time     := 0.901 ns;
      Tw2r:        time     := 0.901 ns;
      TOH:         time     := 0.300 ns;

      TimingChecksOn: Boolean := True;
      InstancePath: STRING := "*";
      Xon: Boolean := True;
      MsgOn: Boolean := True;


      tpd_CKA_DO0_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO1_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO2_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO3_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO4_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO5_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO6_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO7_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO8_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO9_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO10_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO11_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO12_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO13_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO14_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO15_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO16_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO17_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO18_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO19_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO20_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO21_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO22_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO23_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO24_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO25_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO26_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO27_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO28_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO29_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO30_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);
      tpd_CKA_DO31_posedge : VitalDelayType01 :=  (0.708 ns, 0.708 ns);

      tsetup_A0_CKA_posedge_posedge    :  VitalDelayType := 0.126 ns;
      tsetup_A0_CKA_negedge_posedge    :  VitalDelayType := 0.126 ns;
      tsetup_A1_CKA_posedge_posedge    :  VitalDelayType := 0.126 ns;
      tsetup_A1_CKA_negedge_posedge    :  VitalDelayType := 0.126 ns;
      tsetup_A2_CKA_posedge_posedge    :  VitalDelayType := 0.126 ns;
      tsetup_A2_CKA_negedge_posedge    :  VitalDelayType := 0.126 ns;
      tsetup_A3_CKA_posedge_posedge    :  VitalDelayType := 0.126 ns;
      tsetup_A3_CKA_negedge_posedge    :  VitalDelayType := 0.126 ns;
      tsetup_B0_CKB_posedge_posedge    :  VitalDelayType := 0.126 ns;
      tsetup_B0_CKB_negedge_posedge    :  VitalDelayType := 0.126 ns;
      tsetup_B1_CKB_posedge_posedge    :  VitalDelayType := 0.126 ns;
      tsetup_B1_CKB_negedge_posedge    :  VitalDelayType := 0.126 ns;
      tsetup_B2_CKB_posedge_posedge    :  VitalDelayType := 0.126 ns;
      tsetup_B2_CKB_negedge_posedge    :  VitalDelayType := 0.126 ns;
      tsetup_B3_CKB_posedge_posedge    :  VitalDelayType := 0.126 ns;
      tsetup_B3_CKB_negedge_posedge    :  VitalDelayType := 0.126 ns;
      thold_A0_CKA_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_A0_CKA_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_A1_CKA_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_A1_CKA_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_A2_CKA_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_A2_CKA_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_A3_CKA_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_A3_CKA_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_B0_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_B0_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_B1_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_B1_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_B2_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_B2_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_B3_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_B3_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      tsetup_DI0_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI0_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI1_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI1_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI2_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI2_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI3_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI3_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI4_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI4_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI5_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI5_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI6_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI6_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI7_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI7_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI8_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI8_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI9_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI9_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI10_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI10_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI11_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI11_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI12_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI12_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI13_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI13_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI14_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI14_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI15_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI15_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI16_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI16_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI17_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI17_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI18_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI18_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI19_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI19_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI20_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI20_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI21_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI21_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI22_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI22_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI23_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI23_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI24_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI24_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI25_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI25_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI26_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI26_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI27_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI27_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI28_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI28_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI29_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI29_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI30_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI30_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI31_CKB_posedge_posedge    :  VitalDelayType := 0.155 ns;
      tsetup_DI31_CKB_negedge_posedge    :  VitalDelayType := 0.155 ns;
      thold_DI0_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI0_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI1_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI1_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI2_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI2_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI3_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI3_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI4_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI4_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI5_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI5_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI6_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI6_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI7_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI7_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI8_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI8_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI9_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI9_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI10_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI10_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI11_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI11_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI12_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI12_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI13_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI13_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI14_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI14_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI15_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI15_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI16_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI16_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI17_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI17_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI18_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI18_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI19_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI19_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI20_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI20_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI21_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI21_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI22_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI22_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI23_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI23_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI24_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI24_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI25_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI25_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI26_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI26_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI27_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI27_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI28_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI28_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI29_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI29_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI30_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI30_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI31_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_DI31_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      tsetup_WEB_CKB_posedge_posedge   :  VitalDelayType := 0.100 ns;
      tsetup_WEB_CKB_negedge_posedge   :  VitalDelayType := 0.100 ns;
      thold_WEB_CKB_posedge_posedge    :  VitalDelayType := 0.100 ns;
      thold_WEB_CKB_negedge_posedge    :  VitalDelayType := 0.100 ns;
      tsetup_CSAN_CKA_posedge_posedge    :  VitalDelayType := 0.189 ns;
      tsetup_CSAN_CKA_negedge_posedge    :  VitalDelayType := 0.189 ns;
      tsetup_CSBN_CKB_posedge_posedge    :  VitalDelayType := 0.189 ns;
      tsetup_CSBN_CKB_negedge_posedge    :  VitalDelayType := 0.189 ns;
      thold_CSAN_CKA_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_CSAN_CKA_negedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_CSBN_CKB_posedge_posedge     :  VitalDelayType := 0.100 ns;
      thold_CSBN_CKB_negedge_posedge     :  VitalDelayType := 0.100 ns;
      tperiod_CKA                       :  VitalDelayType := 0.901 ns;
      tperiod_CKB                       :  VitalDelayType := 0.901 ns;
      tpw_CKA_posedge                   :  VitalDelayType := 0.300 ns;
      tpw_CKB_posedge                   :  VitalDelayType := 0.300 ns;
      tpw_CKA_negedge                   :  VitalDelayType := 0.300 ns;
      tpw_CKB_negedge                   :  VitalDelayType := 0.300 ns;
      tipd_A0                     :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_A1                     :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_A2                     :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_A3                     :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_B0                     :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_B1                     :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_B2                     :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_B3                     :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI0                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI1                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI2                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI3                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI4                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI5                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI6                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI7                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI8                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI9                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI10                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI11                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI12                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI13                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI14                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI15                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI16                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI17                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI18                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI19                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI20                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI21                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI22                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI23                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI24                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI25                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI26                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI27                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI28                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI29                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI30                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_DI31                    :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_WEB                       :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_CSAN                        :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_CSBN                        :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_CKA                        :  VitalDelayType01 := (0.000 ns, 0.000 ns);
      tipd_CKB                        :  VitalDelayType01 := (0.000 ns, 0.000 ns)
      );

   port(
      A0                            :   IN   std_logic;
      A1                            :   IN   std_logic;
      A2                            :   IN   std_logic;
      A3                            :   IN   std_logic;
      B0                            :   IN   std_logic;
      B1                            :   IN   std_logic;
      B2                            :   IN   std_logic;
      B3                            :   IN   std_logic;
      DO0                        :   OUT   std_logic;
      DO1                        :   OUT   std_logic;
      DO2                        :   OUT   std_logic;
      DO3                        :   OUT   std_logic;
      DO4                        :   OUT   std_logic;
      DO5                        :   OUT   std_logic;
      DO6                        :   OUT   std_logic;
      DO7                        :   OUT   std_logic;
      DO8                        :   OUT   std_logic;
      DO9                        :   OUT   std_logic;
      DO10                        :   OUT   std_logic;
      DO11                        :   OUT   std_logic;
      DO12                        :   OUT   std_logic;
      DO13                        :   OUT   std_logic;
      DO14                        :   OUT   std_logic;
      DO15                        :   OUT   std_logic;
      DO16                        :   OUT   std_logic;
      DO17                        :   OUT   std_logic;
      DO18                        :   OUT   std_logic;
      DO19                        :   OUT   std_logic;
      DO20                        :   OUT   std_logic;
      DO21                        :   OUT   std_logic;
      DO22                        :   OUT   std_logic;
      DO23                        :   OUT   std_logic;
      DO24                        :   OUT   std_logic;
      DO25                        :   OUT   std_logic;
      DO26                        :   OUT   std_logic;
      DO27                        :   OUT   std_logic;
      DO28                        :   OUT   std_logic;
      DO29                        :   OUT   std_logic;
      DO30                        :   OUT   std_logic;
      DO31                        :   OUT   std_logic;
      DI0                        :   IN   std_logic;
      DI1                        :   IN   std_logic;
      DI2                        :   IN   std_logic;
      DI3                        :   IN   std_logic;
      DI4                        :   IN   std_logic;
      DI5                        :   IN   std_logic;
      DI6                        :   IN   std_logic;
      DI7                        :   IN   std_logic;
      DI8                        :   IN   std_logic;
      DI9                        :   IN   std_logic;
      DI10                        :   IN   std_logic;
      DI11                        :   IN   std_logic;
      DI12                        :   IN   std_logic;
      DI13                        :   IN   std_logic;
      DI14                        :   IN   std_logic;
      DI15                        :   IN   std_logic;
      DI16                        :   IN   std_logic;
      DI17                        :   IN   std_logic;
      DI18                        :   IN   std_logic;
      DI19                        :   IN   std_logic;
      DI20                        :   IN   std_logic;
      DI21                        :   IN   std_logic;
      DI22                        :   IN   std_logic;
      DI23                        :   IN   std_logic;
      DI24                        :   IN   std_logic;
      DI25                        :   IN   std_logic;
      DI26                        :   IN   std_logic;
      DI27                        :   IN   std_logic;
      DI28                        :   IN   std_logic;
      DI29                        :   IN   std_logic;
      DI30                        :   IN   std_logic;
      DI31                        :   IN   std_logic;
      WEB                           :   IN   std_logic;
      CKA                            :   IN   std_logic;
      CKB                            :   IN   std_logic;
      CSAN                            :   IN   std_logic;
      CSBN                            :   IN   std_logic
      );

attribute VITAL_LEVEL0 of SZHD130_16X32X1CM2 : entity is TRUE;

end SZHD130_16X32X1CM2;

-- architecture body --
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.VITAL_Primitives.all;
use IEEE.VITAL_Timing.all;

architecture behavior of SZHD130_16X32X1CM2 is
   -- attribute VITALMEMORY_LEVEL1 of behavior : architecture is TRUE;

   CONSTANT True_flg:       integer := 0;
   CONSTANT False_flg:      integer := 1;
   CONSTANT Range_flg:      integer := 2;

   FUNCTION Minimum ( CONSTANT t1, t2 : IN TIME ) RETURN TIME IS
   BEGIN
      IF (t1 < t2) THEN RETURN (t1); ELSE RETURN (t2); END IF;
   END Minimum;

   FUNCTION Maximum ( CONSTANT t1, t2 : IN TIME ) RETURN TIME IS
   BEGIN
      IF (t1 < t2) THEN RETURN (t2); ELSE RETURN (t1); END IF;
   END Maximum;

   FUNCTION BVtoI(bin: std_logic_vector) RETURN integer IS
      variable result: integer;
   BEGIN
      result := 0;
      for i in bin'range loop
         if bin(i) = '1' then
            result := result + 2**i;
         end if;
      end loop;
      return result;
   END; -- BVtoI

   PROCEDURE ScheduleOutputDelay (
       SIGNAL   OutSignal        : OUT std_logic;
       VARIABLE Data             : IN  std_logic;
       CONSTANT Delay            : IN  VitalDelayType01 := VitalDefDelay01;
       VARIABLE Previous_A       : IN  std_logic_vector(AddressSize-1 downto 0);
       VARIABLE Current_A        : IN  std_logic_vector(AddressSize-1 downto 0);
       CONSTANT NO_SER_TOH       : IN  integer
   ) IS
   BEGIN

      if (NO_SER_TOH /= 1) then
         OutSignal <= TRANSPORT 'X' AFTER TOH;
         OutSignal <= TRANSPORT Data AFTER Maximum(Delay(tr10), Delay(tr01));
      else
         if (Current_A /= Previous_A) then
            OutSignal <= TRANSPORT 'X' AFTER TOH;
            OutSignal <= TRANSPORT Data AFTER Maximum(Delay(tr10), Delay(tr01));
         else
            OutSignal <= TRANSPORT Data AFTER Maximum(Delay(tr10), Delay(tr01));
         end if;
      end if;
   END ScheduleOutputDelay;

   FUNCTION TO_INTEGER (
     a: std_logic_vector
   ) RETURN INTEGER IS
     VARIABLE y: INTEGER := 0;
   BEGIN
        y := 0;
        FOR i IN a'RANGE LOOP
            y := y * 2;
            IF a(i) /= '1' AND a(i) /= '0' THEN
                y := 0;
                EXIT;
            ELSIF a(i) = '1' THEN
                y := y + 1;
            END IF;
        END LOOP;
        RETURN y;
   END TO_INTEGER;

   function AddressRangeCheck(AddressItem: std_logic_vector; flag_Address: integer) return integer is
     variable Uresult : std_logic;
     variable status  : integer := 0;

   begin
      if (Bits /= 1) then
         Uresult := AddressItem(0) xor AddressItem(1);
         for i in 2 to AddressItem'length-1 loop
            Uresult := Uresult xor AddressItem(i);
         end loop;
      else
         Uresult := AddressItem(0);
      end if;

      if (Uresult = 'U') then
         status := False_flg;
      elsif (Uresult = 'X') then
         status := False_flg;
      elsif (Uresult = 'Z') then
         status := False_flg;
      else
         status := True_flg;
      end if;

      if (status=False_flg) then
        if (flag_Address = True_flg) then
           -- Generate Error Messae --
           assert FALSE report "** MEM_Error: Unknown value occurred in Address." severity WARNING;
        end if;
      end if;

      if (status=True_flg) then
         if ((BVtoI(AddressItem)) >= Words) then
             assert FALSE report "** MEM_Error: Out of range occurred in Address." severity WARNING;
             status := Range_flg;
         else
             status := True_flg;
         end if;
      end if;

      return status;
   end AddressRangeCheck;

   function CS_monitor(CSItem: std_logic; flag_CS: integer) return integer is
     variable status  : integer := 0;

   begin
      if (CSItem = 'U') then
         status := False_flg;
      elsif (CSItem = 'X') then
         status := False_flg;
      elsif (CSItem = 'Z') then
         status := False_flg;
      else
         status := True_flg;
      end if;

      if (status=False_flg) then
        if (flag_CS = True_flg) then
           -- Generate Error Messae --
           assert FALSE report "** MEM_Error: Unknown value occurred in ChipSelect." severity WARNING;
        end if;
      end if;

      return status;
   end CS_monitor;

   Type memoryArray Is array (Words-1 downto 0) Of std_logic_vector (Bits-1 downto 0);

   SIGNAL CSAN_ipd        : std_logic := 'X';
   SIGNAL CSBN_ipd        : std_logic := 'X';
   SIGNAL CKA_ipd         : std_logic := 'X';
   SIGNAL CKB_ipd         : std_logic := 'X';
   SIGNAL A_ipd          : std_logic_vector(AddressSize-1 downto 0) := (others => 'X');
   SIGNAL B_ipd          : std_logic_vector(AddressSize-1 downto 0) := (others => 'X');
   SIGNAL WEB_ipd       : std_logic := 'X';
   SIGNAL DI_ipd        : std_logic_vector(Bits-1 downto 0) := (others => 'X');
   SIGNAL DO_int        : std_logic_vector(Bits-1 downto 0) := (others => 'X');

begin

   ---------------------
   --  INPUT PATH DELAYs
   ---------------------
   WireDelay : block
   begin
   VitalWireDelay (CKA_ipd, CKA, tipd_CKA);
   VitalWireDelay (CKB_ipd, CKB, tipd_CKB);
   VitalWireDelay (CSAN_ipd, CSAN, tipd_CSAN);
   VitalWireDelay (CSBN_ipd, CSBN, tipd_CSBN);
   VitalWireDelay (WEB_ipd, WEB, tipd_WEB);
   VitalWireDelay (A_ipd(0), A0, tipd_A0);
   VitalWireDelay (A_ipd(1), A1, tipd_A1);
   VitalWireDelay (A_ipd(2), A2, tipd_A2);
   VitalWireDelay (A_ipd(3), A3, tipd_A3);
   VitalWireDelay (B_ipd(0), B0, tipd_B0);
   VitalWireDelay (B_ipd(1), B1, tipd_B1);
   VitalWireDelay (B_ipd(2), B2, tipd_B2);
   VitalWireDelay (B_ipd(3), B3, tipd_B3);
   VitalWireDelay (DI_ipd(0), DI0, tipd_DI0);
   VitalWireDelay (DI_ipd(1), DI1, tipd_DI1);
   VitalWireDelay (DI_ipd(2), DI2, tipd_DI2);
   VitalWireDelay (DI_ipd(3), DI3, tipd_DI3);
   VitalWireDelay (DI_ipd(4), DI4, tipd_DI4);
   VitalWireDelay (DI_ipd(5), DI5, tipd_DI5);
   VitalWireDelay (DI_ipd(6), DI6, tipd_DI6);
   VitalWireDelay (DI_ipd(7), DI7, tipd_DI7);
   VitalWireDelay (DI_ipd(8), DI8, tipd_DI8);
   VitalWireDelay (DI_ipd(9), DI9, tipd_DI9);
   VitalWireDelay (DI_ipd(10), DI10, tipd_DI10);
   VitalWireDelay (DI_ipd(11), DI11, tipd_DI11);
   VitalWireDelay (DI_ipd(12), DI12, tipd_DI12);
   VitalWireDelay (DI_ipd(13), DI13, tipd_DI13);
   VitalWireDelay (DI_ipd(14), DI14, tipd_DI14);
   VitalWireDelay (DI_ipd(15), DI15, tipd_DI15);
   VitalWireDelay (DI_ipd(16), DI16, tipd_DI16);
   VitalWireDelay (DI_ipd(17), DI17, tipd_DI17);
   VitalWireDelay (DI_ipd(18), DI18, tipd_DI18);
   VitalWireDelay (DI_ipd(19), DI19, tipd_DI19);
   VitalWireDelay (DI_ipd(20), DI20, tipd_DI20);
   VitalWireDelay (DI_ipd(21), DI21, tipd_DI21);
   VitalWireDelay (DI_ipd(22), DI22, tipd_DI22);
   VitalWireDelay (DI_ipd(23), DI23, tipd_DI23);
   VitalWireDelay (DI_ipd(24), DI24, tipd_DI24);
   VitalWireDelay (DI_ipd(25), DI25, tipd_DI25);
   VitalWireDelay (DI_ipd(26), DI26, tipd_DI26);
   VitalWireDelay (DI_ipd(27), DI27, tipd_DI27);
   VitalWireDelay (DI_ipd(28), DI28, tipd_DI28);
   VitalWireDelay (DI_ipd(29), DI29, tipd_DI29);
   VitalWireDelay (DI_ipd(30), DI30, tipd_DI30);
   VitalWireDelay (DI_ipd(31), DI31, tipd_DI31);

   end block;

   VitalBUF    (q      => DO0,
                a      => DO_int(0)
               );
   VitalBUF    (q      => DO1,
                a      => DO_int(1)
               );
   VitalBUF    (q      => DO2,
                a      => DO_int(2)
               );
   VitalBUF    (q      => DO3,
                a      => DO_int(3)
               );
   VitalBUF    (q      => DO4,
                a      => DO_int(4)
               );
   VitalBUF    (q      => DO5,
                a      => DO_int(5)
               );
   VitalBUF    (q      => DO6,
                a      => DO_int(6)
               );
   VitalBUF    (q      => DO7,
                a      => DO_int(7)
               );
   VitalBUF    (q      => DO8,
                a      => DO_int(8)
               );
   VitalBUF    (q      => DO9,
                a      => DO_int(9)
               );
   VitalBUF    (q      => DO10,
                a      => DO_int(10)
               );
   VitalBUF    (q      => DO11,
                a      => DO_int(11)
               );
   VitalBUF    (q      => DO12,
                a      => DO_int(12)
               );
   VitalBUF    (q      => DO13,
                a      => DO_int(13)
               );
   VitalBUF    (q      => DO14,
                a      => DO_int(14)
               );
   VitalBUF    (q      => DO15,
                a      => DO_int(15)
               );
   VitalBUF    (q      => DO16,
                a      => DO_int(16)
               );
   VitalBUF    (q      => DO17,
                a      => DO_int(17)
               );
   VitalBUF    (q      => DO18,
                a      => DO_int(18)
               );
   VitalBUF    (q      => DO19,
                a      => DO_int(19)
               );
   VitalBUF    (q      => DO20,
                a      => DO_int(20)
               );
   VitalBUF    (q      => DO21,
                a      => DO_int(21)
               );
   VitalBUF    (q      => DO22,
                a      => DO_int(22)
               );
   VitalBUF    (q      => DO23,
                a      => DO_int(23)
               );
   VitalBUF    (q      => DO24,
                a      => DO_int(24)
               );
   VitalBUF    (q      => DO25,
                a      => DO_int(25)
               );
   VitalBUF    (q      => DO26,
                a      => DO_int(26)
               );
   VitalBUF    (q      => DO27,
                a      => DO_int(27)
               );
   VitalBUF    (q      => DO28,
                a      => DO_int(28)
               );
   VitalBUF    (q      => DO29,
                a      => DO_int(29)
               );
   VitalBUF    (q      => DO30,
                a      => DO_int(30)
               );
   VitalBUF    (q      => DO31,
                a      => DO_int(31)
               );

   --------------------
   --  BEHAVIOR SECTION
   --------------------
   VITALBehavior : PROCESS (CSAN_ipd, 
                            CSBN_ipd,
                            A_ipd,
                            B_ipd,
                            WEB_ipd,
                            DI_ipd,
                            CKA_ipd,
                            CKB_ipd)

   -- timing check results
   VARIABLE Tviol_A_CKA_posedge  : STD_ULOGIC := '0';
   VARIABLE Tviol_B_CKB_posedge  : STD_ULOGIC := '0';
   VARIABLE Tviol_WEB_CKB_posedge  : STD_ULOGIC := '0';
   VARIABLE Tviol_DI_CKB_posedge  : STD_ULOGIC := '0';
   VARIABLE Tviol_CSAN_CKA_posedge  : STD_ULOGIC := '0';
   VARIABLE Tviol_CSBN_CKB_posedge  : STD_ULOGIC := '0';

   VARIABLE Pviol_CKA    : STD_ULOGIC := '0';
   VARIABLE Pviol_CKB    : STD_ULOGIC := '0';
   VARIABLE Pdata_CKA    : VitalPeriodDataType := VitalPeriodDataInit;
   VARIABLE Pdata_CKB    : VitalPeriodDataType := VitalPeriodDataInit;

   VARIABLE Tmkr_A_CKA_posedge   : VitalTimingDataType := VitalTimingDataInit;
   VARIABLE Tmkr_B_CKB_posedge   : VitalTimingDataType := VitalTimingDataInit;
   VARIABLE Tmkr_WEB_CKB_posedge   : VitalTimingDataType := VitalTimingDataInit;
   VARIABLE Tmkr_DI_CKB_posedge   : VitalTimingDataType := VitalTimingDataInit;
   VARIABLE Tmkr_CSAN_CKA_posedge   : VitalTimingDataType := VitalTimingDataInit;
   VARIABLE Tmkr_CSBN_CKB_posedge   : VitalTimingDataType := VitalTimingDataInit;

   VARIABLE DO_zd : std_logic_vector(Bits-1 downto 0) := (others => 'X');
   VARIABLE memoryCore  : memoryArray;

   VARIABLE cka_change   : std_logic_vector(1 downto 0);
   VARIABLE ckb_change   : std_logic_vector(1 downto 0);
   VARIABLE web_cs      : std_logic_vector(1 downto 0);

   -- previous latch data
   VARIABLE Latch_A        : std_logic_vector(AddressSize-1 downto 0) := (others => 'X');
   VARIABLE Latch_B        : std_logic_vector(AddressSize-1 downto 0) := (others => 'X');
   VARIABLE Latch_DI       : std_logic_vector(Bits-1 downto 0) := (others => 'X');
   VARIABLE Latch_WEB      : std_logic := 'X';
   VARIABLE Latch_CSAN       : std_logic := 'X';
   VARIABLE Latch_CSBN       : std_logic := 'X';

   -- internal latch data
   VARIABLE A_i            : std_logic_vector(AddressSize-1 downto 0) := (others => 'X');
   VARIABLE B_i            : std_logic_vector(AddressSize-1 downto 0) := (others => 'X');
   VARIABLE DI_i           : std_logic_vector(Bits-1 downto 0) := (others => 'X');
   VARIABLE WEB_i          : std_logic := 'X';
   VARIABLE CSAN_i           : std_logic := 'X';
   VARIABLE CSBN_i           : std_logic := 'X';

   VARIABLE last_A         : std_logic_vector(AddressSize-1 downto 0) := (others => 'X');

   VARIABLE LastClkAEdge    : std_logic := 'X';
   VARIABLE LastClkBEdge    : std_logic := 'X';

   VARIABLE flag_A: integer   := True_flg;
   VARIABLE flag_B: integer   := True_flg;
   VARIABLE flag_CSAN: integer   := True_flg;
   VARIABLE flag_CSBN: integer   := True_flg;

   VARIABLE LastCycleRAddress  : std_logic_vector(AddressSize-1 downto 0) := (others => 'X');
   VARIABLE Last_tc_ClkA_PosEdge     : time := 0 ns;
   VARIABLE LastCycleWAddress  : std_logic_vector(AddressSize-1 downto 0) := (others => 'X');
   VARIABLE Last_tc_ClkB_PosEdge_WEB : time := 0 ns;

   begin

   ------------------------
   --  Timing Check Section
   ------------------------
   if (TimingChecksOn) then
         VitalSetupHoldCheck (
          Violation               => Tviol_A_CKA_posedge,
          TimingData              => Tmkr_A_CKA_posedge,
          TestSignal              => A_ipd,
          TestSignalName          => "A",
          TestDelay               => 0 ns,
          RefSignal               => CKA_ipd,
          RefSignalName           => "CKA",
          RefDelay                => 0 ns,
          SetupHigh               => tsetup_A0_CKA_posedge_posedge,
          SetupLow                => tsetup_A0_CKA_negedge_posedge,
          HoldHigh                => thold_A0_CKA_negedge_posedge,
          HoldLow                 => thold_A0_CKA_posedge_posedge,
          CheckEnabled            =>
                           NOW /= 0 ns AND CSAN_ipd = '0',
          RefTransition           => 'R',
          HeaderMsg               => InstancePath & "/SZHD130_16X32X1CM2",
          Xon                     => Xon,
          MsgOn                   => MsgOn,
          MsgSeverity             => WARNING);

         VitalSetupHoldCheck (
          Violation               => Tviol_B_CKB_posedge,
          TimingData              => Tmkr_B_CKB_posedge,
          TestSignal              => B_ipd,
          TestSignalName          => "B",
          TestDelay               => 0 ns,
          RefSignal               => CKB_ipd,
          RefSignalName           => "CKB",
          RefDelay                => 0 ns,
          SetupHigh               => tsetup_B0_CKB_posedge_posedge,
          SetupLow                => tsetup_B0_CKB_negedge_posedge,
          HoldHigh                => thold_B0_CKB_negedge_posedge,
          HoldLow                 => thold_B0_CKB_posedge_posedge,
          CheckEnabled            =>
                           NOW /= 0 ns AND CSBN_ipd = '0' AND WEB_ipd /= '1',
          RefTransition           => 'R',
          HeaderMsg               => InstancePath & "/SZHD130_16X32X1CM2",
          Xon                     => Xon,
          MsgOn                   => MsgOn,
          MsgSeverity             => WARNING);

         VitalSetupHoldCheck (
          Violation               => Tviol_WEB_CKB_posedge,
          TimingData              => Tmkr_WEB_CKB_posedge,
          TestSignal              => WEB_ipd,
          TestSignalName          => "WEB",
          TestDelay               => 0 ns,
          RefSignal               => CKB_ipd,
          RefSignalName           => "CKB",
          RefDelay                => 0 ns,
          SetupHigh               => tsetup_WEB_CKB_posedge_posedge,
          SetupLow                => tsetup_WEB_CKB_negedge_posedge,
          HoldHigh                => thold_WEB_CKB_negedge_posedge,
          HoldLow                 => thold_WEB_CKB_posedge_posedge,
          CheckEnabled            =>
                           NOW /= 0 ns AND CSBN_ipd = '0',
          RefTransition           => 'R',
          HeaderMsg               => InstancePath & "/SZHD130_16X32X1CM2",
          Xon                     => Xon,
          MsgOn                   => MsgOn,
          MsgSeverity             => WARNING);

         VitalSetupHoldCheck (
          Violation               => Tviol_DI_CKB_posedge,
          TimingData              => Tmkr_DI_CKB_posedge,
          TestSignal              => DI_ipd,
          TestSignalName          => "DI",
          TestDelay               => 0 ns,
          RefSignal               => CKB_ipd,
          RefSignalName           => "CKB",
          RefDelay                => 0 ns,
          SetupHigh               => tsetup_DI0_CKB_posedge_posedge,
          SetupLow                => tsetup_DI0_CKB_negedge_posedge,
          HoldHigh                => thold_DI0_CKB_negedge_posedge,
          HoldLow                 => thold_DI0_CKB_posedge_posedge,
          CheckEnabled            =>
                           NOW /= 0 ns AND CSBN_ipd = '0' AND WEB_ipd /= '1',
          RefTransition           => 'R',
          HeaderMsg               => InstancePath & "/SZHD130_16X32X1CM2",
          Xon                     => Xon,
          MsgOn                   => MsgOn,
          MsgSeverity             => WARNING);

         VitalSetupHoldCheck (
          Violation               => Tviol_CSAN_CKA_posedge,
          TimingData              => Tmkr_CSAN_CKA_posedge,
          TestSignal              => CSAN_ipd,
          TestSignalName          => "CSAN",
          TestDelay               => 0 ns,
          RefSignal               => CKA_ipd,
          RefSignalName           => "CKA",
          RefDelay                => 0 ns,
          SetupHigh               => tsetup_CSAN_CKA_posedge_posedge,
          SetupLow                => tsetup_CSAN_CKA_negedge_posedge,
          HoldHigh                => thold_CSAN_CKA_negedge_posedge,
          HoldLow                 => thold_CSAN_CKA_posedge_posedge,
          CheckEnabled            => NOW /= 0 ns,
          RefTransition           => 'R',
          HeaderMsg               => InstancePath & "/SZHD130_16X32X1CM2",
          Xon                     => Xon,
          MsgOn                   => MsgOn,
          MsgSeverity             => WARNING);

         VitalSetupHoldCheck (
          Violation               => Tviol_CSBN_CKB_posedge,
          TimingData              => Tmkr_CSBN_CKB_posedge,
          TestSignal              => CSBN_ipd,
          TestSignalName          => "CSBN",
          TestDelay               => 0 ns,
          RefSignal               => CKB_ipd,
          RefSignalName           => "CKB",
          RefDelay                => 0 ns,
          SetupHigh               => tsetup_CSBN_CKB_posedge_posedge,
          SetupLow                => tsetup_CSBN_CKB_negedge_posedge,
          HoldHigh                => thold_CSBN_CKB_negedge_posedge,
          HoldLow                 => thold_CSBN_CKB_posedge_posedge,
          CheckEnabled            => NOW /= 0 ns,
          RefTransition           => 'R',
          HeaderMsg               => InstancePath & "/SZHD130_16X32X1CM2",
          Xon                     => Xon,
          MsgOn                   => MsgOn,
          MsgSeverity             => WARNING);

         VitalPeriodPulseCheck (
          Violation               => Pviol_CKA,
          PeriodData              => Pdata_CKA,
          TestSignal              => CKA_ipd,
          TestSignalName          => "CKA",
          TestDelay               => 0 ns,
          Period                  => tperiod_CKA,
          PulseWidthHigh          => tpw_CKA_posedge,
          PulseWidthLow           => tpw_CKA_negedge,
          CheckEnabled            => NOW /= 0 ns AND CSAN_ipd = '0',
          HeaderMsg               => InstancePath & "/SZHD130_16X32X1CM2",
          Xon                     => Xon,
          MsgOn                   => MsgOn,
          MsgSeverity             => WARNING);

         VitalPeriodPulseCheck (
          Violation               => Pviol_CKB,
          PeriodData              => Pdata_CKB,
          TestSignal              => CKB_ipd,
          TestSignalName          => "CKB",
          TestDelay               => 0 ns,
          Period                  => tperiod_CKB,
          PulseWidthHigh          => tpw_CKB_posedge,
          PulseWidthLow           => tpw_CKB_negedge,
          CheckEnabled            => NOW /= 0 ns AND CSBN_ipd = '0',
          HeaderMsg               => InstancePath & "/SZHD130_16X32X1CM2",
          Xon                     => Xon,
          MsgOn                   => MsgOn,
          MsgSeverity             => WARNING);
   end if;

   -------------------------
   --  Functionality Section
   -------------------------

       if (CSAN_ipd = '1' and CSAN_ipd'event) then
          if (SYN_CS = 0) then
             DO_zd := (OTHERS => 'X');
             DO_int <= TRANSPORT (OTHERS => 'X') AFTER TOH;
          end if;
       end if;

       if (CKA_ipd'event) then
         cka_change := LastClkAEdge&CKA_ipd;
         case cka_change is
            when "01"   =>
                if (CS_monitor(CSAN_ipd,flag_CSAN) = True_flg) then
                   -- Reduce error message --
                   flag_CSAN := True_flg;
                else
                   flag_CSAN := False_flg;
                end if;

                Latch_A    := A_ipd;
                Latch_CSAN  := CSAN_ipd;

                -- read_memory_function
                A_i    := Latch_A;
                CSAN_i  := Latch_CSAN;

                case CSAN_i is
                   when '0' => 
                       -------- Reduce error message --------------------------
                       if (AddressRangeCheck(A_i,flag_A) = True_flg) then
                           if ((A_i = LastCycleWAddress) and
                               ((NOW-Last_tc_ClkB_PosEdge_WEB) < Tw2r)) then
                              -- read and write the same address
                              assert FALSE report "** MEM_Warning: Read and Write the same address." severity WARNING;
                              DO_zd := (OTHERS => 'X');
                              DO_int <= TRANSPORT (OTHERS => 'X') AFTER TOH;
                           else
                              -- Reduce error message --
                              flag_A := True_flg;
                              --------------------------
                              DO_zd := memoryCore(to_integer(A_i));
                              ScheduleOutputDelay(DO_int(0), DO_zd(0),
                              tpd_CKA_DO0_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(1), DO_zd(1),
                              tpd_CKA_DO1_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(2), DO_zd(2),
                              tpd_CKA_DO2_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(3), DO_zd(3),
                              tpd_CKA_DO3_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(4), DO_zd(4),
                              tpd_CKA_DO4_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(5), DO_zd(5),
                              tpd_CKA_DO5_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(6), DO_zd(6),
                              tpd_CKA_DO6_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(7), DO_zd(7),
                              tpd_CKA_DO7_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(8), DO_zd(8),
                              tpd_CKA_DO8_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(9), DO_zd(9),
                              tpd_CKA_DO9_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(10), DO_zd(10),
                              tpd_CKA_DO10_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(11), DO_zd(11),
                              tpd_CKA_DO11_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(12), DO_zd(12),
                              tpd_CKA_DO12_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(13), DO_zd(13),
                              tpd_CKA_DO13_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(14), DO_zd(14),
                              tpd_CKA_DO14_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(15), DO_zd(15),
                              tpd_CKA_DO15_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(16), DO_zd(16),
                              tpd_CKA_DO16_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(17), DO_zd(17),
                              tpd_CKA_DO17_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(18), DO_zd(18),
                              tpd_CKA_DO18_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(19), DO_zd(19),
                              tpd_CKA_DO19_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(20), DO_zd(20),
                              tpd_CKA_DO20_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(21), DO_zd(21),
                              tpd_CKA_DO21_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(22), DO_zd(22),
                              tpd_CKA_DO22_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(23), DO_zd(23),
                              tpd_CKA_DO23_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(24), DO_zd(24),
                              tpd_CKA_DO24_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(25), DO_zd(25),
                              tpd_CKA_DO25_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(26), DO_zd(26),
                              tpd_CKA_DO26_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(27), DO_zd(27),
                              tpd_CKA_DO27_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(28), DO_zd(28),
                              tpd_CKA_DO28_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(29), DO_zd(29),
                              tpd_CKA_DO29_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(30), DO_zd(30),
                              tpd_CKA_DO30_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(31), DO_zd(31),
                              tpd_CKA_DO31_posedge,last_A,A_i,NO_SER_TOH);
                           end if;

                           LastCycleRAddress := A_i;
                       else
                           -- Reduce error message --
                           flag_A := False_flg;
                           --------------------------
                           DO_zd := (OTHERS => 'X');
                           DO_int <= TRANSPORT (OTHERS => 'X') AFTER TOH;
                       end if;

                   when '1' =>  -- do nothing
                   when others =>
                               DO_zd := (OTHERS => 'X');
                               DO_int <= TRANSPORT (OTHERS => 'X') AFTER TOH;
                end case;
                -- end read_memory_function
                last_A := A_ipd;

                if (CSAN_i = '0') then
                   Last_tc_ClkA_PosEdge := NOW;
                end if;

            when "10"   => -- do nothing
            when others => if (NOW /= 0 ns) then
                              assert FALSE report "** MEM_Error: Abnormal transition occurred." severity WARNING;
                           end if;
                           DO_zd := (OTHERS => 'X');
                           DO_int <= TRANSPORT (OTHERS => 'X') AFTER TOH;

         end case;
         LastClkAEdge := CKA_ipd;
       end if;

       if (CKB_ipd'event) then
         ckb_change := LastClkBEdge&CKB_ipd;
         case ckb_change is
            when "01"   =>
                if (CS_monitor(CSBN_ipd,flag_CSBN) = True_flg) then
                   -- Reduce error message --
                   flag_CSBN := True_flg;
                else
                   flag_CSBN := False_flg;
                end if;

                Latch_B    := B_ipd;
                Latch_CSBN  := CSBN_ipd;
                Latch_DI   := DI_ipd;
                Latch_WEB  := WEB_ipd;

                -- write_memory_function
                B_i   := Latch_B;
                CSBN_i := Latch_CSBN;
                DI_i  := Latch_DI;
                WEB_i := Latch_WEB;


                web_cs    := WEB_i&CSBN_i;
                case web_cs is
                   when "00" =>
                       if (AddressRangeCheck(B_i,flag_B) = True_flg) then
                           if ((B_i = LastCycleRAddress) and
                               ((NOW-Last_tc_ClkA_PosEdge) < Tr2w)) then
                              -- read and write the same address
                              assert FALSE report "** MEM_Warning: Read and Write the same address." severity WARNING;
                              DO_zd := (OTHERS => 'X');
                              DO_int <= TRANSPORT (OTHERS => 'X') AFTER TOH;
                           end if;
                           memoryCore(to_integer(B_i)) := DI_i;
                           -- Reduce error message --
                           flag_B := True_flg;
                           --------------------------
                       elsif (AddressRangeCheck(B_i,flag_B) = Range_flg) then
                           -- Reduce error message --
                           flag_B := False_flg;
                           --------------------------
                           -- do nothing
                       else
                           -- Reduce error message --
                           flag_B := False_flg;
                           --------------------------
                           FOR i IN Words-1 downto 0 LOOP
                              memoryCore(i) := (OTHERS => 'X');
                           END LOOP;
                       end if;
                       LastCycleWAddress := B_i;

                   when "11" |
                        "10" |
                        "1X" |
                        "1U" |
                        "1Z" |
                        "01" |
                        "X1" |
                        "U1" |
                        "Z1"   => -- do nothing
                   when others =>
                       if (AddressRangeCheck(B_i,flag_B) = True_flg) then
                           memoryCore(to_integer(B_i)) := (OTHERS => 'X');
                       elsif (AddressRangeCheck(B_i,flag_B) = Range_flg) then
                           -- Reduce error message --
                           flag_B := False_flg;
                           --------------------------
                           -- do nothing
                       else
                           FOR i IN Words-1 downto 0 LOOP
                              memoryCore(i) := (OTHERS => 'X');
                           END LOOP;
                       end if;
                end case;

                -- end write_memory_function

                if ((CSBN_i = '0') and (WEB_i = '0')) then
                   Last_tc_ClkB_PosEdge_WEB := NOW;
                end if;

            when "10"   => -- do nothing
            when others => if (NOW /= 0 ns) then
                              assert FALSE report "** MEM_Error: Abnormal transition occurred." severity WARNING;
                           end if;
                           FOR i IN Words-1 downto 0 LOOP
                              memoryCore(i) := (OTHERS => 'X');
                           END LOOP;
         end case;
         LastClkBEdge := CKB_ipd;
       end if;


       if (Tviol_A_CKA_posedge     = 'X' or
           Tviol_B_CKB_posedge     = 'X' or
           Tviol_WEB_CKB_posedge  = 'X' or
           Tviol_DI_CKB_posedge   = 'X' or
           Tviol_CSAN_CKA_posedge    = 'X' or
           Tviol_CSBN_CKB_posedge    = 'X' or
           Pviol_CKA               = 'X' or
           Pviol_CKB               = 'X'
          ) then

         if (Pviol_CKA = 'X') then
            if (CSAN_ipd /= '1') then
               DO_zd := (OTHERS => 'X');
               DO_int <= TRANSPORT (OTHERS => 'X');
            end if;
         else
            FOR i IN AddressSize-1 downto 0 LOOP
              if (Tviol_A_CKA_posedge = 'X') then
                 Latch_A(i) := 'X';
              else
                 Latch_A(i) := Latch_A(i);
              end if;
            END LOOP;

            if (Tviol_CSAN_CKA_posedge = 'X') then
               Latch_CSAN := 'X';
            else
               Latch_CSAN := Latch_CSAN;
            end if;

                -- read_memory_function
                A_i    := Latch_A;
                CSAN_i  := Latch_CSAN;

                case CSAN_i is
                   when '0' => 
                       -------- Reduce error message --------------------------
                       if (AddressRangeCheck(A_i,flag_A) = True_flg) then
                           if ((A_i = LastCycleWAddress) and
                               ((NOW-Last_tc_ClkB_PosEdge_WEB) < Tw2r)) then
                              -- read and write the same address
                              assert FALSE report "** MEM_Warning: Read and Write the same address." severity WARNING;
                              DO_zd := (OTHERS => 'X');
                              DO_int <= TRANSPORT (OTHERS => 'X') AFTER TOH;
                           else
                              -- Reduce error message --
                              flag_A := True_flg;
                              --------------------------
                              DO_zd := memoryCore(to_integer(A_i));
                              ScheduleOutputDelay(DO_int(0), DO_zd(0),
                              tpd_CKA_DO0_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(1), DO_zd(1),
                              tpd_CKA_DO1_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(2), DO_zd(2),
                              tpd_CKA_DO2_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(3), DO_zd(3),
                              tpd_CKA_DO3_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(4), DO_zd(4),
                              tpd_CKA_DO4_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(5), DO_zd(5),
                              tpd_CKA_DO5_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(6), DO_zd(6),
                              tpd_CKA_DO6_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(7), DO_zd(7),
                              tpd_CKA_DO7_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(8), DO_zd(8),
                              tpd_CKA_DO8_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(9), DO_zd(9),
                              tpd_CKA_DO9_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(10), DO_zd(10),
                              tpd_CKA_DO10_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(11), DO_zd(11),
                              tpd_CKA_DO11_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(12), DO_zd(12),
                              tpd_CKA_DO12_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(13), DO_zd(13),
                              tpd_CKA_DO13_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(14), DO_zd(14),
                              tpd_CKA_DO14_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(15), DO_zd(15),
                              tpd_CKA_DO15_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(16), DO_zd(16),
                              tpd_CKA_DO16_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(17), DO_zd(17),
                              tpd_CKA_DO17_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(18), DO_zd(18),
                              tpd_CKA_DO18_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(19), DO_zd(19),
                              tpd_CKA_DO19_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(20), DO_zd(20),
                              tpd_CKA_DO20_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(21), DO_zd(21),
                              tpd_CKA_DO21_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(22), DO_zd(22),
                              tpd_CKA_DO22_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(23), DO_zd(23),
                              tpd_CKA_DO23_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(24), DO_zd(24),
                              tpd_CKA_DO24_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(25), DO_zd(25),
                              tpd_CKA_DO25_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(26), DO_zd(26),
                              tpd_CKA_DO26_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(27), DO_zd(27),
                              tpd_CKA_DO27_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(28), DO_zd(28),
                              tpd_CKA_DO28_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(29), DO_zd(29),
                              tpd_CKA_DO29_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(30), DO_zd(30),
                              tpd_CKA_DO30_posedge,last_A,A_i,NO_SER_TOH);
                              ScheduleOutputDelay(DO_int(31), DO_zd(31),
                              tpd_CKA_DO31_posedge,last_A,A_i,NO_SER_TOH);
                           end if;

                           LastCycleRAddress := A_i;
                       else
                           -- Reduce error message --
                           flag_A := False_flg;
                           --------------------------
                           DO_zd := (OTHERS => 'X');
                           DO_int <= TRANSPORT (OTHERS => 'X') AFTER TOH;
                       end if;

                   when '1' =>  -- do nothing
                   when others =>
                               DO_zd := (OTHERS => 'X');
                               DO_int <= TRANSPORT (OTHERS => 'X') AFTER TOH;
                end case;
                -- end read_memory_function

         end if;


         if (Pviol_CKB = 'X') then
            if (CSBN_ipd /= '1') then
               if (WEB_ipd /= '1') then
                  FOR i IN Words-1 downto 0 LOOP
                     memoryCore(i) := (OTHERS => 'X');
                  END LOOP;
               end if;
            end if;
         else
            FOR i IN AddressSize-1 downto 0 LOOP
              if (Tviol_B_CKB_posedge = 'X') then
                 Latch_B(i) := 'X';
              else
                 Latch_B(i) := Latch_B(i);
              end if;
            END LOOP;
            FOR i IN Bits-1 downto 0 LOOP
              if (Tviol_DI_CKB_posedge = 'X') then
                 Latch_DI(i) := 'X';
              else
                 Latch_DI(i) := Latch_DI(i);
              end if;
            END LOOP;

            if (Tviol_WEB_CKB_posedge = 'X') then
               Latch_WEB := 'X';
            else
               Latch_WEB := Latch_WEB;
            end if;

            if (Tviol_CSBN_CKB_posedge = 'X') then
               Latch_CSBN := 'X';
            else
               Latch_CSBN := Latch_CSBN;
            end if;

            -- write_memory_function
                -- write_memory_function
                B_i   := Latch_B;
                CSBN_i := Latch_CSBN;
                DI_i  := Latch_DI;
                WEB_i := Latch_WEB;


                web_cs    := WEB_i&CSBN_i;
                case web_cs is
                   when "00" =>
                       if (AddressRangeCheck(B_i,flag_B) = True_flg) then
                           if ((B_i = LastCycleRAddress) and
                               ((NOW-Last_tc_ClkA_PosEdge) < Tr2w)) then
                              -- read and write the same address
                              assert FALSE report "** MEM_Warning: Read and Write the same address." severity WARNING;
                              DO_zd := (OTHERS => 'X');
                              DO_int <= TRANSPORT (OTHERS => 'X') AFTER TOH;
                           end if;
                           memoryCore(to_integer(B_i)) := DI_i;
                           -- Reduce error message --
                           flag_B := True_flg;
                           --------------------------
                       elsif (AddressRangeCheck(B_i,flag_B) = Range_flg) then
                           -- Reduce error message --
                           flag_B := False_flg;
                           --------------------------
                           -- do nothing
                       else
                           -- Reduce error message --
                           flag_B := False_flg;
                           --------------------------
                           FOR i IN Words-1 downto 0 LOOP
                              memoryCore(i) := (OTHERS => 'X');
                           END LOOP;
                       end if;
                       LastCycleWAddress := B_i;

                   when "11" |
                        "10" |
                        "1X" |
                        "1U" |
                        "1Z" |
                        "01" |
                        "X1" |
                        "U1" |
                        "Z1"   => -- do nothing
                   when others =>
                       if (AddressRangeCheck(B_i,flag_B) = True_flg) then
                           memoryCore(to_integer(B_i)) := (OTHERS => 'X');
                       elsif (AddressRangeCheck(B_i,flag_B) = Range_flg) then
                           -- Reduce error message --
                           flag_B := False_flg;
                           --------------------------
                           -- do nothing
                       else
                           FOR i IN Words-1 downto 0 LOOP
                              memoryCore(i) := (OTHERS => 'X');
                           END LOOP;
                       end if;
                end case;

                -- end write_memory_function
            -- end  write_memory_function
         end if;
       end if;

   end PROCESS;

end behavior;
