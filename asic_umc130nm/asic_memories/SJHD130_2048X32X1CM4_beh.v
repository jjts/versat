`timescale 1ns / 1ps
`include "./xdefs.v"

module SJHD130_2048X32X1CM4 (A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,
                             B5,B6,B7,B8,B9,B10,DOA0,DOA1,DOA2,DOA3,DOA4,
                             DOA5,DOA6,DOA7,DOA8,DOA9,DOA10,DOA11,DOA12,
                             DOA13,DOA14,DOA15,DOA16,DOA17,DOA18,DOA19,
                             DOA20,DOA21,DOA22,DOA23,DOA24,DOA25,DOA26,
                             DOA27,DOA28,DOA29,DOA30,DOA31,DOB0,DOB1,
                             DOB2,DOB3,DOB4,DOB5,DOB6,DOB7,DOB8,DOB9,
                             DOB10,DOB11,DOB12,DOB13,DOB14,DOB15,DOB16,
                             DOB17,DOB18,DOB19,DOB20,DOB21,DOB22,DOB23,
                             DOB24,DOB25,DOB26,DOB27,DOB28,DOB29,DOB30,
                             DOB31,DIA0,DIA1,DIA2,DIA3,DIA4,DIA5,DIA6,
                             DIA7,DIA8,DIA9,DIA10,DIA11,DIA12,DIA13,
                             DIA14,DIA15,DIA16,DIA17,DIA18,DIA19,DIA20,
                             DIA21,DIA22,DIA23,DIA24,DIA25,DIA26,DIA27,
                             DIA28,DIA29,DIA30,DIA31,DIB0,DIB1,DIB2,
                             DIB3,DIB4,DIB5,DIB6,DIB7,DIB8,DIB9,DIB10,
                             DIB11,DIB12,DIB13,DIB14,DIB15,DIB16,DIB17,
                             DIB18,DIB19,DIB20,DIB21,DIB22,DIB23,DIB24,
                             DIB25,DIB26,DIB27,DIB28,DIB29,DIB30,DIB31,
                             WEAN,WEBN,CKA,CKB,CSA,CSB,OEA,OEB);
   
   output     DOA0,DOA1,DOA2,DOA3,DOA4,DOA5,DOA6,DOA7,DOA8,
              DOA9,DOA10,DOA11,DOA12,DOA13,DOA14,DOA15,DOA16,DOA17,DOA18,
              DOA19,DOA20,DOA21,DOA22,DOA23,DOA24,DOA25,DOA26,DOA27,DOA28,
              DOA29,DOA30,DOA31;
   output     DOB0,DOB1,DOB2,DOB3,DOB4,DOB5,DOB6,DOB7,DOB8,
              DOB9,DOB10,DOB11,DOB12,DOB13,DOB14,DOB15,DOB16,DOB17,DOB18,
              DOB19,DOB20,DOB21,DOB22,DOB23,DOB24,DOB25,DOB26,DOB27,DOB28,
              DOB29,DOB30,DOB31;
   input      DIA0,DIA1,DIA2,DIA3,DIA4,DIA5,DIA6,DIA7,DIA8,
              DIA9,DIA10,DIA11,DIA12,DIA13,DIA14,DIA15,DIA16,DIA17,DIA18,
              DIA19,DIA20,DIA21,DIA22,DIA23,DIA24,DIA25,DIA26,DIA27,DIA28,
              DIA29,DIA30,DIA31;
   input      DIB0,DIB1,DIB2,DIB3,DIB4,DIB5,DIB6,DIB7,DIB8,
              DIB9,DIB10,DIB11,DIB12,DIB13,DIB14,DIB15,DIB16,DIB17,DIB18,
              DIB19,DIB20,DIB21,DIB22,DIB23,DIB24,DIB25,DIB26,DIB27,DIB28,
              DIB29,DIB30,DIB31;
   input      A0,A1,A2,A3,A4,A5,A6,A7,A8,
              A9,A10;
   input      B0,B1,B2,B3,B4,B5,B6,B7,B8,
              B9,B10;
   input      OEA;                                     
   input      OEB;                                     
   input      WEAN;                                    
   input      WEBN;                                    
   input      CKA;                                     
   input      CKB;                                     
   input      CSA;                                     
   input      CSB;                                     

   function [`DADDR_W-1:0] reverseAddrBits;
      input [`DADDR_W-1:0] word;
      integer 		  i;
      
      begin
	 for (i=0; i < `DADDR_W; i=i+1)
	   reverseAddrBits[i]=word[`DADDR_W-1 - i];
      end
   endfunction

   function [`DATA_W-1:0] reverseDataBits;
      input [`DATA_W-1:0] 		    word;
      integer 				    i;
      
      begin
	 for (i=0; i < `DATA_W; i=i+1)
	   reverseDataBits[i]=word[`DATA_W-1 - i];         
      end
   endfunction    

   wire [`DATA_W-1:0] dinA;
   reg [`DATA_W-1:0]  doutA;
   wire [`DADDR_W-1:0] addrA;
   wire [`DATA_W-1:0] dinB;
   reg [`DATA_W-1:0]  doutB;
   wire [`DADDR_W-1:0] addrB;
   reg [`DATA_W-1:0]  mem [2**`DADDR_W-1:0];
   
   assign addrA =  reverseAddrBits({A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10});

   assign addrB =  reverseAddrBits({B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10});

   assign dinA = reverseDataBits({DIA0,DIA1,DIA2,DIA3,DIA4,DIA5,DIA6,
				  DIA7,DIA8,DIA9,DIA10,DIA11,DIA12,DIA13,
				  DIA14,DIA15,DIA16,DIA17,DIA18,DIA19,DIA20,
				  DIA21,DIA22,DIA23,DIA24,DIA25,DIA26,DIA27,
				  DIA28,DIA29,DIA30,DIA31});

   assign dinB = reverseDataBits({DIB0,DIB1,DIB2,
				  DIB3,DIB4,DIB5,DIB6,DIB7,DIB8,DIB9,DIB10,
				  DIB11,DIB12,DIB13,DIB14,DIB15,DIB16,DIB17,
				  DIB18,DIB19,DIB20,DIB21,DIB22,DIB23,DIB24,
				  DIB25,DIB26,DIB27,DIB28,DIB29,DIB30,DIB31});

   assign {DOA0,DOA1,DOA2,DOA3,DOA4,
           DOA5,DOA6,DOA7,DOA8,DOA9,DOA10,DOA11,DOA12,
           DOA13,DOA14,DOA15,DOA16,DOA17,DOA18,DOA19,
           DOA20,DOA21,DOA22,DOA23,DOA24,DOA25,DOA26,
           DOA27,DOA28,DOA29,DOA30,DOA31} = reverseDataBits(doutA);
   assign {DOB0,DOB1,
           DOB2,DOB3,DOB4,DOB5,DOB6,DOB7,DOB8,DOB9,
           DOB10,DOB11,DOB12,DOB13,DOB14,DOB15,DOB16,
           DOB17,DOB18,DOB19,DOB20,DOB21,DOB22,DOB23,
           DOB24,DOB25,DOB26,DOB27,DOB28,DOB29,DOB30,
           DOB31} = reverseDataBits(doutB);
   
   always @ (posedge CKA) begin 
      if (CSA) begin
	 if (~WEAN)
	   mem[addrA] <= dinA;
	 doutA <= mem[addrA];
      end
   end

   always @ (posedge CKB) begin 
      if (CSB) begin
	 if (~WEBN)
	   mem[addrB] <= dinB;
	 doutB <= mem[addrB];
      end
   end

endmodule
