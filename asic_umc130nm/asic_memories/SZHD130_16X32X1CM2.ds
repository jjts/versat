/******************************************************************************

             Synchronous 2 Port Register File Compiler 

                   UMC 0.13um High Speed Logic Process 
   __________________________________________________________________________


       (C) Copyright 2016 Faraday Technology Corp. All Rights Reserved.

     This source code is an unpublished work belongs to Faraday Technology
     Corp.  It is considered a trade secret and is not to be divulged or
     used by parties who have not received written authorization from
     Faraday Technology Corp.

     Faraday's home page can be found at:
     http://www.faraday-tech.com/
    
________________________________________________________________________________

      Module Name       :  SZHD130_16X32X1CM2  
      Word              :  16                  
      Bit               :  32                  
      Byte              :  1                   
      Mux               :  2                   
      Power Ring Type   :  port                
      Power Ring Width  :  5 (um)              
      Output Loading    :  0.01 (pf)           
      Input Data Slew   :  0.016 (ns)          
      Input Clock Slew  :  0.016 (ns)          

________________________________________________________________________________

      Library          : FSC0H_D
      Memaker          : 201001.1.1
      Date             : 2016/02/18 08:03:33

________________________________________________________________________________

******************************************************************************/



   Description:

     The FSC0H_D_SZ is a synchronous, two port register file. It was created
     according to UMC's 0.13um 1P8M 1.2V High Speed logic process design rules and can be 
     incorporated with Faraday's 0.13um standard cells. Different combinations 
     of words, bits, and aspect ratios can be used to generate the most desirable
     configurations.
    
     By requesting the desired size and timing constraints, the FSC0H_D_SZ 
     compiler is capable of providing suitable synchronous RAM layout instances
     in seconds. It can automatically generate data sheets, Verilog / VHDL
     behavioral simulation models, SCS or Viewlogic symbols, place & route models,
     and test patterns for use in ASIC designs. The duty cycle length can be 
     neglected as long as the setup / hold time and minimum high / low pulse
     widths are satisfied.  This allows the flexibility of a clock falling edge
     during each operation. Both word write and byte write operations are
     supported.



   Features:

       - Synchronous read and write operations
       - Fully customized layout density 
       - Available for 1.2V +/- 10% 
       - Automatic power down to eliminate DC current
       - Clocked address inputs and CSA(B)N to RAM at CK rising edge
       - Clocked WEB input pin to RAM at CK rising edge
       - Clocked DI input pins to RAM at CK rising edge
       - Byte write or word write operations available
       - Verilog / VHDL timing / simulation model generator
       - SPICE netlist generator
       - GDSII layout database
       - Memory compiler preview UI (Memaker)
       - BIST circuitry supported
       - Column mux options for the best aspect ratio
      

   Input Pins:

       Pin Name  Capacitance  Descriptions                                   
       A[3:0]    0.010 pF     Address signals of width 4                     
       B[3:0]    0.010 pF     Address signals of width 4                     
       CKA       0.048 pF     Clock signal for addresses and CSAN            
       CKB       0.048 pF     Clock signal for addresses, WEB, CSBN, and DI  
       CSAN      0.060 pF     Chip select, active low                        
       CSBN      0.060 pF     Chip select, active low                        
       DI[31:0]  0.006 pF     Input data of width 32                         
       WEB       0.045 pF     Write enable signals of 1 bytes, active low    


   Output Pins: 

       Pin Name  Capacitance  Descriptions             
       DO[31:0]  0.004 pF     Output data of width 32  

   Approximated Area Information: 

       RAM area = 208.800 um (Width) x 97.200 um (Height) = 0.020 mm^2
       Power ring width = 5 um


   Process metal options:

       
       ------------------------------------------------------------
       |Five (5) metal layers |  M5 (thick) + M1 ~ M4 (thin)      |
       |-----------------------------------------------------------
       |Six (6) metal layers  |  M5 ~ M6 (thick) + M1 ~ M4 (thin) |
       |-----------------------------------------------------------
       |Six (6) metal layers  |  M6 (thick) + M1 ~ M5 (thin)      |
       |-----------------------------------------------------------
       |Six (6) metal layers  |  M1 ~ M6 (thin)                   |
       |-----------------------------------------------------------
       |Seven (7) metal layers|  M6 ~ M7 (thick) + M1 ~ M5 (thin) |
       |-----------------------------------------------------------
       |Seven (7) metal layers|  M7 (thick) + M1 ~ M6 (thin)      |
       |-----------------------------------------------------------
       |Eight (8) metal layers|  M7 ~ M8 (thick) + M1 ~ M6 (thin) |
       |-----------------------------------------------------------


   Recommended operating conditions:

       Symbol  BC    TC   WC    Units  
       VCC     1.32  1.2  1.08  V      
       TJ      -40   25   125   C      

       Notes:
         1. VCC: Power supply for memory block
         2. TJ : Junction operating temperature



   Operating Conditions:

       Corner  Process  Voltage(v)  Temperature(C)  
       BC      PFNF     1.32        -40             
       TC      PTNT     1.2         25              
       WC      PSNS     1.08        125             

   Clock Slew Rate & Loading Look Up Table (3x5):
       Index                    1      2      3      4      5
       Clock Slew (ns)*     0.016  0.400  0.800
       Output Loading(pF)   0.010  0.050  0.150  0.500  1.300

   Clock & Data Slew Rate Look Up Table (3x3):
       Index                    1      2      3
       Data  Slew (ns)*     0.016  0.400  0.800
       Clock Slew (ns)*     0.016  0.400  0.800
       * For BC: 10.0% ~ 90.0%
       * For TC: 10.0% ~ 90.0%
       * For WC: 10.0% ~ 90.0%

   Power Consumption Per Port:

       Power Type       BC     TC     WC      Unit                  
       Standby Current  2.867  5.439  14.268  uA (CSAN = CSBN = 1)  
       DC Current       2.867  5.439  14.268  uA (CSAN = CSBN = 0)  
       Max. AC Current  0.009  0.008  0.007   mA/MHz                

       Total current   = AC current * Freq + DC current   
       Notes:
        1. All cycles are active
        2. All address bits switching
        3. All data bits switching
        4. Worst of read / write operation
 
 
   Timing Information:

       - CKA(B) input slope = 0.016 ns.
       - Data input slope = 0.016 ns.
       - All timing parameters are measured from 50% of input.
       - Output reference voltage "H" = 50% of VDD, "L" = 50% of VDD.
       - Output loading = 0.01 pF.
       - Delay timing parameters in nano second.

   symbol  BC    TC    WC    Descriptions                                         
   taa     0.47  0.71  1.23  Data access time from CK rising                      
   toh     0.20  0.30  0.50  Output data hold time after CK rising                
   trc     0.61  0.90  1.49  Read cycle time                                      
   tcss    0.11  0.19  0.37  CSAN(CSBN) setup time before CK rising               
   tcshr   0.10  0.10  0.10  CSAN(CSBN) hold time after CK rising in read cycle   
   tcshw   0.10  0.10  0.10  CSAN(CSBN) hold time after CK rising in write cycle  
   twh     0.10  0.10  0.13  WEB hold time after CK rising                        
   tah     0.10  0.10  0.10  Address hold time after CK rising                    
   tas     0.10  0.13  0.27  Address setup time before CK rising                  
   twc     0.61  0.90  1.49  Write cycle time                                     
   tws     0.10  0.10  0.10  WEB setup time before CK rising                      
   tdh     0.10  0.10  0.10  Input data hold time after CK rising                 
   tds     0.10  0.15  0.28  Input data setup time before CK rising               
   thpw    0.20  0.30  0.50  Clock high pulse width                               
   tlpw    0.20  0.30  0.50  Clock low pulse width                                
