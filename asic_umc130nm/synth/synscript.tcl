set_attribute lib_search_path [list /opt/ic_tools/pdk/faraday/umc130/HS/fsc0h_d/2009Q1v3.0/GENERIC_CORE/FrontEnd/synopsys ../asic_memories]
set_attribute hdl_search_path [list ../../verilog_src ../../ipb-xctrl/verilog_src]
set_attribute library [list fsc0h_d_generic_core_tt1p2v25c.lib SJHD130_2048X32X1CM4_TC.lib SEHD130_64X32X1CM4_TC.lib]

read_hdl -v2001 -define ASIC -define ROM -define AXI_IF xaddr_decoder.v \
xaddrgen.v \
xaxi_lite_slave.v \
xboot_rom.v \
xclz.v \
xmem_wrapper64x32.v \
xmem_wrapper2048x32.v \
xconf_mem_slice.v \
xconf_mem.v \
xconf_reg.v \
xconf.v \
../../ipb-xctrl/verilog_src/xctrl.v \
xctr_regf.v \
xdma_addr_decoder.v \
xdma.v \
xeng_addr_decoder.v \
xinmux.v \
xbs.v \
xalu_lite.v \
xalu.v \
xmem.v \
xdiv.v \
xdiv_wrapper.v \
xmult.v \
xdata_eng.v \
xinstr_mem.v \
xprog_mem.v \
xtop.v

elaborate xtop
define_clock -name M_AXI_ACLK -period 5000 [find / -port M_AXI_ACLK] 
#define_clock -name S_AXI_ACLK -period 6000 [find / -port S_AXI_ACLK] 
synthesize -to_mapped
#retime -prepare -min_delay
report gates > gates_report.txt
report timing > timing_report.txt
write_hdl -mapped > xtop_synth.v 
write_sdc > xtop_synth.sdc
exit
