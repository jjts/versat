# ####################################################################

#  Created by Encounter(R) RTL Compiler v11.20-s017_1 on Fri Mar 31 11:16:43 +0100 2017

# ####################################################################

set sdc_version 1.7

set_units -capacitance 1000.0fF
set_units -time 1000.0ps

# Set the current design
current_design xtop

create_clock -name "M_AXI_ACLK" -add -period 5.0 -waveform {0.0 2.5} [get_ports M_AXI_ACLK]
set_clock_gating_check -setup 0.0 
set_wire_load_mode "enclosed"
set_wire_load_selection_group "DEFAULT" -library "fsc0h_d_generic_core_tt1p2v25c"
set_dont_use [get_lib_cells fsc0h_d_generic_core_tt1p2v25c/BHD1HD]
set_dont_use [get_lib_cells fsc0h_d_generic_core_tt1p2v25c/CKLDHD]
set_dont_use [get_lib_cells SJHD130_2048X32X1CM4_TC/SJHD130_2048X32X1CM4]
set_dont_use [get_lib_cells SEHD130_64X32X1CM4_TC/SEHD130_64X32X1CM4]
